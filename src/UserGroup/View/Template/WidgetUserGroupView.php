<?php
namespace Base\Package\UserGroup\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Base\Package\UserGroup\View\ListViewTrait;

class WidgetUserGroupView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $list = $this->getDataList();
        
        $this->getView()->display(
            'UserGroup/WidgetUserGroup.tpl',
            [
                'list' => $list
            ]
        );
    }
}
