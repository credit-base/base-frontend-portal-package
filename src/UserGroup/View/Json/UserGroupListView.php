<?php
namespace Base\Package\UserGroup\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Base\Package\UserGroup\View\ListViewTrait;

class UserGroupListView extends JsonView implements IView
{
    use ListViewTrait;

    public function display() : void
    {
        $list = $this->getDataList();

        $this->encode($list);
    }
}
