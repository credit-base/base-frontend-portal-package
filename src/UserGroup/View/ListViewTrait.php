<?php
namespace Base\Package\UserGroup\View;

use Sdk\UserGroup\Translator\UserGroupTranslator;

trait ListViewTrait
{
    private $count;
    
    private $userGroup;

    private $translator;

    public function __construct(
        array $userGroup,
        int $count
    ) {
        $this->count = $count;
        $this->userGroup = $userGroup;
        $this->translator = new UserGroupTranslator();
        parent::__construct();
    }

    protected function getUserGroup() : array
    {
        return $this->userGroup;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : UserGroupTranslator
    {
        return $this->translator;
    }

    protected function getDataList(): array
    {
        $data = array();

        $translator = $this->getTranslator();
        foreach ($this->getUserGroup() as $items) {
            $data[] = $translator->objectToArray(
                $items,
                array(
                    'id',
                    'name',
                    'shortName'
                )
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'data' => $data
        );

        return $dataList;
    }
}
