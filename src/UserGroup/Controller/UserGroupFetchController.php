<?php
namespace Base\Package\UserGroup\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\UserGroup\Repository\UserGroupRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\UserGroup\View\Json\UserGroupListView;
use Base\Package\UserGroup\View\Template\WidgetUserGroupView;

class UserGroupFetchController extends Controller implements IFetchAbleController
{
    use FetchControllerTrait, WebTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new UserGroupRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository(): UserGroupRepository
    {
        return $this->repository;
    }

    protected function filterAction(): bool
    {
        list($filter, $sort, $page, $size) = $this->filterFormatChange();

        $userGroupList = array();
        list($count, $userGroupList) = $this->getRepository()->scenario(
            UserGroupRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        $this->render(new UserGroupListView($userGroupList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $sort = ['id'];

        $filter = array();

        list($page, $size) = $this->getPageAndSize(USER_GROUP_ALL_SIZE);

        return [$filter, $sort, $page, $size];
    }

    public function userGroupWidget()
    {
        list($filter, $sort, $page, $size) = $this->filterFormatChange();

        $widgetList = array();
        list($count, $widgetList) = $this->getRepository()->scenario(
            UserGroupRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        $this->render(new WidgetUserGroupView($widgetList, $count));
        return true;
    }

    protected function fetchOneAction(int $id): bool
    {
        unset($id);
        return false;
    }
}
