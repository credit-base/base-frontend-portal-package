<?php
namespace Base\Package\CreditPublicity\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\CreditPublicity\View\Template\IndexView;

use Base\Package\Common\Controller\Interfaces\IIndexController;

class CreditPublicityIndexController extends Controller implements IIndexController
{
    use WebTrait;

    public function index()
    {
        $this->render(new IndexView());
        return true;
    }
}
