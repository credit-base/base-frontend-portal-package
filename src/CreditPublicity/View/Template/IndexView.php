<?php
namespace Base\Package\CreditPublicity\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class IndexView extends TemplateView implements IView
{
    public function display()
    {
        $this->getView()->display(
            'CreditPublicity/Index.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PUBLICITY']
            ]
        );
    }
}
