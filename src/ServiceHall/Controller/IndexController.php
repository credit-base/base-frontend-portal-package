<?php
namespace Base\Package\ServiceHall\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\ServiceHall\View\Template\IndexView;
use Base\Package\Common\Controller\Interfaces\IIndexController;

/**
 * @codeCoverageIgnore
 */
class IndexController extends Controller implements IIndexController
{
    use WebTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function index()
    {
        $data = ['type'=>'nav','data'=>'这是办事服务页面！'];
      
        $this->render(new IndexView($data));
        return true;
    }
}
