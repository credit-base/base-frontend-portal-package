<?php
namespace Base\Package\ServiceHall\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

/**
 * @codeCoverageIgnore
 */
class IndexView extends TemplateView implements IView
{
    private $data;

    public function __construct(array $data)
    {
        parent::__construct();
        $this->data = $data;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->data);
    }
    
    protected function getServiceHallsData() : array
    {
        return $this->data;
    }

    public function display()
    {
        $data = $this->getServiceHallsData();
    
        $this->getView()->display(
            'ServiceHall/Index.tpl',
            [
                'nav' => NAV['NAV_SERVICE_HALL'],
                'data' => $data
            ]
        );
    }
}
