<?php
namespace Base\Package\Home\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class IndexView extends TemplateView implements IView
{
    private $data;

    public function __construct(array $data)
    {
        parent::__construct();
        $this->data = $data;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->data);
    }
    
    protected function getCacheData() : array
    {
        return $this->data;
    }

    public function display()
    {
        $cacheData = $this->getCacheData();
  
        $this->getView()->display(
            'Home/Index.tpl',
            [
                'nav' => NAV['NAV_INDEX'],
                'data' => $cacheData
            ]
        );
    }
}
