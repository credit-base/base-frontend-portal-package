<?php
namespace Base\Package\Home\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Home\View\Template\IndexView;
use Base\Package\Home\Cache\Query\HomeFragmentCacheQuery;
use Base\Package\Common\Controller\Interfaces\IIndexController;

class IndexController extends Controller implements IIndexController
{
    use WebTrait;
    
    private $homeFragmentCacheQuery;

    public function __construct()
    {
        parent::__construct();
        $this->homeFragmentCacheQuery = new HomeFragmentCacheQuery();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->homeFragmentCacheQuery);
    }

    protected function getHomeFragmentCacheQuery()
    {
        return $this->homeFragmentCacheQuery;
    }

    public function index()
    {
        $data = $this->fetchNews();
      
        $this->render(new IndexView($data));
        return true;
    }

    protected function fetchNews()
    {
        $data = array();
        $data = $this->getHomeFragmentCacheQuery()->get();
        
        return $data;
    }
}
