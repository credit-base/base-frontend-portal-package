<?php
namespace Base\Package\Home\Cache\Query;

use Marmot\Framework\Query\FragmentCacheQuery;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Base\Package\Home\Cache\Persistence\HomeCache;

use Base\Sdk\News\Model\News;
use Base\Sdk\Common\Model\IEnableAble;
use Sdk\News\Repository\NewsRepository;

use Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

/**
* @todo
* @SuppressWarnings(PHPMD)
*/
class HomeFragmentCacheQuery extends FragmentCacheQuery
{
    use HomeFragmentCacheQueryTrait;

    const SIZE = 5;

    const BANNER_SIZE = 6;

    const TTL = 60;

    private $repository;

    private $concurrentAdapter;
    
    public function __construct()
    {
        parent::__construct('indexs', new HomeCache());
        $this->repository = new NewsRepository();
        $this->concurrentAdapter = new ConcurrentAdapter();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
        unset($this->concurrentAdapter);
    }

    protected function getRepository() : NewsRepository
    {
        return $this->repository;
    }

    protected function getWebsiteCustomizeRepository() : WebsiteCustomizeRepository
    {
        return new WebsiteCustomizeRepository();
    }

    protected function getConcurrentAdapter() : ConcurrentAdapter
    {
        return $this->concurrentAdapter;
    }

    /**
    * @todo
    * @codeCoverageIgnore
    */
    protected function fetchCacheData()
    {
        $repository = $this->getRepository();

        list(
            $bannerFilter,
            $specialGovernmentsFilter,
            $organizationsFilter,
            $dynamicsFilter,
            $policyFilter,
            $creditResearchesFilter,
            $sort,
            $websiteCustomizesFilter

        ) = $this->filterFormatChange();

        $this->getConcurrentAdapter()->addPromise(
            'banners',
            $repository->scenario(NewsRepository::LIST_MODEL_UN)
                ->searchAsync($bannerFilter, $sort, DEFAULT_PAGE, self::BANNER_SIZE),
            $repository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'specialGovernments',
            $repository->scenario(NewsRepository::LIST_MODEL_UN)
                ->searchAsync($specialGovernmentsFilter, $sort, DEFAULT_PAGE, self::SIZE),
            $repository->getAdapter()
        );
            
        $this->getConcurrentAdapter()->addPromise(
            'organizations',
            $repository->scenario(NewsRepository::LIST_MODEL_UN)
                ->searchAsync($organizationsFilter, $sort, DEFAULT_PAGE, self::SIZE),
            $repository->getAdapter()
        );
    
        $this->getConcurrentAdapter()->addPromise(
            'dynamics',
            $repository->scenario(NewsRepository::LIST_MODEL_UN)
                ->searchAsync($dynamicsFilter, $sort, DEFAULT_PAGE, self::SIZE),
            $repository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'policy',
            $repository->scenario(NewsRepository::LIST_MODEL_UN)
                ->searchAsync($policyFilter, $sort, DEFAULT_PAGE, self::SIZE),
            $repository->getAdapter()
        );

        $this->getConcurrentAdapter()->addPromise(
            'creditResearches',
            $repository->scenario(NewsRepository::LIST_MODEL_UN)
                ->searchAsync($creditResearchesFilter, $sort, DEFAULT_PAGE, self::SIZE),
            $repository->getAdapter()
        );

        $websiteCustomizeRepository = $this->getWebsiteCustomizeRepository();
        $this->getConcurrentAdapter()->addPromise(
            'websiteCustomizes',
            $websiteCustomizeRepository->scenario(WebsiteCustomizeRepository::LIST_MODEL_UN)
                ->searchAsync($websiteCustomizesFilter, ['-id'], DEFAULT_PAGE, DEFAULT_PAGE),
            $websiteCustomizeRepository->getAdapter()
        );

        $data = $this->getConcurrentAdapter()->run();

        $data = $this->newsArray($data);
        
        return $data;
    }

    protected function filterFormatChange()
    {
        $sort = ['-stick','-updateTime'];

        $bannerFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $bannerFilter['status'] = IEnableAble::STATUS['ENABLED'];
        $bannerFilter['bannerStatus'] = News::BANNER_STATUS['ENABLED'];

        //信用动态
        $dynamicsFilter['parentCategory'] = NEWS_PARENT_CATEGORY['CREDIT_DYNAMICS'];
        $dynamicsFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $dynamicsFilter['status'] = IEnableAble::STATUS['ENABLED'];
        
        //失信专项治理
        $specialGovernmentsFilter['newsType'] = NEWS_TYPE['SPECIAL_GOVERN'];
        $specialGovernmentsFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $specialGovernmentsFilter['status'] = IEnableAble::STATUS['ENABLED'];

        //政策法规
        $policyFilter['category'] = NEWS_CATEGORY['POLICY_STATUTE'];
        $policyFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $policyFilter['status'] = IEnableAble::STATUS['ENABLED'];

        //组织架构
        $organizationsFilter['category'] = NEWS_CATEGORY['ORGANIZATIONAL_STRUCTURE'];
        $organizationsFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $organizationsFilter['status'] = IEnableAble::STATUS['ENABLED'];

        //信用研究
        $creditResearchesFilter['category'] = NEWS_CATEGORY['CREDIT_RESEARCH'];
        $creditResearchesFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $creditResearchesFilter['status'] = IEnableAble::STATUS['ENABLED'];

        //首页配置项
        $websiteCustomizesFilter['category'] = WebsiteCustomize::CATEGORY['HOME_PAGE'];
        $websiteCustomizesFilter['status'] = WebsiteCustomize::STATUS['PUBLISHED'];

        return [
            $bannerFilter,
            $specialGovernmentsFilter,
            $organizationsFilter,
            $dynamicsFilter,
            $policyFilter,
            $creditResearchesFilter,
            $sort,
            $websiteCustomizesFilter
        ];
    }

    protected function getTtl() : int
    {
        return self::TTL;
    }
    
    /**
     * 获取片段,如果缓存失效则必须重新更新该片段缓存
     * @todo
     * @codeCoverageIgnore
     */
    public function get()
    {
        //从缓存获取数据
        $cacheData = $this->getCacheLayer()->get($this->getFragmentKey());

        if ($cacheData) {
            return $cacheData;
        }

        $cacheData = $this->refresh();
 
        if (!$cacheData) {
            return array();
        }

        return $cacheData;
    }
}
