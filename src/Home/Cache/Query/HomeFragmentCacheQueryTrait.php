<?php
namespace Base\Package\Home\Cache\Query;

use Sdk\News\Translator\NewsTranslator;
use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;

/**
* @todo
* @SuppressWarnings(PHPMD)
* @codeCoverageIgnore
*/
trait HomeFragmentCacheQueryTrait
{
    protected function getTranslator() : NewsTranslator
    {
        return new NewsTranslator();
    }

    protected function getWebsiteCustomizeTranslator() : WebsiteCustomizeTranslator
    {
        return new WebsiteCustomizeTranslator();
    }

    public function newsArray($newsList)
    {
        $dataArray = array();

        $policyList = empty($newsList['policy'][0]) ? array() : $newsList['policy'][1];
        $policies = [];
        foreach ($policyList as $policy) {
            $policies[] = $this->getTranslator()->objectToArray(
                $policy,
                array('id','title','source','description','newsType','updateTime')
            );
        }
        $dataArray['policies'] = $policies;

        $bannerList = empty($newsList['banners'][0]) ? array() : $newsList['banners'][1];
        $banners = [];
        foreach ($bannerList as $banner) {
            $banners[] = $this->getTranslator()->objectToArray(
                $banner,
                array('id','title','source','description','bannerImage','bannerStatus','newsType','updateTime')
            );
        }
        $dataArray['banners'] = $banners;

        $dynamicList = empty($newsList['dynamics'][0]) ? array() : $newsList['dynamics'][1];
        $dynamics = [];
        foreach ($dynamicList as $dynamic) {
            $dynamics[] = $this->getTranslator()->objectToArray(
                $dynamic,
                array('id','title','source','description','newsType','updateTime')
            );
        }
        $dataArray['dynamics'] = $dynamics;

        $organizationList = empty($newsList['organizations'][0]) ? array() : $newsList['organizations'][1];
        $organizations = [];
        foreach ($organizationList as $organization) {
            $organizations[] = $this->getTranslator()->objectToArray(
                $organization,
                array('id','title','source','description','newsType','updateTime')
            );
        }
        $dataArray['organizations'] = $organizations;

        $creditResearchList= empty($newsList['creditResearches'][0]) ? array() : $newsList['creditResearches'][1];
        $creditResearches = [];
        foreach ($creditResearchList as $creditResearch) {
            $creditResearches[] = $this->getTranslator()->objectToArray(
                $creditResearch,
                array('id','title','source','description','newsType','updateTime')
            );
        }
        $dataArray['creditResearches'] = $creditResearches;

        $specialGovernmentList = empty($newsList['specialGovernments'][0]) ? array()
        : $newsList['specialGovernments'][1];
        $specialGovernments = [];
        foreach ($specialGovernmentList as $specialGovernment) {
            $specialGovernments[] = $this->getTranslator()->objectToArray(
                $specialGovernment,
                array('id','title','source','description','newsType','updateTime')
            );
        }
        $dataArray['specialGovernments'] = $specialGovernments;

        $websiteCustomizes =  empty($newsList['websiteCustomizes'][0]) ? array()
            : $newsList['websiteCustomizes'][1];

        $dataArray['websiteCustomizes'] = $this->websiteCustomizesArray($websiteCustomizes);

        return $dataArray;
    }

    public function websiteCustomizesArray($websiteCustomizes):array
    {
        $websiteCustomizeTranslator = $this->getWebsiteCustomizeTranslator();
        $newData = [];
        
        foreach ($websiteCustomizes as $websiteCustomize) {
            $newData = $websiteCustomizeTranslator->objectToArray(
                $websiteCustomize,
                array('id','content')
            );
        }
   
        return $newData;
    }
}
