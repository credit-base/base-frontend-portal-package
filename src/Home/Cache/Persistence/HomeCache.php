<?php
namespace Base\Package\Home\Cache\Persistence;

use Marmot\Framework\Classes\Cache;

class HomeCache extends Cache
{
    public function __construct()
    {
        parent::__construct('home');
    }
}
