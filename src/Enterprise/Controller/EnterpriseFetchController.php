<?php
namespace Base\Package\Enterprise\Controller;

use Base\Package\Enterprise\View\Json\PermitResourceCatalogDataView;
use Base\Sdk\ResourceCatalog\Model\ISearchData;
use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

use Base\Package\Common\Controller\Interfaces\IFetchAbleController;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Statistical\Controller\StatisticalControllerTrait;
use Base\Package\Enterprise\View\Json\ListJsonView;
use Base\Package\Enterprise\View\Template\ListView;
use Base\Package\Enterprise\View\Template\EnterpriseView;

class EnterpriseFetchController extends Controller implements IFetchAbleController
{
    const TYPE = [
        SEARCH_NAV['SEARCH_NAV_CREDIT_INFO'] => 1,
        SEARCH_NAV['SEARCH_NAV_UNIFIED_SOCIAL_CREDIT_CODE'] => 2,
        SEARCH_NAV['SEARCH_NAV_LEGAL_PERSON_NAME'] => 3,
    ];

    use WebTrait, FetchControllerTrait, StatisticalControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new EnterpriseRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : EnterpriseRepository
    {
        return $this->repository;
    }

    protected function getBjSearchDataRepository() : BjSearchDataRepository
    {
        return new BjSearchDataRepository();
    }

    protected function filterAction() : bool
    {
        $keyword = $this->getRequest()->get('keyword', '');

        $type = $this->getRequest()->get('type', SEARCH_NAV['SEARCH_NAV_CREDIT_INFO']);

        if (!isset(self::TYPE[$type])) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        list($page, $size) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($keyword, $type);

        list($count, $enterpriseList) = $this->getRepository()->scenario(
            EnterpriseRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new ListJsonView($enterpriseList, $count, $type));
            return true;
        }

        $this->render(new ListView($enterpriseList, $count, $type));
        return true;
    }

    protected function fetchOneAction($id) : bool
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        /** @var Enterprise $enterprise */
        $enterprise = $this->getRepository()->scenario(EnterpriseRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
        $statistical = $this->statistical(
            'enterpriseRelationInformationCount',
            array(
                'unifiedSocialCreditCode' => $enterprise->getUnifiedSocialCreditCode(),
                'dimension' => ISearchData::DIMENSION['SHGK'],
            )
        );
        list($permitCount, $permitList) = $this->getBjSearchDataRepository()
            ->scenario(BjSearchDataRepository::LIST_MODEL_UN)
            ->search(
                array(
                    'infoClassify' => ISearchData::INFO_CLASSIFY['XZXK'],
                    'dimension' => ISearchData::DIMENSION['SHGK'],
                    'identify' => $enterprise->getUnifiedSocialCreditCode()
                ),
                ['-updateTime'],
                DEFAULT_PAGE,
                INDEX_LIST_SIZE
            );

        if ($enterprise instanceof INull || $statistical instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new EnterpriseView($enterprise, $statistical, $permitList, $permitCount));
        return true;
    }

    protected function filterFormatChange($keyword, $type) : array
    {
        $sort = ['-updateTime'];

        $filter = array();

        if ($type == SEARCH_NAV['SEARCH_NAV_CREDIT_INFO'] && !empty($keyword)) {
            $filter['name'] = $keyword;
        }
        if ($type == SEARCH_NAV['SEARCH_NAV_UNIFIED_SOCIAL_CREDIT_CODE'] && !empty($keyword)) {
            $filter['unifiedSocialCreditCode'] = $keyword;
        }
        if ($type == SEARCH_NAV['SEARCH_NAV_LEGAL_PERSON_NAME'] && !empty($keyword)) {
            $filter['principal'] = $keyword;
        }

        return [$filter, $sort];
    }

    //获取行政许可资源目录数据
    public function getResourceCatalogData($id) : bool
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        $id = marmot_decode($id);

        $infoClassify = $this->getRequest()->get('infoClassify', marmot_encode(ISearchData::INFO_CLASSIFY['XZXK']));
        $infoClassify = marmot_decode($infoClassify);

        $enterprise = $this->getRepository()->scenario(EnterpriseRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        list($permitCount, $permitList) = $this->getBjSearchDataRepository()
            ->scenario(BjSearchDataRepository::LIST_MODEL_UN)
            ->search(
                array(
                    'infoClassify' => $infoClassify,
                    'dimension' => ISearchData::DIMENSION['SHGK'],
                    'identify' => $enterprise->getUnifiedSocialCreditCode()
                ),
                ['-updateTime'],
                DEFAULT_PAGE,
                INDEX_LIST_SIZE
            );

        $this->render(new PermitResourceCatalogDataView($permitList, $permitCount));
        return true;
    }
}
