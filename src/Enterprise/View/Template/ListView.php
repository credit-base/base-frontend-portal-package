<?php
namespace Base\Package\Enterprise\View\Template;

use Base\Package\Enterprise\Controller\EnterpriseFetchController;
use Sdk\Enterprise\Translator\EnterpriseTranslator;
use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;

class ListView extends TemplateView implements IView
{

    private $list;
    private $count;
    private $type;
    private $translator;

    const TPL_MAP = [
        SEARCH_NAV['SEARCH_NAV_CREDIT_INFO'] => 'Enterprise/List.tpl',
        SEARCH_NAV['SEARCH_NAV_UNIFIED_SOCIAL_CREDIT_CODE'] => 'Enterprise/ListForCode.tpl',
        SEARCH_NAV['SEARCH_NAV_LEGAL_PERSON_NAME'] => 'Enterprise/ListForPrincipal.tpl',
    ];

    public function __construct(array $enterpriseList, int $count, string $type)
    {
        parent::__construct();

        $this->list = $enterpriseList;
        $this->count = $count;
        $this->type = $type;
        $this->translator = new EnterpriseTranslator();
    }

    protected function getEnterpriseList() : array
    {
        return $this->list;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getType() : string
    {
        return $this->type;
    }

    protected function getTranslator() : EnterpriseTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $lists = $this->getEnterpriseList();

        $data = array();
        foreach ($lists as $enterprise) {
            $enterpriseInfo = $translator->objectToArray(
                $enterprise,
                array(
                    'id',
                    'name',
                    'unifiedSocialCreditCode',
                    'registrationStatus',
                )
            );
            $enterpriseInfo['awardTotal'] = 0;
            $enterpriseInfo['penaltyTotal'] = 0;
            $data[] = $enterpriseInfo;
        }

        $list['total'] = $this->getCount();
        $list['list'] = $data;

        $this->getView()->display(
            self::TPL_MAP[$this->getType()],
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' =>  $list
            ]
        );
    }
}
