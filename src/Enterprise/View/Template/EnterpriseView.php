<?php
namespace Base\Package\Enterprise\View\Template;

use Base\Sdk\ResourceCatalog\Model\ISearchData;
use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;
use Sdk\Enterprise\Translator\EnterpriseTranslator;
use Sdk\ResourceCatalog\Model\BjSearchData;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;
use Sdk\Statistical\Translator\StaticsEnterpriseRelationInformationCountTranslator as RelationCountTranslator;

class EnterpriseView extends TemplateView implements IView
{
    const DETAIL_URL = [
        ISearchData::INFO_CLASSIFY['XZXK'] => '/doublePublicity',
        ISearchData::INFO_CLASSIFY['XZCF'] => '/doublePublicity',
        ISearchData::INFO_CLASSIFY['HONGMD'] => '/redBlacks',
        ISearchData::INFO_CLASSIFY['HEIMD'] => '/redBlacks',
    ];
    private $enterprise;
    private $statistical;

    private $permitList;
    private $permitCount;

    private $translator;
    private $statisticalTranslator;
    private $bjSearchDataTranslator;

    public function __construct(
        $enterprise,
        $statistical,
        array $permitList = array(),
        int $permitCount = 0
    ) {
        parent::__construct();

        $this->enterprise = $enterprise;
        $this->statistical = $statistical;
        $this->translator = new EnterpriseTranslator();
        $this->statisticalTranslator = new RelationCountTranslator();
        $this->bjSearchDataTranslator = new BjSearchDataTranslator();
        $this->permitList = $permitList;
        $this->permitCount = $permitCount;
    }

    protected function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return $this->translator;
    }

    protected function getRelationCountTranslator() : RelationCountTranslator
    {
        return $this->statisticalTranslator;
    }

    protected function getEnterprise()
    {
        return $this->enterprise;
    }

    protected function getStatistical()
    {
        return $this->statistical;
    }

    protected function getPermitCount() : int
    {
        return $this->permitCount;
    }

    protected function getPermitList() : array
    {
        return $this->permitList;
    }

    protected function getBjSearchDataTranslator() : BjSearchDataTranslator
    {
        return $this->bjSearchDataTranslator;
    }

    public function display()
    {
        $enterpriseData = $this->getEnterprise();
        $enterprise = $this->getEnterpriseTranslator()->objectToArray(
            $enterpriseData,
            [
                'id','name','unifiedSocialCreditCode','enterpriseType',
                'registrationStatus','principal','registrationAuthority',
                'establishmentDate'
            ]
        );
        $enterprise['penaltyTotal'] = 0;
        $enterprise['awardTotal'] = 0;
        $statistical = $this->getRelationCountTranslator()->objectToArray(
            $this->getStatistical()
        );

        $permitList = $this->getPermitList();

        $permits = [];
        /** @var BjSearchData $permit */
        foreach ($permitList as $permit) {
            $permitData = $this->getBjSearchDataTranslator()->objectToArray(
                $permit,
                ['id','sourceUnit','infoClassify','template'=>['id','name']]
            );
            $permitData['url'] = self::DETAIL_URL[$permit->getInfoClassify()].'/'.marmot_encode($permit->getId());
            $permits[] = $permitData;
        }

        $this->getView()->display('Enterprise/Show.tpl', [
            'enterprise'=>$enterprise,
            'statistical'=>$statistical,
            'nav'=>NAV['NAV_INDEX'],
            'resourceCatalogData' => [
                'permit' => [
                    'count' => $this->getPermitCount(),
                    'data' => $permits
                ]
            ]
        ]);
    }
}
