<?php
namespace Base\Package\Enterprise\View\Json;

use Base\Sdk\ResourceCatalog\Model\ISearchData;
use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;
use Sdk\ResourceCatalog\Model\BjSearchData;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

class PermitResourceCatalogDataView extends JsonView implements IView
{
    const DETAIL_URL = [
        ISearchData::INFO_CLASSIFY['XZXK'] => '/doublePublicity',
        ISearchData::INFO_CLASSIFY['XZCF'] => '/doublePublicity',
        ISearchData::INFO_CLASSIFY['HONGMD'] => '/redBlacks',
        ISearchData::INFO_CLASSIFY['HEIMD'] => '/redBlacks',
    ];

    private $dataList;

    private $count;

    public function __construct(
        array $dataList,
        int $count
    ) {
        parent::__construct();
        $this->dataList = $dataList;
        $this->count = $count;
    }

    public function __destruct()
    {
        unset($this->dataList);
        unset($this->count);
    }

    protected function getDataList() : array
    {
        return $this->dataList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : BjSearchDataTranslator
    {
        return new BjSearchDataTranslator();
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $permitList = $this->getDataList();

        $permits = [];
        /** @var BjSearchData $permit */
        foreach ($permitList as $permit) {
            $permitData = $translator->objectToArray(
                $permit,
                ['id','sourceUnit','infoClassify','template'=>['id','name']]
            );
            $permitData['url'] = self::DETAIL_URL[$permit->getInfoClassify()] . '/' .marmot_encode($permit->getId());
            $permits[] = $permitData;
        }

        $list['total'] = $this->getCount();
        $list['list'] = $permits;

        $this->encode($list);
    }
}
