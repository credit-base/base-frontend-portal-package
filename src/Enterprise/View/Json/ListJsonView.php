<?php
namespace Base\Package\Enterprise\View\Json;

use Sdk\Enterprise\Translator\EnterpriseTranslator;
use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

class ListJsonView extends JsonView implements IView
{
    private $enterpriseList;

    private $count;

    private $type;

    private $translator;

    public function __construct(array $enterpriseList, int $count, string $type)
    {
        parent::__construct();

        $this->enterpriseList = $enterpriseList;
        $this->count = $count;
        $this->type = $type;
        $this->translator = new EnterpriseTranslator();
    }

    public function __destruct()
    {
        unset($this->enterpriseList);
        unset($this->count);
        unset($this->type);
        unset($this->translator);
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getType() : string
    {
        return $this->type;
    }

    protected function getList() : array
    {
        return $this->enterpriseList;
    }

    protected function getTranslator() : EnterpriseTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();

        $translator = $this->getTranslator();
        $enterpriseLists = $this->getList();
        foreach ($enterpriseLists as $enterprise) {
            $enterpriseData = $translator->objectToArray(
                $enterprise,
                array(
                    'id',
                    'name',
                    'unifiedSocialCreditCode',
                    'registrationStatus',
                )
            );
            $enterpriseData['awardTotal'] = 0;
            $enterpriseData['penaltyTotal'] = 0;
            $data[] = $enterpriseData;
        }

        $list['total'] = $this->getCount();
        $list['type'] = $this->getType();
        $list['list'] = $data;

        $this->encode($list);
    }
}
