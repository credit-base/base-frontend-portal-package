<?php
namespace Base\Package\Journal\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\Journal\View\Template\ListView;
use Base\Package\Journal\View\Json\ListJsonView;
use Sdk\Journal\Repository\JournalRepository;

class JournalFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new JournalRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : JournalRepository
    {
        return $this->repository;
    }

    protected function filterAction()
    {
        $sort = ['-updateTime'];

        list($page, $size) = $this->getPageAndSize(PHP_INT_MAX);
        list($filter, $sort) = $this->filterFormatChange();

        $count = 0;
        $journalList = array();

        list($count, $journalList) = $this->getRepository()->scenario(
            JournalRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);
      
        if ($this->getRequest()->isAjax()) {
            $this->render(new ListJsonView($journalList, $count));
            return true;
        }

        $this->render(new ListView($journalList, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $sort = ['-updateTime'];

        $filter = array();

        $year = $this->getRequest()->get('year', date('Y'));

        $filter['year'] = $year;
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        return [$filter, $sort];
    }

    protected function fetchOneAction(int $id)
    {
        unset($id);
        return false;
    }
}
