<?php
namespace Base\Package\Journal\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\Journal\Translator\JournalTranslator;

class ListView extends TemplateView implements IView
{
    private $journalList;

    private $count;

    private $translator;

    public function __construct(array $journalList, int $count)
    {
        $this->journalList = $journalList;
        $this->count = $count;
        $this->translator = new JournalTranslator();
        parent::__construct();
    }

    protected function getJournalList() : array
    {
        return $this->journalList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getTranslator() : JournalTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();

        $translator = $this->getTranslator();

        foreach ($this->getJournalList() as $journal) {
            $data[] = $translator->objectToArray(
                $journal,
                array(
                    'id',
                    'title',
                    'source',
                    'year',
                    'cover',
                    'attachment',
                    'authImages',
                    'updateTime',
                    'userGroup' => ['id', 'name'],
                )
            );
        }

        $list['total'] = $this->getCount();
        $list['data'] = $data;

        $nav = NAV['NAV_JOURNAL'];
        
        $this->getView()->display(
            'Journal/List.tpl',
            [
                'nav'=> $nav,
                'list' =>  $list
            ]
        );
    }
}
