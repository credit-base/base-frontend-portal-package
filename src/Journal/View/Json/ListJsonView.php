<?php
namespace Base\Package\Journal\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\Journal\Translator\JournalTranslator;

class ListJsonView extends JsonView implements IView
{
    private $journalList;

    private $count;

    private $translator;

    public function __construct(array $journalList, int $count)
    {
        $this->journalList = $journalList;
        $this->count = $count;
        $this->translator = new JournalTranslator();
        parent::__construct();
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getJournalList() : array
    {
        return $this->journalList;
    }

    protected function getTranslator() : JournalTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();

        $translator = $this->getTranslator();

        foreach ($this->getJournalList() as $journal) {
            $data[] = $translator->objectToArray(
                $journal,
                array(
                    'id',
                    'title',
                    'source',
                    'cover',
                    'attachment',
                    'authImages',
                    'year',
                    'userGroup' => ['id', 'name'],
                    'updateTime',
                )
            );
        }

        $list['total'] = $this->getCount();
        $list['data'] = $data;

        $this->encode($list);
    }
}
