<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ValidateUrlTrait;

use Base\Package\Common\Controller\Factory\RevokeControllerFactory;
use Base\Package\Common\Controller\Interfaces\IRevokeAbleController;

class RevokeController extends Controller
{
    use WebTrait, ValidateUrlTrait;

    protected function getRevokeController(string $resource) : IRevokeAbleController
    {
        return RevokeControllerFactory::getRevokeController($resource);
    }

    public function revoke(string $resource, $id)
    {
        $id = marmot_decode($id);

        if ($this->validateIndexScenario($id)) {
            $revokeController = $this->getRevokeController($resource);

            return $revokeController->revoke($id);
        }

        $this->displayError();
        return false;
    }
}
