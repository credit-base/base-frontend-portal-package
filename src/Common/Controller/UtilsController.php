<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Utils\Cryptojs;
use Base\Package\Common\Utils\Captcha;
use Base\Package\Common\Utils\OSS;

/**
 * @codeCoverageIgnore
 */
class UtilsController extends Controller
{
    use WebTrait;

    /**
     * utils/captcha  [GET]
     * 获取图片验证码captcha
     */
    public function captcha()
    {
        Captcha::render();
    }

    /**
     * utils/oss  [GET]
     * 获取oss
     */
    public function oss()
    {
        if ($ossResponse = OSS::ossResponse()) {
            $data = array('status' => 1, 'data' => $ossResponse);
            echo json_encode($data);

            return true;
        }

        $data = array('status' => 0, 'data' => []);
        echo json_encode($data);

        return false;
    }

    /**
     * utils/hash  [GET]
     * 获取hash
     */
    public function hash()
    {
        if ($hash = Cryptojs::generateHash()) {
            $data = array('status' => 1, 'data' => $hash);
            echo json_encode($data);

            return true;
        }

        $data = array('status' => 0, 'data' => []);
        echo json_encode($data);

        return false;
    }
}
