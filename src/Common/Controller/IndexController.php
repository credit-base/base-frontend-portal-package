<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Factory\IndexControllerFactory;
use Base\Package\Common\Controller\Interfaces\IIndexController;

class IndexController extends Controller
{
    use WebTrait;
    
    protected function getIndexController(string $resource) : IIndexController
    {
        return IndexControllerFactory::getIndexController($resource);
    }

    public function index(string $resource)
    {
        $indexController = $this->getIndexController($resource);
        return $indexController->index();
    }
}
