<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ValidateUrlTrait;
use Base\Package\Common\Controller\Factory\FetchControllerFactory;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

class FetchController extends Controller
{
    use WebTrait, ValidateUrlTrait;
    
    protected function getFetchController(string $resource) : IFetchAbleController
    {
        return FetchControllerFactory::getFetchController($resource);
    }

    public function filter(string $resource)
    {
        $fetchController = $this->getFetchController($resource);

        return $fetchController->filter();
    }

    public function fetchOne(string $resource, $id = '')
    {
        $id = marmot_decode($id);

        if ($this->validateIndexScenario($id)) {
            $fetchController = $this->getFetchController($resource);

            return $fetchController->fetchOne($id);
        }

        $this->displayError();
        return false;
    }
}
