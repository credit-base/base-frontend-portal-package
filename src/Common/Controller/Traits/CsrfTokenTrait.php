<?php
namespace Base\Package\Common\Controller\Traits;

use Marmot\Core;

use Base\Package\Common\Utils\CsrfToken;

/**
 * @codeCoverageIgnore
 */
trait CsrfTokenTrait
{
    protected function validateCsrfToken() : bool
    {
        if ($this->getRequest()->isAjax()) {
            return $this->validateHeaderToken();
        }
        if ($this->getRequest()->isPostMethod()) {
            return $this->validateBodyToken();
        }
        if ($this->getRequest()->isGetMethod()) {
            return $this->validateQueryToken();
        }
        return false;
    }

    private function validateQueryToken() : bool
    {
        Core::setLastError(CSRF_VERIFY_FAILURE);
        return false;
    }

    private function validateBodyToken() : bool
    {
        $csrfToken = $this->getRequest()->post('_csrf_token', '');
        return CsrfToken::validate($csrfToken);
    }

    private function validateHeaderToken() : bool
    {
        $csrfToken = $this->getRequest()->getHeader('X-CSRF-TOKEN', '');
        return CsrfToken::validate($csrfToken);
    }
}
