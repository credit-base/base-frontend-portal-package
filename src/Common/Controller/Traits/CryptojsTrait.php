<?php
namespace Base\Package\Common\Controller\Traits;

use Base\Package\Common\Utils\Cryptojs;

/**
 * @codeCoverageIgnore
 */
trait CryptojsTrait
{
    public function decrypt(string $jsonString)
    {
        return Cryptojs::decrypt($jsonString);
    }

    public function hash()
    {
        return Cryptojs::generateHash();
    }
}
