<?php
namespace Base\Package\Common\Controller\Traits;

trait RevokeControllerTrait
{
    use GlobalCheckTrait;

    public function revoke(int $id)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        return $this->revokeAction($id);
    }

    abstract protected function revokeAction(int $id) : bool;
}
