<?php
namespace Base\Package\Common\Controller\Traits;

use Marmot\Core;
use Marmot\Interfaces\INull;

/**
 * @codeCoverageIgnore
 */
trait GlobalCheckTrait
{
    use CsrfTokenTrait;

    public function globalCheck()
    {
        return $this->globalCsrfCheck()
            && $this->checkUserExist()
            && $this->checkUserStatusEnable();
    }

    public function globalCsrfCheck()
    {
        return true;
        return $this->validateCsrfToken();
    }

    private function checkUserExist()
    {
        if (Core::$container->get('member') instanceof INull) {
            Core::setLastError(NEED_SIGNIN);
            return false;
        }
        return true;
    }

    protected function checkUserStatusEnable() : bool
    {
        if (Core::$container->get('member')->isDisabled()) {
            Core::setLastError(USER_STATUS_DISABLE);
            return false;
        }

        return true;
    }
}
