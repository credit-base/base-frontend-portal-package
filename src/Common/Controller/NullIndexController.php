<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;

use Base\Package\Common\Controller\Interfaces\IIndexController;

class NullIndexController implements IIndexController, INull
{
    public function index()
    {
        Core::setLastError(ROUTE_NOT_EXIST);
    }
}
