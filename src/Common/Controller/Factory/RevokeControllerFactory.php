<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullRevokeController;
use Base\Package\Common\Controller\Interfaces\IRevokeAbleController;

class RevokeControllerFactory
{
    const MAPS = array(
        'praises'=>'\Base\Package\Interaction\Controller\Praise\PraiseRevokeController',
        'qas'=>'\Base\Package\Interaction\Controller\Qa\QaRevokeController',
        'appeals'=>'\Base\Package\Interaction\Controller\Appeal\AppealRevokeController',
        'feedbacks'=>'\Base\Package\Interaction\Controller\Feedback\FeedbackRevokeController',
        'complaints'=>'\Base\Package\Interaction\Controller\Complaint\ComplaintRevokeController',
    );

    public static function getRevokeController(string $resource) : IRevokeAbleController
    {
        $enableController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($enableController) ? new $enableController : new NullRevokeController();
    }
}
