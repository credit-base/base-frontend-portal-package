<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullFetchController;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

class FetchControllerFactory
{
    const MAPS = array(
        'news'=>'Base\Package\News\Controller\NewsFetchController',
        'journals'=>'Base\Package\Journal\Controller\JournalFetchController',
        'creditPhotographys'=>'Base\Package\CreditPhotography\Controller\CreditPhotographyFetchController',
        'members/creditPhotography'=>'Base\Package\CreditPhotography\Controller\MemberCreditPhotographyFetchController',
        'doublePublicity'=>'Base\Package\ResourceCatalog\Controller\DoublePublicityFetchController',
        'redBlacks'=>'Base\Package\ResourceCatalog\Controller\RedBlackFetchController',
        'userGroups'=>'Base\Package\UserGroup\Controller\UserGroupFetchController',
        'enterprises'=>'Base\Package\Enterprise\Controller\EnterpriseFetchController',

        'praises'=>'\Base\Package\Interaction\Controller\Praise\PraiseFetchController',
        'qas'=>'\Base\Package\Interaction\Controller\Qa\QaFetchController',
        'appeals'=>'\Base\Package\Interaction\Controller\Appeal\AppealFetchController',
        'feedbacks'=>'\Base\Package\Interaction\Controller\Feedback\FeedbackFetchController',
        'complaints'=>'\Base\Package\Interaction\Controller\Complaint\ComplaintFetchController',

        'publicPraises'=>'\Base\Package\Interaction\Controller\Praise\PublicFetchController',
        'publicQas'=>'\Base\Package\Interaction\Controller\Qa\PublicFetchController',
    );

    public static function getFetchController(string $resource) : IFetchAbleController
    {
        $fetchController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';
        return class_exists($fetchController) ? new $fetchController : new NullFetchController();
    }
}
