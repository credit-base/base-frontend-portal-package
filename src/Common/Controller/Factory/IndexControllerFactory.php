<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullIndexController;
use Base\Package\Common\Controller\Interfaces\IIndexController;

class IndexControllerFactory
{
    const MAPS = array(
        'news' => 'Base\Package\News\Controller\NewsIndexController',
        'creditPublicities' => 'Base\Package\CreditPublicity\Controller\CreditPublicityIndexController',
        'creditDynamics' => 'Base\Package\News\Controller\CreditDynamics\NewsIndexController',
        'disciplinaries' => 'Base\Package\News\Controller\Disciplinary\NewsIndexController',
        'serviceHalls' => 'Base\Package\ServiceHall\Controller\IndexController',
    );

    public static function getIndexController(string $resource) : IIndexController
    {
        $indexController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($indexController) ? new $indexController : new NullIndexController();
    }
}
