<?php
namespace Base\Package\Common\Controller\Factory;

use Base\Package\Common\Controller\NullOperationController;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

class OperationControllerFactory
{
    const MAPS = array(
        'creditPhotographys'=>'Base\Package\CreditPhotography\Controller\CreditPhotographyOperateController',

        'praises'=>'\Base\Package\Interaction\Controller\Praise\PraiseOperateController',
        'qas'=>'\Base\Package\Interaction\Controller\Qa\QaOperateController',
        'appeals'=>'\Base\Package\Interaction\Controller\Appeal\AppealOperateController',
        'feedbacks'=>'\Base\Package\Interaction\Controller\Feedback\FeedbackOperateController',
        'complaints'=>'\Base\Package\Interaction\Controller\Complaint\ComplaintOperateController',
    );
    
    public static function getOperationController(string $resource) : IOperateAbleController
    {
        $operationController = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($operationController) ? new $operationController : new NullOperationController();
    }
}
