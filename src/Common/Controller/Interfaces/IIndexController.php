<?php
namespace Base\Package\Common\Controller\Interfaces;

interface IIndexController
{
    public function index();
}
