<?php
namespace Base\Package\Common\Controller\Interfaces;

interface IOperateAbleController
{
    public function add();

    public function edit(int $id);
}
