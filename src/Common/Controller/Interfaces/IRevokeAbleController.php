<?php
namespace Base\Package\Common\Controller\Interfaces;

interface IRevokeAbleController
{
    public function revoke(int $id);
}
