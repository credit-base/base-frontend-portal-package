<?php
namespace Base\Package\Common\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\ValidateUrlTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;
use Base\Package\Common\Controller\Factory\OperationControllerFactory;

class OperationController extends Controller
{
    use WebTrait, ValidateUrlTrait;
    
    protected function getOperationController(string $resource) : IOperateAbleController
    {
        return OperationControllerFactory::getOperationController($resource);
    }

    public function add(string $resource): bool
    {
        $operationController = $this->getOperationController($resource);
        return $operationController->add();
    }

    public function edit(string $resource, $id): bool
    {
        $id = marmot_decode($id);
        if ($this->validateIndexScenario($id)) {
            $operationController = $this->getOperationController($resource);

            return $operationController->edit($id);
        }

        $this->displayError();
        return false;
    }
}
