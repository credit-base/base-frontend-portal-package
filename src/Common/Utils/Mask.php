<?php
namespace Base\Package\Common\Utils;

/**
 * @codeCoverageIgnore
 */
class Mask
{
    public static function mask(
        string $string,
        int $start,
        $length = 0
    ) {
        return Mask::substrReplaceCn($string, '*', $start, $length);
    }

    /**
     * @SuppressWarnings(PHPMD.ElseExpression)
     */
    public static function substrReplaceCn($string, $repalce = '*', $start = 0, $len = 0)
    {
        $count = mb_strlen($string, 'UTF-8');

        if (!$count) {
            return $string;
        }

        if ($len == 0) {
            $end = $count;
        } else {
            $end = $start + $len;
        }

        $byte = 0;
        $returnString = '';
        
        while ($byte < $count) {
            $tmpString = mb_substr($string, $byte, 1, 'UTF-8');
            if ($start <= $byte && $byte < $end) {
                $returnString .= $repalce;
            } else {
                $returnString .= $tmpString;
            }
            $byte ++;
        }
        
        return $returnString;
    }

    public static function substrReplaceByMiddle(string $string, $start, $end = 0, $replace = "*", $charset = "UTF-8")
    {
        $len = mb_strlen($string, $charset);
        
        if ($start == 0 || $start > $len) {
            $start = 1;
        }

        if ($end !=0 && $end > $len) {
            $end = $len-2;
        }

        $endStart = $len-$end;
        $top = mb_substr($string, 0, $start, $charset);
        $bottom = "";

        if ($endStart > 0) {
            $bottom = mb_substr($string, $endStart, $end, $charset);
        }
       
        $len = $len - mb_strlen($top, $charset);
        $len = $len - mb_strlen($bottom, $charset);
        $newStr = $top;
     
        for ($i = 0; $i < $len; $i++) {
            $newStr.= $replace;
        }

        $newStr.= $bottom;

        return $newStr;
    }
}
