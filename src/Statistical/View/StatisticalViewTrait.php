<?php
namespace Base\Package\Statistical\View;

use Sdk\Statistical\Model\Statistical;

use Base\Package\Statistical\View\ITranslatorFactory;

/**
 * @codeCoverageIgnore
 */
trait StatisticalViewTrait
{
    protected function getStatisticalTranslator(string $type)
    {
        $translatorFactory = new ITranslatorFactory();
    
        $translator = $translatorFactory->getTranslator($type);
      
        return new $translator;
    }

    public function statisticalArray(string $type, $statistical)
    {
        $data = array();
    
        $translator = $this->getStatisticalTranslator($type);

        $data = $translator->objectToArray(
            $statistical
        );

        return $data;
    }
}
