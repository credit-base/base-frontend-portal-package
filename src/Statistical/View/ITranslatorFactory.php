<?php
namespace Base\Package\Statistical\View;

use Sdk\Statistical\Translator\NullStatisticalTranslator;

/**
 * @codeCoverageIgnore
 */
class ITranslatorFactory
{
    const MAPS = array(
        'staticsCreditPhotography'=>
            'Sdk\Statistical\Translator\StaticsCreditPhotographyTranslator',
    );

    public function getTranslator(string $type)
    {
        $translator = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($translator) ? new $translator : new NullStatisticalTranslator();
    }
}
