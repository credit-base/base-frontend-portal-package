<?php
namespace Base\Package\Statistical\Controller;

use Sdk\Statistical\Adapter\Statistical\NullStatisticalAdapter;

/**
* @todo
* @codeCoverageIgnore
*/
class IAdapterFactory
{
    const MAPS = array(
        'staticsCreditPhotography'=>
            'Sdk\Statistical\Adapter\Statistical\StaticsCreditPhotographyAdapter',
        'enterpriseRelationInformationCount' =>
            'Sdk\Statistical\Adapter\Statistical\StaticsEnterpriseRelationInformationCountAdapter'
    );

    public function getAdapter(string $type)
    {
        $adapter = isset(self::MAPS[$type]) ? self::MAPS[$type] : '';

        return class_exists($adapter) ? new $adapter : new NullStatisticalAdapter();
    }
}
