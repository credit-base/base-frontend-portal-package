<?php
/**
 * \d+(,\d+)*
 * 路由设置
 */
return [
        //监控检测
        [
            'method'=>'GET',
            'rule'=>'/healthz',
            'controller'=>[
                'Base\Package\Home\Controller\HealthzController',
                'healthz'
            ]
        ],
        //首页
        [
            'method'=>'GET',
            'rule'=>'/index',
            'controller'=>[
                'Base\Package\Home\Controller\IndexController',
                'index'
            ]
        ],
    ];
