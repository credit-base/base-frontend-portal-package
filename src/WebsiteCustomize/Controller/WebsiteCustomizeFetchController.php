<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\WebsiteCustomize\View\Template\SearchView;
use Base\Package\WebsiteCustomize\View\Template\FooterView;
use Base\Package\WebsiteCustomize\View\Template\HeaderView;
use Base\Package\WebsiteCustomize\View\Template\ToolBoxView;
use Base\Package\WebsiteCustomize\View\Template\NavView;

use Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

class WebsiteCustomizeFetchController extends Controller
{
    use WebTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new WebsiteCustomizeRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : WebsiteCustomizeRepository
    {
        return $this->repository;
    }

    protected function getCustomizeData(string $category)
    {
        $websiteCustomize = array();
        $websiteCustomize = $this->getRepository()
                                 ->customize($category);

        return $websiteCustomize;
    }

    public function search()
    {
        $category = 'homePage';

        $websiteCustomize = $this->getCustomizeData($category);
        
        $this->render(new SearchView($websiteCustomize));
        
        return true;
    }

    public function footer()
    {
        $category = 'homePage';

        $websiteCustomize = $this->getCustomizeData($category);
        
        $this->render(new FooterView($websiteCustomize));
        return true;
    }

    public function header($signInStatus)
    {
        $category = 'homePage';

        $websiteCustomize = $this->getCustomizeData($category);
        
        $this->render(new HeaderView($websiteCustomize, $signInStatus));
        return true;
    }

    public function toolBox()
    {
        $category = 'homePage';

        $websiteCustomize = $this->getCustomizeData($category);
        
        $this->render(new ToolBoxView($websiteCustomize));
        return true;
    }

    public function nav(int $param)
    {
        $category = 'homePage';
      
        $websiteCustomize = $this->getCustomizeData($category);
    
        $this->render(new NavView($websiteCustomize, $param));
        return true;
    }
}
