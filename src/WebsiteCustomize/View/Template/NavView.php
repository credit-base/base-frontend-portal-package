<?php
namespace Base\Package\WebsiteCustomize\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class NavView extends TemplateView implements IView
{
    use ViewTrait;

    public function display()
    {
        $data = $this->getData();
        
        $this->getView()->display(
            'Layout/Nav.tpl',
            [
                'nav'=>$this->getParam(),
                'navData' =>  $data['content']['nav'],
            ]
        );
    }
}
