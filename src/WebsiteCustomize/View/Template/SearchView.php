<?php
namespace Base\Package\WebsiteCustomize\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class SearchView extends TemplateView implements IView
{
    use ViewTrait;

    public function display()
    {
        $data = $this->getData();
       
        $this->getView()->display(
            'Layout/Search.tpl',
            [
                'searchData' =>  $data['content']['headerSearch'],
            ]
        );
    }
}
