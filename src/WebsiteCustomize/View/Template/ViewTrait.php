<?php
namespace Base\Package\WebsiteCustomize\View\Template;

use Marmot\Framework\Classes\Filter;

use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;

trait ViewTrait
{
    private $websiteCustomize;

    private $translator;

    private $param;

    public function __construct($websiteCustomize, $param = 0)
    {
        $this->websiteCustomize = $websiteCustomize;
        $this->param = $param;
        $this->translator = new WebsiteCustomizeTranslator();
        parent::__construct();
    }

    protected function getWebsiteCustomize()
    {
        return $this->websiteCustomize;
    }

    protected function getParam()
    {
        return $this->param;
    }

    protected function getTranslator() : WebsiteCustomizeTranslator
    {
        return $this->translator;
    }

    protected function getData(): array
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getWebsiteCustomize(),
            array('content')
        );

        if (isset($data['content']['rightToolBar'])) {
            $rightToolBar = $data['content']['rightToolBar'];
            $data['content']['rightToolBar'] = $this->getDescriptionByDhtml($rightToolBar);
        }

        if (isset($data['content']['headerSearch'])) {
            $headerSearch = $data['content']['headerSearch'];
            $data['content']['headerSearch'] = $this->getHeaderSearchAddInputData($headerSearch);
        }
        
        return $data;
    }

    protected function getDescriptionByDhtml(array $list):array
    {
        foreach ($list as $key => $val) {
            if (isset($val['description'])) {
                $val['description'] = Filter::dhtmlspecialchars($val['description']);
            }
            $list[$key] = $val;
        }

        return $list;
    }

    protected function getHeaderSearchAddInputData(array $list):array
    {
        foreach ($list as $key => $val) {
            $val['identify'] = $key;
            
            foreach (SEARCH_NAV_INPUT_CN as $inputKey => $inputCn) {
                if ($key == $inputKey) {
                    $val['inputData'] = $inputCn;
                }
            }
            foreach (SEARCH_NAV_LINK as $linkKey => $link) {
                if ($key == $linkKey) {
                    $val['link'] = $link;
                }
            }

            $list[$key] = $val;
        }

        return array_values($list);
    }
}
