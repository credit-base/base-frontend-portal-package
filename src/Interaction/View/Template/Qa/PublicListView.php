<?php
namespace Base\Package\Interaction\View\Template\Qa;

use Sdk\Interaction\Translator\Qa\QaTranslator;
use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class PublicListView extends TemplateView implements IView
{
    use ListViewTrait;

    protected function getTranslator() : QaTranslator
    {
        return new QaTranslator();
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $lists = $this->getList();

        $data = array();
        foreach ($lists as $qa) {
            $data[] = $translator->objectToArray(
                $qa
            );
        }

        $list['total'] = $this->getCount();
        $list['list'] = $data;

        $this->getView()->display(
            'Interaction/Qa/PublicList.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' =>  $list
            ]
        );
    }
}
