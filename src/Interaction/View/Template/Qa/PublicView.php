<?php
namespace Base\Package\Interaction\View\Template\Qa;

use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Qa\QaTranslator;

use Base\Package\Interaction\View\ViewTrait;

class PublicView extends TemplateView implements IView
{
    use ViewTrait;

    protected function getQaTranslator() : QaTranslator
    {
        return new QaTranslator();
    }

    public function display()
    {
        $qaData = $this->getData();
        $qaData = $this->getQaTranslator()->objectToArray(
            $qaData
        );

        $this->getView()->display('Interaction/Qa/PublicDetail.tpl', [
            'nav'=>NAV['NAV_INDEX'],
            'data' => $qaData
        ]);
    }
}
