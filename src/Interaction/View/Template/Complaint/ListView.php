<?php
namespace Base\Package\Interaction\View\Template\Complaint;

use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;
use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    protected function getTranslator() : ComplaintTranslator
    {
        return new ComplaintTranslator();
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $lists = $this->getList();

        $data = array();
        foreach ($lists as $complaint) {
            $data[] = $translator->objectToArray(
                $complaint
            );
        }

        $list['total'] = $this->getCount();
        $list['list'] = $data;

        $this->getView()->display(
            'Interaction/Complaint/List.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' =>  $list
            ]
        );
    }
}
