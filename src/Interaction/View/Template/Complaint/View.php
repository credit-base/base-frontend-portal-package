<?php
namespace Base\Package\Interaction\View\Template\Complaint;

use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

use Base\Package\Interaction\View\ViewTrait;

class View extends TemplateView implements IView
{
    use ViewTrait;

    protected function getComplaintTranslator() : ComplaintTranslator
    {
        return new ComplaintTranslator();
    }

    public function display()
    {
        $complaintData = $this->getData();
        $complaint = $this->getComplaintTranslator()->objectToArray(
            $complaintData
        );

        $this->getView()->display('Interaction/Complaint/Detail.tpl', [
            'nav'=>NAV['NAV_INDEX'],
            'data' => $complaint
        ]);
    }
}
