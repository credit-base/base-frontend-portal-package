<?php
namespace Base\Package\Interaction\View\Template\Complaint;

use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;

class AddView extends TemplateView implements IView
{
    public function __construct()
    {
        parent::__construct();
    }

    public function display()
    {
        $this->getView()->display('Interaction/Complaint/Add.tpl', [
            'nav'=>NAV['NAV_INDEX']
        ]);
    }
}
