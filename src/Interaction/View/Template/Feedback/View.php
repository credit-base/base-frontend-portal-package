<?php
namespace Base\Package\Interaction\View\Template\Feedback;

use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

use Base\Package\Interaction\View\ViewTrait;

class View extends TemplateView implements IView
{
    use ViewTrait;

    protected function getFeedbackTranslator() : FeedbackTranslator
    {
        return new FeedbackTranslator();
    }

    public function display()
    {
        $feedbackData = $this->getData();
        $feedback = $this->getFeedbackTranslator()->objectToArray(
            $feedbackData
        );

        $this->getView()->display('Interaction/Feedback/Detail.tpl', [
            'nav'=>NAV['NAV_INDEX'],
            'data' => $feedback
        ]);
    }
}
