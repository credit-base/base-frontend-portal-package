<?php
namespace Base\Package\Interaction\View\Template\Feedback;

use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;

class AddView extends TemplateView implements IView
{
    public function __construct()
    {
        parent::__construct();
    }

    public function display()
    {
        $this->getView()->display('Interaction/Feedback/Add.tpl', [
            'nav'=>NAV['NAV_INDEX']
        ]);
    }
}
