<?php
namespace Base\Package\Interaction\View\Template\Feedback;

use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;
use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    protected function getTranslator() : FeedbackTranslator
    {
        return new FeedbackTranslator();
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $lists = $this->getList();

        $data = array();
        foreach ($lists as $feedback) {
            $data[] = $translator->objectToArray(
                $feedback
            );
        }

        $list['total'] = $this->getCount();
        $list['list'] = $data;

        $this->getView()->display(
            'Interaction/Feedback/List.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' =>  $list
            ]
        );
    }
}
