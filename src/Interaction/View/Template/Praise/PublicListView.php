<?php
namespace Base\Package\Interaction\View\Template\Praise;

use Sdk\Interaction\Translator\Praise\PraiseTranslator;
use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class PublicListView extends TemplateView implements IView
{
    use ListViewTrait;

    protected function getTranslator() : PraiseTranslator
    {
        return new PraiseTranslator();
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $lists = $this->getList();

        $data = array();
        foreach ($lists as $prise) {
            $data[] = $translator->objectToArray(
                $prise
            );
        }

        $list['total'] = $this->getCount();
        $list['list'] = $data;

        $this->getView()->display(
            'Interaction/Praise/PublicList.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' =>  $list
            ]
        );
    }
}
