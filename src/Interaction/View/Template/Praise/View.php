<?php
namespace Base\Package\Interaction\View\Template\Praise;

use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;

use Base\Package\Interaction\View\ViewTrait;

class View extends TemplateView implements IView
{
    use ViewTrait;

    protected function getPraiseTranslator() : PraiseTranslator
    {
        return new PraiseTranslator();
    }

    public function display()
    {
        $praiseData = $this->getData();
        $praise = $this->getPraiseTranslator()->objectToArray(
            $praiseData
        );

        $this->getView()->display('Interaction/Praise/Detail.tpl', [
            'nav'=>NAV['NAV_INDEX'],
            'data' => $praise
        ]);
    }
}
