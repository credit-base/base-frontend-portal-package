<?php
namespace Base\Package\Interaction\View\Template\Appeal;

use Sdk\Interaction\Translator\Appeal\AppealTranslator;
use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    protected function getTranslator() : AppealTranslator
    {
        return new AppealTranslator();
    }

    public function display()
    {
        $translator = $this->getTranslator();
        $lists = $this->getList();

        $data = array();
        foreach ($lists as $appeal) {
            $data[] = $translator->objectToArray(
                $appeal
            );
        }

        $list['total'] = $this->getCount();
        $list['list'] = $data;

        $this->getView()->display(
            'Interaction/Appeal/List.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' =>  $list
            ]
        );
    }
}
