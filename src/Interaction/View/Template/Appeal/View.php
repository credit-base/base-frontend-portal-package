<?php
namespace Base\Package\Interaction\View\Template\Appeal;

use Marmot\Framework\View\Template\TemplateView;
use Marmot\Interfaces\IView;
use Sdk\Interaction\Translator\Appeal\AppealTranslator;

use Base\Package\Interaction\View\ViewTrait;

class View extends TemplateView implements IView
{
    use ViewTrait;

    protected function getAppealTranslator() : AppealTranslator
    {
        return new AppealTranslator();
    }

    public function display()
    {
        $appealData = $this->getData();
        $appeal = $this->getAppealTranslator()->objectToArray(
            $appealData
        );
        
        $this->getView()->display('Interaction/Appeal/Detail.tpl', [
            'nav'=>NAV['NAV_INDEX'],
            'data' => $appeal
        ]);
    }
}
