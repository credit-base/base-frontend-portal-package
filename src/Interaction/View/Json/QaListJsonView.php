<?php
namespace Base\Package\Interaction\View\Json;

use Sdk\Interaction\Translator\Qa\QaTranslator;
use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class QaListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getQaTranslator() : QaTranslator
    {
        return new QaTranslator();
    }

    public function display()
    {
        $data = array();

        $translator = $this->getQaTranslator();
        $qaLists = $this->getList();
        foreach ($qaLists as $qa) {
            $data[] = $translator->objectToArray(
                $qa
            );
        }


        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
