<?php
namespace Base\Package\Interaction\View\Json;

use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;
use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class ComplaintListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getComplaintTranslator() : ComplaintTranslator
    {
        return new ComplaintTranslator();
    }

    public function display()
    {
        $data = array();

        $translator = $this->getComplaintTranslator();
        $complaintLists = $this->getList();
        foreach ($complaintLists as $complaint) {
            $data[] = $translator->objectToArray(
                $complaint
            );
        }


        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
