<?php
namespace Base\Package\Interaction\View\Json;

use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;
use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class FeedbackListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getFeedbackTranslator() : FeedbackTranslator
    {
        return new FeedbackTranslator();
    }

    public function display()
    {
        $data = array();

        $translator = $this->getFeedbackTranslator();
        $feedbackLists = $this->getList();
        foreach ($feedbackLists as $feedback) {
            $data[] = $translator->objectToArray(
                $feedback
            );
        }


        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
