<?php
namespace Base\Package\Interaction\View\Json;

use Sdk\Interaction\Translator\Praise\PraiseTranslator;
use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class PraiseListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getPraiseTranslator() : PraiseTranslator
    {
        return new PraiseTranslator();
    }

    public function display()
    {
        $data = array();

        $translator = $this->getPraiseTranslator();
        $praiseLists = $this->getList();
        foreach ($praiseLists as $praise) {
            $data[] = $translator->objectToArray(
                $praise
            );
        }


        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
