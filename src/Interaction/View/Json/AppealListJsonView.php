<?php
namespace Base\Package\Interaction\View\Json;

use Sdk\Interaction\Translator\Appeal\AppealTranslator;
use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class AppealListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getAppealTranslator() : AppealTranslator
    {
        return new AppealTranslator();
    }

    public function display()
    {
        $data = array();

        $translator = $this->getAppealTranslator();
        $appealLists = $this->getList();
        foreach ($appealLists as $appeal) {
            $data[] = $translator->objectToArray(
                $appeal
            );
        }


        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
