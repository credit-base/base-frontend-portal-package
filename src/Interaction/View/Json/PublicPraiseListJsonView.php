<?php
namespace Base\Package\Interaction\View\Json;

use Sdk\Interaction\Translator\Praise\PraiseTranslator;
use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class PublicPraiseListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getPraiseTranslator() : PraiseTranslator
    {
        return new PraiseTranslator();
    }

    public function display()
    {
        $praiseList = array();

        $praiseTranslator = $this->getPraiseTranslator();
        $publicPraiseLists = $this->getList();
        foreach ($publicPraiseLists as $value) {
            $praiseList[] = $praiseTranslator->objectToArray(
                $value
            );
        }


        $dataList = array(
            'total' => $this->getCount(),
            'list' => $praiseList,
        );

        $this->encode($dataList);
    }
}
