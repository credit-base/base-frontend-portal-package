<?php
namespace Base\Package\Interaction\View\Json;

use Sdk\Interaction\Translator\Qa\QaTranslator;
use Marmot\Framework\View\Json\JsonView;
use Marmot\Interfaces\IView;

use Base\Package\Interaction\View\ListViewTrait;

class PublicQaListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    protected function getQaTranslator() : QaTranslator
    {
        return new QaTranslator();
    }

    public function display()
    {
        $qaList = array();

        $qaTranslator = $this->getQaTranslator();
        $publicQaLists = $this->getList();
        foreach ($publicQaLists as $item) {
            $qaList[] = $qaTranslator->objectToArray(
                $item
            );
        }

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $qaList,
        );

        $this->encode($dataList);
    }
}
