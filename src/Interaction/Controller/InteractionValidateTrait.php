<?php
namespace Base\Package\Interaction\Controller;

use Sdk\Interaction\WidgetRules\InteractionWidgetRules;

use Sdk\Common\WidgetRules\WidgetRules;

trait InteractionValidateTrait
{
    protected function getInteractionWidgetRules() : InteractionWidgetRules
    {
        return new InteractionWidgetRules();
    }

    protected function getWidgetRules() : WidgetRules
    {
        return new WidgetRules();
    }

    protected function validateCommonContentScenario(
        $title,
        $content,
        $acceptUserGroupId
    ) : bool {
        $interactionWidgetRules = $this->getInteractionWidgetRules();
        $commonWidgetRules = $this->getWidgetRules();
        
        return $interactionWidgetRules->content($content, 'content')
            && $interactionWidgetRules->title($title, 'title')
            && $commonWidgetRules->formatNumeric($acceptUserGroupId, 'acceptUserGroupId');
    }

    protected function validateCommonItemsScenario(
        $name,
        $identify,
        $type,
        $contact,
        $images
    ) : bool {
        $interactionWidgetRules = $this->getInteractionWidgetRules();
      
        return $interactionWidgetRules->subject($name, 'name')
            && $interactionWidgetRules->identify($identify)
            && $interactionWidgetRules->contact($contact)
            && $interactionWidgetRules->type($type)
            && $interactionWidgetRules->images($images);
    }

    protected function validatePraiseAndComplaintScenario(
        $title,
        $content,
        $name,
        $identify,
        $contact,
        $subject,
        $images,
        $type,
        $acceptUserGroupId
    ) : bool {
        $interactionWidgetRules = $this->getInteractionWidgetRules();
     
        return $interactionWidgetRules->subject($subject, 'subject')
            && $this->validateCommonScenario(
                $title,
                $content,
                $name,
                $identify,
                $type,
                $contact,
                $images,
                $acceptUserGroupId
            );
    }

    protected function validateCommonScenario(
        $title,
        $content,
        $name,
        $identify,
        $type,
        $contact,
        $images,
        $acceptUserGroupId
    ): bool {
        
        return $this->validateCommonContentScenario($title, $content, $acceptUserGroupId)
            && $this->validateCommonItemsScenario($name, $identify, $type, $contact, $images);
    }
}
