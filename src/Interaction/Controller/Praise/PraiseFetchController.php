<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\PraiseRepository;

use Base\Package\Common\Controller\Interfaces\IFetchAbleController;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Traits\GlobalCheckTrait;

use Base\Package\Interaction\View\Json\PraiseListJsonView;
use Base\Package\Interaction\View\Template\Praise\ListView;
use Base\Package\Interaction\View\Template\Praise\View;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class PraiseFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait,GlobalCheckTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new PraiseRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : PraiseRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        list($page, $size) = $this->getPageAndSize(SIZE);
        list($filter, $sort) = $this->filterFormatChange();

        list($count, $praiseList) = $this->getRepository()->scenario(
            PraiseRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new PraiseListJsonView($praiseList, $count));
            return true;
        }

        $this->render(new ListView($praiseList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $praise = $this->getRepository()
                    ->scenario(PraiseRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);
        
        if ($praise instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new View($praise));
        return true;
    }
}
