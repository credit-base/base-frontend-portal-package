<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\PraiseRepository;
use Sdk\Interaction\Model\Praise;

use Base\Package\Common\Controller\Interfaces\IFetchAbleController;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Interaction\View\Json\PublicPraiseListJsonView;
use Base\Package\Interaction\View\Template\Praise\PublicListView;
use Base\Package\Interaction\View\Template\Praise\PublicView;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class PublicFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new PraiseRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : PraiseRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($page, $size) = $this->getPageAndSize(SIZE);
        list($filter, $sort) = $this->filterFormatChange(Praise::STATUS['PUBLISH']);

        list($count, $praiseList) = $this->getRepository()->scenario(
            PraiseRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new PublicPraiseListJsonView($praiseList, $count));
            return true;
        }

        $this->render(new PublicListView($praiseList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $praise = $this->getRepository()
                    ->scenario(PraiseRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);
        
        if ($praise instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new PublicView($praise));
        return true;
    }
}
