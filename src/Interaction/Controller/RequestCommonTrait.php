<?php
namespace Base\Package\Interaction\Controller;

use Marmot\Core;

use Sdk\Interaction\Model\Praise;

trait RequestCommonTrait
{
    protected function getAddRequestCommonData() : array
    {
        $request = $this->getRequest();
        $requestData = array();

        $requestData['content'] =  $request->post('content', '');
        $requestData['title'] =  $request->post('title', '');
        $requestData['name'] =  $request->post('name', '');
        $requestData['identify'] =  $request->post('identify', '');
        $requestData['certificates'] =  $request->post('certificates', array());
        $requestData['type'] =  intval(marmot_decode($request->post('type', '')));
        $requestData['contact'] =  $request->post('contact', '');
        $requestData['subject'] =  $request->post('subject', '');
        $requestData['images'] =  $request->post('images', array());
        $requestData['acceptUserGroupId'] =  intval(marmot_decode($request->post('acceptUserGroupId', '')));
        
        return $requestData;
    }

    protected function filterFormatChange($scene = CONSTANT)
    {
        $request = $this->getRequest();
        
        $sort = $request->get('sort', '-updateTime');
        $title = $request->get('title', '');
        
        $filter = array();

        if (!empty($title)) {
            $filter['title'] = $title;
        }

        if ($scene != Praise::STATUS['PUBLISH']) {
            $filter['member'] = Core::$container->get('member')->getId();
        }

        if ($scene == Praise::STATUS['PUBLISH']) {
            $filter['status'] = Praise::STATUS['PUBLISH'];
        }
        
        return [$filter, [$sort]];
    }
}
