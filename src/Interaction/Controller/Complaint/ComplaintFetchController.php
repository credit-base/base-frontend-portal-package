<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\ComplaintRepository;

use Base\Package\Common\Controller\Interfaces\IFetchAbleController;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Traits\GlobalCheckTrait;

use Base\Package\Interaction\View\Json\ComplaintListJsonView;
use Base\Package\Interaction\View\Template\Complaint\ListView;
use Base\Package\Interaction\View\Template\Complaint\View;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class ComplaintFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait, GlobalCheckTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new ComplaintRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : ComplaintRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        list($page, $size) = $this->getPageAndSize(SIZE);
        list($filter, $sort) = $this->filterFormatChange();

        list($count, $complaintList) = $this->getRepository()->scenario(
            ComplaintRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new ComplaintListJsonView($complaintList, $count));
            return true;
        }

        $this->render(new ListView($complaintList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $complaint = $this->getRepository()
                    ->scenario(ComplaintRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);
        
        if ($complaint instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new View($complaint));
        return true;
    }
}
