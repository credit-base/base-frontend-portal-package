<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Interfaces\IRevokeAbleController;
use Base\Package\Common\Controller\Traits\RevokeControllerTrait;

use Sdk\Interaction\Command\Complaint\RevokeComplaintCommand;
use Sdk\Interaction\CommandHandler\Complaint\ComplaintCommandHandlerFactory;

class ComplaintRevokeController extends Controller implements IRevokeAbleController
{
    use WebTrait, RevokeControllerTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ComplaintCommandHandlerFactory());
    }

    public function __destruct()
    {
        unset($this->commandBus);
    }

    protected function getCommandBus():CommandBus
    {
        return $this->commandBus;
    }

    protected function revokeAction(int $id)
    {
        
        $command = new RevokeComplaintCommand($id);
        
        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }

        $this->displayError();
        return false;
    }
}
