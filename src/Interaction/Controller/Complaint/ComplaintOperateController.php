<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Interfaces\IOperateAbleController;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Interaction\View\Template\Complaint\AddView;

use Base\Package\Interaction\Controller\RequestCommonTrait;
use Base\Package\Interaction\Controller\InteractionValidateTrait;

use Sdk\Interaction\Command\Complaint\AddComplaintCommand;
use Sdk\Interaction\CommandHandler\Complaint\ComplaintCommandHandlerFactory;

class ComplaintOperateController extends Controller implements IOperateAbleController
{
    use WebTrait, OperateControllerTrait, RequestCommonTrait, InteractionValidateTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new ComplaintCommandHandlerFactory());
    }

    public function __destruct()
    {
        unset($this->commandBus);
    }

    protected function getCommandBus():CommandBus
    {
        return $this->commandBus;
    }

    protected function addView() : bool
    {
        $this->render(new AddView());
        return true;
    }

    protected function addAction()
    {
        $requestData = $this->getAddRequestCommonData();
        
        if ($this->validatePraiseAndComplaintScenario(
            $requestData['title'],
            $requestData['content'],
            $requestData['name'],
            $requestData['identify'],
            $requestData['contact'],
            $requestData['subject'],
            $requestData['images'],
            $requestData['type'],
            $requestData['acceptUserGroupId']
        )) {
            $command = new AddComplaintCommand(
                $requestData['title'],
                $requestData['content'],
                $requestData['name'],
                $requestData['identify'],
                $requestData['contact'],
                $requestData['subject'],
                $requestData['images'],
                $requestData['type'],
                $requestData['acceptUserGroupId']
            );
         
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }
}
