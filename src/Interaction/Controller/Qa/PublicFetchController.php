<?php
namespace Base\Package\Interaction\Controller\Qa;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\QaRepository;
use Sdk\Interaction\Model\Qa;

use Base\Package\Common\Controller\Interfaces\IFetchAbleController;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Interaction\View\Json\PublicQaListJsonView;
use Base\Package\Interaction\View\Template\Qa\PublicListView;
use Base\Package\Interaction\View\Template\Qa\PublicView;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class PublicFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new QaRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : QaRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($page, $size) = $this->getPageAndSize(SIZE);
        list($filter, $sort) = $this->filterFormatChange(Qa::STATUS['PUBLISH']);

        list($count, $qaDataList) = $this->getRepository()->scenario(
            QaRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new PublicQaListJsonView($qaDataList, $count));
            return true;
        }

        $this->render(new PublicListView($qaDataList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $qaData = $this->getRepository()
                    ->scenario(QaRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);
        
        if ($qaData instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new PublicView($qaData));
        return true;
    }
}
