<?php
namespace Base\Package\Interaction\Controller\Qa;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Interfaces\IOperateAbleController;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Interaction\View\Template\Qa\AddView;

use Base\Package\Interaction\Controller\RequestCommonTrait;
use Base\Package\Interaction\Controller\InteractionValidateTrait;

use Sdk\Interaction\Command\Qa\AddQaCommand;
use Sdk\Interaction\CommandHandler\Qa\QaCommandHandlerFactory;

class QaOperateController extends Controller implements IOperateAbleController
{
    use WebTrait, OperateControllerTrait, RequestCommonTrait, InteractionValidateTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new QaCommandHandlerFactory());
    }

    public function __destruct()
    {
        unset($this->commandBus);
    }

    protected function addView() : bool
    {
        $this->render(new AddView());
        return true;
    }

    protected function getCommandBus():CommandBus
    {
        return $this->commandBus;
    }

    protected function addAction()
    {
        $requestData = $this->getAddRequestCommonData();
        
        if ($this->validateCommonContentScenario(
            $requestData['title'],
            $requestData['content'],
            $requestData['acceptUserGroupId']
        )) {
            $command = new AddQaCommand(
                $requestData['title'],
                $requestData['content'],
                $requestData['acceptUserGroupId']
            );
         
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }
}
