<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\FeedbackRepository;

use Base\Package\Common\Controller\Interfaces\IFetchAbleController;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Traits\GlobalCheckTrait;

use Base\Package\Interaction\View\Json\FeedbackListJsonView;
use Base\Package\Interaction\View\Template\Feedback\ListView;
use Base\Package\Interaction\View\Template\Feedback\View;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class FeedbackFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, RequestCommonTrait,GlobalCheckTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new FeedbackRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : FeedbackRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        list($page, $size) = $this->getPageAndSize(SIZE);
        list($filter, $sort) = $this->filterFormatChange();

        list($count, $feedbackList) = $this->getRepository()->scenario(
            FeedbackRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new FeedbackListJsonView($feedbackList, $count));
            return true;
        }

        $this->render(new ListView($feedbackList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $feedback = $this->getRepository()
                    ->scenario(FeedbackRepository::FETCH_ONE_MODEL_UN)
                    ->fetchOne($id);
        
        if ($feedback instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new View($feedback));
        return true;
    }
}
