<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\Interaction\Repository\AppealRepository;

use Base\Package\Common\Controller\Interfaces\IFetchAbleController;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Traits\GlobalCheckTrait;

use Base\Package\Interaction\View\Json\AppealListJsonView;
use Base\Package\Interaction\View\Template\Appeal\ListView;
use Base\Package\Interaction\View\Template\Appeal\View;

use Base\Package\Interaction\Controller\RequestCommonTrait;

class AppealFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait,RequestCommonTrait, GlobalCheckTrait;

    private $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new AppealRepository();
    }

    public function __destruct()
    {
        unset($this->repository);
    }

    protected function getRepository() : AppealRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        list($page, $size) = $this->getPageAndSize(SIZE);
        list($filter, $sort) = $this->filterFormatChange();

        list($count, $appealList) = $this->getRepository()->scenario(
            AppealRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);

        if ($this->getRequest()->isAjax()) {
            $this->render(new AppealListJsonView($appealList, $count));
            return true;
        }

        $this->render(new ListView($appealList, $count));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $appeal = $this->getRepository()
                        ->scenario(AppealRepository::FETCH_ONE_MODEL_UN)
                        ->fetchOne($id);
                     
        if ($appeal instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new View($appeal));
        return true;
    }
}
