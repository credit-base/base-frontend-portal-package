<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Interfaces\IOperateAbleController;
use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Interaction\View\Template\Appeal\AddView;

use Base\Package\Interaction\Controller\RequestCommonTrait;
use Base\Package\Interaction\Controller\InteractionValidateTrait;

use Sdk\Interaction\Command\Appeal\AddAppealCommand;
use Sdk\Interaction\CommandHandler\Appeal\AppealCommandHandlerFactory;

class AppealOperateController extends Controller implements IOperateAbleController
{
    use WebTrait, OperateControllerTrait, RequestCommonTrait, InteractionValidateTrait;

    protected $commandBus;

    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new AppealCommandHandlerFactory());
    }

    public function __destruct()
    {
        unset($this->commandBus);
    }

    protected function getCommandBus():CommandBus
    {
        return $this->commandBus;
    }

    protected function addView() : bool
    {
        $this->render(new AddView());
        return true;
    }

    protected function addAction()
    {
        $requestData = $this->getAddRequestCommonData();
        
        if ($this->validateAddAppealScenario(
            $requestData['title'],
            $requestData['content'],
            $requestData['name'],
            $requestData['identify'],
            $requestData['contact'],
            $requestData['certificates'],
            $requestData['images'],
            $requestData['type'],
            $requestData['acceptUserGroupId']
        )) {
            $command = new AddAppealCommand(
                $requestData['title'],
                $requestData['content'],
                $requestData['name'],
                $requestData['identify'],
                $requestData['contact'],
                $requestData['certificates'],
                $requestData['images'],
                $requestData['type'],
                $requestData['acceptUserGroupId']
            );
            
            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    protected function validateAddAppealScenario(
        $title,
        $content,
        $name,
        $identify,
        $contact,
        $certificates,
        $images,
        $type,
        $acceptUserGroupId
    ) : bool {
        $interactionWidgetRules = $this->getInteractionWidgetRules();
        
        return $interactionWidgetRules->certificates($certificates, 'certificates')
            && $this->validateCommonScenario(
                $title,
                $content,
                $name,
                $identify,
                $type,
                $contact,
                $images,
                $acceptUserGroupId
            );
    }

    protected function editView(int $id) : bool
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }

    protected function editAction(int $id)
    {
        unset($id);
        Core::setLastError(ROUTE_NOT_EXIST);
        $this->displayError();
        return false;
    }
}
