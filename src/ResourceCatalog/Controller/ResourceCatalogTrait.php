<?php
namespace Base\Package\ResourceCatalog\Controller;

use Sdk\Template\Model\BjTemplate;
use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;
use Base\Sdk\ResourceCatalog\Model\SearchData;

trait ResourceCatalogTrait
{
    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new BjSearchDataRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : BjSearchDataRepository
    {
        return $this->repository;
    }

    /**
    * 屏蔽类中所有PMD警告
    * @SuppressWarnings(PHPMD)
    */
    protected function filterFormatChange($type)
    {
        $request = $this->getRequest();
        
        $name = $request->get('name', '');
        $identify = $request->get('identify', '');
        $userGroup = $request->get('userGroup', '');
        $subjectCategory = $request->get('subjectCategory', 'MA');
        $scene = $request->get('scene', 'MA');

        $filter = array();
        $filter['dimension'] = BjTemplate::DIMENSION['SHGK'];
        $filter['status'] = SearchData::DATA_STATUS['CONFIRM'];

        if (!empty($name)) {
            $filter['name'] = $name;
        }

        if (!empty($identify)) {
            $filter['identify'] = $identify;
        }

        if (!empty($userGroup)) {
            $filter['sourceUnit'] = marmot_decode($userGroup);
        }

        $sceneArray = [
            'positive' => 1,
            'negative' => 2
        ];

        if ($type == 'RedBlack' && $scene == marmot_encode($sceneArray['positive'])) {
            $filter['subjectCategory'] = marmot_decode($subjectCategory);
            $filter['infoClassify'] = BjTemplate::INFO_CLASSIFY['RMD'];
        }

        if ($type == 'RedBlack' && $scene == marmot_encode($sceneArray['negative'])) {
            $filter['subjectCategory'] = marmot_decode($subjectCategory);
            $filter['infoClassify'] = BjTemplate::INFO_CLASSIFY['HMD'];
        }

        if ($type == 'DoublePublicity' && $scene == marmot_encode($sceneArray['positive'])) {
            $filter['infoClassify'] = BjTemplate::INFO_CLASSIFY['XZXK'];
        }

        if ($type == 'DoublePublicity' && $scene == marmot_encode($sceneArray['negative'])) {
            $filter['infoClassify'] = BjTemplate::INFO_CLASSIFY['XZCF'];
        }

        return [$filter, ['-updateTime'], $scene];
    }
}
