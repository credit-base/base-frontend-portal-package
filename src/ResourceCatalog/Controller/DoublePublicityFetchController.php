<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\ResourceCatalog\View\Template\DoublePublicityDataView;
use Base\Package\ResourceCatalog\View\Template\DoublePublicityListView;
use Base\Package\ResourceCatalog\View\Json\DoublePublicityListJsonView;

class DoublePublicityFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, ResourceCatalogTrait;

    protected function filterAction() : bool
    {
        list($page, $size) = $this->getPageAndSize(SEARCH_LIST_SIZE);
        list($filter, $sort, $scene) = $this->filterFormatChange('DoublePublicity');

        $doublePublicityList = array();
        list($count, $doublePublicityList) = $this->getRepository()->scenario(
            BjSearchDataRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);
    
        if ($this->getRequest()->isAjax()) {
            $this->render(new DoublePublicityListJsonView($doublePublicityList, $count, $scene));
            return true;
        }

        $this->render(new DoublePublicityListView($doublePublicityList, $count, $scene));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $doublePublicityData = $this->getRepository()
            ->scenario(BjSearchDataRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
       
        if ($doublePublicityData instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $this->render(new DoublePublicityDataView($doublePublicityData));
        return true;
    }
}
