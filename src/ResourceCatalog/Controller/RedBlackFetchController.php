<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\ResourceCatalog\View\Template\RedBlackDataView;
use Base\Package\ResourceCatalog\View\Template\RedBlackListView;
use Base\Package\ResourceCatalog\View\Json\RedBlackListJsonView;

class RedBlackFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, ResourceCatalogTrait;

    protected function filterAction() : bool
    {
        list($page, $size) = $this->getPageAndSize(SEARCH_LIST_SIZE);
        list($filter, $sort, $scene) = $this->filterFormatChange('RedBlack');

        $redBlackList = array();
        list($count, $redBlackList) = $this->getRepository()
            ->scenario(BjSearchDataRepository::LIST_MODEL_UN)->search($filter, $sort, $page, $size);
    
        if ($this->getRequest()->isAjax()) {
            $this->render(new RedBlackListJsonView($redBlackList, $count, $scene));
            return true;
        }

        $this->render(new RedBlackListView($redBlackList, $count, $scene));
        return true;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $redBlackData = $this->getRepository()
            ->scenario(BjSearchDataRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);
       
        if ($redBlackData instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new RedBlackDataView($redBlackData));
        return true;
    }
}
