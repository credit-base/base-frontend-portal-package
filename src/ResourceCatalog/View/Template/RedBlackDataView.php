<?php
namespace Base\Package\ResourceCatalog\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Base\Package\ResourceCatalog\View\ViewTrait;

class RedBlackDataView extends TemplateView implements IView
{
    use ViewTrait;

    public function display()
    {
        $data = $this->getSearchData();
        
        $this->getView()->display(
            'RedBlack/Show.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PUBLICITY'],
                'data' => $data
            ]
        );
    }
}
