<?php
namespace Base\Package\ResourceCatalog\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Base\Package\ResourceCatalog\View\ListViewTrait;

class DoublePublicityListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $list = $this->getDataList();
        
        $this->getView()->display(
            'DoublePublicity/List.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PUBLICITY'],
                'data' => $list
            ]
        );
    }
}
