<?php
namespace Base\Package\ResourceCatalog\View;

use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

trait ListViewTrait
{
    private $count;

    private $scene;
    
    private $searchDataList;

    private $translator;

    public function __construct(
        array $searchDataList,
        int $count,
        string $scene
    ) {
        parent::__construct();
        $this->count = $count;
        $this->scene = $scene;
        $this->searchDataList = $searchDataList;
        $this->translator = new BjSearchDataTranslator();
    }

    protected function getBjSearchDataList(): array
    {
        return $this->searchDataList;
    }

    protected function getCount(): int
    {
        return $this->count;
    }
    
    protected function getScene(): string
    {
        return $this->scene;
    }

    protected function getTranslator(): BjSearchDataTranslator
    {
        return $this->translator;
    }

    protected function getDataList(): array
    {
        $data = array();

        foreach ($this->getBjSearchDataList() as $bjSearchData) {
            $item = $this->getTranslator()->objectToArray(
                $bjSearchData,
                array(
                    'id',
                    'name',
                    'template' => []
                )
            );
            
            $item['template'] = $item['template']['name'];
            $data[] = $item;
        }

        $list = array(
            'list' => $data,
            'total' => $this->getCount(),
            'scene' => $this->getScene()
        );

        return $list;
    }
}
