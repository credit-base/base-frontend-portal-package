<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Base\Package\ResourceCatalog\View\ListViewTrait;

class RedBlackListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    public function display(): void
    {
        $redBlackList = $this->getDataList();

        $this->encode($redBlackList);
    }
}
