<?php
namespace Base\Package\ResourceCatalog\View;

use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

trait ViewTrait
{
    use DataFormatTrait;

    private $searchData;

    private $translator;

    public function __construct($searchData)
    {
        parent::__construct();
        $this->searchData = $searchData;
        $this->translator = new BjSearchDataTranslator();
    }

    protected function getBjSearchData()
    {
        return $this->searchData;
    }

    protected function getTranslator(): BjSearchDataTranslator
    {
        return $this->translator;
    }

    protected function getSearchData():array
    {
        $data = array();

        $data = $this->getTranslator()->objectToArray(
            $this->getBjSearchData()
        );
     
        if (!empty($data['itemsData']) && !empty($data['template'])) {
            $data['itemsData'] = $this->itemsRelationTemplate($data['itemsData'], $data['template']);
            unset($data['template']);
        }
       
        return $data;
    }
}
