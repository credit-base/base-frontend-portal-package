<?php
namespace Base\Package\ResourceCatalog\View;

use Base\Package\Common\Utils\Mask;

use Base\Sdk\Template\Model\Template;

trait DataFormatTrait
{
    protected function itemsRelationTemplate(array $itemsData, array $template):array
    {
        $resultList = [];
        $templateItems = $this->getTemplateIdentify($template['items']);
        foreach ($itemsData['data'] as $key => $value) {
            foreach ($templateItems as $identify => $val) {
                if ($key == $identify) {
                    if (marmot_decode($val['isMasked']['id']) == Template::IS_MASKED['SHI']) {
                        if (!empty($val['maskRule'])) {
                            $value =  Mask::substrReplaceByMiddle(
                                $value,
                                $val['maskRule'][0],
                                $val['maskRule'][1]
                            );
                        }
                    }
                    $resultList[$key] = [
                        'name' => $val['name'],
                        'value' => $value,
                        'dimension' => $val['dimension'],
                    ];
                }
            }
        }
       
        return array_values($resultList);
    }

    protected function getTemplateIdentify(array $items) : array
    {
        $list = [];
        foreach ($items as $val) {
            $list[$val['identify']]= $val;
        }
    
        return $list;
    }
}
