<?php
namespace Base\Package\Member\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\Member\Translator\MemberTranslator;

class VerifyUserNameJsonView extends JsonView implements IView
{
    private $member;

    private $translator;

    public function __construct($member)
    {
        $this->member = $member;
        $this->translator = new MemberTranslator();
        parent::__construct();
    }

    protected function getMember()
    {
        return $this->member;
    }

    protected function getTranslator() : MemberTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();
        
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getMember()
        );

        $this->encode($data);
    }
}
