<?php
namespace Base\Package\Member\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class UpdatePasswordView extends TemplateView implements IView
{
    public function display()
    {
        $this->getView()->display(
            'Member/UpdatePassword.tpl'
        );
    }
}
