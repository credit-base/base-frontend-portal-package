<?php
namespace Base\Package\Member\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\Member\Translator\MemberTranslator;

class EditView extends TemplateView implements IView
{
    private $member;

    private $translator;

    public function __construct($member)
    {
        parent::__construct();
        $this->member = $member;
        $this->translator = new MemberTranslator();
    }

    protected function getMember()
    {
        return $this->member;
    }

    protected function getTranslator() : MemberTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();
        
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getMember()
        );

        $this->getView()->display('Member/Edit.tpl', ['data'=>$data]);
    }
}
