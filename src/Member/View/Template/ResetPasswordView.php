<?php
namespace Base\Package\Member\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class ResetPasswordView extends TemplateView implements IView
{
    public function display()
    {
        $this->getView()->display(
            'Member/ResetPassword.tpl'
        );
    }
}
