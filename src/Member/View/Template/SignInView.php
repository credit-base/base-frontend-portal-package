<?php
namespace Base\Package\Member\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class SignInView extends TemplateView implements IView
{
    private $backUrl;

    public function __construct(string $backUrl)
    {
        parent::__construct();
        $this->backUrl = $backUrl;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->backUrl);
    }

    public function getBackUrl() : string
    {
        return $this->backUrl;
    }

    public function display()
    {
        $backUrl = $this->getBackUrl();
        $this->getView()->display(
            'Member/SignIn.tpl',
            ['backUrl'=>$backUrl]
        );
    }
}
