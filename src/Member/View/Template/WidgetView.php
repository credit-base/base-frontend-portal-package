<?php
namespace Base\Package\Member\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\Member\Model\Member;
use Sdk\Member\Translator\MemberTranslator;

class WidgetView extends TemplateView implements IView
{
    private $member;

    private $nav;

    private $translator;

    public function __construct($member, int $nav)
    {
        $this->member = $member;
        $this->nav = $nav;
        $this->translator = new MemberTranslator();
        parent::__construct();
    }

    protected function getMember()
    {
        return $this->member;
    }

    protected function getNav() : int
    {
        return $this->nav;
    }

    protected function getTranslator() : MemberTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();
        
        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getMember()
        );

        $this->getView()->display(
            'Member/WidgetMember.tpl',
            ['member'=>$data, 'nav'=>$this->getNav()]
        );
    }
}
