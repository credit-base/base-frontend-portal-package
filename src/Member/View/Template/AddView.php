<?php
namespace Base\Package\Member\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\Member\Translator\MemberTranslator;

class AddView extends TemplateView implements IView
{
    public function display()
    {
        $securityQuestionList =array();
        $securityQuestions = MemberTranslator::SECURITY_QUESTION_CN;

        foreach ($securityQuestions as $id => $securityQuestion) {
            $securityQuestionList[] = array(
                'id' => marmot_encode($id),
                'name' => $securityQuestion
            );
        }
        
        $this->getView()->display('Member/SignUp.tpl', ['securityQuestionList'=>$securityQuestionList]);
    }
}
