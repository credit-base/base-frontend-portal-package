<?php

namespace Base\Package\Member\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\GlobalCheckTrait;

use Base\Sdk\Member\Model\MemberFactory;
use Sdk\Member\Repository\MemberRepository;
use Base\Package\Member\View\Template\WidgetView;

class WidgetController extends Controller
{
    use MemberControllerTrait, GlobalCheckTrait, WebTrait;

    public function memberWidget($nav = 0)
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = Core::$container->get('member')->getId();

        $member = $this->getRepository()->scenario(MemberRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        $this->render(new WidgetView($member, $nav));
        return true;
    }
}
