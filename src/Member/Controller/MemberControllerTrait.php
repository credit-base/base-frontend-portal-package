<?php
namespace Base\Package\Member\Controller;

use Marmot\Framework\Classes\CommandBus;

use Sdk\User\WidgetRules\UserWidgetRules;

use Sdk\Member\WidgetRules\MemberWidgetRules;
use Sdk\Member\Repository\MemberRepository;
use Sdk\Member\CommandHandler\Member\MemberCommandHandlerFactory;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
trait MemberControllerTrait
{
    protected function getMemberWidgetRules() : MemberWidgetRules
    {
        return new MemberWidgetRules();
    }

    protected function getUserWidgetRules() : UserWidgetRules
    {
        return new UserWidgetRules();
    }

    protected function getRepository() : MemberRepository
    {
        return new MemberRepository();
    }

    protected function getCommandBus() : CommandBus
    {
        return new CommandBus(new MemberCommandHandlerFactory());
    }

    protected function validateAddScenario(
        $userName,
        $realName,
        $cardId,
        $cellphone,
        $email,
        $contactAddress,
        $securityAnswer,
        $password,
        $confirmPassword,
        $securityQuestion
    ) : bool {
        $userWidgetRule = $this->getUserWidgetRules();
        $memberWidgetRule = $this->getMemberWidgetRules();
        return $memberWidgetRule->userName($userName)
            && $userWidgetRule->realName($realName)
            && $userWidgetRule->cardId($cardId)
            && $userWidgetRule->cellphone($cellphone)
            && $memberWidgetRule->email($email)
            && $memberWidgetRule->contactAddress($contactAddress)
            && $memberWidgetRule->securityAnswer($securityAnswer)
            && $userWidgetRule->password($password)
            && $userWidgetRule->confirmPassword($password, $confirmPassword)
            && $memberWidgetRule->securityQuestion($securityQuestion);
    }

    protected function validateEditScenario(
        $gender
    ) : bool {
        return empty($gender) ? true : $this->getMemberWidgetRules()->gender($gender);
    }

    protected function validateSignInScenario(
        $userName,
        $password
    ) : bool {
        return $this->getMemberWidgetRules()->userName($userName)
            && $this->getUserWidgetRules()->password($password);
    }

    protected function validateResetPasswordScenario(
        $userName,
        $securityAnswer,
        $password,
        $confirmPassword
    ) : bool {
        $userWidgetRule = $this->getUserWidgetRules();
        $memberWidgetRule = $this->getMemberWidgetRules();
        return $memberWidgetRule->userName($userName)
            && $memberWidgetRule->securityAnswer($securityAnswer)
            && $userWidgetRule->password($password)
            && $userWidgetRule->confirmPassword($password, $confirmPassword);
    }

    protected function validateUpdatePasswordScenario(
        $oldPassword,
        $password,
        $confirmPassword
    ) : bool {
        $userWidgetRule = $this->getUserWidgetRules();
        return $userWidgetRule->password($oldPassword, 'oldPassword')
            && $userWidgetRule->password($password)
            && $userWidgetRule->confirmPassword($password, $confirmPassword);
    }

    protected function validateValidateSecurityScenario(
        $securityAnswer
    ) : bool {
        return $this->getMemberWidgetRules()->securityAnswer($securityAnswer);
    }

    protected function validateVerifyUserNameScenario(
        $userName
    ) : bool {
        return $this->getMemberWidgetRules()->userName($userName);
    }
}
