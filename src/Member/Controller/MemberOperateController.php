<?php
namespace Base\Package\Member\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Server;
use Marmot\Framework\Classes\Filter;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\CryptojsTrait;
use Base\Package\Common\Controller\Traits\GlobalCheckTrait;

use Sdk\Member\Model\Member;
use Sdk\Member\Repository\MemberRepository;
use Base\Package\Member\View\Template\AddView;
use Base\Package\Member\View\Template\EditView;
use Base\Package\Member\View\Template\SignInView;
use Sdk\Member\Command\Member\AddMemberCommand;
use Sdk\Member\Command\Member\EditMemberCommand;
use Sdk\Member\Command\Member\SignInMemberCommand;
use Sdk\Member\Command\Member\SignOutMemberCommand;

class MemberOperateController extends Controller
{
    use WebTrait, MemberControllerTrait, CryptojsTrait, GlobalCheckTrait;
    
    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function add()
    {
        if (!Core::$container->get('member') instanceof INull) {
            $this->locationIndex();
        }
        
        if ($this->getRequest()->isGetMethod()) {
            return $this->addView();
        }

        return $this->addAction();
    }

    protected function addView() : bool
    {
        $this->render(new AddView());
        return true;
    }

    protected function addAction() : bool
    {
        $request = $this->getRequest();
        $userName = $request->post('userName', '');
        $realName = $request->post('realName', '');
        $cardId = $request->post('cardId', '');
        $cellphone = $request->post('cellphone', '');
        $email = $request->post('email', '');
        $contactAddress = $request->post('contactAddress', '');
        $securityAnswer = $request->post('securityAnswer', '');
        $securityQuestion = $request->post('securityQuestion', '');
        $securityQuestion = marmot_decode($securityQuestion);
        $password = $request->post('password', '');
        $confirmPassword = $request->post('confirmPassword', '');
        //密码加密
        $password = $this->decrypt($password);
        $password = is_null($password) ? '' : $password;
        //确认密码加密
        $confirmPassword = $this->decrypt($confirmPassword);
        $confirmPassword = is_null($confirmPassword) ? '' : $confirmPassword;

        //验证
        if ($this->validateAddScenario(
            $userName,
            $realName,
            $cardId,
            $cellphone,
            $email,
            $contactAddress,
            $securityAnswer,
            $password,
            $confirmPassword,
            $securityQuestion
        )) {
            //初始化命令
            $commandBus = $this->getCommandBus();
            $command = new AddMemberCommand(
                $userName,
                $realName,
                $cardId,
                $cellphone,
                $email,
                $contactAddress,
                $securityAnswer,
                $password,
                $securityQuestion
            );

            //执行命令
            if ($commandBus->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function edit()
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        $id = Core::$container->get('member')->getId();
        if ($this->getRequest()->isGetMethod()) {
            return $this->editView($id);
        }

        return $this->editAction($id);
    }

    protected function editView(int $id) : bool
    {
        $member = $this->getRepository()->scenario(MemberRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        if ($member instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            $this->displayError();
            return false;
        }
        
        $this->render(new EditView($member));
        return true;
    }

    protected function editAction(int $id)
    {
        $gender = $this->getRequest()->post('gender', '');
        $gender = marmot_decode($gender);
        
        if ($this->validateEditScenario($gender)) {
            $commandBus = $this->getCommandBus();

            $command = new EditMemberCommand(
                $gender,
                $id
            );

            if ($commandBus->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function signIn()
    {
        if (!Core::$container->get('member') instanceof INull) {
            $this->locationIndex();
        }

        if ($this->getRequest()->isGetMethod()) {
            return $this->signInView();
        }

        return $this->signInAction();
    }
    
    protected function signInView() : bool
    {
        $backUrl = $this->getBackUrl();

        $this->render(new SignInView($backUrl));
        return true;
    }
    
    /**
     * @todo
     * 验证路由
     * @codeCoverageIgnore
     */
    protected function getBackUrl()
    {
        $backUrl = Server::get('HTTP_REFERER', '');
        return Filter::htmlFilter($backUrl);
    }

    protected function signInAction()
    {
        $request = $this->getRequest();
        $userName = $request->post('userName', '');
        $password = $request->post('password', '');
        //密码加密
        $password = $this->decrypt($password);
        $password = is_null($password) ? '' : $password;

        if ($this->validateSignInScenario(
            $userName,
            $password
        )) {
            $commandBus = $this->getCommandBus();

            $command = new SignInMemberCommand(
                $userName,
                $password
            );

            if ($commandBus->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function signOut()
    {
        if ($this->getCommandBus()->send(new SignOutMemberCommand())) {
            if ($this->getRequest()->isAjax()) {
                $this->displaySuccess();
                return true;
            }

            $this->locationIndex();
        }

        $this->displayError();
        return false;
    }
    
    /**
     * @codeCoverageIgnore
     */
    protected function locationIndex()
    {
        header('Location: /');
    }
}
