<?php
namespace Base\Package\Member\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;
use Marmot\Framework\Controller\WebTrait;

use Base\Package\Common\Controller\Traits\CryptojsTrait;
use Base\Package\Common\Controller\Traits\GlobalCheckTrait;

use Sdk\Member\Repository\MemberRepository;
use Base\Package\Member\View\Template\ResetPasswordView;
use Base\Package\Member\View\Template\UpdatePasswordView;
use Base\Package\Member\View\Json\VerifyUserNameJsonView;
use Sdk\Member\Command\Member\ResetPasswordMemberCommand;
use Sdk\Member\Command\Member\UpdatePasswordMemberCommand;
use Sdk\Member\Command\Member\ValidateSecurityMemberCommand;

class MemberPasswordController extends Controller
{
    const MEMBER_SEARCH_COUNT = 1;

    use WebTrait, MemberControllerTrait, CryptojsTrait, GlobalCheckTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function resetPassword()
    {
        if (!Core::$container->get('member') instanceof INull) {
            $this->locationIndex();
        }

        if ($this->getRequest()->isGetMethod()) {
            return $this->resetPasswordView();
        }

        return $this->resetPasswordAction();
    }
    
    /**
     * @codeCoverageIgnore
     * @todo 确认下在cli模式下，是否可以测试 header 信息
     */
    protected function locationIndex()
    {
        header('Location: /');
    }
    
    protected function resetPasswordView() : bool
    {
        $this->render(new ResetPasswordView());
        return true;
    }

    protected function resetPasswordAction() : bool
    {
        $request = $this->getRequest();
        $userName = $request->post('userName', '');
        $securityAnswer = $request->post('securityAnswer', '');
        $password = $request->post('password', '');
        $confirmPassword = $request->post('confirmPassword', '');

        //密码加密
        $password = $this->decrypt($password);
        $password = is_null($password) ? '' : $password;
        //确认密码加密
        $confirmPassword = $this->decrypt($confirmPassword);
        $confirmPassword = is_null($confirmPassword) ? '' : $confirmPassword;

        if ($this->validateResetPasswordScenario(
            $userName,
            $securityAnswer,
            $password,
            $confirmPassword
        )) {
            $commandBus = $this->getCommandBus();

            $command = new ResetPasswordMemberCommand(
                $userName,
                $securityAnswer,
                $password
            );

            if ($commandBus->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function verifyUserName()
    {
        $userName = $this->getRequest()->post('userName', '');

        if (!$this->validateVerifyUserNameScenario($userName)) {
            $this->displayError();
            return false;
        }

        $filter['userName'] = $userName;
        $sort = ['-updateTime'];

        list($count, $memberList) = $this->getRepository()->scenario(
            MemberRepository::FETCH_ONE_MODEL_UN
        )->search($filter, $sort);

        if ($count != self::MEMBER_SEARCH_COUNT) {
            Core::setLastError(NAME_NOT_EXIT);
            $this->displayError();
            return false;
        }

        foreach ($memberList as $member) {
            $member = $member;
        }

        $this->render(new VerifyUserNameJsonView($member));
        return true;
    }

    public function validateSecurity(string $id)
    {
        $id = marmot_decode($id);

        $securityAnswer = $this->getRequest()->post('securityAnswer', '');

        if ($this->validateValidateSecurityScenario(
            $securityAnswer
        )) {
            $commandBus = $this->getCommandBus();

            $command = new ValidateSecurityMemberCommand(
                $securityAnswer,
                $id
            );

            if ($commandBus->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function updatePassword()
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }

        if ($this->getRequest()->isGetMethod()) {
            return $this->updatePasswordView();
        }

        return $this->updatePasswordAction();
    }

    protected function updatePasswordView() : bool
    {
        $this->render(new UpdatePasswordView());
        return true;
    }

    protected function updatePasswordAction() : bool
    {
        $id = Core::$container->get('member')->getId();
        
        $request = $this->getRequest();
        $oldPassword = $request->post('oldPassword', '');
        $password = $request->post('password', '');
        $confirmPassword = $request->post('confirmPassword', '');
        //旧加密
        $oldPassword = $this->decrypt($oldPassword);
        $oldPassword = is_null($oldPassword) ? '' : $oldPassword;
        //密码加密
        $password = $this->decrypt($password);
        $password = is_null($password) ? '' : $password;
        //确认密码加密
        $confirmPassword = $this->decrypt($confirmPassword);
        $confirmPassword = is_null($confirmPassword) ? '' : $confirmPassword;

        if ($this->validateUpdatePasswordScenario(
            $oldPassword,
            $password,
            $confirmPassword
        )) {
            $commandBus = $this->getCommandBus();

            $command = new UpdatePasswordMemberCommand(
                $password,
                $oldPassword,
                $id
            );

            if ($commandBus->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }
}
