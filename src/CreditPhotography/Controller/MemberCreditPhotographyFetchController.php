<?php
namespace Base\Package\CreditPhotography\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\CreditPhotography\View\Json\MemberCreditPhotographyListJsonView;
use Base\Package\CreditPhotography\View\Template\MemberCreditPhotographyListView;
use Base\Package\CreditPhotography\View\Template\MemberCreditPhotographyView;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Base\Package\Common\Controller\Traits\GlobalCheckTrait;
use Base\Package\Statistical\Controller\StatisticalControllerTrait;

class MemberCreditPhotographyFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, StatisticalControllerTrait, GlobalCheckTrait;

    const STATISTICAL = 'staticsCreditPhotography';

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CreditPhotographyRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : CreditPhotographyRepository
    {
        return $this->repository;
    }

    /**
    * @todo
    * @codeCoverageIgnore
    */
    protected function filterAction() : bool
    {
        if (!$this->globalCheck()) {
            $this->displayError();
            return false;
        }
        
        list($page, $size) = $this->getPageAndSize($size = 12);
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        list($count, $list) = $this->getRepository()
            ->scenario(CreditPhotographyRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
        
        $statisticalNumber = $this->statisticalNumber();
 
        if ($this->getRequest()->isAjax()) {
            $this->render(new MemberCreditPhotographyListJsonView($list, $count, $statisticalNumber));
            return true;
        }
    
        $this->render(new MemberCreditPhotographyListView($list, $count, $statisticalNumber));
        return true;
    }

    protected function filterFormatChange()
    {
        $request = $this->getRequest();
        
        $sort = '-updateTime';
        $applyStatus = $request->get('applyStatus', 'MQ');
        
        $filter = array();
        $filter['member'] = Core::$container->get('member')->getId();
        $filter['applyStatus'] = marmot_decode($applyStatus);
        $filter['status'] = CreditPhotography::STATUS['NOMAL'];

        return [$filter, [$sort]];
    }

    /**
    * @todo
    * @codeCoverageIgnore
    */
    private function statisticalNumber()
    {
        $filter['memberId'] = Core::$container->get('member')->getId();
        $statistical = $this->statistical(self::STATISTICAL, $filter);

        return $statistical;
    }

    protected function fetchOneAction(int $id) : bool
    {
        $creditPhotography = $this->getRepository()
            ->scenario(CreditPhotographyRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($creditPhotography instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new MemberCreditPhotographyView($creditPhotography));
        return true;
    }
}
