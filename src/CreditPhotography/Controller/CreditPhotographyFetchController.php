<?php
namespace Base\Package\CreditPhotography\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Base\Package\CreditPhotography\View\Json\ListJsonView;
use Base\Package\CreditPhotography\View\Template\ListView;
use Base\Package\CreditPhotography\View\Template\CreditPhotographyView;
use Base\Package\CreditPhotography\View\Template\DownloadPdfView;

use Base\Sdk\Common\Model\IApplyAble;
use Base\Sdk\CreditPhotography\Model\CreditPhotography;

class CreditPhotographyFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait;

    protected $repository;

    public function __construct()
    {
        parent::__construct();
        $this->repository = new CreditPhotographyRepository();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->repository);
    }

    protected function getRepository() : CreditPhotographyRepository
    {
        return $this->repository;
    }

    protected function filterAction() : bool
    {
        list($page, $size) = $this->getPageAndSize($size = 12);
        list($filter, $sort) = $this->filterFormatChange();

        $list = array();
        list($count, $list) = $this->getRepository()
            ->scenario(CreditPhotographyRepository::LIST_MODEL_UN)
            ->search($filter, $sort, $page, $size);
        
        if ($this->getRequest()->isAjax()) {
            $this->render(new ListJsonView($list, $count));
            return true;
        }
    
        $this->render(new ListView($list, $count));
        return true;
    }

    protected function filterFormatChange()
    {
        $sort = '-updateTime';
        
        $filter = array();
        $filter['applyStatus'] = IApplyAble::APPLY_STATUS['APPROVE'];
        $filter['status'] = CreditPhotography::STATUS['NOMAL'];

        return [$filter, [$sort]];
    }

    protected function fetchOneAction(int $id) : bool
    {
        $creditPhotography = $this->getRepository()
            ->scenario(CreditPhotographyRepository::FETCH_ONE_MODEL_UN)
            ->fetchOne($id);

        if ($creditPhotography instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
        
        $this->render(new CreditPhotographyView($creditPhotography));
        return true;
    }

    /**
     * @codeCoverageIgnore
     */
    public function downloadPdf():bool
    {
        $data = [];
        $this->render(new DownloadPdfView($data));
        return true;
    }
}
