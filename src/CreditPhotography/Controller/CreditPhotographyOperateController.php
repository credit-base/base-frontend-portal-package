<?php
namespace Base\Package\CreditPhotography\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;
use Marmot\Framework\Classes\CommandBus;

use Base\Package\Common\Controller\Traits\OperateControllerTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

use Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules;
use Sdk\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;
use Sdk\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand;
use Sdk\CreditPhotography\CommandHandler\CreditPhotography\CreditPhotographyCommandHandlerFactory;

use Base\Package\CreditPhotography\View\Template\AddView;

class CreditPhotographyOperateController extends Controller implements IOperateAbleController
{
    use WebTrait, OperateControllerTrait;
    
    public function __construct()
    {
        parent::__construct();
        $this->commandBus = new CommandBus(new CreditPhotographyCommandHandlerFactory());
        $this->creditPhotographyWidgetRule = new CreditPhotographyWidgetRules();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->commandBus);
        unset($this->creditPhotographyWidgetRule);
    }

    protected function getCommandBus() : CommandBus
    {
        return $this->commandBus;
    }

    protected function getCreditPhotographyWidgetRule() : CreditPhotographyWidgetRules
    {
        return $this->creditPhotographyWidgetRule;
    }

    protected function addView()
    {
        $this->render(new AddView());
        return true;
    }

    protected function addAction()
    {
        $description = $this->getRequest()->post('description', '');
        $attachments = $this->getRequest()->post('attachments', array());

        if ($this->validateAddScenario(
            $description,
            $attachments
        )) {
            $command = new AddCreditPhotographyCommand(
                $description,
                $attachments
            );

            if ($this->getCommandBus()->send($command)) {
                $this->displaySuccess();
                return true;
            }
        }

        $this->displayError();
        return false;
    }

    public function delete($id)
    {
        $id = marmot_decode($id);
        $command = new DeleteCreditPhotographyCommand($id);

        if ($this->getCommandBus()->send($command)) {
            $this->displaySuccess();
            return true;
        }
        
        $this->displayError();
        return false;
    }

    public function editView(int $id)
    {
        unset($id);
        return false;
    }

    public function editAction(int $id)
    {
        unset($id);
        return false;
    }

    protected function validateAddScenario(
        $description,
        $attachments
    ) {
        return $this->getCreditPhotographyWidgetRule()->description($description)
            && $this->getCreditPhotographyWidgetRule()->attachments($attachments);
    }
}
