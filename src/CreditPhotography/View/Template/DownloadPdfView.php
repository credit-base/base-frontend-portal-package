<?php
// phpcs：ignoreFile
// @codingStandardsIgnoreFile
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

/**
*@todo 有mock数据，单元测试和md检测修改？？ 
* 屏蔽类中所有PMD警告
* @codeCoverageIgnore
* @SuppressWarnings(PHPMD)
*/
class DownloadPdfView extends TemplateView implements IView
{
    private $data;

    public function __construct($data = array())
    {
        parent::__construct();
        $this->data = $data;
    }

    protected function getData()
    {
        return $this->data;
    }

    public function display()
    {
        $data = '{
            "enterprise": {
                "id": "MywuNjUwMw",
                "name": "润合建筑装饰工程有限公司",
                "unifiedSocialCreditCode": "91140100583314510M",
                "address": "华德中心广场C座1单元1801室",
                "registrationCapital": "6000.000000",
                "businessTermTo": "2031-10-11",
                "businessScope": "建设工程",
                "registrationAuthority": "市场监督管理局",
                "principal": "段鹏强",
                "category": 0,
                "registrationStatus": "在业",
                "createTime": 1596614949,
                "updateTime": 1596614949,
                "status": 0,
                "statusTime": 0,
                "industryCategory": "",
                "industryCode": 4350,
                "enterpriseTypeCode": "",
                "enterpriseType": "有限责任公司(自然人投资或控股)",
                "currency": "",
                "establishedDate": "2011-10-12 00:00:00",
                "approveDate": "2020-07-22 10:17:46",
                "businessStatus": ""
            },
            "code": "20210723154327841365",
            "date": "2021年07月23日   15:43:34",
            "publicUnit": "市社会信用体系建设工作领导小组办公室",
            "resourceCatalogData": {
                "licensing": [],
                "sanction": [],
                "red": [],
                "black": [],
                "ycjyml": [],
                "other": []
            },
            "qrcode": "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUAAAAFACAIAAABC8jL9AAAf4klEQVR4nO3de5QcZZ0+8Ofbc5/JJITcJ2QIgcQNGFgCmBhQllUDLBIuB7OKHCUICytyObK4gPyAwCJy2dWjB2WP67KCCnp2RXZBMME1REQJBEHWJGpIIJMht7llbt093V3f3x/Vl+rqnp6e6uruemeez8nJ6bzpqnq7up6ut+p9q0pUFURkplC1K0BE3jHARAZjgIkMxgATGYwBJjIYA0xkMAaYyGAMMJHBGGAigzHARAarHfMdIlKBepTPaGNFC3+uio0wDUg1CvC9hp63qEouKyDG/MjcAxMZjAEmMhgDTGQwBpjIYAwwkcEYYCKDMcBEBmOAiQw29kAOKlI5xgwUmKfRA1R8n2cQRrxURakBDsKKM320DVXLBNh62YQmMhgDTGQwBpjIYAwwkcEYYCKDMcBEBmOAiQxWroEc5eibrWSvXfDHDASk9zsg1fCXQVsvR2L5psA3VOG70gTkJ8YboytfeWxCExmMASYyGANMZDAGmMhgDDCRwRhgIoMxwEQGYz/wuAV/jIcHwRmP4a0m5q75EjHANDZvt++p5AwnLTahiQzGABMZjAEmMhgDTGQwBpjIYAwwkcEYYCKDsR84uDx3igZ/qAn7e/0yeQPs+QYaARGEsRDefg4m8O1EKo9NaCKDMcBEBmOAiQzGABMZjAEmMhgDTGQwBpjIYJO3H9iIzt4CvNXf96l8747mHTnGpVwBnrQrNC9va8P3Z7WUY6oJyaCtl01oIoMxwEQGY4CJDMYAExmMASYyGANMZDAGmMhgpfYDT8JOQg8q3AEbkDEewWd05W2TdyRWYcEf1VDJ0RrBn2rSYhOayGAMMJHBGGAigzHARAZjgIkMxgATGYwBJjIY+4H9NFFHULAPNrDGDrBBdycYlwp/riCMhfA2idHpnahbbxqb0EQGY4CJDMYAExmMASYyGANMZDAGmMhgDDCRwcbuB/bcDeihC64cXY4B6QmchOMuAlKN0RhxX4Ext16OxPJTxb714Iy7CMJHnszYhCYyGANMZDAGmMhgDDCRwRhgIoMxwEQGY4CJDFbGfuACPYGV7NPz1iEZkF7HgKzDAnzv7/XwkX2/vcGY8wyOUgPsbz++7xtlQIYTTMJxFwWWVYApsQkONqGJDMYAExmMASYyGANMZDAGmMhgDDCRwRhgIoOV2g/s+2NBAtJ5GJB+bN+xo3WCKenRKhPjpiS5KllD3zMfnEEjHgTnF7CSm00pn5pNaCKDMcBEBmOAiQzGABMZjAEmMhgDTGQwBpjIYFJKH1RAenSD0x1dyRoGpJvdm4B3zgdHQB+tEvD7eARqcVSMgPycVb4abEITGYwBJjIYA0xkMAaYyGAMMJHBGGAigzHARAarTj+wv91i5Xiyhu/VKMeE5gr+I2O8qfxXWVKAjX7qTECGfwRkpEEBvv8+VnK4TnCewlWmDYBNaCKDMcBEBmOAiQxW0tVIHhfp9837jDiJlVsNVS3x+CfvHHwutCz96lf1rbdc71QA2e+UH/6wyGoH5PvyXSWvOUvjHrhqxvXN5f0iRfL8/o5WWGQF3EHt7cX+/bnvlMCcjJzkqtONRPaOLp209Ot0KuwS+5/OvWJuoXPC0Qq9V7SrSyMRZM9B7D1wNqa5Khjg6rCjldUEzUlg3gynC53/m35DMYVpxRRqOIx4PP2jgvTPgYikXsDkdq/pSgpwhRtRRj/GxTWhZVkFWrDOuLoKi9wt5y0sZl/tLkz/QWZ92QfArkK/BL9lHqjhD8G6I0cBRtxfpvhqFK5Y3n2ma6edu/ces9DDJGhpkdpa938X/0nyMfrhL0HDJnR1FP4Jy3vsmnt2qviTWK5y5yH3GIWzZmlTk/b3p39UoCqA/cJVSJXHAFfHaMelzjc4/+k6xeUsdJUXKHTtbIsqnDYNixbh4MFkm9luxtvnsVIZRsEPQmXFbqSqGS2Nef85WuvVVkyhPcP0bJ2nwQsXYtUqqauzmwTJv1VVVewzz6nC8Xx08g0DXH15965F7tPytpZHa0I7Z5t7kmy0QrzvfVi6NHM2SwTpHwjXWS6qOAa4OkY7BnbuDO2/c/fMzsL00fKYhZ5JaysuvhhNTVmLd87Xj6WQNwxwdeS2n52hGK2HNnfCvHMrUOjRccfhssvQ0GDPVFKdy3a1eO64ingSqzqcEc095yyO0Rqu93sbkuE8uHUqtlBEPvQhjUbx9NPo67Nn5zywZoCrZewAl+Pn1fd5+jtDz3MrfsLcsRmFj4Hzptc1tKNAYe60SO32iy2sqZHVq3XePDz5JPbsgSpSHUg8AK6iUvfAvg/J8LAsc1tw6aSNtrd07pzzfsziT2I5Z+6xMBTCiSfKCSfoSy/Jli26c6cMDFQyw942AH+nKodSljX25YS+f87gBLiKvwiWZSFfPtOvR9uv5n3n2IWHD8lT/6wzj8KKC2TWgtShtiV/3qq73oRakr4+QSACtQCoxCO6dBUWLUeoxj3MIxrF4CAOHUJfHxIJNDfL8uWlr5ZKRjH4G3ZRC2WAfZyh50W7DoDtEoHqQC+iQ0DyWh9XdbMKRTB1Juoac4+oYSXwyJfx5AOoqcUxy/A3V2D1pdJ6hB7owFWno7sz885ayFRoDTACAFILzDwK923CvGPH1Vz3jAEeL57Eqo7cjd4VDADo2CEPXaqd2yUuUCgyl+wlx0Klp60Famv03M/Lp+7ShmbnDNUed9H2Fyrt6NiDPb/Di9fhsUdx4z04ql1/tw8JRx1mQ1shcWg8VbR/L3r26dxFRTa8qcIY4OpwHfHmD/Nzj+LPbwqgh6HdQPYeOPO6DpgLqYM8/Qg+dpUctcQ9q9o6XPBZ+asL8Oi/6eP/gW3b0bMNnx3Ah5bI+kf0xRdhWQAQj+Hg8zIyCACtCxAZ0mgPAFjuE+au14xxFTHAVTZafy8ANLYiCgCwpgAzR52FxhA7oFYcdTXp2bn6jSQU0iOmyw034cJL8F//hVmzcN4aqa/Xz10Zunxdcjbbt+DLm7RvEAAuvhqbntIdPQDEyswwt6pMb3UxwNUx2klm51GlSm0ywGedI+vuHXVe+3bha1fh0F401uQdmSNWAq8/Lz3v4Yy/xTEL5eabkwtVlYEe/PJ7uvJimX00tm5CVw8soHkKVn4cL/xURuy6AtFh+fMWLFmhjmPsdMOBMa4iBrhq8g7kEOc45DgQBgA0TcXixbkHycmSOksTdQgD9YCVNXP7t0A3P4UHLkdkGO1fwwVfwMcuRet0EdH4CP5jPZ5+GJv+Bw9txH//AMOWArLqYzhyLqJABAAQDus3r8fz38W6f8Lam1FXn54zc1t1Jg2llFFUu14eOQdOukYWJ1/FgGFgGBjJOmZG9iE0FIgAw0A4eWScGh+VeqfWofE43S/6yg6984vW2tP1+Z9oLIaXN+m//5t21WHFJ3TH73X7do1A4nU443zUNWAEiEAjgNThiIXoDuHhB/DsE+lhn66aj/btjEvhNRaEqTxX3tuyxpx27ABrQZ4rPd7FlVJJfyvvSw2dX4/rC8t8bTHoMHQYGs3MR7L32yKiVuptw8k9sGs+8ldr8I2n5ep7Ub8QexP66+36i/9F1yF96CG8Hcb7z8Hqtfqzn2BIMQytP0JWrRaIRqFhIAyEarH2C3rqRbprQO+7V3f+0cdtd8wVVeKXFZxllQmb0NWh+TpU3f+MQYcAwD4Wzd2wxD5NZQFh6BBQD9F87xSRBQtx46248FN4/HF0dMg/3oWWFqz6a2zbJbfcA0vxwoZkg/nkD2PufO3vyzShLcjUI7D+G/r622g/BkfO1OxxY36uFxonBrg6JOeANo84xD4G3rMbv/oRUn2/7v1dRyf6hiQMNLmHerj2jbJwoX75ywJIKAQAX/oSbroJtbXY/Eu88joSQEODfHwtAInHdETTAQYgbW14+bcIhaSuzrUI5cFw9TDA1Sf5LglSVe3tsxvP8uqLevevkYqn+63DioMxjQLDcUlYcIRKXee6+w7Kn7bgpR/h5NX48Celtl5FEI/rj3+McAIAFi3Cyafqb57B99djYAcEiDqOq+vrXUHV7ANyqjwGuMryZiAZudiIWFAAhy3948ios4gDUYgF1NYhFHLOITkfK4Geg9j4JDb9WN/+HUai2PRzHHMqFi4FgK1b8dP/RgwIhfChszBjBv7fndjxOmqBduA9QMd9yQRVDANcNZrTleoKs1x7o/5pt+zZg+lHjHG5z3FxDAzIF66VBQvce10AO3foleeh893km4+YLmddjOapAASw7rtP39uH5mb5/Oflllug0I5B7EvNuRnQEYyym+W+t+oY4OrQVDeMZK6KT75Ov5Cjj8YTT2BkBLVjfU2qSCRk6lSEQsi+KY+I6Iw5mH8K3tyHk5fLhRfhok+gbT5q60QE8TgWLAjdcTvWfBwnLkdtLaDy3Wf0/q/gpz/Vvj6ZMwfz2nNPsOWedfN7DVFRAvd0wtHq420qbypQwzFP/OR9g+fTRdrZiT/+ESecILNnI/sEsvTtx2O3Yd9OXP4g3rciWctIBJs26fe+J2vX4vzz7V+QwgfAfu2Nva3e4C/LmzE37JICzHXqNK4aqmM/mfdErvYfkv5upC4tGnU+9qLT/26ehpZpo51b0oEeiQwi+1S2vvqsfPs6WAlddZFc8SBq65xv0FCNHDEbNXVw/Hy4upHc3delCf4GUEkBDbCHhQbn+/Ol8ulnI+U9+sUr/4MHP4XosEaAOIrVADTX4/wb5PKvquswWC089S08cjPiOSfDQpAaVQAWkMi3kt9/Bu54QmbNt/+Vt8LjahpUsglWyU3UmxKbljwGribX7jeTjR2vYHBIAPRDDxU7N3kfEI3Khn/Hmutlxvys7z4Wwy8369vRPFO1QGcAAMLQLs3z4ND+7bLnXcya72w1OD9CsfWjMmCAq8OZW/foSKjGEohBAY02ItxS7Ezj3QJoOCbhIdd+UuvqsfpSbHgNAwPJN6siPIjYiB4JTAUADAIHAAVapqEm80AzLD8Pxy5lT1IwMcBVk//Q145EHBqBAPLZq+Sa++G4BYfz3a4DYD2nWQHUQVO3as7MXETWXIS/OV9iseSbO3fink/hnT8gnryBDlqBU4C6elxzN879XHK0ViiE+nrNV9vRjt6pkhjgKstzLldVY0AECohVo42N6csFxDFVnmsI7HGXNckR0c53JmdeW6s1NQDQ34NvfUn/vE0AxJI/FloLCBAbkX+9A+3H62kfcVYJ2SPGnBXmeKwqMulywonElYSs62AUiEOiQBQ40C0DA8g55kz3GGdNGE3+Uc1c6OfsZ07GPhHHv67HS79ARBEBaqbYy5K6aWLVIwJ0H8a//AN2/p/zSqPM5KMUVmjFUTYGuPrckQAQ1+SlfMMxPXDAGeB0dJGzG9QwNAxEMhcVZrKdGVZp6XM/1u99C/0xCQPNc7Dm79S+7vf4VVh5voSBYeCNN/HQLdrfV+iHxm4RWJbG4zwYrhY2oasjt+80q9dnRDAEABiB1Ncj58gzdyoRsa891BBE3YesmdT96gW97ToMJBTAlFa5+7s41KVh+wC7UW56ULsO41cvAMDPn0fzDaF/flRTc8itp4bD6OqCqkyZghkzfF9LNKZg7YFldNWuWkZZK2nPReNABIgATa1oa3OOncq7lGShvdOOJM9u5eYc2/9P7/gS9vViGLAacdU/4syzYUmy7R2DzJwtd3wdsxdhGDpo4Ykf6qOPIB7PXWjy0w4P2+11DA6W/tkzcx7/ug3CZlNgwyjfNmPSHjgI7TRva7z4mqv94Ow4rAgASG09YmHEwsml550kXW5fvlsPWDlRV0X/Yb31Zmx9EwqI4OK/lSuvQ6gGieQNA+wRI7J4Kdb/i171WTl8GIjr3XfKUcfoR1cjFJLcyx5rawVQkbFHa/vB3zEeE4NJAZ5Ick/nZv1vHBiBAHjxJ3rb68lRjci6mTtSuU0XJi8eHgEU7rB1dennr9HnNtr3iMXpq2T9fdI6FYAqYHct2UO+QiE57wLceIvefRciUe3swpWfk/sflEs/7f5REJGpUzUUkkQCLUV3VpOvGODqyLM3S5UDkLq65Anp3fswuC/9PtcE7ukt+6wSUFPjHN2FnTv1iiv0179OPtG3rU2++yjmzk3+r6buZalQhdiFX7gee/biWw+LAnv36dXXYGgYV1yhoZA4j7pFpLVVvTZMqHQMcNW4NvpM5AB8cKXMn4/ubsQE+4ueYYMFVSxbhrY2cTy1VF97DW+/Lc3NCIUwb57cey+OPS6z9Bkz0NyM4WHMny/19clZNTfjttv08GG8/DJiMTQ14fBhuG52mfoYSHUmlbpGaPyCdTFD8A9m/KqhRqPYuxfvvYfhYTQ0YN48tLejoQHJ0ZTQLVvQ2TnGdfwu8TgSCaxaJe3tWTWJRrFtGzo7UVeHRYuweDGcTYDBQTz2mL79ttxwgxx9dFbvsWWhsxORCKZNw+zZkj2Qw9XPXPzGMFE3m3L8hI1ZeQZ4fPyqoXXuufaj7jMRbWnBGWfI6tVyyima3hPmXLHkrRD5IudvYZEffLxvdgr4ZsMAB+WbKMC3AK9cmdU5lJo7mptx6qly9dU45hjXcXK6VZz3BFjhQmQHz3uhKvr6MDiIpiZMny61tYXPxuU1UTcbBjgo30QBvgX4gx90TuZuKi9YILfeKiedlL5JXYlcSfZY2NODn/0Mr76aDPDSpbjkEsyZA+6Bi6iGNwENcEB4+Ox+bSuJlSvt2WUuHbLnkO7UnTNH7rpL/vIv854f8lBY6nwGB3Hvvfj977Pe0N6OO++UuXOL+chpAQlwOaby3ZhbVLBGYgWfFjSuWUn6Kh8gOdo51ZZWVRw4oA89pPv2IT1uMbWI9IKcyx2z0Hk8PO7CRAJPPYW33tJ0zW179uD739donvsEFFB4Hfq1eoOmTB+KAa4Oye43stOWfp3M9u7dePJJOI5+nXPI+90XLnT+EIyvMBrFK6/AsiRVw8xbt23DoUOmp8tc7AeujnQc063b9H9JKsOaSOCZZ+Qzn8GMGbktt2SqAU0k1LJg9/pYFqJRHD6MSEQjEVjJ65JQX6+NjZg6FXZvcOqPhkLpw+/0jteZxmRhIoGuLjvTmc5qVYjo4KCEw+VaTTQWBrg6MqMjRdJhQDq99oGxCIaHsWEDPvlJOE5BSyKhe/bo7t145x3t6NB330VnJw4c0AMHpLdXR0acWc8cVNtCIUybhjlzMGsW2tqkvR3t7ViwAAsX4rjj0Nyc/xgvFML06ZK+HY+jFS3NzWhoMP1siLkY4CpLxtLRW2snNRlpVbz5Js47D93d2LVL33hDX3tN33gDPT0IhxGJIBZL7y7F8berMLNLtSz09qK3Fzt2JMtDIWlsRGOjtrZiyRKccop84ANYvBjt7WhtTU7V2IgVK7SjI7lLT85aoCrHH4/Zs3Nb+FQZPAtdHZluJGcfkut1IpF8LMOsWdizB+++i+Fh13zcO9jRC53/a5PRCmtrMX8+Fi7EaafJBRdg8WJMn46BAdxzD3butDOczOvcubj9dixciKpuDEachfa3GpnJGeCqSAbYPleUOvoEYDeVEQ5Ldzf6+3VwUFInt+BseOfMsMhCdVy9JMUUisjixTjzTLnwQixdimefxebNOjgoTU1YvhyXXioLF45vvGcZMMAeMcCeJVaudI1zhH2yt68P3d0YHER23TRfetORK74Q2SVFFgqAmhrMny+XXIJzz8WyZWhtRUMDUhcnVRcDXPAdAfiGTFH8l5RYuTITKsuCfW+anh6MjGRFt7UVLS2wb4s1fTpCIe3uRpE7z+zCjOnT0dqKzk4kEuP+hCI48kicfrpccQVWrcLMmaNtHpUcHWXEJmpegH3/DfOwrAIqWY3cZcVXrkx2BYfDum8fenslHs/TTv7MZ2TdOl23Dr29sn49Ghr0llswMmIfHifff9llcvbZeZa6d6/eemtW9ex5XnutXHSRfvrTOHAArn37VVfJ4sX5P9rGjbpxY+adDQ1YsUKuuw7nnivNzZrzGY0Y3ljJnXOZAsyz0FViH+ju36+HDtk/os7dZjJUzc2yZo0sX46nn0YigdmzMX26nHEGYjG89predJPdryOnnYazz8Zrr2H6dJx0Et56C93dWLwY0agdYAEwZw4+8IHkaeoVK7BkCc45Bz09AGTXLvzhD8nfgrVrccIJeOONrKrW12P5chkawsaNSNcwGsXmzdi8GatW4YtfxJo1qKuryIqjLAxwdcjevTh4UGOxvG3d5BHsihU45xzr8cflqKNkyRIMDWFoCPX1qqrbtyPueOrZO+/offdh+XI59lj90Y/w6qty7bV4//sz81yxQp54Inlo3dyMxkZ58EFYFhob8Z3vWDffnJnVzp2YNg1Ll2ZKtmzBO+/A2RfloC+/jDffxOmny89/7sN6oXFigKuksxOO0OZ5MXu23HabtLToD36gJ56I44/PTNvTg699LWtuS5fKd76DpiaZNg033ojhYcyYYbeQk2pq0NSkn/gEenvl8stx9tl6/fXo75d77kF9fVYbbmhI778fM2fKXXehp0e/8Q0MDck99wD5DqftwqEhbNjgbTVQiTgWumo038mn9N9YtgynnJL8j74+7N6d+dPRodmT4E9/0uuv14cf1v5+/fa39frr8eKLzkXAPtZ69VX85jfo6EAkgq1b8dvfoq/PXa1EAu3tcs01aGvDokVyzTV53uNcNFUV98DVoTn/lOyhVNixQ9evl69/HQCmTs06wmxqksZGRCKZmcyZI5ddhtmz0dgoZ52FpUuxZAly9pny7LOIxdDWhlmz5KmnEI/juOOwa5e7cnv24KWXcOyxqK+XFSu0oQHpqyzyfRADTgFPXNwDV40U7OzRzk77pBEAOfNMtLdj7lzMnYt583DSSVi2LD0HtLcDQEOD3TGbfJG6KU9SPK7Dw3jvPezdi4EBJBLYvx/vvYf+foyMwLmvrqtDPK4vvIDeXuzejXhczj/f/vlIn4J27v/dQzWpsrgHrqb0mefc8VLJF3YvwqxZOHRIv/IVAJg7Vx54AA0NmfEVCxfi+eetK6+Uj3xEHntM770XGzbIN7+Js87KtJ9feUUvuQS/+IWOjIRuvx2XXaZ///e6b5+cfDKGh7MqMHWqPPpo8nVrq3Z14cMfxpQpyP6tcbXhuROuljIGuJLd6wHpyh/HfaFSL5wZVlcrGqlrlQC0tcm6dQDQ0iL2A3vt969ejWOP1fvuyyy4pQU334yPfhTOy5IOHtTnnnO3qC1Lt251D/BasABbt2a9r74eixYhtURnJdVROHkEZGOzlRrgIHRqB+QuJ+OqvCWS2/50740tS/v7EY/jwAFMmYJ33wWAlhZtb0ckYr8f4TCeeQabNikgBw/i2WfR1SVXXonDh/U//zP/CK1oVAYH09cVZS3x4EHMmmU3qp1Vx/79OHzYxGNgbyNGCkwVkAEqaaWOxGKAi6xGboCT5fa0OWOYFZCmJixbhm3bcOSRUEVHBwDU1eGYY9DRgXA4GZ6ZM7WrK5NSEZk5EyLa0yOpvuKsQc5tbZgxAzt2aCwG19Lb26GqHR1ZhbW1OOooDA7aS3FVOP26JvsDBm1D94vvn6vKQykZ4CKrMVqAk/+bPYIyK28578k7STGFZRVigKsRYJ7Eqo705u77UHPfTdQoTgzsRiIyGANMZDAGmMhgDDCRwapzEitQXeGV4ftHLjDDSp5Y8vdzBacbwts6rPyGXa4Ae+4Kr+Sy/N3QK1mNSm4oAVm9AVGODbuUFcUmNJHBGGAigzHARAZjgIkMxgATGYwBJjIYA0xksIlwNZLn/jcPYyHK0QEbhGEtk7BH15sKbwA+XE5o+pcXkKuIqVoCsgGXaTtkE5rIYAwwkcEYYCKDMcBEBmOAiQzGABMZjAEmMthEGMhRGPt708ox4sXbVJW838XEHq4zEQJcyVtG+L4sDoFyqvDq9SZQQ4PYhCYyGANMZDAGmMhgDDCRwRhgIoMxwEQGY4CJDFbqA74LCFR32XgFpPIBf2iDZxP1c/kuiA/4NuWpMx4EJPbecKhJ6Sq/AbAJTWQwBpjIYAwwkcEYYCKDMcBEBmOAiQzGABMZbCJc0F8OAem59f2mFlRWld9sjHm0iu/V8H2EmbdJgj+0ywj+fq6A/HwXg01oIoMxwEQGY4CJDMYAExmMASYyGANMZDAGmMhgY/cDG9QnltdE7fk0/XupjAl/N4LJOxIrIIMrynF/En+3ywmwlfvL99tulLKG2YQmMhgDTGQwBpjIYAwwkcEYYCKDMcBEBmOAiQw2efuBK9nZ663bNjg19J3Rfcu+r6tS7rtSaoCD8E1MzjFJQVjzwVfhtVT5Zz6xCU1kMAaYyGAMMJHBGGAigzHARAZjgIkMxgATGaxcAznK0Tcb8J5Pzx/Z33VldK94QNahZ0F8tMrk5PuPRRCGWxXmrYbB/1y+4x05iMgfDDCRwRhgIoMxwEQGY4CJDMYAExmMASYyGPuBxyc4Iw0C0tEakGpU0oS6I4e5yjGuy9+xEAVUcsyA52fQ+LusAoKzej1Ug3fkIJq8GGAigzHARAZjgIkMxgATGYwBJjIYA0xksMnbD1xAhUdrBGEshO9dqcEXhNVeOgZ43AJyD4qAVINKVOKPI5vQRAZjgIkMxgATGYwBJjIYA0xkMAaYyGAMMJHBJm8/8OTsLw3CUJNJOGgEXtdh1e7IYfrXEJBHq1TyBhqTje9fcVVWL5vQRAZjgIkMxgATGYwBJjIYA0xkMAaYyGAMMJHBSu0HZtdikSq2oibqN1LhZ9OYcr+EyTsSy3eTcwRFxZ6fYoTKP8aFTWgigzHARAZjgIkMxgATGYwBJjIYA0xkMAaYyGAMMJHBZAL3qhNNeNwDExmMASYyGANMZDAGmMhgDDCRwRhgIoMxwEQGY4CJDMYAExmMASYy2P8H7s1ZhdktF+AAAAAASUVORK5CYII=",
            "creditPromises": [],
            "creditPromiseCount": 0,
            "contractPerformances": [],
            "contractPerformanceCount": 0
        }';

        $data = json_decode($data, true);
        $this->getView()->display(
            'CreditPhotography/DownloadPdf.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PHOTOGRAPHY'],
                'enterprise'=> $data['enterprise'],
                'code'=> $data['code'],
                'date' => $data['date'],
                'publicUnit' => $data['publicUnit'],
                'resourceCatalogData' => $data['resourceCatalogData'],
                'qrcode' => $data['qrcode'],
                'creditPromises' => $data['creditPromises'],
                'creditPromiseCount' => $data['creditPromiseCount'],
                'contractPerformances' => $data['contractPerformances'],
                'contractPerformanceCount' => $data['contractPerformanceCount']
            ]
        );
    }
}
