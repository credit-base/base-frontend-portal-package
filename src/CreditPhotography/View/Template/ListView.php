<?php
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Base\Package\CreditPhotography\View\ListViewTrait;

class ListView extends TemplateView implements IView
{
    use ListViewTrait;

    public function display()
    {
        $data = $this->getList();

        $list['data'] = $data;
        $list['total'] = $this->getCount();
        
        $this->getView()->display(
            'CreditPhotography/List.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PHOTOGRAPHY'],
                'list' => $list
            ]
        );
    }
}
