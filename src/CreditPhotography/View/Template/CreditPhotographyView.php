<?php
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Base\Package\CreditPhotography\View\ViewTrait;

class CreditPhotographyView extends TemplateView implements IView
{
    use ViewTrait;

    public function display()
    {
        $data = $this->getData();
        
        $this->getView()->display(
            'CreditPhotography/Show.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PHOTOGRAPHY'],
                'data' => $data
            ]
        );
    }
}
