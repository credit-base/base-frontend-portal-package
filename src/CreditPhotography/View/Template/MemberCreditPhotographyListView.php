<?php
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;


use Base\Package\CreditPhotography\View\ListViewTrait;

class MemberCreditPhotographyListView extends TemplateView implements IView
{
    use ListViewTrait;
    
    const STATISTICAL = 'staticsCreditPhotography';

    public function display()
    {
        $data = $this->getList();

        $statisticalNumber = $this->getStaticsNumByType(self::STATISTICAL);

        $list['data'] = $data;
        $list['total'] = $this->getCount();
        $list['statisticalNumber'] = $statisticalNumber;
        
        $this->getView()->display(
            'CreditPhotography/MyList.tpl',
            [
                'list' => $list
            ]
        );
    }
}
