<?php
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

class AddView extends TemplateView implements IView
{
    public function display()
    {
        $this->getView()->display(
            'CreditPhotography/Add.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PHOTOGRAPHY'],
            ]
        );
    }
}
