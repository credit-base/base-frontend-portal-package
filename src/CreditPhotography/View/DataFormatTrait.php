<?php
namespace Base\Package\CreditPhotography\View;

trait DataFormatTrait
{
    protected function getDataWithType(array $data):int
    {
        $type = 1;
        if (!$this->validateIdentify($data['attachments'][0]['identify'])) {
            $type = 2;
        }

        return $type;
    }

    protected function getDataFormat(array $list):array
    {
        foreach ($list as $key => $value) {
            $list[$key]['type'] = $this->getDataWithType($value);
        }

        return $list;
    }

    protected function validateIdentify(string $identify):bool
    {
        if (!empty($identify)) {
            if ($this->getCreditPhotographyWidgetRule()->isVideo($identify)) {
                return true;
            }
        }

        return false;
    }
}
