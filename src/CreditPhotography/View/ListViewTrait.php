<?php
namespace Base\Package\CreditPhotography\View;

use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;
use Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules;

use Base\Package\Statistical\View\StatisticalViewTrait;

trait ListViewTrait
{
    use DataFormatTrait, StatisticalViewTrait;

    private $count;

    private $creditPhotographyList;

    private $translator;

    private $creditPhotographyWidgetRule;

    private $statistical;

    public function __construct(
        array $creditPhotographyList,
        int $count,
        $statistical = array()
    ) {
        parent::__construct();
        $this->count = $count;
        $this->creditPhotographyList = $creditPhotographyList;
        $this->statistical = $statistical;
        $this->translator = new CreditPhotographyTranslator();
        $this->creditPhotographyWidgetRule = new CreditPhotographyWidgetRules();
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getCreditPhotographyList() : array
    {
        return $this->creditPhotographyList;
    }

    protected function getTranslator() : CreditPhotographyTranslator
    {
        return $this->translator;
    }

    protected function getCreditPhotographyWidgetRule() : CreditPhotographyWidgetRules
    {
        return $this->creditPhotographyWidgetRule;
    }

    protected function getStatistical()
    {
        return $this->statistical;
    }

    protected function getList() : array
    {
        $translator = $this->getTranslator();
        
        $data = array();
        foreach ($this->getCreditPhotographyList() as $creditPhotography) {
            $data[] = $translator->objectToArray(
                $creditPhotography,
                array(
                    'id',
                    'description',
                    'attachments',
                    'member'=>['id','realName'],
                    'createTime',
                    'updateTime'
                )
            );
        }

        $data = $this->getDataFormat($data);

        return $data;
    }

    protected function getStaticsNumByType(string $type = 'staticsCreditPhotography')
    {
        $statistical = $this->getStatistical();
        $statisticalNumber = [];
        
        if (!empty($statistical)) {
            $statisticalNumber = $this->statisticalArray(
                $type,
                $statistical
            );
        }

        return $statisticalNumber;
    }
}
