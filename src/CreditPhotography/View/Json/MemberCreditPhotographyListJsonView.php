<?php
namespace Base\Package\CreditPhotography\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Base\Package\CreditPhotography\View\ListViewTrait;

class MemberCreditPhotographyListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    const STATISTICAL = 'staticsCreditPhotography';
    
    public function display(): void
    {
        $data = $this->getList();

        $statisticalNumber = $this->getStaticsNumByType(self::STATISTICAL);

        $dataList = array(
            'list' => $data,
            'total' => $this->getCount(),
            'statisticalNumber' => $statisticalNumber
        );

        $this->encode($dataList);
    }
}
