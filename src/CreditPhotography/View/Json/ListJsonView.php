<?php
namespace Base\Package\CreditPhotography\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Base\Package\CreditPhotography\View\ListViewTrait;

class ListJsonView extends JsonView implements IView
{
    use ListViewTrait;

    public function display(): void
    {
        $data = $this->getList();

        $dataList = array(
            'total' => $this->getCount(),
            'list' => $data,
        );

        $this->encode($dataList);
    }
}
