<?php
namespace Base\Package\CreditPhotography\View;

use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;
use Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules;

trait ViewTrait
{
    use DataFormatTrait;
    
    private $creditPhotography;

    private $translator;

    private $creditPhotographyWidgetRule;

    public function __construct($creditPhotography)
    {
        parent::__construct();
        $this->creditPhotography = $creditPhotography;
        $this->translator = new CreditPhotographyTranslator();
        $this->creditPhotographyWidgetRule = new CreditPhotographyWidgetRules();
    }

    protected function getCreditPhotography()
    {
        return $this->creditPhotography;
    }

    protected function getTranslator(): CreditPhotographyTranslator
    {
        return $this->translator;
    }

    protected function getCreditPhotographyWidgetRule() : CreditPhotographyWidgetRules
    {
        return $this->creditPhotographyWidgetRule;
    }

    protected function getData():array
    {
        $data = $this->getTranslator()->objectToArray(
            $this->getCreditPhotography()
        );

        $data['type'] = $this->getDataWithType($data);
       
        return $data;
    }
}
