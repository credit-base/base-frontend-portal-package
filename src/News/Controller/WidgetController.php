<?php
namespace Base\Package\News\Controller;

use Marmot\Framework\Classes\Controller;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\News\Model\News;
use Sdk\News\Repository\NewsRepository;
use Base\Package\News\View\Template\WidgetView;

class WidgetController extends Controller
{
    use NewsControllerTrait;

    public function newsWidget()
    {
        list($filter, $sort) = $this->filterFormatChange();

        $newsList = array();

        list($count, $newsList) = $this->getRepository()->scenario(
            NewsRepository::LIST_MODEL_UN
        )->search($filter, $sort, DEFAULT_PAGE, WIDGET_SIZE);

        unset($count);

        $this->render(new WidgetView($newsList));
        return true;
    }

    protected function filterFormatChange()
    {
        $sort = ['-updateTime'];
        $filter = array();

        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        return [$filter, $sort];
    }
}
