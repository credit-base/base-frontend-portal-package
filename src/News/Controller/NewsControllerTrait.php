<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Sdk\News\Model\News;
use Sdk\News\Repository\NewsRepository;
use Sdk\News\WidgetRules\NewsWidgetRules;

trait NewsControllerTrait
{
    protected $news;

    private $repository;

    private $newsWidgetRules;

    protected $concurrentAdapter;

    public function __construct()
    {
        parent::__construct();
        $this->news = new News();
        $this->repository = new NewsRepository();
        $this->newsWidgetRules = new NewsWidgetRules();
        $this->concurrentAdapter = new ConcurrentAdapter();
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this->news);
        unset($this->repository);
        unset($this->newsWidgetRules);
        unset($this->concurrentAdapter);
    }

    protected function getNews() : News
    {
        return $this->news;
    }

    protected function getRepository() : NewsRepository
    {
        return $this->repository;
    }

    protected function getNewsWidgetRules() : NewsWidgetRules
    {
        return $this->newsWidgetRules;
    }

    protected function getConcurrentAdapter() : ConcurrentAdapter
    {
        return $this->concurrentAdapter;
    }

    protected function validateFilterScenario(
        $type
    ) : bool {
        return $this->getNewsWidgetRules()->newsType($type);
    }

    protected function validateIndexScenario(
        $category
    ) : bool {
        if (!array_key_exists($category, NEWS_CATEGORY_INDEX)) {
            Core::setLastError(NEWS_TYPE_NOT_EXIST);
            return false;
        }

        return true;
    }
}
