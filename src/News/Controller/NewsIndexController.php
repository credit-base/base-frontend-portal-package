<?php
namespace Base\Package\News\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Sdk\News\Model\News;
use Sdk\News\Repository\NewsRepository;
use Base\Package\News\Controller\NewsControllerTrait;
use Base\Package\News\View\Template\IndexView;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Package\Common\Controller\Interfaces\IIndexController;

class NewsIndexController extends Controller implements IIndexController
{
    use WebTrait, NewsControllerTrait;

    public function index()
    {
        $category = $this->getRequest()->get('category', '');
        $category = (int)marmot_decode($category);

        if (!$this->validateIndexScenario($category)) {
            $this->displayError();
            return false;
        };

        $newsTypeList = NEWS_CATEGORY_INDEX[$category];

        $repository = $this->getRepository()->scenario(NewsRepository::LIST_MODEL_UN);

        list($bannerFilter, $newsFilter, $sort) = $this->filterFormatChange($newsTypeList);
        $bannerFilter['category'] = $category;

        $this->getConcurrentAdapter()->addPromise(
            'banners',
            $repository->searchAsync($bannerFilter, $sort, DEFAULT_PAGE, BANNER_SIZE),
            $repository->getAdapter()
        );

        foreach ($newsTypeList as $key => $value) {
            unset($value);
            $this->getConcurrentAdapter()->addPromise(
                $key,
                $repository->searchAsync($newsFilter[$key], $sort, DEFAULT_PAGE, INDEX_LIST_SIZE),
                $repository->getAdapter()
            );
        }

        $data = $this->getConcurrentAdapter()->run();

        $this->render(new IndexView($data, $category));
        return true;
    }

    protected function filterFormatChange($newsTypeList)
    {
        $sort = ['-stick','-updateTime'];

        $bannerFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $bannerFilter['status'] = IEnableAble::STATUS['ENABLED'];
        $bannerFilter['bannerStatus'] = News::BANNER_STATUS['ENABLED'];

        $newsFilter = array();

        foreach ($newsTypeList as $key => $value) {
            $newsFilter[$key] = array(
                'newsType' => $value,
                'dimension' => News::DIMENSION['SOCIOLOGY'],
                'status' => IEnableAble::STATUS['ENABLED']
            );
        }

        return [
            $bannerFilter,
            $newsFilter,
            $sort
        ];
    }
}
