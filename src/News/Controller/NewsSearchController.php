<?php
namespace Base\Package\News\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\News\Model\News;
use Sdk\News\Repository\NewsRepository;
use Base\Package\News\View\Template\SearchView;
use Base\Package\News\View\Json\SearchJsonView;

class NewsSearchController extends Controller
{
    use WebTrait, NewsControllerTrait;

    public function search()
    {
        $page = $this->getRequest()->get('page', 1);
        $keyword = $this->getRequest()->get('keyword', '');

        list($filter, $sort) = $this->filterFormatChange($keyword);

        $count = 0;
        $newsList = array();

        list($count, $newsList) = $this->getRepository()->scenario(
            NewsRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, SEARCH_LIST_SIZE);
      
        if ($this->getRequest()->isAjax()) {
            $this->render(new SearchJsonView($newsList, $count, $keyword));
            return true;
        }

        $this->render(new SearchView($newsList, $count, $keyword));
        return true;
    }

    protected function filterFormatChange($keyword)
    {
        $sort = ['-stick','-updateTime'];

        $filter = array();

        $filter['title'] = $keyword;
        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        return [$filter, $sort];
    }
}
