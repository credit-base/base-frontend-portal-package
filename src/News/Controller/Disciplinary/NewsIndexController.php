<?php
namespace Base\Package\News\Controller\Disciplinary;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Sdk\News\Model\News;
use Sdk\News\Repository\NewsRepository;
use Base\Package\News\Controller\NewsControllerTrait;
use Base\Package\News\View\Template\Disciplinary\IndexView;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Package\Common\Controller\Interfaces\IIndexController;

class NewsIndexController extends Controller implements IIndexController
{
    use WebTrait, NewsControllerTrait;

    public function index()
    {
        $newsCategoryList = NEWS_DISCIPLINARY_INDEX;

        $newsRepository = $this->getRepository()->scenario(NewsRepository::LIST_MODEL_UN);

        list($bannerFilter, $newsFilter, $sort) = $this->filterFormatChange($newsCategoryList);

        $this->getConcurrentAdapter()->addPromise(
            'banners',
            $newsRepository->searchAsync($bannerFilter, $sort, DEFAULT_PAGE, BANNER_SIZE),
            $newsRepository->getAdapter()
        );

        foreach ($newsCategoryList as $key => $value) {
            unset($value);
            $this->getConcurrentAdapter()->addPromise(
                $key,
                $newsRepository->searchAsync($newsFilter[$key], $sort, DEFAULT_PAGE, NEWS_INDEX_SIZE),
                $newsRepository->getAdapter()
            );
        }

        $data = $this->getConcurrentAdapter()->run();

        $this->render(new IndexView($data));
        return true;
    }

    protected function filterFormatChange($newsCategoryList)
    {
        $sort = ['-stick','-updateTime'];

        $bannerFilter['parentCategory'] = NEWS_PARENT_CATEGORY['DISCIPLINARY'];
        $bannerFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $bannerFilter['status'] = IEnableAble::STATUS['ENABLED'];
        $bannerFilter['bannerStatus'] = News::BANNER_STATUS['ENABLED'];

        $newsFilter = array();

        foreach ($newsCategoryList as $key => $value) {
            $newsFilter[$key] = array(
                'category' => $value,
                'dimension' => News::DIMENSION['SOCIOLOGY'],
                'status' => IEnableAble::STATUS['ENABLED']
            );
        }

        return [
            $bannerFilter,
            $newsFilter,
            $sort
        ];
    }
}
