<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Controller\WebTrait;

use Base\Sdk\Common\Model\IEnableAble;
use Base\Package\Common\Controller\Traits\FetchControllerTrait;
use Base\Package\Common\Controller\Interfaces\IFetchAbleController;

use Sdk\News\Model\News;
use Base\Package\News\View\Template\NewsView;
use Base\Package\News\View\Template\ListView;
use Base\Package\News\View\Json\ListJsonView;
use Sdk\News\Repository\NewsRepository;

class NewsFetchController extends Controller implements IFetchAbleController
{
    use WebTrait, FetchControllerTrait, NewsControllerTrait;

    protected function filterAction()
    {
        $type = $this->getRequest()->get('type', '');
        $type = (int)marmot_decode($type);

        if (empty($type) || !$this->validateFilterScenario($type)) {
            return false;
        };

        list($page, $size) = $this->getPageAndSize();
        list($filter, $sort) = $this->filterFormatChange($type);

        $count = 0;
        $newsList = array();

        list($count, $newsList) = $this->getRepository()->scenario(
            NewsRepository::LIST_MODEL_UN
        )->search($filter, $sort, $page, $size);
      
        if ($this->getRequest()->isAjax()) {
            $this->render(new ListJsonView($newsList, $count, $type));
            return true;
        }

        $bannerList = array();

        if (!$this->getRequest()->isAjax()) {
            list($bannerFilter, $sort) = $this->filterFormatChange($type);

            $bannerFilter['bannerStatus'] = News::BANNER_STATUS['ENABLED'];

            list($bannerCount, $bannerList) = $this->getRepository()->scenario(
                NewsRepository::LIST_MODEL_UN
            )->search($bannerFilter, $sort, DEFAULT_PAGE, BANNER_SIZE);

            unset($bannerCount);
        }

        $this->render(new ListView($newsList, $count, $type, $bannerList));
        return true;
    }

    protected function filterFormatChange($type)
    {
        $sort = ['-stick','-updateTime'];

        $filter = array();

        $filter['newsType'] = $type;
        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        return [$filter, $sort];
    }

    protected function fetchOneAction(int $id)
    {
        $news = $this->getOne($id);

        if ($news) {
            $newsList = $this->relevantArticles($news->getNewsType());
            $this->render(new NewsView($news, $newsList));
            return true;
        }
    
        return false;
    }

    protected function getOne($id)
    {
        if (empty($id)) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        $news = $this->getRepository()->scenario(NewsRepository::FETCH_ONE_MODEL_UN)->fetchOne($id);

        if ($news instanceof INull) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }
      
        if ($news->getStatus() == IEnableAble::STATUS['DISABLED']) {
            Core::setLastError(RESOURCE_NOT_EXIST);
            return false;
        }

        return $news;
    }

    protected function relevantArticles($type)
    {
        list($filter, $sort) = $this->filterFormatChange($type);

        $newsList = array();

        list($count, $newsList) = $this->getRepository()->scenario(
            NewsRepository::LIST_MODEL_UN
        )->search($filter, $sort, DEFAULT_PAGE, NEWS_LIST_SIZE);

        unset($count);

        return $newsList;
    }
}
