<?php
namespace Base\Package\News\View\Template\Disciplinary;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\News\Translator\NewsTranslator;

class IndexView extends TemplateView implements IView
{
    private $data;

    private $translator;

    public function __construct(
        array $data
    ) {
        $this->data = $data;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getTranslator() : NewsTranslator
    {
        return $this->translator;
    }

    protected function getData() : array
    {
        return $this->data;
    }

    public function display()
    {
        $data = array();

        $newTranslator = $this->getTranslator();
        foreach ($this->getData() as $key => $val) {
            if (!empty($val[0])) {
                foreach ($val[1] as $news) {
                    $data[$key][] = $newTranslator->objectToArray(
                        $news,
                        array(
                            'id',
                            'title',
                            'description',
                            'bannerImage',
                            'newsType',
                            'updateTime',
                            'source'
                        )
                    );
                }
            }
        }

        $nav = NAV['NAV_DISCIPLINARY'];

        $this->getView()->display(
            'News/Disciplinary/Index.tpl',
            [
                'nav'=> $nav,
                'data' => $data
            ]
        );
    }
}
