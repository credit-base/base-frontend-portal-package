<?php
namespace Base\Package\News\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\News\Translator\NewsTranslator;

class SearchView extends TemplateView implements IView
{
    private $newsList;

    private $count;

    private $keyword;

    private $translator;

    public function __construct(array $newsList, int $count, string $keyword)
    {
        $this->newsList = $newsList;
        $this->count = $count;
        $this->keyword = $keyword;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getNewsList() : array
    {
        return $this->newsList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getKeyword() : string
    {
        return $this->keyword;
    }

    protected function getTranslator() : NewsTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();

        $translator = $this->getTranslator();

        foreach ($this->getNewsList() as $news) {
            $data[] = $translator->objectToArray(
                $news,
                array(
                    'id',
                    'title',
                    'description',
                    'source',
                    'updateTime',
                    'newsType'
                )
            );
        }

        $list['total'] = $this->getCount();
        $list['list'] = $data;
        $keyword = $this->getKeyword();
        $list['keyword'] = $keyword;
      
        $this->getView()->display(
            'News/Search.tpl',
            [
                'list' =>  $list,
                'keyword' => $keyword,
                'searchNav' => SEARCH_NAV['SEARCH_NAV_NEWS']
            ]
        );
    }
}
