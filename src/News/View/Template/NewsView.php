<?php
namespace Base\Package\News\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\News\Translator\NewsTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
* @codeCoverageIgnore
*/
class NewsView extends TemplateView implements IView
{
    const TYPE_LINK = '/news?type=%s';
    const CATEGORY_LINK = '/news/index?category=%s';
    const PARENT_CATEGORY_LINK = '/%s/index';

    const  NEWS_CATEGORY_LINK = array(
        NEWS_CATEGORY['SUPERVISION']=>'creditPublicities',
        NEWS_CATEGORY['TRUSTWORTHINESS_DISHONESTY']=>'creditPublicities'
    );
    const  NEWS_PARENT_CATEGORY_LINK = array(
        NEWS_PARENT_CATEGORY['CREDIT_DYNAMICS']=>'creditDynamics',
        NEWS_PARENT_CATEGORY['DISCIPLINARY']=>'disciplinaries',
        NEWS_CATEGORY['SUPERVISION']=>'creditPublicities',
        NEWS_CATEGORY['TRUSTWORTHINESS_DISHONESTY']=>'creditPublicities'
    );

    const NEWS_NAME = array(
        NEWS_CATEGORY['SUPERVISION']=>'信用公示',
        NEWS_CATEGORY['TRUSTWORTHINESS_DISHONESTY']=>'信用公示'
    );

    const NOT_LINK_NEWS_CATEGORY = array(
        NEWS_CATEGORY['DISCIPLINARY_MEMORANDUM'],
        NEWS_CATEGORY['DISCIPLINARY_DYNAMICS']
    );
    
    private $news;

    private $newsList;

    private $translator;

    public function __construct($news, array $newsList)
    {
        $this->news = $news;
        $this->newsList = $newsList;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getNews()
    {
        return $this->news;
    }

    protected function getNewsList() : array
    {
        return $this->newsList;
    }

    protected function getTranslator() : NewsTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();

        $translator = $this->getTranslator();
        $data = $translator->objectToArray(
            $this->getNews()
        );

        foreach ($this->getNewsList() as $news) {
            $relevantArticles[] = $translator->objectToArray(
                $news,
                array(
                    'id',
                    'title',
                    'updateTime',
                    'createTime',
                    'source',
                    'newsType'=>[],
                )
            );
        }

        $breadcrumb = $this->breadcrumbTrail($data);
        $nav = NAV_NEWS[marmot_decode($data['newsType']['id'])];

        $this->getView()->display('News/Show.tpl', [
            'data'=>$data,
            'breadcrumb'=>$breadcrumb,
            'relevantArticles'=>$relevantArticles,
            'nav'=>$nav
            ]);
    }

    private function breadcrumbTrail($data)
    {
        $breadcrumb = array();

        $newsType = marmot_decode($data['newsType']['id']);

        $category = array_key_exists($newsType, NEWS_TYPE_MAPPING) ? NEWS_TYPE_MAPPING[$newsType][1] : 0;
        $parentCategory = array_key_exists($newsType, NEWS_TYPE_MAPPING) ? NEWS_TYPE_MAPPING[$newsType][0] : 0;
        $categoryName = array_key_exists($category, NEWS_CATEGORY_CN) ? NEWS_CATEGORY_CN[$category] : '';
        $parentCategoryName = array_key_exists(
            $parentCategory,
            NEWS_PARENT_CATEGORY_CN
        ) ? NEWS_PARENT_CATEGORY_CN[$parentCategory] : '';

        if (!empty($parentCategory)) {
            if (empty(self::NEWS_PARENT_CATEGORY_LINK[$parentCategory])) {
                $breadcrumb[0] = array(
                    'url' => '/',
                    'name' => $parentCategoryName
                );
            }
            if (!empty(self::NEWS_PARENT_CATEGORY_LINK[$parentCategory])) {
                $breadcrumb[0] = array(
                    'url' => sprintf(
                        self::PARENT_CATEGORY_LINK,
                        self::NEWS_PARENT_CATEGORY_LINK[$parentCategory]
                    ),
                    'name' => $parentCategoryName
                );
            }
        }

        if (!in_array($category, self::NOT_LINK_NEWS_CATEGORY)) {
            if (!empty($category)) {
                if (empty(self::NEWS_PARENT_CATEGORY_LINK[$category])) {
                    $breadcrumb[1] = array(
                        'url' => sprintf(self::CATEGORY_LINK, marmot_encode($category)),
                        'name' => $categoryName
                    );
                }
                if (!empty(self::NEWS_PARENT_CATEGORY_LINK[$category])) {
                    $breadcrumb[1] = array(
                        'url' => sprintf(
                            self::PARENT_CATEGORY_LINK,
                            self::NEWS_PARENT_CATEGORY_LINK[$category]
                        ),
                        'name' => self::NEWS_NAME[$category]
                    );
                }
            }
        }

        $breadcrumb[2] = array(
            'url' => sprintf(self::TYPE_LINK, marmot_encode($newsType)),
            'name' => $data['newsType']['name']
        );

        return $breadcrumb;
    }
}
