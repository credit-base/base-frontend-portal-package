<?php
namespace Base\Package\News\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\News\Translator\NewsTranslator;

class WidgetView extends TemplateView implements IView
{
    private $newsList;

    private $translator;

    public function __construct(array $newsList)
    {
        $this->newsList = $newsList;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getNewsList() : array
    {
        return $this->newsList;
    }

    protected function getTranslator() : NewsTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();

        $translator = $this->getTranslator();
        foreach ($this->getNewsList() as $news) {
            $data[] = $translator->objectToArray(
                $news,
                array(
                    'id',
                    'title',
                    'newsType'=>[],
                )
            );
        }

        $this->getView()->display('News/WidgetNews.tpl', ['data'=>$data]);
    }
}
