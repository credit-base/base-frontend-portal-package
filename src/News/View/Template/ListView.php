<?php
namespace Base\Package\News\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class ListView extends TemplateView implements IView
{
    private $newsList;

    private $count;

    private $type;

    private $bannerList;

    private $translator;

    public function __construct(array $newsList, int $count, int $type, array $bannerList)
    {
        $this->newsList = $newsList;
        $this->count = $count;
        $this->type = $type;
        $this->bannerList = $bannerList;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getNewsList() : array
    {
        return $this->newsList;
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getType() : int
    {
        return $this->type;
    }

    protected function getBannerList() : array
    {
        return $this->bannerList;
    }

    protected function getTranslator() : NewsTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();
        $bannerList = array();

        $translator = $this->getTranslator();

        foreach ($this->getNewsList() as $news) {
            $data[] = $translator->objectToArray(
                $news,
                array(
                    'id',
                    'title',
                    'description',
                    'newsType',
                    'source',
                    'updateTime'
                )
            );
        }

        foreach ($this->getBannerList() as $news) {
            $bannerList[] = $translator->objectToArray(
                $news,
                array(
                    'id',
                    'title',
                    'newsType',
                    'source',
                    'description',
                    'bannerImage'
                )
            );
        }

        $list['total'] = $this->getCount();
        $list['list'] = $data;

        $type = $this->getType();
        $list['name'] = NEWS_TYPE_CN[$type];
        $list['type'] = marmot_encode($type);

        $nav = NAV_NEWS[$type];
        
        $this->getView()->display(
            'News/List.tpl',
            [
                'nav'=> $nav,
                'list' =>  $list,
                'bannerList' => $bannerList
            ]
        );
    }
}
