<?php
namespace Base\Package\News\View\Template\CreditDynamics;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\News\Translator\NewsTranslator;

class IndexView extends TemplateView implements IView
{
    private $data;

    private $translator;

    public function __construct(
        array $data
    ) {
        $this->data = $data;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getData() : array
    {
        return $this->data;
    }

    protected function getTranslator() : NewsTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();

        $translator = $this->getTranslator();
        foreach ($this->getData() as $key => $val) {
            if (!empty($val[0])) {
                foreach ($val[1] as $news) {
                    $data[$key][] = $translator->objectToArray(
                        $news,
                        array(
                            'id',
                            'title',
                            'description',
                            'bannerImage',
                            'newsType',
                            'updateTime',
                            'source'
                        )
                    );
                }
            }
        }

        $nav = NAV['NAV_CREDIT_DYNAMICS'];

        $this->getView()->display(
            'News/CreditDynamics/Index.tpl',
            [
                'nav'=> $nav,
                'data' => $data
            ]
        );
    }
}
