<?php
namespace Base\Package\News\View\Template;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Template\TemplateView;

use Sdk\News\Translator\NewsTranslator;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
* @codeCoverageIgnore
*/
class IndexView extends TemplateView implements IView
{
    private $data;

    private $category;

    private $translator;

    public function __construct(
        array $data,
        int $category
    ) {
        $this->data = $data;
        $this->category = $category;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getData() : array
    {
        return $this->data;
    }

    protected function getCategory() : int
    {
        return $this->category;
    }

    protected function getTranslator() : NewsTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();
        $bannerList = array();
        $category = $this->getCategory();
        $newsTypeList = NEWS_CATEGORY_INDEX[$category];

        $translator = $this->getTranslator();
        foreach ($this->getData() as $key => $val) {
            if ($key == 'banners') {
                if (!empty($val[0])) {
                    foreach ($val[1] as $news) {
                        $bannerList[] = $translator->objectToArray(
                            $news,
                            array(
                                'id',
                                'title',
                                'description',
                                'bannerImage',
                                'newsType',
                                'source'
                            )
                        );
                    }
                }
            }

            if ($key != 'banners') {
                $data[$key]['data'] = array();
                $newsTypeId = isset($newsTypeList[$key]) ? $newsTypeList[$key] : 0;

                $data[$key]['newsType'] = array(
                    'id' => marmot_encode($newsTypeId),
                    'name' => array_key_exists($newsTypeId, NEWS_TYPE_CN) ?
                            NEWS_TYPE_CN[$newsTypeId] :
                            '',
                );

                if (!empty($val[0])) {
                    foreach ($val[1] as $news) {
                        $data[$key]['data'][] = $translator->objectToArray(
                            $news,
                            array(
                                'id',
                                'title',
                                'description',
                                'updateTime',
                                'newsType',
                                'source'
                            )
                        );
                    }
                }
            }
        }

        $nav = NAV_NEWS[$category];

        $this->getView()->display(
            'News/Index.tpl',
            [
                'nav'=> $nav,
                'data' => $data,
                'bannerList' => $bannerList
            ]
        );
    }
}
