<?php
namespace Base\Package\News\View\Json;

use Marmot\Interfaces\IView;
use Marmot\Framework\View\Json\JsonView;

use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class ListJsonView extends JsonView implements IView
{
    private $newsList;

    private $count;

    private $type;

    private $translator;

    public function __construct(array $newsList, int $count, int $type)
    {
        $this->newsList = $newsList;
        $this->count = $count;
        $this->type = $type;
        $this->translator = new NewsTranslator();
        parent::__construct();
    }

    protected function getCount() : int
    {
        return $this->count;
    }

    protected function getType() : int
    {
        return $this->type;
    }

    protected function getNewsList() : array
    {
        return $this->newsList;
    }

    protected function getTranslator() : NewsTranslator
    {
        return $this->translator;
    }

    public function display()
    {
        $data = array();

        $translator = $this->getTranslator();

        foreach ($this->getNewsList() as $news) {
            $data[] = $translator->objectToArray(
                $news,
                array(
                    'id',
                    'title',
                    'description',
                    'newsType',
                    'source',
                    'updateTime'
                )
            );
        }

        $list['total'] = $this->getCount();
        $list['list'] = $data;
        
        $type = $this->getType();
        $list['name'] = NEWS_TYPE_CN[$type];
        $list['type'] = marmot_encode($type);

        $this->encode($list);
    }
}
