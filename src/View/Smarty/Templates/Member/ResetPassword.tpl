<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/user/sign-in.css">
  {/block}

  {block name=body}
  <div class="main user-form-wrapper" id="main">
    <div class="container">
      <div class="reset-password-steps js-steps">
        <div class="step-header">
          <img class="step-header-item is-active" src="{#portal_url#}/img/user/bg-reset-password-1.png" alt="第一步">
          <img class="step-header-item" src="{#portal_url#}/img/user/bg-reset-password-2.png" alt="第二步">
          <img class="step-header-item" src="{#portal_url#}/img/user/bg-reset-password-3.png" alt="第三步">
          <img class="step-header-item" src="{#portal_url#}/img/user/bg-reset-password-4.png" alt="第四步">
        </div>
        <div class="step-body">
          <div class="step-body-item is-active">
            <form class="layui-form sign-in-form" id="js_verify_user_name_form">
              <!-- 用户名 userName -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-user"></i>
                  <input type="text" name="userName" placeholder="请输入用户名" autocomplete="off" minlength="2"
                    maxlength="20" class="layui-input">
                </div>
              </div>
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <button type="button" class="layui-btn layui-btn-danger layui-btn-fluid"
                    id="js_verify_user_name_submit_btn">下一步</button>
                </div>
              </div>
            </form>
          </div>
          <div class="step-body-item">
            <form class="layui-form sign-in-form" id="js_validate_security_form">
              <!-- 密保问题 securityQuestion -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-question"></i>
                  <input type="text" name="securityQuestion" placeholder="请选择密保问题" autocomplete="off" readonly class="layui-input">
                </div>
              </div>
              <!-- 密保答案 securityAnswer -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-answer-question"></i>
                  <input type="text" name="securityAnswer" placeholder="请输入密保答案" autocomplete="off" minlength="1"
                    maxlength="30" class="layui-input">
                </div>
              </div>
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <button type="button" class="layui-btn layui-btn-danger layui-btn-fluid"
                    id="js_validate_security_submit_btn">下一步</button>
                </div>
              </div>
            </form>
          </div>
          <div class="step-body-item">
            <form class="layui-form sign-in-form" id="js_reset_password_form">
              <!-- 密码 password -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-password"></i>
                  <input type="password" name="password" placeholder="请输入新密码" minlength="8" maxlength="20" autocomplete="off" class="layui-input">
                </div>
              </div>
              <!-- 确认密码 confirmPassword -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-password"></i>
                  <input type="password" name="confirmPassword" placeholder="请输入确认密码" minlength="8" maxlength="20" autocomplete="off"
                    class="layui-input">
                </div>
              </div>
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <button type="button" class="layui-btn layui-btn-danger layui-btn-fluid"
                    id="js_reset_password_submit_btn">下一步</button>
                </div>
              </div>
            </form>
          </div>
          <div class="step-body-item">
            <form class="layui-form sign-in-form">
              <div class="reset-success">
                <i class="iconfont icon-sucess"></i>
                <span>密码重置成功！</span>
              </div>
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <button type="button" class="layui-btn layui-btn-danger layui-btn-fluid"
                    id="js_complete_submit">完成</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
    <script src="{#portal_url#}/js/user/sign-in.js"></script>
  {/block}
