<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/user/sign-in.css">
  {/block}

  {block name=body}
    <div class="main user-form-wrapper" id="main">
    <div class="container">
      <div class="sign-in-tabs js-tabs">
        <div class="tab-head">
          <div class="tab-head-item is-active"><a href="/members/signIn">登录</a></div>
          <span class="dividing-line">/</span>
          <div class="tab-head-item"><a href="/members/add">注册</a></div>
        </div>
        <div class="tab-body">
          <input type="hidden" name="backUrl" id="backUrl" class="backUrl" value="{$backUrl}"/>
          <div class="tab-body-item is-active">
            <!-- 登录 -->
            <form class="layui-form sign-in-form" id="js_sign_in_form">
              <!-- 用户名 userName -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-user"></i>
                  <input type="text" name="userName" placeholder="请输入用户名" minlength="2"
                    maxlength="20" autocomplete="off" minlength="1"
                    maxlength="30" class="layui-input">
                </div>
              </div>
              <!-- 密码 password -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-password"></i>
                  <input type="password" name="password" placeholder="请输入密码" autocomplete="off" minlength="8" maxlength="20" class="layui-input">
                </div>
              </div>
              <!-- 滑动验证码 -->
              <!-- <div class="layui-form-item">
                <div class="layui-input-block">
                  <div class="slide-verify" id="signInVerify"></div>
                </div>
              </div> -->
              <div class="layui-form-item layui-form-item-sm">
                <!-- 忘记密码 link? -->
                <a href="/members/resetPassword" class="link-forgot-password">忘记密码？</a>

                <!-- 记住密码 checkbox 屏蔽掉 -->
                <!-- <input type="checkbox" name="rememberPassword" title="记住密码" lay-skin="primary"
                  class="input-remember-password"> -->
              </div>
              <div class="layui-form-item layui-form-item-sm">
                <div class="layui-input-block">
                  <!-- 此URL用于获取从跳转到登录页面的地址 -->
                  <input type="hidden" id="backUrl" name="backUrl">
                  <button type="button" class="layui-btn layui-btn-danger layui-btn-fluid"
                    id="sign_in_submit_btn">立即登录</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
    <script src="{#portal_url#}/js/user/sign-in.js"></script>
  {/block}
