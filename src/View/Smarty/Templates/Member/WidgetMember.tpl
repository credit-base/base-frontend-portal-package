{config_load 'smarty.conf'}
<!-- 侧导航 -->
<div class="home-aside">
  <div class="panel user-info-panel">
    <div class="panel-header">
      <!-- 个人资料 -->
      <div class="dropdown visible-sm-down js-dropdown">
        <div class="dropdown-link">
          <i class="iconfont icon-down"></i>
        </div>
        <div class="dropdown-menu">
          <a href="/complaints" class="dropdown-menu-item {if $nav|default:0 eq 5 }is-active{/if}">信用投诉</a>
          <a href="/praises" class="dropdown-menu-item {if $nav|default:0 eq 6 }is-active{/if}">信用表扬</a>
          <a href="/appeals" class="dropdown-menu-item {if $nav|default:0 eq 7 }is-active{/if}">异议申诉</a>
          <a href="/feedbacks" class="dropdown-menu-item {if $nav|default:0 eq 8 }is-active{/if}">问题反馈</a>
          <a href="/qas" class="dropdown-menu-item {if $nav|default:0 eq 9 }is-active{/if}">信用问答</a>
          <span class="dropdown-menu-item">信用承诺</span>
          <span class="dropdown-menu-item">信用异议</span>
          <span class="dropdown-menu-item">信用申报</span>
          <a href="/members/creditPhotography" class="dropdown-menu-item {if $nav|default:0 eq 4 }is-active{/if}">信用问答</a>
          <span class="dropdown-menu-item">咨询建议</span>
          <span class="dropdown-menu-item">意见征集</span>
          <span class="dropdown-menu-item">信用修复</span>
          <a href="/members/creditPhotography" class="dropdown-menu-item {if $nav|default:0 eq 4 }is-active{/if}">我的随手拍</a>
          <span class="dropdown-menu-item">信用保函</span>
        </div>
      </div>
      <h3 class="panel-title">
        个人资料
      </h3>
    </div>
    <div class="panel-body">
      <div class="user-info-row">
        <!-- 头像 -->
        <a href="#" class="user-avatar">
          <img src="{#portal_url#}/img/user/default-avatar.png" alt="{$member.userName}">
        </a>
        <div class="user-info-detail">
          <!-- 名称 -->
          <a href="/members/edit" class="user-name">{$member.userName}</a>
          <div class="user-operate">
            <!-- 修改密码 -->
            <a href="/members/updatePassword" class="link-edit-password">修改密码</a>
            <!-- 修改资料 -->
            <a href="/members/edit" class="link-edit-info">修改资料</a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- 信用报告下载记录 -->
  <div class="panel recording-menu visible-sm-up">
    <div class="panel-header">
      <h3 class="panel-title">
        信用报告下载记录
      </h3>
    </div>
    <div class="panel-body">
      <span class="aside-item">信用报告下载记录</span>
    </div>
  </div>
  <!-- 进度查询 -->
  <div class="panel recording-menu visible-sm-up">
    <div class="panel-header">
      <h3 class="panel-title">
        进度查询
      </h3>
    </div>
    <div class="panel-body">
      <a href="/complaints" class="aside-item {if $nav|default:0 eq 5 }is-active{/if}">信用投诉</a>
      <a href="/praises" class="aside-item {if $nav|default:0 eq 6 }is-active{/if}">信用表扬</a>
      <a href="/appeals" class="aside-item {if $nav|default:0 eq 7 }is-active{/if}">异议申诉</a>
      <a href="/feedbacks" class="aside-item {if $nav|default:0 eq 8 }is-active{/if}">问题反馈</a>
      <a href="/qas" class="aside-item {if $nav|default:0 eq 9 }is-active{/if}">信用问答</a>
      <span class="aside-item">信用承诺</span>
      <span class="aside-item">信用异议</span>
      <span class="aside-item">信用申报</span>
      <span class="aside-item">咨询建议</span>
      <span class="aside-item">意见征集</span>
      <span class="aside-item">信用修复</span>
      <a href="/members/creditPhotography" class="aside-item {if $nav|default:0 eq 4 }is-active{/if}">我的随手拍</a>
      <span class="aside-item">信用保函</span>
    </div>
  </div>
</div>
