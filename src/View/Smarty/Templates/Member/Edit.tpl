<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/user/index.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <!-- 面包屑导航 -->
      <div class="breadcrumb-container">
        <span class="breadcrumb-label">您所在位置：</span>
        <span class="breadcrumb-current">个人中心</span>
      </div>

      <!-- 侧导航 -->
      {marmot_widget widget="Base\Package\Member\Controller\WidgetController" func=memberWidget parameters=0}
      <div class="home-content">
        <!-- 主体内容 -->
        <div class="panel home-content-panel">
          <div class="panel-header">
            <h3 class="panel-title">
              修改资料
            </h3>
          </div>
          <div class="panel-body">
            <h3 class="user-info-title">
              <i class="iconfont icon-user-name"></i>
              个人信息修改
            </h3>
            <form class="layui-form user-info-form" id="js_edit_member_form">
              <!-- 手机号：cellphone -->
              <div class="layui-form-item">
                <label class="layui-form-label">手机号：</label>
                <div class="layui-input-block">
                  <input type="tel" name="cellphone" value="{$data.cellphoneMask}" placeholder="请输入手机号" autocomplete="off"
                    class="layui-input" disabled>
                </div>
              </div>
              <!-- 姓 名：realName -->
              <div class="layui-form-item">
                <label class="layui-form-label">姓 名：</label>
                <div class="layui-input-block">
                  <input type="text" name="realName" value="{$data.realName}" placeholder="请输入姓名" autocomplete="off"
                    class="layui-input" disabled>
                </div>
              </div>
              <!-- 性 别：gender -->
              <div class="layui-form-item">
                <label class="layui-form-label">性 别：</label>
                <div class="layui-input-block">
                  <select name="gender" lay-filter="gender" disabled id="js_select_gender">
                    <!-- <option value="">请选择性别</option>-->
                    <option value="Lw" {if $data.gender.id == Lw} selected {/if}>未知</option>
                    <option value="MA" {if $data.gender.id == MA} selected {/if}>男</option>
                    <option value="MQ"{if $data.gender.id == MQ} selected {/if} >女</option>
                  </select>
                </div>
              </div>
              <!-- 邮 箱：email -->
              <div class="layui-form-item">
                <label class="layui-form-label">邮 箱：</label>
                <div class="layui-input-block">
                  <input type="email" name="email" value="{$data.email}" placeholder="请输入邮箱" autocomplete="off"
                    class="layui-input" disabled>
                </div>
              </div>
              <!-- 身份证号：cardId -->
              <div class="layui-form-item">
                <label class="layui-form-label">身份证号：</label>
                <div class="layui-input-block">
                  <input type="text" name="cardId" value="{$data.cardIdMask}" placeholder="请输入身份证号" autocomplete="off"
                    class="layui-input" disabled>
                </div>
              </div>
              <!-- 联系地址：contactAddress -->
              <div class="layui-form-item">
                <label class="layui-form-label">联系地址：</label>
                <div class="layui-input-block">
                  <input type="text" name="contactAddress" value="{$data.contactAddress}" placeholder="请输入联系地址" autocomplete="off"
                    class="layui-input" disabled>
                </div>
              </div>
              <div class="form-btn-bar">
                <button type="button" class="layui-btn layui-btn-danger" id="js_edit_member_change_btn">修改资料</button>
                <button type="button" class="layui-btn layui-btn-danger is-hidden"
                  id="js_edit_member_submit_btn">确认修改</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/js/user/index.js"></script>
  {/block}
