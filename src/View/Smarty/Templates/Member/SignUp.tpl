<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/user/sign-in.css">
  {/block}

  {block name=body}
    <div class="main user-form-wrapper" id="main">
    <div class="container">
      <div class="sign-in-tabs js-tabs">
        <div class="tab-head">
          <div class="tab-head-item"><a href="/members/signIn">登录</a></div>
          <span class="dividing-line">/</span>
          <div class="tab-head-item is-active"><a href="/members/add">注册</a></div>
        </div>
        <div class="tab-body">
          <div class="tab-body-item  is-active">
            <!-- 注册 -->
            <form class="layui-form sign-in-form" id="js_sign_up_form">
              <!-- 用户名 userName -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-user"></i>
                  <input type="text" name="userName" placeholder="请输入用户名" autocomplete="off" minlength="2"
                    maxlength="20" class="layui-input">
                </div>
              </div>
              <!-- 真实姓名 realName -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-user"></i>
                  <input type="text" name="realName" placeholder="请输入真实姓名" autocomplete="off" minlength="2"
                    maxlength="15" class="layui-input">
                </div>
              </div>
              <!-- 身份证号 cardId -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-card-identity"></i>
                  <input type="text" name="cardId" placeholder="请输入身份证号" autocomplete="off" minlength="15"
                    maxlength="18" class="layui-input">
                </div>
              </div>
              <!-- 手机号 cellphone -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-cellphone"></i>
                  <input type="tel" name="cellphone" placeholder="请输入手机号" autocomplete="off" maxlength="11"
                    class="layui-input">
                </div>
              </div>
              <!-- 邮箱 email -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-email"></i>
                  <input type="email" name="email" placeholder="请输入邮箱" autocomplete="off" minlength="2" maxlength="30"
                    class="layui-input">
                </div>
              </div>
              <!-- 地址 contactAddress -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-address"></i>
                  <input type="text" name="contactAddress" placeholder="请输入地址" autocomplete="off" minlength="1"
                    maxlength="255" class="layui-input">
                </div>
              </div>
              <!-- 密保问题 securityQuestion -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-question"></i>
                  <select name="securityQuestion" lay-filter="securityQuestion">
                    <option value="">请选择密保问题</option>
                    {foreach $securityQuestionList as $securityQuestion}
                      <option value="{$securityQuestion.id}">{$securityQuestion.name}</option>
                    {/foreach}
                  </select>
                </div>
              </div>
              <!-- 密保答案 securityAnswer -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-answer-question"></i>
                  <input type="text" name="securityAnswer" placeholder="请输入密保答案" autocomplete="off" minlength="1"
                    maxlength="30" class="layui-input">
                </div>
              </div>
              <!-- 密码 password -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-password"></i>
                  <input type="password" name="password" placeholder="请输入密码（8~20位大写字母+小写字母+数字+特殊字符）" autocomplete="off"
                    minlength="8" maxlength="20" class="layui-input">
                </div>
              </div>
              <!-- 确认密码 confirmPassword -->
              <div class="layui-form-item">
                <div class="layui-input-block">
                  <i class="iconfont icon-password"></i>
                  <input type="password" name="confirmPassword" placeholder="请输入确认密码" autocomplete="off" minlength="8"
                    maxlength="20" class="layui-input">
                </div>
              </div>
              <!-- 滑动验证码 -->
              <!-- <div class="layui-form-item">
                <div class="layui-input-block">
                  <div class="slide-verify" id="signUpVerify"></div>
                </div>
              </div> -->
              <div class="layui-form-item layui-form-item-sm">
                <div class="layui-input-block">
                  <button type="button" class="layui-btn layui-btn-danger layui-btn-fluid"
                    id="sign_up_submit_btn">立即注册</button>
                </div>
              </div>
              <!-- 网站声明及用户协议 -->
              <div class="layui-form-item">
                <label for="userAgreement" class="input-agreement">
                  <input type="checkbox" name="userAgreement" lay-skin="primary" lay-filter="userAgreement">
                </label>

                <a href="javascript:;" class="link-agreement" id="jsViewAgreement">
                   <!-- 《网站声明及用户协议》--><网站声明及用户协议>
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
    <script src="{#portal_url#}/js/user/sign-in.js"></script>
    <script type="template" id="agreement_content">
      <div class="agreement-info">
          <h2>用户协议及网站声明</h2>
          <div class="agreement-content">
            <p>信用中国（{#superior_platform#}{#region_name#}）网站提醒您：在浏览、使用信用中国（{#superior_platform#}{#region_name#}）网站与服务前，请您务必仔细阅读并透彻理解本声明。如果您使用信用中国（{#superior_platform#}{#region_name#}）网站与服务，您的使用行为将被视为对本声明全部内容的认可。</p>
            <p>除另有特别声明外，信用中国（{#superior_platform#}{#region_name#}）网站提供本网站服务时所依托软件的著作权、专利权及其他知识产权均归信用中国（{#superior_platform#}{#region_name#}）所有。上述及其他任何本网站包含的内容的知识产权均受到法律保护，未经信用中国（{#superior_platform#}{#region_name#}）网站、用户或相关权利人书面许可，任何人不得以任何形式进行使用或创造相关衍生作品。</p>
            <p>信用中国（{#superior_platform#}{#region_name#}）充分尊重作者的知识产权，对于转发的稿件，均注明作者，指明出处。如果您对信用中国（{#superior_platform#}{#region_name#}）网站刊用您的稿件存有异议，请及时通知我们，如涉嫌侵权，我们将立即移除相关稿件。</p>
            <p>信用中国（{#superior_platform#}{#region_name#}）充分尊重作者的知识产权，对于转发的稿件，均指明出处。如果各相关机构或个人对信用中国（{#superior_platform#}{#region_name#}）网站刊用贵机构或个人的稿件存有异议，请及时通知我们，如涉嫌侵权，一旦查证属实，我们将立即移除相关稿件。</p>
            <p>任何单位或个人认为通过信用中国（{#superior_platform#}{#region_name#}）提供的内容涉嫌侵犯其信息网络传播权或其他合法权益，应该及时向信用中国（{#superior_platform#}{#region_name#}）提出书面权利通知，并提供身份证明、权属证明及详细侵权情况证明。信用中国（{#superior_platform#}{#region_name#}）在收到上述法律文件后，将会依法尽快断开相关链接内容。</p>
            <p>为用户提供权威信用数据查询服务，是信用中国（{#superior_platform#}{#region_name#}）一直努力追求的目标，但您知悉并理解，使用信用中国（{#superior_platform#}{#region_name#}）查询模块所查询出的结果内容可能由第三方提供，信用中国（{#superior_platform#}{#region_name#}）无法对其提供内容的合法性、真实性、准确性进行一一核实，亦对第三方提供的内容本身不承担任何法律责任。若您对查询结果存有异议，请与信息提供方直接沟通处理，信用中国（{#superior_platform#}{#region_name#}）将在网站中详细标注各种信息的具体来源。同时，在确认相关查询结果内容确需修改的，也欢迎您及时与信用中国（{#superior_platform#}{#region_name#}）联系，您的监督与支持是我们不断改进服务的动力。</p>
            <p>用户在信用中国（{#superior_platform#}{#region_name#}）使用上述第三方提供的产品或服务时（如有），除遵守本协议约定外，还应遵守第三方的用户协议。信用中国（{#superior_platform#}{#region_name#}）和第三方对可能出现的纠纷在法律规定和约定的范围内各自承担责任。</p>
            <p>
              除非信用中国（{#superior_platform#}{#region_name#}）书面许可，任何单位和个人不得从事下列任一行为：
              <ul>
                <li>（1）对信用中国（{#superior_platform#}{#region_name#}）进行反向工程、反向汇编、反向编译，或者以其他方式尝试发现本网站的源代码；</li>
                <li>（2）对信用中国（{#superior_platform#}{#region_name#}）拥有知识产权的内容进行使用、出租、出借、复制、修改、链接、转载、汇编、发表、出版、建立镜像站点等；</li>
                <li>（3）对信用中国（{#superior_platform#}{#region_name#}）或者浏览网站过程中释放到任何终端内存中的数据、网站运行过程中客户端与服务器端的交互数据，以及网站运行所必需的系统数据，进行复制、修改、增加、删除、挂接运行或创作任何衍生作品，形式包括但不限于使用插件、外挂或非经信用中国（{#superior_platform#}{#region_name#}）授权的第三方工具/服务接入网站和相关系统；</li>
                <li>（4）通过修改或伪造网站运行中的指令、数据，增加、删减、变动网站的功能或运行效果，或者将用于上述用途的网站、方法进行运营或向公众传播，无论这些行为是否为商业目的；</li>
                <li>（5）通过非信用中国（{#superior_platform#}{#region_name#}）开发、授权的第三方网站、插件、外挂、系统，登录或使用信用中国（{#superior_platform#}{#region_name#}）网站及服务，或制作、发布、传播非信用中国（{#superior_platform#}{#region_name#}）开发、授权的第三方网站、插件、外挂、系统。</li>
              </ul>
            </p>
          </div>
        </div>
    </script>
  {/block}
