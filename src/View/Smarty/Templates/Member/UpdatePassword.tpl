<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/user/index.css">
  {/block}

  {block name=body}

  <div class="main" id="main">
    <div class="container">
      <!-- 面包屑导航 -->
      <div class="breadcrumb-container">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/members/edit">个人中心</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">修改密码</span>
      </div>

      <!-- 侧导航 -->
      {marmot_widget widget="Base\Package\Member\Controller\WidgetController" func=memberWidget parameters=0}
      <div class="home-content">
        <!-- 主体内容 -->
        <div class="panel home-content-panel">
          <div class="panel-header">
            <h3 class="panel-title">
              修改密码
            </h3>
          </div>
          <div class="panel-body">
            <form class="layui-form user-info-form" id="js_update_password_form">
              <!-- 当前密码： oldPassword -->
              <div class="layui-form-item">
                <label class="layui-form-label">当前密码：</label>
                <div class="layui-input-block">
                  <input type="password" name="oldPassword" placeholder="请输入当前密码"  minlength="8" maxlength="20" autocomplete="off"
                    class="layui-input">
                </div>
              </div>
              <!-- 新密码： password -->
              <div class="layui-form-item">
                <label class="layui-form-label">新 密 码：</label>
                <div class="layui-input-block">
                  <input type="password" name="password" placeholder="请输入新密码" minlength="8" maxlength="20" autocomplete="off" class="layui-input">
                </div>
              </div>
              <!-- 确认密码： confirmPassword -->
              <div class="layui-form-item">
                <label class="layui-form-label">确认密码：</label>
                <div class="layui-input-block">
                  <input type="password" name="confirmPassword" placeholder="请输入确认密码" minlength="8" maxlength="20" autocomplete="off"
                    class="layui-input">
                </div>
              </div>
              <div class="form-btn-bar">
                <button type="button" class="layui-btn layui-btn-danger"
                  id="js_update_password_submit_btn">确认修改</button>
                <button type="button" class="layui-btn layui-btn-primary btn-reset"
                  id="js_update_password_reset_btn">取消</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/js/user/index.js"></script>
  {/block}
