<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/service-hall/index.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="credit-card-container">
        <div class="credit-card-section">
          <div class="section-header">
            <span class="section-header-icon iconfont icon-shouxin"></span>
            <h2 class="section-header-title">信用助手</h2>
          </div>
          <div class="section-body">
            <ul class="credit-card-list">
              <li class="list-item">
                <a class="list-item-link" href="/publicQas">
                  <img class="list-item-cover" src="{#portal_url#}/img/service-hall/xin-yong-wen-da.jpg" alt="信用问答" />
                  <h3 class="list-item-title">信用问答</h3>
                </a>
              </li>
              <li class="list-item">
                <a class="list-item-link" href="/feedbacks/add">
                  <img class="list-item-cover" src="{#portal_url#}/img/service-hall/xin-yong-biao-yang.png" alt="问题反馈" />
                  <h3 class="list-item-title">问题反馈</h3>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}
