<{extends file='../../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/static/libs/swiper/css/swiper.min.css">
  <link rel="stylesheet" href="{#portal_url#}/css/news/list.css">
  {/block}

  {block name=body}

  <div class="main" id="main">
    <div class="container">
    {if !empty($data.banners)}
      <div class="news-banner-container">
        <div class="banner-swiper swiper-container" id="news_banner_swiper">
          <div class="swiper-wrapper">
            {foreach $data.banners as $item}
            <div class="swiper-slide">
              <div class="banner-swiper-item">
                <div class="banner-swiper-box">
                  <a class="banner-swiper-link" href="/news/{$item.id}">
                    <img class="banner-swiper-image" src="{$item.bannerImage.identify}" alt="{$item.title}">
                  </a>
                </div>
                <div class="banner-swiper-news">
                  <h3 class="banner-swiper-title">
                    <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.title}">
                      {$item.title}
                    </a>
                  </h3>
                  <div class="banner-swiper-meta">
                    <span class="banner-swiper-category">
                      <span class="category-label">专栏</span>
                      <span class="category-name">{$item.newsType.name}</span>
                    </span>
                    <span class="banner-swiper-source" title="{$item.source}">{$item.source}</span>
                  </div>
                  <p class="banner-swiper-description">
                    <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.description}">
                      {$item.description}
                    </a>
                  </p>
                  <div class="banner-swiper-view-more">
                    <a class="link-primary" href="/news/{$item.id}">查看详情&gt;&gt;</a>
                  </div>
                </div>
              </div>
            </div>
            {/foreach}
          </div>
          <div class="swiper-pagination" id="swiper_pagination"></div>
        </div>
      </div>
    {/if}  
      <div class="news-content-container">
        <div class="news-main-container">
          <div class="news-category-container">
            <div class="news-category-block">
              <div class="category-block-header">
                <div class="category-block-title">
                  <span class="iconfont category-block-title-icon icon-file"></span>
                  <span class="category-block-title-text">联合奖惩案例</span>
                </div>
                <a class="category-block-more" href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['DISCIPLINARY_CASE']){/php}">查看更多&gt;&gt;</a>
              </div>
              <div class="category-block-body">
                <img class="category-block-cover" src="{#portal_url#}/img/news/cover-notice.png" alt="联合奖惩案例">
              {if !empty($data.disciplinaryCase)}
                <ul class="category-block-list">
                {foreach $data.disciplinaryCase as $item}
                  <li class="list-item">
                    <a class="link-hover-primary list-item-title" href="/news/{$item.id}" title="{$item.title}">{$item.title}</a>
                    <span class="list-item-time" title="{$item.updateTimeFormat}">{$item.updateTimeFormat}</span>
                  </li>
                 {/foreach} 
                </ul>
                {else}
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无新闻数据</h4>
                  </div>
                {/if}
              </div>
            </div>
            <div class="news-category-block">
              <div class="category-block-header">
                <div class="category-block-title">
                  <span class="iconfont category-block-title-icon icon-file"></span>
                  <span class="category-block-title-text">联合奖惩备忘录</span>
                </div>
                <a class="category-block-more" href="/news?type={php} echo marmot_encode(NEWS_TYPE['DISCIPLINARY_MEMORANDUM']){/php}">查看更多&gt;&gt;</a>
              </div>
              <div class="category-block-body">
                <img class="category-block-cover" src="{#portal_url#}/img/news/cover-activity.png" alt="联合奖惩备忘录">
                 {if !empty($data.disciplinaryMemorandum)}
                <ul class="category-block-list">
                {foreach $data.disciplinaryMemorandum as $item}
                  <li class="list-item">
                    <a class="link-hover-primary list-item-title" href="/news/{$item.id}" title="{$item.title}">{$item.title}</a>
                    <span class="list-item-time" title="{$item.updateTimeFormat}">{$item.updateTimeFormat}</span>
                  </li>
                 {/foreach} 
                </ul>
                {else}
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无新闻数据</h4>
                  </div>
                {/if}
              </div>
            </div>
            <div class="news-category-block">
              <div class="category-block-header">
                <div class="category-block-title">
                  <span class="iconfont category-block-title-icon icon-file"></span>
                  <span class="category-block-title-text">联合奖惩动态</span>
                </div>
                <a class="category-block-more" href="/news?type={php} echo marmot_encode(NEWS_TYPE['DISCIPLINARY_DYNAMICS']){/php}">查看更多&gt;&gt;</a>
              </div>
              <div class="category-block-body">
                <img class="category-block-cover" src="{#portal_url#}/img/news/cover-activity.png" alt="联合奖惩动态">
                 {if !empty($data.disciplinaryDynamics)}
                <ul class="category-block-list">
                {foreach $data.disciplinaryDynamics as $item}
                  <li class="list-item">
                    <a class="link-hover-primary list-item-title" href="/news/{$item.id}" title="{$item.title}">{$item.title}</a>
                    <span class="list-item-time" title="{$item.updateTimeFormat}">{$item.updateTimeFormat}</span>
                  </li>
                 {/foreach} 
                </ul>
                {else}
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无新闻数据</h4>
                  </div>
                {/if}
              </div>
            </div>
          </div>
        </div>
        <!-- 侧栏 -->
        {marmot_widget widget="Base\Package\News\Controller\WidgetController" func=newsWidget}
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/swiper/js/swiper.min.js"></script>
  <script src="{#portal_url#}/js/news/list.js"></script>
  {/block}
