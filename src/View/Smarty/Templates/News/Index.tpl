<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/static/libs/swiper/css/swiper.min.css">
  <link rel="stylesheet" href="{#portal_url#}/css/news/list.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      {if !empty($bannerList)}
      <div class="news-banner-container">
        <div class="banner-swiper swiper-container" id="news_banner_swiper">
          <div class="swiper-wrapper">
            {foreach $bannerList as $item}
            <div class="swiper-slide">
              <div class="banner-swiper-item">
                <div class="banner-swiper-box">
                  <a class="banner-swiper-link" href="/news/{$item.id}">
                    <img class="banner-swiper-image" src="{$item.bannerImage.identify}" alt="{$item.title}">
                  </a>
                </div>
                <div class="banner-swiper-news">
                  <h3 class="banner-swiper-title">
                    <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.title}">{$item.title}</a>
                  </h3>
                  <div class="banner-swiper-meta">
                    <span class="banner-swiper-category">
                      <span class="category-label">专栏</span>
                      <span class="category-name">{$item.newsType.name}</span>
                    </span>
                    <span class="banner-swiper-source" title="{$item.source}">{$item.source}</span>
                  </div>
                  <p class="banner-swiper-description">
                    <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.description}">{$item.description}</a>
                  </p>
                  <div class="banner-swiper-view-more">
                    <a class="link-primary" href="/news/{$item.id}">查看详情&gt;&gt;</a>
                  </div>
                </div>
              </div>
            </div>
            {/foreach}
          </div>
          <div class="swiper-pagination" id="swiper_pagination"></div>
        </div>
      </div>
      {/if}
      <div class="news-content-container">
        <div class="news-tabs-container | js-tabs">
          <ul class="tab-head">
            {foreach $data as $key=>$value}
              {if !empty($value.newsType.name)}
                <li class="tab-head-item {if key($data) == $key} is-active {/if}">{$value.newsType.name}</li>
              {/if}
            {/foreach}
          </ul>

          <div class="tab-body">
          {foreach $data as $key=> $value}
            <div class="tab-body-item {if key($data) == $key} is-active {/if}">
              {if !empty($value.data)}
                <ul class="news-list">
                  {foreach $value.data as $item}
                  <li class="list-item">
                    <h3 class="list-item-title">
                      <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.title}">{$item.title}</a>
                    </h3>
                    <p class="list-item-description">
                      <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.description}">{$item.description}</a>
                    </p>
                    <div class="list-item-meta">
                      <a class="list-item-category" href="/news?type={$item.newsType.id}">
                        <span class="category-label">专栏</span>
                        <span class="category-name">{$item.newsType.name}</span>
                      </a>
                      <div class="list-item-info">
                        <span class="list-item-source" title="{$item.source}">
                            <i class="iconfont icon-earth"></i>
                            {$item.source}
                        </span>
                        <span class="list-item-time">
                            <i class="iconfont icon-time"></i>
                            {$item.updateTimeFormat}
                        </span>
                      </div>
                    </div>
                  </li>
                  {/foreach}
                </ul>
                <div class="news-view-more">
                  <a class="news-view-more-btn" href="/news?type={$value.newsType.id}" role="button">查看更多</a>
                </div>
                {else}
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无新闻数据</h4>
                  </div>
                {/if}
              </div>
          {/foreach}
          </div>
        </div>
        <!-- 侧栏 -->
        {marmot_widget widget="Base\Package\News\Controller\WidgetController" func=newsWidget}
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/swiper/js/swiper.min.js"></script>
  <script src="{#portal_url#}/js/news/list.js"></script>
  {/block}
