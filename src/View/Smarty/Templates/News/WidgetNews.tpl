<div class="news-aside-container visible-md-up">
  <div class="news-aside-search">
    <h3 class="aside-search-title">文章搜索</h3>
    <form class="aside-search-form" action="/news/search" method="GET">
      <input class="aside-search-input" type="text" name="keyword" placeholder="请输入文章标题">
      <button class="aside-search-btn" type="submit">
        <span class="iconfont icon-search"></span>
      </button>
    </form>
  </div>
  <div class="news-aside-block news-aside-express">
    <div class="aside-block-header">
      <h3 class="aside-block-title">
        <span class="iconfont icon-news"></span>
        <span class="aside-block-title-text">专栏直通车</span>
      </h3>
    </div>
    <div class="aside-block-body">
      <ul class="express-list">
        <li class="list-item">
          <a class="list-item-name link-hover-primary" href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['ORGANIZATIONAL_STRUCTURE']){/php}">组织架构</a>
        </li>
        <li class="list-item">
          <a class="list-item-name link-hover-primary" href="javascript:;">信用双公示</a>
        </li>
        <li class="list-item">
          <a class="list-item-name link-hover-primary" href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['POLICY_STATUTE']){/php}">政策法规</a>
        </li>
        <li class="list-item">
          <a class="list-item-name link-hover-primary" href="javascript:;">信用服务</a>
        </li>
        <li class="list-item">
          <a class="list-item-name link-hover-primary" href="/disciplinaries/index">联合奖惩</a>
        </li>
        <li class="list-item">
          <a class="list-item-name link-hover-primary" href="/creditDynamics/index">信用动态</a>
        </li>
      </ul>
    </div>
  </div>
  <div class="news-aside-block news-aside-heat">
    <div class="aside-block-header">
      <h3 class="aside-block-title">
        <span class="iconfont icon-news"></span>
        <span class="aside-block-title-text">最新新闻</span>
      </h3>
    </div>
    <div class="aside-block-body">
      <ul class="heat-news-list">
        {foreach $data as $key => $item}
        <li class="list-item">
          <span class="list-item-badge">{$key+1}</span>
          <h4 class="list-item-title">
            <a class="link-hover-primary" href="/news/{$item.id}">
              {$item.title}
            </a>
          </h4>
        </li>
        {/foreach}
      </ul>
    </div>
  </div>
</div>
