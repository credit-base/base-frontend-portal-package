<{extends file='../../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/static/libs/swiper/css/swiper.min.css">
  <link rel="stylesheet" href="{#portal_url#}/css/news/list.css">
  {/block}

  {block name=body}

  <div class="main" id="main">
    <div class="container">
    {if !empty($data.banners)}
      <div class="news-banner-container">
        <div class="banner-swiper swiper-container" id="news_banner_swiper">
          <div class="swiper-wrapper">
            {foreach $data.banners as $item}
            <div class="swiper-slide">
              <div class="banner-swiper-item">
                <div class="banner-swiper-box">
                  <a class="banner-swiper-link" href="/news/{$item.id}">
                    <img class="banner-swiper-image" src="{$item.bannerImage.identify}" alt="{$item.title}">
                  </a>
                </div>
                <div class="banner-swiper-news">
                  <h3 class="banner-swiper-title">
                    <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.title}">
                      {$item.title}
                    </a>
                  </h3>
                  <div class="banner-swiper-meta">
                    <span class="banner-swiper-category">
                      <span class="category-label">专栏</span>
                      <span class="category-name">{$item.newsType.name}</span>
                    </span>
                    <span class="banner-swiper-source" title="{$item.source}">{$item.source}</span>
                  </div>
                  <p class="banner-swiper-description">
                    <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.description}">
                      {$item.description}
                    </a>
                  </p>
                  <div class="banner-swiper-view-more">
                    <a class="link-primary" href="/news/{$item.id}">查看详情&gt;&gt;</a>
                  </div>
                </div>
              </div>
            </div>
            {/foreach}
          </div>
          <div class="swiper-pagination" id="swiper_pagination"></div>
        </div>
      </div>
    {/if}  
      <div class="news-content-container">
        <div class="news-main-container">
          <div class="news-category-container">
            <div class="news-category-block">
              <div class="category-block-header">
                <div class="category-block-title">
                  <span class="iconfont category-block-title-icon icon-file"></span>
                  <span class="category-block-title-text">通知公告</span>
                </div>
                <a class="category-block-more" href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['NOTICE_ANNOUNCEMENT']){/php}">查看更多&gt;&gt;</a>
              </div>
              <div class="category-block-body">
                <img class="category-block-cover" src="{#portal_url#}/img/news/cover-notice.png" alt="通知公告">
              {if !empty($data.noticeAnnouncement)}
                <ul class="category-block-list">
                {foreach $data.noticeAnnouncement as $item}
                  <li class="list-item">
                    <a class="link-hover-primary list-item-title" href="/news/{$item.id}" title="{$item.title}">{$item.title}</a>
                    <span class="list-item-time" title="{$item.updateTimeFormat}">{$item.updateTimeFormat}</span>
                  </li>
                 {/foreach} 
                </ul>
                {else}
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无新闻数据</h4>
                  </div>
                {/if}
              </div>
            </div>
            <div class="news-category-block">
              <div class="category-block-header">
                <div class="category-block-title">
                  <span class="iconfont category-block-title-icon icon-file"></span>
                  <span class="category-block-title-text">工作动态</span>
                </div>
                <a class="category-block-more" href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['WORK_DYNAMICS']){/php}">查看更多&gt;&gt;</a>
              </div>
              <div class="category-block-body">
                <img class="category-block-cover" src="{#portal_url#}/img/news/cover-activity.png" alt="工作动态">
                 {if !empty($data.workDynamics)}
                <ul class="category-block-list">
                {foreach $data.workDynamics as $item}
                  <li class="list-item">
                    <a class="link-hover-primary list-item-title" href="/news/{$item.id}" title="{$item.title}">{$item.title}</a>
                    <span class="list-item-time" title="{$item.updateTimeFormat}">{$item.updateTimeFormat}</span>
                  </li>
                 {/foreach} 
                </ul>
                {else}
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无新闻数据</h4>
                  </div>
                {/if}
              </div>
            </div>
            <div class="news-category-block">
              <div class="category-block-header">
                <div class="category-block-title">
                  <span class="iconfont category-block-title-icon icon-file"></span>
                  <span class="category-block-title-text">信用新闻</span>
                </div>
                <a class="category-block-more" href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['CREDIT_NEWS']){/php}">查看更多&gt;&gt;</a>
              </div>
              <div class="category-block-body">
                <img class="category-block-cover" src="{#portal_url#}/img/news/cover-activity.png" alt="信用新闻">
                 {if !empty($data.creditNews)}
                <ul class="category-block-list">
                {foreach $data.creditNews as $item}
                  <li class="list-item">
                    <a class="link-hover-primary list-item-title" href="/news/{$item.id}" title="{$item.title}">{$item.title}</a>
                    <span class="list-item-time" title="{$item.updateTimeFormat}">{$item.updateTimeFormat}</span>
                  </li>
                 {/foreach} 
                </ul>
                {else}
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无新闻数据</h4>
                  </div>
                {/if}
              </div>
            </div>
            <div class="news-category-block">
              <div class="category-block-header">
                <div class="category-block-title">
                  <span class="iconfont category-block-title-icon icon-file"></span>
                  <span class="category-block-title-text">信用建设</span>
                </div>
                <a class="category-block-more" href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['CREDIT_CONSTRUCTION']){/php}">查看更多&gt;&gt;</a>
              </div>
              <div class="category-block-body">
                <img class="category-block-cover" src="{#portal_url#}/img/news/cover-activity.png" alt="信用建设">
                 {if !empty($data.creditConstruction)}
                <ul class="category-block-list">
                {foreach $data.creditConstruction as $item}
                  <li class="list-item">
                    <a class="link-hover-primary list-item-title" href="/news/{$item.id}" title="{$item.title}">{$item.title}</a>
                    <span class="list-item-time" title="{$item.updateTimeFormat}">{$item.updateTimeFormat}</span>
                  </li>
                 {/foreach} 
                </ul>
                {else}
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无新闻数据</h4>
                  </div>
                {/if}
              </div>
            </div>
          </div>
        </div>
        <!-- 侧栏 -->
        {marmot_widget widget="Base\Package\News\Controller\WidgetController" func=newsWidget}
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/swiper/js/swiper.min.js"></script>
  <script src="{#portal_url#}/js/news/list.js"></script>
  {/block}
