<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/static/libs/share-js/css/share.min.css">
  <link rel="stylesheet" href="{#portal_url#}/css/news/detail.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="news-content-container">
        <div class="news-main-container">
          <div class="breadcrumb-container mb-15">
            <span class="breadcrumb-label">您所在位置：</span>
            {foreach $breadcrumb as $each}
            {if !empty($each['url'])}
            <a class="breadcrumb-link" href="{$each['url']}">{$each['name']}</a>
            <span class="breadcrumb-seperator">&gt;&gt;</span>
            {/if}
            {/foreach}
            <span class="breadcrumb-current">新闻详情</span>
          </div>
          <div class="news-detail-container mb-20">
            <div class="news-header">
              <h2 class="news-title">{$data.title}</h2>
              <div class="news-meta">
                <div class="news-meta-list">
                  <span class="news-meta-item">发布时间：{$data.updateTimeFormat}</span>
                  <span class="news-meta-item">新闻来源：{$data.source}</span>
                  <span class="news-meta-item">专栏：{$data.newsType.name}</span>
                </div>
                <div class="news-meta-share">
                  <span class="iconfont icon-share"></span>
                  <div class="social-share share-component"></div>
                </div>
              </div>
            </div>
            <div class="news-body">
              <div class="news-content">{$data.content}</div>
              <div class="news-footer">
                {if !empty({$data.publishUserGroup.name})}
                <span class="iconfont icon-earth"></span>
                <span>{$data.publishUserGroup.name}</span>
                {/if}
                <!--<span class="iconfont icon-time"></span>
                <span>{$data.updateTimeFormat}</span>-->
              </div>
            </div>
          </div>
          {if !empty($relevantArticles)}
          <div class="news-related-container">
            <div class="related-news-header">
              <h3 class="related-news-title">
                <span class="iconfont icon-news"></span>
                <span class="related-news-title-text">相关文章</span>
              </h3>
            </div>
            <div class="related-news-body">
              <ul class="related-news-list">
                {foreach $relevantArticles as $idx => $item}
                <li class="list-item">
                  <h4 class="list-item-title">
                    <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.title}">
                      {$idx+1}.&nbsp;{$item.title}
                    </a>
                  </h4>
                  <span class="list-item-time">发布时间: {$item.updateTimeFormat}</span>
                </li>
                {/foreach}
              </ul>
            </div>
          </div>
          {/if}
        </div>
        <!-- 侧栏 -->
        {marmot_widget widget="Base\Package\News\Controller\WidgetController" func=newsWidget}
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/share-js/js/jquery.share.min.js"></script>
  <script src="{#portal_url#}/js/news/detail.js"></script>
  <script>
    $('.social-share').share({
      disabled: ['google', 'facebook', 'twitter'],
      sites: ['wechat', 'weibo']
    })
  </script>
  {/block}
