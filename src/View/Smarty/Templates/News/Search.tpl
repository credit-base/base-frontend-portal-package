<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/static/libs/swiper/css/swiper.min.css">
  <link rel="stylesheet" href="{#portal_url#}/css/news/search.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="breadcrumb-container mb-15">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/">首页</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">文章搜索</span>
      </div>
      <div class="news-content-container">
        <div class="news-main-container">
          <div class="news-search-result">
            <div class="search-result-header">
              <span>搜索"</span>
              <span class="search-keyword">{$list.keyword}</span>
              <span>"为您找到的相关信息</span>
              <span class="search-result-count">{$list.total}</span>
              <span>条</span>
            </div>
            {if !empty($list.list)}
            <div class="search-result-body">
              <ul class="search-result-list news-list" id="news_list">
                {foreach $list.list as $item}
                <li class="list-item">
                  <h3 class="list-item-title">
                    <a class="link-hover-primary" href="/news/{$item.id}">
                      {$item.title}
                    </a>
                  </h3>
                  <p class="list-item-description">
                    <a class="link-hover-primary" href="/news/{$item.id}" title="{$item.description}">{$item.description}</a>
                  </p>
                  <div class="list-item-meta">
                    <a class="list-item-category" href="/news/{$item.id}">
                      <span class="category-label">专栏</span>
                      <span class="category-name">{$item.newsType.name}</span>
                    </a>
                    <div class="list-item-info">
                      <span class="iconfont icon-earth"></span>
                      <span class="list-item-source">{$item.source}</span>
                      <span class="iconfont icon-time"></span>
                      <span class="list-item-time">{$item.updateTimeFormat}</span>
                    </div>
                  </div>
                </li>
                {/foreach}
              </ul>
            </div>
            <div class="search-result-footer">
              <div class="pagination-container">
                <input type="hidden" id="pagination_init_total" value="{$list.total}">
                <div class="pagination" id="pagination"></div>
              </div>
            </div>
              {else}
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无新闻数据</h4>
                  </div>
            {/if}
          </div>
        </div>
        <!-- 侧栏 -->
        {marmot_widget widget="Base\Package\News\Controller\WidgetController" func=newsWidget}
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/swiper/js/swiper.min.js"></script>
  <script src="{#portal_url#}/js/news/search.js"></script>
  {/block}
