<{extends file='../Layout/Main.tpl' }>

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="breadcrumb-container mb-15">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/serviceHalls/index">办事服务</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">信用服务（企业/事业单位信用信息查询）</span>
      </div>

      <div class="credit-search-container">
        <div class="credit-search-filter">
          <h2 class="credit-search-title">企业/事业单位信用信息查询</h2>
          <div class="credit-search-form">
            <input class="credit-search-input" id="input_search" type="text" name="keyword" value=""
              placeholder="请输入企业/事业单位名称">
            <input type="hidden" name="type" value="MA">
            <button class="credit-search-submit" id="btn_search" type="button" role="button">
              <span class="icon-searchs"></span>
              <span>搜索</span>
            </button>
          </div>
          <div class="credit-search-type">
            <span class="fl">信用类型 ：</span>
            <ul class="fl">
              <li class="fl type-item is-active" data-type="Lw">不限</li>
              <li class="fl type-item" data-type="NCs">失信被执行机构</li>
              <li class="fl type-item" data-type="Nyw">企业经营异常名录</li>
              <li class="fl type-item" data-type="MCspLg">重大税收违法案件当事人名单</li>
              <li class="fl type-item" data-type="MCspLg">政府采购严重违法失信名单</li>
            </ul>
          </div>
        </div>
        <div class="credit-search-list" id="credit_search_list">
          <table>
            <thead>
              <tr>
                <th width="410">主体名称</th>
                <th width="198">统一社会信用代码</th>
                <th width="120">登记状态</th>
                <th width="160">联合奖惩名单标识</th>
                <th width="100">操作</th>
              </tr>
            </thead>
            <tbody>
              {if !empty($data['list'])}
              {foreach $data['list'] as $item}
              <tr>
                <td><a class="link-hover-primary" href="/enterprises/{$item.id}">{$item.name}</a></td>
                <td><a class="link-hover-primary" href="/enterprises/{$item.id}">{$item.unifiedSocialCreditCode}</a>
                </td>
                <td><a class="link-hover-primary" href="/enterprises/{$item.id}">{$item.registrationStatus}</a></td>
                <td>
                  <p>奖({$item.awardTotal}) 惩({$item.penaltyTotal})</p>
                </td>
                <td><a class="link-hover-primary" href="/enterprises/{$item.id}">查看详情</a></td>
              </tr>
              {/foreach}
              {else}
              <tr>
                <td colspan="5">
                  <div class="empty-data-container">
                    <div class="empty-data-icon"></div>
                    <h4 class="empty-data-tip">暂无数据</h4>
                  </div>
                </td>
              </tr>
              {/if}
            </tbody>
          </table>
        </div>
        <div class="pagination-container">
          <input type="hidden" id="pagination_init_total" value="{$data.total}">
          <div class="pagination" id="pagination"></div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/js/enterprise/list.js"></script>
  {/block}
