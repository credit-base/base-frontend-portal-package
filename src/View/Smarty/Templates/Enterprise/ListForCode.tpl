<{extends file='../Layout/Main.tpl' }>

{block name=body}
<div class="main" id="main">
  <div class="container">
    <div class="breadcrumb-container mb-15">
      <span class="breadcrumb-label">您所在位置：</span>
      <a class="breadcrumb-link" href="/publicities/index">信用公示</a>
      <span class="breadcrumb-seperator">&gt;&gt;</span>
      <span class="breadcrumb-current">统一社会信用代码</span>
    </div>

    <div class="credit-search-container">
      <div class="credit-search-filter">
        <h2 class="credit-search-title">统一社会信用代码查询</h2>
        <div class="credit-search-form">
          <input class="credit-search-input" id="input_search" type="text" name="keyword" value="" placeholder="请输入统一社会信用代码">
          <input type="hidden" name="type" value="MQ">
          <button class="credit-search-submit" id="btn_search" type="button" role="button">
            <span class="icon-searchs"></span>
            <span>搜索</span>
          </button>
        </div>
      </div>
      <div class="credit-search-list" id="credit_search_list">
        <table>
          <thead>
            <tr>
              <th width="410">主体名称</th>
              <th width="198">统一社会信用代码</th>
              <th width="120">登记状态</th>
              <th width="160">联合奖惩名单标识</th>
              <th width="100">操作</th>
            </tr>
          </thead>
          <tbody>
            {if !empty($data['list'])}
            {foreach $data['list'] as $item}
            <tr>
              <td><a class="link-hover-primary" href="/enterprises/{$item.id}">{$item.name}</a></td>
              <td><a class="link-hover-primary" href="/enterprises/{$item.id}">{$item.unifiedSocialCreditCode}</a></td>
              <td><a class="link-hover-primary" href="/enterprises/{$item.id}">{$item.registrationStatus}</a></td>
              <td>
                <p>奖({$item.awardTotal}) 惩({$item.penaltyTotal})</p>
              </td>
              <td><a class="link-hover-primary" href="/enterprises/{$item.id}">查看详情</a></td>
            </tr>
            {/foreach}
            {else}
            <tr>
              <td colspan="5">
                <div class="empty-data-container">
                  <div class="empty-data-icon"></div>
                  <h4 class="empty-data-tip">暂无数据</h4>
                </div>
              </td>
            </tr>
            {/if}
          </tbody>
        </table>
      </div>
      <div class="pagination-container">
        <input type="hidden" id="pagination_init_total" value="{$data.total}">
        <div class="pagination" id="pagination"></div>
      </div>
    </div>
  </div>
</div>
{/block}

{block name=pageScript}
<script src="{#portal_url#}/js/enterprise/list.js"></script>
{/block}
