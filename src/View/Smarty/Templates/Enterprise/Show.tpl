<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/enterprise/detail.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="breadcrumb-container mb-15 visible-sm-up">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/creditPublicities/index">信用公示</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <a class="breadcrumb-link" href="/enterprises?type=unifiedSocialCreditCode">企业统一社会信用代码</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">详情</span>
      </div>

      <div class="enterprise-detail-container">
        <!-- 企业基本信息 -->
        <div class="enterprise-basic">
          <div class="enterprise-basic-main">
            <div class="enterprise-basic-row">
              <div class="enterprise-name">
                <span class="enterprise-name-text" title="{$enterprise.name}">{$enterprise.name}</span>
                <span class="enterprise-name-status is-success">{$enterprise.registrationStatus}</span>
                <input id="enterprise_id" type="hidden" val="{$enterprise.id}">
              </div>
              <ul class="enterprise-award-punishment">
                <li class="award-punishment-item is-award is-wip">
                  <span class="item-label">联合激励数量:</span>
                  <strong class="item-count">{$enterprise.awardTotal}</strong>
                </li>
                <li class="award-punishment-item is-punishment is-wip">
                  <span class="item-label">联合惩戒数量:</span>
                  <strong class="item-count">{$enterprise.penaltyTotal}</strong>
                </li>
              </ul>
            </div>
            <div class="enterprise-basic-row enterprise-meta">
              <ul class="enterprise-meta-list">
                <li class="meta-item">
                  <span class="meta-item-label">法人信息:</span>
                  <p class="meta-item-content" title="{$enterprise.principal}">{$enterprise.principal}</p>
                </li>
                <li class="meta-item">
                  <span class="meta-item-label">统一社会信用代码:</span>
                  <p class="meta-item-content" title="{$enterprise.unifiedSocialCreditCode}">
                    {$enterprise.unifiedSocialCreditCode}</p>
                </li>
                <li class="meta-item">
                  <span class="meta-item-label">成立日期:</span>
                  <p class="meta-item-content" title="{$enterprise.establishmentDate}">{$enterprise.establishmentDate}
                  </p>
                </li>
                <li class="meta-item">
                  <span class="meta-item-label">企业类型:</span>
                  <p class="meta-item-content" title="{$enterprise.enterpriseType}">{$enterprise.enterpriseType}</p>
                </li>
                <li class="meta-item">
                  <span class="meta-item-label">登记机关:</span>
                  <p class="meta-item-content" title="{$enterprise.registrationAuthority}">
                    {$enterprise.registrationAuthority}</p>
                </li>
              </ul>
            </div>
            <div class="enterprise-basic-row enterprise-graph">
              <div class="graph-item">
                <div class="graph-item-main">
                  <div class="graph-chart" id="chart_credit_score"></div>
                </div>
                <h3 class="graph-item-title">信用评分等级:<strong class="color-primary">B</strong></h3>
              </div>
              <div class="graph-item">
                <div class="graph-item-main">
                  <div class="graph-chart" id="chart_industry_score"></div>
                </div>
                <h3 class="graph-item-title">行业信用评分等级:<strong class="color-primary">B</strong></h3>
              </div>
              <div class="graph-item">
                <div class="graph-item-main">
                  <img src="/enterprises/{$enterprise['id']}/qrCode" alt="{$enterprise.name}" />
                </div>
                <h3 class="graph-item-title">信用二维码</h3>
              </div>
            </div>
            <div class="enterprise-basic-row enterprise-action">
              <div class="enterprise-action-secondary">
                <a class="btn-action is-secondary is-wip" href="javascript:;" role="button">
                  <span class="btn-action-icon iconfont icon-fankui"></span>
                  <span class="btn-action-text">我要评价</span>
                </a>
                <a class="btn-action is-secondary is-wip" href="javascript:;" role="button">
                  <span class="btn-action-icon iconfont icon-award"></span>
                  <span class="btn-action-text">我要承诺</span>
                </a>
                <a class="btn-action is-secondary is-wip" href="javascript:;" role="button">
                  <span class="btn-action-icon iconfont icon-thumb-up"></span>
                  <span class="btn-action-text">我要表扬</span>
                </a>
                <a class="btn-action is-secondary is-wip" href="javascript:;" role="button">
                  <span class="btn-action-icon iconfont icon-envelope-open"></span>
                  <span class="btn-action-text">我要投诉</span>
                </a>
              </div>
              <div class="enterprise-action-primary">
                <button class="btn-action is-primary is-wip is-orange" type="button" role="button">
                  <span class="btn-action-icon iconfont icon-download"></span>
                  <span class="btn-action-text">下载报告</span>
                </button>
                <button class="btn-action is-primary is-wip is-red" type="button" role="button">
                  <span class="btn-action-icon iconfont icon-cross"></span>
                  <span class="btn-action-text">交叉分析</span>
                </button>
                <button class="btn-action is-primary is-wip is-red" type="button" role="button">
                  <span class="btn-action-icon iconfont icon-recorrect"></span>
                  <span class="btn-action-text">异议纠错</span>
                </button>
                <button class="btn-action is-primary is-wip is-blue" type="button" role="button">
                  <span class="btn-action-icon iconfont icon-report"></span>
                  <span class="btn-action-text">第三方信用报告</span>
                </button>
              </div>
            </div>
          </div>
          <div class="enterprise-basic-tip">
            <p class="tip-content"><strong>风险提示：</strong>本网站仅基于已掌握的信息提供查询服务，查询结果不代表本网站对被查询对象信用状况的评价，仅供参考，请注意识别和防范信用风险。
            </p>
            <i class="iconfont icon-warning-o tip-icon"></i>
          </div>
        </div>

        <!-- 企业高级信息 -->
        <div class="enterprise-advance">
          <div class="enterprise-advance-aside">
            <div class="aside-block">
              <div class="aside-block-header">
                <h3 class="aside-block-title">企业图谱</h3>
                <span class="aside-block-icon is-relation"></span>
              </div>
              <div class="aside-block-main">
                <div class="aside-block-trigger" id="chart_relation" role="button">
                  <img src="{#portal_url#}/img/enterprise/chart-relation.png" alt="">
                </div>
              </div>
            </div>
            <div class="aside-block">
              <div class="aside-block-header">
                <h3 class="aside-block-title">企业信用画像分析</h3>
                <span class="aside-block-icon is-user-analysis"></span>
              </div>
              <div class="aside-block-main">
                <div class="aside-block-trigger" id="chart_user_analysis" role="button">
                  <img src="{#portal_url#}/img/enterprise/chart-user-analysis.png" alt="">
                </div>
              </div>
            </div>
            <div class="aside-block">
              <div class="aside-block-header">
                <h3 class="aside-block-title">企业预警预测趋势图</h3>
                <span class="aside-block-icon is-trend"></span>
              </div>
              <div class="aside-block-main">
                <div class="aside-block-trigger" id="chart_trend" role="button">
                  <img src="{#portal_url#}/img/enterprise/chart-trend.png" alt="">
                </div>
              </div>
            </div>
          </div>

          <div class="enterprise-advance-main">
            <!-- 企业关联信息 -->
            <div class="enterprise-related" id="enterprise_related">
              <div class="table-tabs-container | js-tabs">
                <ul class="tab-head">
                  <li class="tab-head-item is-active" data-type="permit">
                    <span class="tab-head-item-label">行政许可</span>
                    <span class="tab-head-item-count">{$statistical.licensingTotal}</span>
                  </li>
                  <li class="tab-head-item" data-type="punishment">
                    <span class="tab-head-item-label">行政处罚</span>
                    <span class="tab-head-item-count">{$statistical.punishTotal}</span>
                  </li>
                  <li class="tab-head-item" data-type="redRank">
                    <span class="tab-head-item-label">守信激励</span>
                    <span class="tab-head-item-count">{$statistical.incentiveTotal}</span>
                  </li>
                  <li class="tab-head-item" data-type="blackRank">
                    <span class="tab-head-item-label">失信惩戒</span>
                    <span class="tab-head-item-count">{$statistical.penaltyTotal}</span>
                  </li>
                  <li class="tab-head-item">
                    <span class="tab-head-item-label">重点关注单</span>
                    <span class="tab-head-item-count">0</span>
                  </li>
                  <li class="tab-head-item">
                    <span class="tab-head-item-label">信用承诺</span>
                    <span class="tab-head-item-count">0</span>
                  </li>
                  <li class="tab-head-item">
                    <span class="tab-head-item-label">合同履约</span>
                    <span class="tab-head-item-count">0</span>
                  </li>
                  <li class="tab-head-item">
                    <span class="tab-head-item-label">双随机一公开</span>
                    <span class="tab-head-item-count">0</span>
                  </li>
                </ul>
                <div class="tab-body">
                  <!-- 行政许可 -->
                  <div class="tab-body-item is-active">
                    {if $resourceCatalogData.permit.count gt 0}
                    <table>
                      <thead>
                        <tr>
                          <th>编号</th>
                          <th>信用信息事项</th>
                          <th>发布单位</th>
                          <th>操作</th>
                        </tr>
                      </thead>
                      <tbody>
                        {foreach $resourceCatalogData.permit.data as $resourceCatalogInfo}
                        <tr>
                          <td>
                            <a class="link-hover-primary"
                              href="{$resourceCatalogInfo.url}">{$resourceCatalogInfo.id}</a>
                          </td>
                          <td>{$resourceCatalogInfo.template.name}</td>
                          <td>{$resourceCatalogInfo.sourceUnit.name}</td>
                          <td>
                            <a class="link-hover-primary" href="{$resourceCatalogInfo.url}">查看</a>
                          </td>
                        </tr>
                        {/foreach}
                      </tbody>
                    </table>
                    {else}
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无数据</h4>
                    </div>
                    {/if}
                    <div class="pagination-container">
                      <input type="hidden" value="{$statistical.licensingTotal}">
                      <div class="pagination"></div>
                    </div>
                  </div>
                  <!-- 行政处罚 -->
                  <div class="tab-body-item">
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无数据</h4>
                    </div>
                    <div class="pagination-container">
                      <input type="hidden" value="{$statistical.punishTotal}">
                      <div class="pagination"></div>
                    </div>
                  </div>
                  <!-- 红名单 -->
                  <div class="tab-body-item">
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无数据</h4>
                    </div>
                    <div class="pagination-container">
                      <input type="hidden" value="{$statistical.incentiveTotal}">
                      <div class="pagination"></div>
                    </div>
                  </div>
                  <!-- 黑名单 -->
                  <div class="tab-body-item">
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无数据</h4>
                    </div>
                    <div class="pagination-container">
                      <input type="hidden" value="{$statistical.penaltyTotal}">
                      <div class="pagination"></div>
                    </div>
                  </div>
                  <!-- 重点关注单 -->
                  <div class="tab-body-item">
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无数据</h4>
                    </div>
                    <div class="pagination-container">
                      <input type="hidden" value="0">
                      <div class="pagination"></div>
                    </div>
                  </div>
                  <!-- 信用承诺 -->
                  <div class="tab-body-item">
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无数据</h4>
                    </div>
                    <!-- <table>
                      <thead>
                        <tr>
                          <th>编号</th>
                          <th>承诺类型</th>
                          <th>承诺有效期</th>
                          <th>承诺状态</th>
                          <th>操作</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="5">
                            <div class="empty-data-container">
                              <div class="empty-data-icon"></div>
                              <h4 class="empty-data-tip">暂无数据</h4>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a class="link-hover-primary" href="javascript:;">MA</a>
                          </td>
                          <td>审批替代性</td>
                          <td>2021年12月</td>
                          <td>监管中</td>
                          <td>
                            <a class="link-hover-primary" href="javascript:;">查看</a>
                          </td>
                        </tr>
                      </tbody>
                    </table> -->
                    <div class="pagination-container">
                      <input type="hidden" value="0">
                      <div class="pagination"></div>
                    </div>
                  </div>
                  <!-- 合同履约 -->
                  <div class="tab-body-item">
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无数据</h4>
                    </div>
                    <!-- <table>
                      <thead>
                        <tr>
                          <th>编号</th>
                          <th>合同名称</th>
                          <th>合同编号</th>
                          <th>合同状态</th>
                          <th>操作</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td colspan="5">
                            <div class="empty-data-container">
                              <div class="empty-data-icon"></div>
                              <h4 class="empty-data-tip">暂无数据</h4>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a class="link-hover-primary" href="javascript:;">MA</a>
                          </td>
                          <td>采购合同</td>
                          <td>CG212993283</td>
                          <td>监管中</td>
                          <td>
                            <a class="link-hover-primary" href="javascript:;">查看</a>
                          </td>
                        </tr>
                      </tbody>
                    </table> -->
                    <div class="pagination-container">
                      <input type="hidden" value="0">
                      <div class="pagination"></div>
                    </div>
                  </div>
                  <!-- 双随机一公开 -->
                  <div class="tab-body-item">
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无数据</h4>
                    </div>
                    <div class="pagination-container">
                      <input type="hidden" value="0">
                      <div class="pagination"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!-- 企业统计信息 -->
            <div class="enterprise-statistics">
              <div class="table-tabs-container | js-tabs">
                <ul class="tab-head">
                  <li class="tab-head-item is-active">企业运营信息</li>
                  <li class="tab-head-item">行业组织信息</li>
                  <li class="tab-head-item">金融信贷信息</li>
                </ul>
                <div class="tab-body">
                  <div class="tab-body-item is-active">
                    <div class="statistics-block">
                      <div class="statistics-block-header">
                        <h3 class="statistics-block-title">
                          <span class="statistics-block-icon iconfont icon-enterprise-finance"></span>
                          <span class="statistics-block-title-text">企业财务信息</span>
                        </h3>
                      </div>
                      <div class="statistics-block-body">
                        <table>
                          <thead>
                            <tr>
                              <th>资产总额</th>
                              <th>销售总额</th>
                              <th>利用总额</th>
                              <th>操作</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr></tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="statistics-block">
                      <div class="statistics-block-header">
                        <h3 class="statistics-block-title">
                          <span class="statistics-block-icon iconfont icon-enterprise-comment"></span>
                          <span class="statistics-block-title-text">企业管理体系评价信息</span>
                        </h3>
                      </div>
                      <div class="statistics-block-body">
                        <table>
                          <thead>
                            <tr>
                              <th>管理体系模块名称</th>
                              <th>等级</th>
                              <th>级别说明</th>
                              <th>操作</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr></tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="tab-body-item">
                    <div class="statistics-block">
                      <div class="statistics-block-header">
                        <h3 class="statistics-block-title">
                          <span class="statistics-block-icon iconfont icon-enterprise-finance"></span>
                          <span class="statistics-block-title-text">行业组织的评价信息</span>
                        </h3>
                      </div>
                      <div class="statistics-block-body">
                        <table>
                          <thead>
                            <tr>
                              <th>行业名称</th>
                              <th>评价类型</th>
                              <th>评价内容</th>
                              <th>操作</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr></tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="statistics-block">
                      <div class="statistics-block-header">
                        <h3 class="statistics-block-title">
                          <span class="statistics-block-icon iconfont icon-enterprise-comment"></span>
                          <span class="statistics-block-title-text">公共事业单位评价信息</span>
                        </h3>
                      </div>
                      <div class="statistics-block-body">
                        <table>
                          <thead>
                            <tr>
                              <th>公共事业单位名称</th>
                              <th>评价类型</th>
                              <th>评价内容</th>
                              <th>操作</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr></tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div class="tab-body-item">
                    <div class="statistics-block">
                      <div class="statistics-block-header">
                        <h3 class="statistics-block-title">
                          <span class="statistics-block-icon iconfont icon-enterprise-finance"></span>
                          <span class="statistics-block-title-text">商业银行信贷评价信息</span>
                        </h3>
                      </div>
                      <div class="statistics-block-body">
                        <table>
                          <thead>
                            <tr>
                              <th>商业银行名称</th>
                              <th>信贷业务时间</th>
                              <th>评价信息</th>
                              <th>操作</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr></tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="statistics-block">
                      <div class="statistics-block-header">
                        <h3 class="statistics-block-title">
                          <span class="statistics-block-icon iconfont icon-enterprise-comment"></span>
                          <span class="statistics-block-title-text">民间信贷评价信息</span>
                        </h3>
                      </div>
                      <div class="statistics-block-body">
                        <table>
                          <thead>
                            <tr>
                              <th>民间信贷组织名称</th>
                              <th>信贷业务时间</th>
                              <th>评价信息</th>
                              <th>操作</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr></tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/echarts/echarts.min.js"></script>
  <script src="{#portal_url#}/js/enterprise/detail.js"></script>
  {/block}
