<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/publicity/index.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="news-content-container">
        <!-- 信息公示 -->
        <div class="publicity">
          <!-- 信用双公示 -->
          <div class="publicity-list">
            <div class="publicity-title fl">
              <span class="publicity-title-icon iconfont icon-gongshi fl"></span>
              <span class="publicity-title-text fl">信用双公示</span>
            </div>
            <div class="publicity-link hover-block fl">
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="/doublePublicity?scene=MA">
                  <img src="https://static.zx.qixinyun.com/pxiang-xuke.jpg" alt="">
                  <p>行政许可</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="/doublePublicity?scene=MQ">
                  <img src="https://static.zx.qixinyun.com/pxiang-chufa.jpg" alt="">
                  <p>行政处罚</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="javascript:;" target="_blank">
                  <img src="https://static.zx.qixinyun.com/pxiang-portal-sgs.png" alt="">
                  <p>双公示目录</p>
                </a>
              </div>
            </div>
          </div>

          <!-- 信用守信信息与信用失信信息 -->
          <div class="publicity-list">
            <div class="publicity-title fl">
              <span class="publicity-title-icon iconfont icon-shouxin fl"></span>
              <span class="publicity-title-text fl">信用守信信息与信用失信信息</span>
            </div>
            <div class="publicity-link hover-block fl">
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="/redBlacks?scene=MA">
                  <img src="{#portal_url#}/img/publicity/credit-red.jpeg" alt="">
                  <p>诚信红榜</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="/redBlacks?scene=MQ">
                  <img src="{#portal_url#}/img/publicity/credit-black.jpeg" alt="">
                  <p>失信黑榜</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="javascript:;" target="_blank">
                  <img src="{#portal_url#}/img/publicity/credit-good.jpeg" alt="">
                  <p>诚信信用记录</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="javascript:;" target="_blank">
                  <img src="{#portal_url#}/img/publicity/credit-bad.jpeg" alt="">
                  <p>不良信用记录</p>
                </a>
              </div>
            </div>
          </div>

          <!-- 失信被执行人曝光平台 -->
          <div class="publicity-list">
            <div class="publicity-title fl">
              <span class="publicity-title-icon iconfont icon-shixin fl"></span>
              <span class="publicity-title-text fl">失信被执行人曝光平台</span>
            </div>
            <div class="publicity-link hover-block fl">
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="javascript:;">
                  <img src="{#portal_url#}/img/publicity/credit-shixin.png" alt="">
                  <p>失信被执行人曝光平台</p>
                </a>
              </div>
            </div>
          </div>

          <!-- 信用反馈 -->
          <div class="publicity-list">
            <div class="publicity-title fl">
              <span class="publicity-title-icon iconfont icon-fankui fl"></span>
              <span class="publicity-title-text fl">信用反馈</span>
            </div>
            <div class="publicity-link hover-block fl">
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="/complaints/add">
                  <img src="{#portal_url#}/img/publicity/credit-tousu.jpeg" alt="">
                  <p>信用投诉</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="/publicPraises">
                  <img src="{#portal_url#}/img/publicity/credit-biaoy.jpeg" alt="">
                  <p>信用表扬</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="javascript:;">
                  <img src="{#portal_url#}/img/publicity/credit-promise.jpeg" alt="">
                  <p>信用承诺</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="/appeals/add">
                  <img src="{#portal_url#}/img/publicity/credit-shensu.jpeg" alt="">
                  <p>异议申诉</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="javascript:;">
                  <img src="{#portal_url#}/img/publicity/credit-yiyi.png" alt="">
                  <p>信用异议</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="javascript:;">
                  <img src="{#portal_url#}/img/publicity/credit-shenbao.png" alt="">
                  <p>信用申报</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="javascript:;">
                  <img src="{#portal_url#}/img/publicity/credit-contract.png" alt="">
                  <p>合同履约</p>
                </a>
              </div>
            </div>
          </div>

          <!-- 信用监管 -->
          <div class="publicity-list">
            <div class="publicity-title fl">
              <span class="publicity-title-icon iconfont icon-jianguan fl"></span>
              <span class="publicity-title-text fl">信用监管</span>
            </div>
            <div class="publicity-link hover-block fl">
              <div class="publicity-link-box fl">
                <a class="publicity-link-item"
                  href="/news?type={php} echo marmot_encode(NEWS_TYPE['DEPARTMENTAL_SUPERVISION']){/php}">
                  <img src="{#portal_url#}/img/publicity/credit-bum.jpeg" alt="">
                  <p>部门监管</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item"
                  href="/news?type={php} echo marmot_encode(NEWS_TYPE['MEDIA_SUPERVISION']){/php}">
                  <img src="{#portal_url#}/img/publicity/credit-meit.jpeg" alt="">
                  <p>媒体监管</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item"
                  href="/news?type={php} echo marmot_encode(NEWS_TYPE['INDUSTRY_SUPERVISION']){/php}">
                  <img src="{#portal_url#}/img/publicity/credit-hy.jpeg" alt="">
                  <p>行业监管</p>
                </a>
              </div>
              <!--<div class="publicity-link-box fl">
                <a class="publicity-link-item" href="/news?type={php} echo marmot_encode(NEWS_TYPE['CLASSIC_CASE']){/php}" target="_blank">
                  <img src="{#portal_url#}/img/publicity/credit-important.jpeg" alt="">
                  <p>重点监管</p>
                </a>
              </div>-->
            </div>
          </div>

          <!-- 守信激励与失信惩戒 -->
          <div class="publicity-list">
            <div class="publicity-title fl">
              <span class="publicity-title-icon iconfont icon-shouxin fl"></span>
              <span class="publicity-title-text fl">守信激励与失信惩戒</span>
            </div>
            <div class="publicity-link hover-block fl">
              <div class="publicity-link-box fl">
                <a class="publicity-link-item"
                  href="/news?type={php} echo marmot_encode(NEWS_TYPE['TRUSTWORTHY_INFORMATION']){/php}">
                  <img src="{#portal_url#}/img/publicity/credit-sx.jpeg" alt="">
                  <p>守信信息</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item"
                  href="/news?type={php} echo marmot_encode(NEWS_TYPE['SAFETY_ACCIDENT']){/php}">
                  <img src="{#portal_url#}/img/publicity/credit-aq.jpeg" alt="">
                  <p>安全信息</p>
                </a>
              </div>
              <div class="publicity-link-box fl">
                <a class="publicity-link-item"
                  href="/news?type={php} echo marmot_encode(NEWS_TYPE['BAD_PHENOMENON']){/php}">
                  <img src="{#portal_url#}/img/publicity/credit-bl.jpeg" alt="">
                  <p>不良现象</p>
                </a>
              </div>
            </div>
          </div>

          <!-- 企业统一社会信用代码公示 -->
          <div class="publicity-list">
            <div class="publicity-title fl">
              <span class="publicity-title-icon iconfont icon-shehui fl"></span>
              <span class="publicity-title-text fl">企业统一社会信用代码公示</span>
            </div>
            <div class="publicity-link hover-block fl">
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="/enterprises?type=unifiedSocialCreditCode">
                  <img src="{#portal_url#}/img/publicity/credit-tongy.jpeg" alt="">
                  <p>企业统一社会信用代码公示</p>
                </a>
              </div>
            </div>
          </div>

          <!-- 目录导出 -->
          <div class="publicity-list">
            <div class="publicity-title fl">
              <span class="publicity-title-icon iconfont icon-daochu fl"></span>
              <span class="publicity-title-text fl">目录导出</span>
            </div>
            <div class="publicity-link hover-block fl">
              <div class="publicity-link-box fl">
                <a class="publicity-link-item" href="javascript:;">
                  <img src="{#portal_url#}/img/publicity/credit-daochu.png" alt="">
                  <p>目录导出</p>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  {/block}
