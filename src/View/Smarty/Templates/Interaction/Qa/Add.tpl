<{extends file='../../Layout/Main.tpl' }>
  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/interaction/interaction-form.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <!-- 面包屑导航 -->
      <div class="breadcrumb-container">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/serviceHalls/index">办事服务</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">信用问答</span>
      </div>

      <div class="tabs-container interaction-form-container">
        <div class="tab-head">
          <h3 class="tab-head-item is-active">我要提问</h3>
        </div>
        <div class="tab-body">
          <form class="layui-form interaction-form" action="javascript:;">
            <div class="layui-form-item">
              <label class="layui-form-label">标题：</label>
              <div class="layui-input-block">
                <input type="text" name="title" id="title" placeholder="请输入标题" autocomplete="off" minlength="1"
                  maxlength="150" class="layui-input">
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">内容：</label>
              <div class="layui-input-block">
                <textarea name="content" id="content" placeholder="请输入内容" autocomplete="off" minlength="1"
                  maxlength="2000" rows="4" class="layui-textarea"></textarea>
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">受理委办局：</label>
              <div class="layui-input-block">
                <select name="acceptUserGroupId" id="acceptUserGroup" lay-filter="acceptUserGroupId"
                  lay-filter="acceptUserGroupId"></select>
              </div>
            </div>
            <div class="layui-form-item action-bar">
              <button class="layui-btn btn-submit layui-btn-danger" id="btnSubmit">确认提交</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/js/interaction/qa/qa-add.js"></script>
  {/block}
