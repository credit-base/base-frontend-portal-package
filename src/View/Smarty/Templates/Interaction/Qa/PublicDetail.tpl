<{extends file='../../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/interaction/interaction-detail.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="breadcrumb-container mb-15">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/serviceHalls/index">办事服务</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <a class="breadcrumb-link" href="/publicQas">信用问答</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <a class="breadcrumb-link" href="javascript:;">详情</a>
      </div>

      <div class="publicity-detail">
        <div class="publicity-detail-title">
          <span class="iconfont icon-gongshi">
            信用问答详情
          </span>
        </div>
        <!--问答信息-->
        <table class="table-detail-container">
          <tbody>
            <tr>
              <th>标题：</th>
              <td>{$data['title']}</td>
            </tr>
            <tr>
              <th>内容：</th>
              <td>{$data['content']}</td>
            </tr>
            <tr>
              <th>受理委办局：</th>
              <td>{$data['acceptUserGroup']['name']}</td>
            </tr>
            <tr>
              <th>最后更新时间：</th>
              <td>{$data['updateTimeFormat']}</td>
            </tr>
          </tbody>
        </table>

         <!--受理信息-->
         <table class="table-detail-container">
          <tbody>
            <tr>
              <th>受理委办局：</th>
              <td>{$data['acceptUserGroup']['name']}</td>
            </tr>
            <tr>
              <th>受理情况：</th>
              <td>{$data['admissibility']['name']}</td>
            </tr>
            <tr>
              <th>受理时间：</th>
              <td>{$data['reply']['createTimeFormat']}</td>
            </tr>
            <tr>
              <th>回复内容：</th>
              <td>{$data['reply']['content']}</td>
            </tr>
            <tr>
              <th>回复图片：</th>
              <td>
                <div class="credit-photo-gallery layui-row layui-col-space15">
                  {foreach $data['reply']['images'] as $item}
                  <div class="layui-col-xs6 layui-col-sm6 layui-col-md4">
                    <div class="credit-photo-cover">
                      <img src="{#portal_file_url#}{$item['identify']}" data-image-preview="creditPhoto">
                    </div>
                  </div>
                  {/foreach}
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {/block}
