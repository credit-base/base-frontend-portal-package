<{extends file='../../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/interaction/interaction-detail.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="breadcrumb-container mb-15">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/serviceHalls/index">办事服务</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <a class="breadcrumb-link" href="/publicPraises">信用表扬</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <a class="breadcrumb-link" href="javascript:;">详情</a>
      </div>

      <div class="publicity-detail">
        <div class="publicity-detail-title">
          <span class="iconfont icon-gongshi">
            信用表扬详情
          </span>
        </div>
        <table class="table-detail-container">
          <tbody>
            <tr>
              <th>标题：</th>
              <td>{$data['title']}</td>
            </tr>
            <tr>
              <th>内容：</th>
              <td>{$data['content']}</td>
            </tr>
            <tr>
              <th>反馈人真实姓名/企业名称：</th>
              <td>{$data['name']}</td>
            </tr>
            <tr>
              <th>统一社会信用代码/反馈人身份证号：</th>
              <td>{if $data.type.id eq MQ}{$data.identify}{else}{$data.identifyMask}{/if}</td>
            </tr>
            <tr>
              <th>被表扬主体：</th>
              <td>{$data['subject']}</td>
            </tr>
            <tr>
              <th>被表扬类型：</th>
              <td>{$data['type']['name']}</td>
            </tr>
            <tr>
              <th>联系方式：</th>
              <td>{$data['contact']}</td>
            </tr>
            <tr>
              <th>图片：</th>
              <td>
                <div class="credit-photo-gallery layui-row layui-col-space15">
                  {foreach $data['images'] as $item}
                  <div class="layui-col-xs6 layui-col-sm6 layui-col-md4">
                    <div class="credit-photo-cover">
                      <img src="{#portal_file_url#}{$item['identify']}" data-image-preview="creditPhoto">
                    </div>
                  </div>
                  {/foreach}
                </div>
              </td>
            </tr>
            <tr>
              <th>受理委办局：</th>
              <td>{$data['acceptUserGroup']['name']}</td>
            </tr>
            <tr>
              <th>最后更新时间：</th>
              <td>{$data['updateTimeFormat']}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {/block}
