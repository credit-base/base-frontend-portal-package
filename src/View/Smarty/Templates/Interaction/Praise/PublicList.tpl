<{extends file='../../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/interaction/public-praise-list.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="main" id="main">
        <div class="container">
          <div class="breadcrumb-container mb-15">
            <span class="breadcrumb-label">您所在位置：</span>
            <a class="breadcrumb-link" href="/serviceHalls/index">办事服务</a>
            <span class="breadcrumb-seperator">&gt;&gt;</span>
            <a class="breadcrumb-link" href="javascript:;">信用表扬</a>
          </div>

          <div class="clearfix">
            <div class="float-right">
              <a href="/praises/add" class="layui-btn layui-btn-danger btn-add-praise">我要表扬</a>
            </div>
            <div class="float-left">
              <form class="search-form" action="javascript:;">
                <div class="layui-form-item">
                  <div class="layui-inline">
                    <div class="layui-input-inline">
                      <input type="text" name="title" id="inputSearch" autocomplete="off" class="layui-input"
                        placeholder="请输入标题名称">
                    </div>
                    <button class="layui-btn layui-btn-danger" id="btnSearch" type="button">
                      <span class="iconfont icon-search"></span>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <div class="tabs-container public-praise-list">
            <div class="tab-head">
              <h3 class="tab-head-item is-active">信用表扬</h3>
            </div>
            <div class="tab-body">
              <table class="layui-table interaction-list" id="praiseList" lay-size="lg">
                <thead>
                  <tr>
                    <th>标题</th>
                    <th>被表扬类型</th>
                    <th>被表扬主体</th>
                    <th>受理委办局</th>
                    <th>最后更新时间</th>
                  </tr>
                </thead>
                <tbody>
                  {if !empty($data['list'])}
                  {foreach $data['list'] as $item}
                  <tr>
                    <td><a class="link-hover-primary" href="/publicPraises/{$item.id}">{$item.title}</a></td>
                    <td><a class="link-hover-primary" href="/publicPraises/{$item.id}">{$item.type.name}</a></td>
                    <td><a class="link-hover-primary" href="/publicPraises/{$item.id}">{$item.name}</a></td>
                    <td><a class="link-hover-primary" href="/publicPraises/{$item.id}">{$item.acceptUserGroup.name}</a>
                    </td>
                    <td><a class="link-hover-primary" href="/publicPraises/{$item.id}">{$item.updateTimeFormat}</a></td>
                  </tr>
                  {/foreach}
                  {else}
                  <tr>
                    <td colspan="10">
                      <div class="empty-data-container">
                        <div class="empty-data-icon"></div>
                        <h4 class="empty-data-tip">暂无内容</h4>
                      </div>
                    </td>
                  </tr>
                  {/if}
                </tbody>
              </table>
              <!-- pagination -->
              <div class="pagination-container">
                <input type="hidden" id="pagination_init_total" value="{$data['total']}">
                <div class="pagination" id="pagination"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/js/interaction/praise/public-praise.js"></script>
  {/block}
