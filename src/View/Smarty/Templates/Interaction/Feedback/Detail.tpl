<{extends file='../../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/user/index.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <!-- 面包屑导航 -->
      <div class="breadcrumb-container">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/members/edit">个人中心</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">问题反馈</span>
      </div>

      <!-- 侧导航 -->
      {marmot_widget widget="Base\Package\Member\Controller\WidgetController" func=memberWidget parameters=8}
      <div class="home-content">
        <!-- 主体内容 -->
        <div class="panel home-content-panel">
          <div class="panel-header">
            <h3 class="panel-title">
              我的问题反馈
            </h3>
          </div>
          <div class="panel-body">
            <div class="clearfix">
              <div class="float-right">
                {if $data.status.id eq Lw && $data.acceptStatus.id eq Lw}
                <button class="layui-btn layui-btn-sm layui-btn-primary layui-border-red" id="btnRevoke"
                  data-id="{$data.id}">撤销</button>
                {/if}
              </div>
            </div>
            {if $data.status.id != LC0}
            <div class="recording-detail-header">
              <h3 class="title">
                <span class="text">
                  受理信息
                </span>
              </h3>
            </div>
            <table class="recording-detail-table">
              <tbody>
                <tr>
                  <th>受理状态：</th>
                  <td>{$data['acceptStatus']['name']}</td>
                </tr>
                <tr>
                  <th>受理委办局：</th>
                  <td>{$data['acceptUserGroup']['name']}</td>
                </tr>
                <!-- 受理状态为受理完成才显示 -->
                {if $data['acceptStatus']['id'] eq MQ}
                <tr>
                  <th>受理情况：</th>
                  <td>{$data['admissibility']['name']}</td>
                </tr>
                <tr>
                  <th>受理时间：</th>
                  <td>{$data['reply']['createTimeFormat']}</td>
                </tr>
                <tr>
                  <th>回复内容：</th>
                  <td>{$data['reply']['content']}</td>
                </tr>
                <tr>
                  <th>回复图片：</th>
                  <td>
                    <div class="credit-photo-gallery layui-row layui-col-space15">
                      {foreach $data['reply']['images'] as $item}
                      <div class="layui-col-xs6 layui-col-sm6 layui-col-md4">
                        <div class="credit-photo-cover">
                          <img src="{#portal_file_url#}{$item['identify']}" data-image-preview="creditPhoto">
                        </div>
                      </div>
                      {/foreach}
                    </div>
                  </td>
                </tr>
                {/if}
              </tbody>
            </table>
            {/if}
            <div class="recording-detail-header">
              <h3 class="title">
                <span class="text">
                  问题反馈信息
                </span>
              </h3>
            </div>
            <table class="recording-detail-table">
              <tbody>
                <tr>
                  <th>标题：</th>
                  <td>{$data['title']}</td>
                </tr>
                <tr>
                  <th>内容：</th>
                  <td>{$data['content']}</td>
                </tr>
                <tr>
                  <th>状态：</th>
                  <td>
                    <span
                      class="layui-badge layui-bg-{if $data.status.id neq LC0}green{else}gray{/if}">{$data.status.name}</span>
                  </td>
                </tr>
                <tr>
                  <th>最后更新时间：</th>
                  <td>{$data['updateTimeFormat']}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- 主体内容 -->
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/js/interaction/feedback/feedback-detail.js"></script>
  {/block}
