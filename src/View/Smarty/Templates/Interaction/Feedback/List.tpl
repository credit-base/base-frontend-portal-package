<{extends file='../../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/user/index.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <!-- 面包屑导航 -->
      <div class="breadcrumb-container">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/members/edit">个人中心</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">问题反馈</span>
      </div>

      <!-- 侧导航 -->
      {marmot_widget widget="Base\Package\Member\Controller\WidgetController" func=memberWidget parameters=8}
      <div class="home-content">
        <!-- 主体内容 -->
        <div class="panel home-content-panel">
          <div class="panel-header">
            <h3 class="panel-title">
              我的问题反馈
            </h3>
          </div>
          <div class="panel-body">
            <table lay-even class="layui-table recording-list-table" id="feedbackList">
              <colgroup>
                <col width="80">
                <col>
              </colgroup>
              <thead>
                <tr>
                  <th>标题</th>
                  <th>受理委办局</th>
                  <th>受理状态</th>
                  <th>受理情况</th>
                  <th>状态</th>
                  <th>操作</th>
                </tr>
              </thead>
              <tbody>
                {if !empty($data.list)}
                {foreach $data.list as $item}
                <tr>
                  <td><a href="/feedbacks/{$item['id']}">{$item.title}</a></td>
                  <td>{$item.acceptUserGroup.name}</td>
                  <td>{if $item.status.id eq LC0}--{else}{$item.acceptStatus.name}{/if}</td>
                  <td>
                    {if $item.acceptStatus.id neq MQ || $item.status.id eq LC0}
                      --
                    {else}
                      {$item.admissibility.name}
                    {/if}
                  </td>
                  <td>
                    <span
                      class="layui-badge layui-bg-{if $item.status.id neq LC0}green{else}gray{/if}">{$item.status.name}</span>
                  </td>
                  <td>
                    {if $item.status.id eq Lw && $item.acceptStatus.id eq Lw}
                    <button class="layui-btn layui-btn-sm layui-btn-primary layui-border-red btn-revoke"
                      data-id="{$item['id']}">撤销</button>
                    {/if}
                    <a href="/feedbacks/{$item['id']}"
                      class="layui-btn layui-btn-sm layui-btn-primary layui-border-black">查看</a>
                  </td>
                </tr>
                {/foreach}
                {else}
                <tr>
                  <td colspan="10">
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无内容</h4>
                    </div>
                  </td>
                </tr>
                {/if}
              </tbody>
            </table>
            <!-- pagination -->
            <div class="pagination-container">
              <input type="hidden" id="pagination_init_total" value="{$data['total']}">
              <div class="pagination" id="pagination"></div>
            </div>
          </div>
        </div>
        <!-- 主体内容 -->
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/js/interaction/feedback/feedback-list.js"></script>
  {/block}
