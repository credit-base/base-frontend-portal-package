<{extends file='../../Layout/Main.tpl' }>
  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/interaction/interaction-form.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <!-- 面包屑导航 -->
      <div class="breadcrumb-container">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/creditPublicities/index">信息公示</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">异议申诉</span>
      </div>

      <div class="tabs-container interaction-form-container">
        <ul class="tab-head js-appeal-tabs">
          <li class="tab-head-item is-active" data-type="MQ">企业申诉</li>
          <li class="tab-head-item" data-type="MA">个人申诉</li>
        </ul>
        <div class="tab-body tab-body-item is-active">
          <form class="layui-form interaction-form" action="javascript:;">
            <div class="layui-form-item">
              <label class="layui-form-label">标题：</label>
              <div class="layui-input-block">
                <input type="text" name="title" id="title" placeholder="请输入标题" autocomplete="off" minlength="1"
                  maxlength="150" class="layui-input">
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label" id="nameLabel">企业名称：</label>
              <div class="layui-input-block">
                <input type="text" name="name" id="name" placeholder="请输入企业名称" autocomplete="off" minlength="2"
                  maxlength="50" class="layui-input">
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label" id="identifyLabel">统一社会信用代码：</label>
              <div class="layui-input-block">
                <input type="text" name="identify" id="identify" placeholder="请输入统一社会信用代码" autocomplete="off"
                  minlength="15" maxlength="18" class="layui-input">
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">联系电话：</label>
              <div class="layui-input-block">
                <input type="tel" name="contact" id="contact" placeholder="请输入联系电话" autocomplete="off"
                  class="layui-input">
              </div>
            </div>
            <div class="layui-form-item is-hidden" id="idCardForm">
              <label class="layui-form-label">身份证：</label>
              <div class="layui-input-block">
                <div class="picture-card clearfix">
                  <div class="picture-card__item positive-card float-left" id="positiveCard">
                    <div class="picture-card__item-picture" id="positiveCardUpload"></div>
                  </div>
                  <div class="picture-card__item reverse-card float-right" id="reverseCard">
                    <div class="picture-card__item-picture" id="reverseCardUpload"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="layui-form-item" id="businessLicenseForm">
              <label class="layui-form-label">营业执照：</label>
              <div class="layui-input-block">
                <div class="picture-card">
                  <div class="picture-card__item business-license-card" id="certificates">
                    <div class="picture-card__item-picture" id="businessLicenseUpload"></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">内容：</label>
              <div class="layui-input-block">
                <textarea name="content" id="content" placeholder="请输入内容" autocomplete="off" minlength="1"
                  maxlength="2000" rows="4" class="layui-textarea"></textarea>
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">图片：</label>
              <div class="layui-input-block">
                <div class="file-container">
                  <ul class="file-list" id="uploadFileList">
                    <li class="file-list-item__action">
                      <div id="fileUpload">
                        <span class="iconfont icon-add"></span>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="layui-form-item">
              <label class="layui-form-label">受理委办局：</label>
              <div class="layui-input-block">
                <select name="acceptUserGroupId" id="acceptUserGroup" lay-filter="acceptUserGroupId"
                  lay-filter="acceptUserGroupId"></select>
              </div>
            </div>
            <div class="layui-form-item action-bar">
              <button class="layui-btn btn-submit layui-btn-danger" id="btnSubmit">确认提交</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/js/common/jquery-uploader.js"></script>
  <script src="{#portal_url#}/js/interaction/appeal/appeal-add.js"></script>
  {/block}
