<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        html {
            /*width: 210mm;*/
            height: 22mm;
        }
        body {
            border:0;
            margin: 0;
        }
        .report-head {
            margin: auto;
            width: 210mm;
            height: 22mm;
            padding: 5mm 10mm 0;
            box-sizing: border-box;
            background-size: 210mm;
            background-image: url('https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/report/report_head_bg.png');
            /*background-color: #f00;*/
        }
        .report-head .box {
            border-bottom: 1px dashed #000;
            padding-bottom: 5mm;
        }
        .report-head .box img {
            width: 60mm;
            vertical-align: bottom;
        }
        .report-head .num {
            float: right;
            margin-top: 2mm;
        }
        .report-head .num p {
            margin: 0;
            font-size: 3mm;
            line-height: 5mm;
        }
    </style>
    <title>页眉</title>
</head>
<body onload="subst()">
    <!--征信报告/页眉-->
    <div class="report-head">
        <div class="box">
            <img src="https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/report/report_home_logo.png">
            <div class="num">
                <p>报告编号：{$code}</p>
                <p>生成时间：{$date}</p>
                <span class="page"></span>
                <span class="topage"></span>
            </div>
        </div>
    </div>
    <script>
        function subst() {
            var vars = {};
            const box = document.querySelector('.report-head')

            var query_strings_from_url = document.location.search.substring(1).split('&');
            for (var query_string in query_strings_from_url) {
                if (query_strings_from_url.hasOwnProperty(query_string)) {
                    var temp_var = query_strings_from_url[query_string].split('=', 2);
                    vars[temp_var[0]] = decodeURI(temp_var[1]);
                }
            }
            var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages'];
            for (var css_class in css_selector_classes) {
                if (css_selector_classes.hasOwnProperty(css_class)) {
                    var element = document.getElementsByClassName(css_selector_classes[css_class]);
                    for (var j = 0; j < element.length; ++j) {
                        const page = vars[css_selector_classes[css_class]]
                        if(page == 1) {
                            box.innerHTML = ''
                        }
                    }
                }
            }
        }
    </script>
</body>
</html>
