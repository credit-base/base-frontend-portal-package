<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/credit-pat/list.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="news-content-container">
        <div class="news-main-container">
          <div class="credit-pat-container">
            <div class="credit-pat-header">
              <div class="header-text">信用随手拍</div>
              <a class="header-release" href="/creditPhotographys/add">我要发布</a>
            </div>
            {if !empty($list.data)}
            <div class="credit-pat-body">
              <ul class="credit-pat-list" id="credit_pat_list">
                {foreach $list.data as $item}
                <li class="list-item">
                  <a href="/creditPhotographys/{$item.id}" class="list-item-link">
                    {* 图片/视频展示 *}
                    {if !empty($item['attachments'][0]['identify'])}
                    <img class="list-item-img" src="{#portal_file_url#}{$item['attachments'][0]['identify']}">
                    {else}
                    <img class="list-item-img"
                      src="https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/ossSandbox/202022103257c75c10385343fbf2bbd0cb16b27eca8065388f02.jpg">
                    {/if}
                    {* 图片/视频展示 *}
                    <div class="list-item-description">{$item.description}</div>
                    <div class="list-item-info">
                      {* <img class="info-logo"
                        src="https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/ossSandbox/90670845.png"> *}
                      <div class="info-description">
                        <span>由</span>
                        <span class="info-name">{$item.member.realName}</span>
                        <span>发表于</span>
                        <span class="info-time">{$item.updateTimeFormat}</span>
                      </div>
                    </div>
                  </a>
                </li>
                {/foreach}
              </ul>
            </div>
            <div class="pagination-container">
              <input type="hidden" id="pagination_init_total" value="{$list.total}">
              <div class="pagination" id="pagination"></div>
            </div>
            {else}
            <div class="empty-data-container">
              <div class="empty-data-icon"></div>
              <h4 class="empty-data-tip">暂无内容</h4>
            </div>
            {/if}
          </div>
        </div>
        <!-- 侧栏 -->
        {marmot_widget widget="Base\Package\News\Controller\WidgetController" func=newsWidget}
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/masonry/masonry.pkgd.min.js"></script>
  <script src="{#portal_url#}/js/credit-pat/list.js"></script>
  {/block}
