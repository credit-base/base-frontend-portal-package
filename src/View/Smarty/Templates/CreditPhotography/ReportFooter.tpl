<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>页脚</title>
    <style>
        html {
            height: 15mm;
        }
        body {
            margin: 0;
            border:0;
        }
        .report-page {
            width: 210mm;
            height: 15mm;
            line-height: 15mm;
            box-sizing: border-box;
            margin: auto;
            text-align: center;
            background-image: url("https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/report/report_foot_bg.png");
            background-size: 210mm;
            /*background-color: #000;*/
        }
    </style>
</head>
<body onload="subst()">
    <div class="report-page">
        第 <span class="page"></span> 页
        共 <span class="topage"></span> 页
    </div>
    <script>
        function subst() {
            var vars = {};
            const pageDom = document.querySelector('.report-page')
            var query_strings_from_url = document.location.search.substring(1).split('&');
            for (var query_string in query_strings_from_url) {
                if (query_strings_from_url.hasOwnProperty(query_string)) {
                    var temp_var = query_strings_from_url[query_string].split('=', 2);
                    vars[temp_var[0]] = decodeURI(temp_var[1]);
                }
            }
            var css_selector_classes = ['page', 'frompage', 'topage', 'webpage', 'section', 'subsection', 'date', 'isodate', 'time', 'title', 'doctitle', 'sitepage', 'sitepages'];
            for (var css_class in css_selector_classes) {
                if (css_selector_classes.hasOwnProperty(css_class)) {
                    var element = document.getElementsByClassName(css_selector_classes[css_class]);
                    const page = vars[css_selector_classes[css_class]] - 1
                    for (var j = 0; j < element.length; ++j) {
                        if(page == 0) {
                            pageDom.innerHTML = ''
                        }
                        element[j].textContent = page;
                    }
                }
            }
        }
    </script>
</body>
</html>
