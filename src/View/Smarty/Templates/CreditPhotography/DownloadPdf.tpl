<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        html {
            /*width: 210mm;*/
        }
        body {
            margin: 0;
            font-size: 12px;
        }
        @media print {
            .new-page {
                page-break-before: always;
            }
        }
        .report-head {
            width: 190mm;
            height: 22mm;
            box-sizing: border-box;
        }
        .report-head .box {
            border-bottom: 1px dashed #000;
            padding-bottom: 5mm;
        }
        .report-head .box img {
            width: 60mm;
            vertical-align: bottom;
        }
        .report-head .num {
            float: right;
            margin-top: 2mm;
        }
        .report-head .num p {
            margin: 0;
            font-size: 3mm;
            line-height: 5mm;
        }
        .report-home table td {
            padding: 5px;
        }
        .report-home {
            margin: auto;
            width: 210mm;
            height: 260.1mm;
            padding: 5mm 10mm;
            box-sizing: border-box;
            background-image: url('https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/report/report_body_bg.png');
            background-repeat: no-repeat;
            background-size: 210mm 260.1mm;
            /*background-color: #0f0;*/
        }
        .report-home table td {
            padding: 3mm 7mm;
            text-align: left;
        }
        .report-summary {
            margin: auto;
            width: 210mm;
            height: 260.1mm;
            padding: 5mm 10mm;
            box-sizing: border-box;
            background-image: url('https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/report/report_body_bg.png');
            background-size: 210mm 260.1mm;
        }
        .report-summary .base table td {
            padding: 5px;
        }
        .report-summary .common table td {
            padding: 5px;
            text-align: left;
        }
        .report-summary .report-head {
            width: 190mm;
            height: 22mm;
            box-sizing: border-box;
        }
        .report-summary .report-head .box {
            border-bottom: 1px dashed #000;
            padding-bottom: 5mm;
        }
        .report-summary .report-head .box img {
            width: 60mm;
            vertical-align: bottom;
        }
        .report-summary .report-head .num {
            float: right;
            margin-top: 2mm;
        }
        .report-summary .report-head .num p {
            margin: 0;
            font-size: 3mm;
            line-height: 5mm;
        }
        .report-summary .title {
            text-align: center;
            height: 25mm;
            position: relative;
            line-height: 25mm;
            font-size: 10mm;
            margin: 10mm auto;
            font-weight: bold;
        }
        .report-summary .title img{
            position:absolute;
            top: 0;
            right: 0;
            width: 25mm;
        }
        .report-summary .status {
            color: #fff;
            text-align: right;
            margin: 4mm auto 7mm;
            line-height: 6mm;
            overflow: hidden;
        }
        .report-summary .status .name{
            color: #000;
            float: left;
            font-size: 6mm;
            font-weight: bold;
        }
        .report-summary .status .state-icon{
            padding: .5mm 1mm;
            margin-left: 3mm;
            font-size: 3mm;
        }
        .report-summary .base table td,
        .report-summary .common table td{
            font-size: 3mm;
            padding: 4mm;
            text-align: left;
        }
        .report-summary .base table .bold,
        .report-summary .common table .bold{
            font-weight: bold;
        }
        .report-summary .text .head {
            margin: 7mm auto 4mm;
            font-weight: bold;
        }
        .report-summary .text .content {
            margin: 4mm auto 0;
            font-size: 3mm;
            border: 1px solid #000;
            padding: 3mm;
            line-height: 7mm;
        }
        .report-main {
            margin: auto;
            width: 210mm;
            padding: 5mm 10mm;
            box-sizing: border-box;
            background-image: url("https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/report/report_body_bg.png");
            background-repeat: repeat-y;
            background-size: 210mm 260.1mm;
        }
        .report-main .base-info div {
            overflow: hidden;
        }
        .report-main .base-info div span {
            float: left;
            text-align: left;
        }
        .report-main .list-info {
            border: 1px solid #000;
            padding: 10px;
        }
        .report-main .list-info div {
            position: relative;
            overflow: hidden;
            line-height: 30px;
        }
        .report-main .list-info .box {
            border-bottom: 1px dashed #000;
        }
        .report-main .list-info .box:last-child {
            border-bottom: none;
        }
        .report-main .list-info div div {
            overflow: hidden;
        }
        .content-span {
            float: left;
            width: 70%;
        }
        .box .title {
            position: relative;
            overflow: visible;
        }
        .label {
            color: #f90000;
            background-color: #f90000;
        }
        .info-label {
            color: #f90000;
        }
        .list-title {
            font-weight: bold;
            margin: 25px auto 15px;
        }
        .red-text {
            color: #f00;
        }
        .no-content {
            min-height: 60px;
        }
        .left-content {
            float: left;
            width: 30%;
        }
        .table-content {
            text-align: center;
            width: 100%;
        }
        .common-title {
            margin: 7mm auto 3mm;
            font-weight: bold;
        }
        .title-label {
            font-weight: bold;
            position: absolute;
            right: 30px;
            top: 30px;
        }
        /* 最下面的结束样式 */
        .end-box {
            padding: 0 2mm;
            margin: 5mm 1mm 0;
            position:relative;
            border: 1px dashed #c5c5c5;
        }
        .end-text {
            position: absolute;
            top: -2.3mm;
            left: 50%;
            transform: translateX(-50%);
            font-weight: bold;
        }
    </style>
    <title>{$enterprise['name']}</title>
</head>
<body onload="subst()">
    <!--征信报告/封面-->
    <div class="report-home">
        <div class="head">
            <img style="height: 15mm;" src="https://credit-base.oss-cn-hangzhou.aliyuncs.com/portal-sandbox/img/report/report_home_logo.png" alt="">
            <img style="height: 25mm;float: right;" src="{$qrcode}" alt="">
        </div>
        <div class="content" style="text-align: center;">
            <h1 style="font-size: 40px;margin-top: 50mm;">
                法人和非法人组织公共信用信息报告
            </h1>
            <div style="margin-top: 15mm;line-height: 10mm;display: inline-block;text-align: left;">
                <div>
                    <span style="font-weight: bold;width: 150px;text-align: left;display: inline-block;">主体名称：</span>
                    <span style="text-align: left;display: inline-block;">{$enterprise['name']}</span>
                </div>
                <div>
                    <span style="font-weight: bold;width: 150px;text-align: left;display: inline-block;">统一社会信用代码：</span>
                    <span style="text-align: left;display: inline-block;">{$enterprise['unifiedSocialCreditCode']}</span>
                </div>
                <div>
                    <span style="font-weight: bold; width: 150px;text-align: left;display: inline-block;">报告编号：</span>
                    <span style="text-align: left;display: inline-block;">{$code}</span>
                </div>
            </div>
            <div style="text-align: center;margin: 85mm auto;" >
                <table cellspacing="0" style="background-color: #fff;margin: auto;">
                    <tr>
                        <td style="border: 1px solid #afafaf;">生成日期</td>
                        <td style="border: 1px solid #afafaf;border-left: none;">2021-01-02</td>
                    </tr>
                    <tr >
                        <td style="border: 1px solid #afafaf;border-top: none;">出具单位</td>
                        <td style="border: 1px solid #afafaf;border-left: none;border-top: none;">
                            {$publicUnit}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <!--征信报告/概要-->
    <div class="report-summary">

        <div class="title">
            <div>概要</div>
            <img src="{$qrcode}">
        </div>

        <div class="status">
            <span class="name">
              {$enterprise['name']}
            </span>

        {if !empty($enterprise['registrationStatus']) }
        <span class="state-icon" style="background-color: #209966;">
                {$enterprise['registrationStatus']}
            </span>
        {/if}

        {if 0 > 0}
        <span class="state-icon" style="background-color: #f90000;">
          守信激励对象
        </span>
        {/if}

        {if 0 > 0}
        <span class="state-icon" style="background-color: #000;">
          失信惩戒对象
        </span>
        {/if}

    </div>

        <div class="base">
            <p style="margin-bottom: 3mm;font-weight: bold;">基础信息</p>
            <table cellspacing="0" class="table-content">
                <tr>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">统一社会信用代码</td>
                    <td style="border-top: 1px solid #000;border-left: 1px solid #000;">{$enterprise['unifiedSocialCreditCode']}</td>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">法定代表人</td>
                    <td style="border: 1px solid #000;border-bottom: none;">{$enterprise['principal']}</td>
                </tr>
                <tr>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">企业类型</td>
                    <td style="border-top: 1px solid #000;border-left: 1px solid #000;">{$enterprise['enterpriseType']}</td>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">成立日期</td>
                    <td style="border: 1px solid #000;border-bottom: none;">{$enterprise['establishedDate']}</td>
                </tr>
                <tr>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">登记机关</td>
                    <td style="border-top: 1px solid #000;border-left: 1px solid #000;">{$enterprise['registrationAuthority']}</td>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">住所</td>
                    <td style="border: 1px solid #000;border-bottom: none;"></td>
                </tr>
                <tr>
                    <td class="bold" style="border: 1px solid #000;border-right: none;">存续状态</td>
                    <td colspan="3" style="border: 1px solid #000;">{$enterprise['registrationStatus']}</td>
                </tr>
            </table>
        </div>

        <div class="common">
            <p class="common-title">公共信用信息</p>
            <table cellspacing="0" class="table-content">
                <tr>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">行政许可信息</td>
                    <td style="border-top: 1px solid #000;border-left: 1px solid #000;">0条</td>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">行政处罚信息</td>
                    <td style="border: 1px solid #000;border-bottom: none;">0条</td>
                </tr>
                <tr>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">守信激励信息</td>
                    <td style="border-top: 1px solid #000;border-left: 1px solid #000;">0条</td>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">失信惩戒信息</td>
                    <td style="border: 1px solid #000;border-bottom: none;">0条</td>
                </tr>
                <tr>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">重点关注信息</td>
                    <td style="border-top: 1px solid #000;border-left: 1px solid #000;">0条</td>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">资质/资格信息</td>
                    <td style="border: 1px solid #000;border-bottom: none;">0条</td>
                </tr>
                <tr>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">风险提示信息</td>
                    <td style="border-top: 1px solid #000;border-left: 1px solid #000;">0条</td>
                    <td class="bold" style="border-top: 1px solid #000;border-left: 1px solid #000;">信用承诺</td>
                    <td style="border: 1px solid #000;border-bottom: none;">{$creditPromiseCount}条</td>
                </tr>
                <tr>
                    <td class="bold" style="border: 1px solid #000;border-right: none;">合同履约</td>
                    <td style="border: 1px solid #000;border-right: none;">0条</td>
                    <td class="bold" style="border: 1px solid #000;border-right: none;">其他信息</td>
                    <td style="border: 1px solid #000;">0条</td>
                </tr>
            </table>
        </div>

        <div class="text">
            <p class="head">说明</p>
            <div class="content">
                <div>
                    1.本文件客观地展示了各类主体的信用信息，“信用宜宾”承诺在数据汇总、加工、整合的过程中保持客观和中立。
                </div>
                <div>
                    2.您对本文件内容的正式性如存有疑问，可通过扫一扫核验码或登录“信用宜宾”网站（http://credit.yibin.gov.cn/）查询或核验。
                </div>
                <div >
                    3.如认为所展示信息存在错误、遗漏、公开期限不符合规定以及其他侵犯信息主体合法权益的，可按照网站“信用信息异议申诉”提出异议申诉；如需对相关行政处罚信息进行信用修复，可按照网站“行政处罚信息信用修复流程指引”提出信用修复申请。
                </div>
                <div >
                    4.本查询结果仅依现有数据展示相关信息，供社会参考使用。使用相关信息的单位和个人应对信息使用行为的合法性负责。
                </div>
                <div>
                    5.因篇幅优先，单类数据仅按更新程度展示前100条信息。
                </div>
            </div>
        </div>
    </div>

    <!--征信报告/正文-->
    <div class="report-main">
        <!--正文+二维码-->
        <div style="text-align: center;height: 100px;position: relative;line-height: 100px;font-size: 30px;margin: 20px auto 20px;font-weight: bold;">
        <div>正文</div>
        <img style="position:absolute;
          top: 0;
          right: 0;
          width: 100px;"
         src="{$qrcode}" alt="">
        </div>

        <!--基础信息-->
        <div style="color: #fff;text-align: right;margin: 10px auto 10px;line-height: 25px;">
        <div style="overflow: hidden;">

          <span style="color: #000;float: left;font-size: 6mm;font-weight: bold;">
             {$enterprise['name']}
          </span>

          {if !empty($enterprise['registrationStatus']) }
            <span style="background-color: #209966;padding: 2px 5px;margin-left: 10px;">
                {$enterprise['registrationStatus']}
            </span>
        {/if}

            {if 0 > 0}
            <span style="background-color: #f90000;padding: 2px 5px;margin-left: 10px;">
                    守信激励对象
                  </span>
                  {/if}
            {if 0 > 0}
            <span style="background-color: #000;padding: 2px 5px;margin-left: 10px;">
                失信惩戒对象
              </span>
               {/if}
        </div>

        <div>
            <p style="font-weight: bold;margin: 20px auto 10px;color: #000;text-align: left;">基础信息</p>
            <div class="base-info" style="border: 1px solid #000;padding: 10px;color: #000;">
                <div>
            <span class="left-content">
              企业名称：
            </span>
                    <span class="">
              {$enterprise['name']}
            </span>
                </div>
                <div>
            <span class="left-content">
              统一社会信用代码：
            </span>
                    <span>
              {$enterprise['unifiedSocialCreditCode']}
            </span>
                </div>
                <div>
            <span class="left-content">
              法定代表人/负责人/执行事务合伙人：
            </span>
                    <span>
               {$enterprise['principal']}
            </span>
                </div>
                <div>
            <span class="left-content">
              企业类型：
            </span>
                    <span>
              {$enterprise['enterpriseType']}
            </span>
                </div>
                <div>
            <span class="left-content">
              成立日期：
            </span>
                    <span>
             {$enterprise['establishedDate']}
            </span>
                </div>
                <div>
            <span class="left-content">
              登记机关：
            </span>
                    <span>
              {$enterprise['registrationAuthority']}
            </span>
                </div>
            </div>
        </div>
        </div>

        <!--行政许可-->
        <div>
            <p class="list-title">行政许可信息（共 <span class="red-text">0</span> 条）</p>
            <div class="list-info">
                {if !empty($resourceCatalogData['licensing'])}
                {foreach $resourceCatalogData['licensing'] as $key=>$resourceCatalog}
                <div class="box">
                <div class="title">
                  <span class="title-label">第 <span>{$key+1}</span> 条</span>
                </div>
                {foreach $resourceCatalog['detailedData'] as $k => $val}
                <div>
                  <span class="left-content">
                   {$val['name']}：
                  </span>
                  <span class="content-span">
                    {$val['value']}
                  </span>
                </div>
                {/foreach}
                </div>
                {/foreach}
                {else}
                <div class="box no-content">
                    查询期内无相关记录
                </div>
                {/if}
            </div>
        </div>

        <!--行政处罚-->
        <div>
            <p class="list-title">行政处罚信息（共 <span class="red-text">0</span> 条）</p>
            <div class="list-info">
                {if !empty($resourceCatalogData['sanction'])}
                {foreach $resourceCatalogData['sanction'] as $key=>$resourceCatalog}
                <div class="box">
                <div class="title">
                  <span class="title-label">第 <span>{$key+1}</span> 条</span>
                </div>
                {foreach $resourceCatalog['detailedData'] as $k => $val}
                <div>
                  <span class="left-content">
                   {$val['name']}：
                  </span>
                  <span class="content-span">
                    {$val['value']}
                  </span>
                </div>
                {/foreach}
                </div>
                {/foreach}
                {else}
                <div class="box no-content">
                    查询期内无相关记录
                </div>
                {/if}
            </div>
        </div>

        <!--红名单-->
        <div>
            <p class="list-title">守信激励信息（共 <span class="red-text">0</span> 条）</p>
            <div class="list-info">
                {if !empty($resourceCatalogData['red'])}
                {foreach $resourceCatalogData['red'] as $key=>$resourceCatalog}
                <div class="box">
                <div class="title">
                  <span class="label">1</span>
                  <span class="info-label">{$resourceCatalog['typeName']}</span>
                  <span class="title-label">第 <span>{$key+1}</span> 条</span>
                </div>
                {foreach $resourceCatalog['detailedData'] as $k => $val}
                <div>
                  <span class="left-content">
                    {$val['name']}：
                  </span>
                  <span class="content-span">
                    {$val['value']}
                  </span>
                </div>
                {/foreach}
                </div>
                {/foreach}
                {else}
                <div class="box no-content">
                    查询期内无相关记录
                </div>
                {/if}
            </div>
        </div>

        <!--黑名单-->
        <div>
            <p class="list-title">失信惩戒对象（共 <span class="red-text">0</span> 条）</p>
            <div class="list-info">
                {if !empty($resourceCatalogData['black'])}
                {foreach $resourceCatalogData['black'] as $key=>$resourceCatalog}
                <div class="box">
                <div class="title">
                  <span class="label">1</span>
                  <span class="info-label">{$resourceCatalog['typeName']}</span>
                  <span class="title-label">第 <span>{$key+1}</span> 条</span>
                </div>
                {foreach $resourceCatalog['detailedData'] as $k => $val}
                <div>
                  <span class="left-content">
                    {$val['name']}：
                  </span>
                  <span class="content-span">
                    {$val['value']}
                  </span>
                </div>
                {/foreach}
                </div>
                {/foreach}
                {else}
                <div class="box no-content">
                    查询期内无相关记录
                </div>
                {/if}
            </div>
        </div>

        <!--重点关注-->
        <div>
            <p class="list-title">重点关注信息（共 <span class="red-text">0</span> 条）</p>
            <div class="list-info">
                {if !empty($resourceCatalogData['ycjyml'])}
                {foreach $resourceCatalogData['ycjyml'] as $key=>$resourceCatalog}
                <div class="box">
                <div class="title">
                  <span class="label">1</span>
                  <span class="info-label">{$resourceCatalog['typeName']}</span>
                  <span class="title-label">第 <span>{$key+1}</span> 条</span>
                </div>
                {foreach $resourceCatalog['detailedData'] as $k => $val}
                <div>
                  <span class="left-content">
                    {$val['name']}：
                  </span>
                  <span class="content-span">
                    {$val['value']}
                  </span>
                </div>
                {/foreach}
                </div>
                {/foreach}
                {else}
                <div class="box no-content">
                    查询期内无相关记录
                </div>
                {/if}
            </div>
        </div>

        <!--信用承诺-->
        <div>
            <p class="list-title">信用承诺（共 <span class="red-text">0</span> 条）</p>
            <div class="list-info">
                {if !empty($creditPromises)}
                {foreach $creditPromises as $key=>$creditPromise}
                <div class="box">
                    <div class="title">
                        <span class="title-label">第 <span>{$key+1}</span> 条</span>
                    </div>
                    <div>
                      <span class="left-content">
                        信用承诺主体：
                      </span>
                        <span class="content-span">
                        {$creditPromise['subjectName']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        信用承诺主体的统一社会信用代码：
                      </span>
                        <span class="content-span">
                        {$creditPromise['unifiedSocialCreditCode']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        信用承诺类型：
                      </span>
                        <span class="content-span">
                        {$creditPromise['category']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        信用承诺时间：
                      </span>
                        <span class="content-span">
                        {$creditPromise['promiseTime']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        承诺状态：
                      </span>
                        <span class="content-span">
                        {$creditPromise['status']['label']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        受理单位：
                      </span>
                        <span class="content-span">
                        {$creditPromise['userGroup']['label']}
                      </span>
                    </div>
                </div>
                {/foreach}
                {else}
                <div class="box no-content">
                    查询期内无相关记录
                </div>
                {/if}
            </div>
        </div>

        <!--合同履约-->
        <div>
            <p class="list-title">合同履约（共 <span class="red-text">0</span> 条）</p>
            <div class="list-info">
                {if !empty($contractPerformances)}
                {foreach $contractPerformances as $key=>$contractPerformance}
                <div class="box">
                    <div class="title">
                        <span class="title-label">第 <span>{$key+1}</span> 条</span>
                    </div>
                    <div>
                      <span class="left-content">
                        合同名称：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['name']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        合同编号：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['contractNo']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        项目业主单位（合同甲方）：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['projectOwner']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        统一社会信用代码：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['unifiedSocialCreditCode']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        承建方1（合同乙方）：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['contractorOne']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        承建方1统一社会信用代码：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['unifiedSocialCreditCodeOne']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        承建方2（合同乙方）：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['contractorTwo']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        承建方2统一社会信用代码：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['unifiedSocialCreditCodeTwo']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        履约情况1：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['performanceOne']}
                      </span>
                    </div>
                    <div>
                      <span class="left-content">
                        履约情况2：
                      </span>
                        <span class="content-span">
                        {$contractPerformance['performanceTwo']}
                      </span>
                    </div>
                </div>
                {/foreach}
                {else}
                <div class="box no-content">
                    查询期内无相关记录
                </div>
                {/if}
            </div>
        </div>

        <!--其它-->
        <div>
            <p class="list-title">
                其他信息（共 <span class="red-text">0</span> 条）
            </p>
            <div class="list-info">
                {if !empty($resourceCatalogData['other'])}
                {foreach $resourceCatalogData['other'] as $key=>$resourceCatalog}
                <div class="box">
                <div class="title">
                  <span class="label">1</span>
                  <span class="info-label">{$resourceCatalog['typeName']}</span>
                  <span class="title-label">
                      第 <span>{$key+1}</span> 条
                  </span>
                </div>
                {foreach $resourceCatalog['detailedData'] as $k => $val}
                <div>
                  <span class="left-content">
                    {$val['name']}：
                  </span>
                  <span class="content-span">
                    {$val['value']}
                  </span>
                </div>
                {/foreach}
                </div>
                {/foreach}
                {else}
                <div class="box no-content">
                    查询期内无相关记录
                </div>
                {/if}
            </div>
        </div>

        <!--综合信用状况分析-->
        <div>
            <p class="list-title">
                综合信用状况分析（共 <span class="red-text">0</span> 条）
            </p>
            <div class="list-info">
                <div class="box no-content">
                    查询期内无相关记录
                </div>
            </div>
        </div>

        <!--信用状况提升建议-->
        <div>
            <p class="list-title">
                信用状况提升建议（共 <span class="red-text">0</span> 条）
            </p>
            <div class="list-info">
                <div class="box no-content">
                    查询期内无相关记录
                </div>
            </div>
        </div>

        <!--地方信用网站公共信用信息报告补充内容-->
        <div>
            <p class="list-title">
                地方信用网站公共信用信息报告补充内容（共 <span class="red-text">0</span> 条）
            </p>
            <div class="list-info">
                <div class="box no-content">
                    查询期内无相关记录
                </div>
            </div>
        </div>

        <!-- 结束 -->
        <div class="end-box">
            <span class="end-text">结束</span>
        </div>
    </div>

<script>
    const px = 983.05
    const a4Height = 260.1
    const mainElement = document.querySelector('.report-main')
    const mainElementHeight = mainElement.offsetHeight
    const page = Math.ceil( mainElementHeight / px )
    mainElement.style.height = page * a4Height + 'mm'

    function subst() {
        const px = 983.05
        const a4Height = 260.1
        const mainElement = document.querySelector('.report-main')
        const mainElementHeight = mainElement.offsetHeight
        const page = Math.ceil( mainElementHeight / px )
        mainElement.style.height = page * a4Height + 'mm'
    }
</script>
</body>
</html>
