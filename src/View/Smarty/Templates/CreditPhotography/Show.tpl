<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/static/libs/videojs/video-js.min.css">
  <link rel="stylesheet" href="{#portal_url#}/static/libs/share-js/css/share.min.css">
  <link rel="stylesheet" href="{#portal_url#}/css/credit-pat/detail.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="news-content-container">
        <div class="news-main-container">
          <div class="breadcrumb-container mb-15">
            <span class="breadcrumb-label">您所在位置：</span>
            <a class="breadcrumb-link" href="/creditPhotographys">信用随手拍</a>
            <span class="breadcrumb-seperator">&gt;&gt;</span>
            <span class="breadcrumb-current">详情</span>
          </div>
          <div class="credit-photo-detail">
            <div class="detail-header">
              <div class="detail-header-meta">
                <span class="meta-item">作者：{$data.member.realName}</span>
                <span class="meta-item">发布时间：{$data.updateTimeFormat}</span>
              </div>
              <div class="detail-header-share">
                <i class="iconfont icon-share"></i>
                <div class="social-share"></div>
              </div>
            </div>
            <div class="detail-body">
              <div class="credit-photo-description">
                <p>{$data.description}</p>
              </div>
              {* 视频展示 *}
              {if $data['type'] == 1}
              <div class="credit-photo-video">
                {foreach $data['attachments'] as $item}
                <video class="video-js vjs-big-play-centered" id="credit_photo_video" poster="{#portal_file_url#}{$item['identify']}">
                  <source src="{#portal_file_url#}{$item['identify']}" type="video/mp4">
                </video>
                {/foreach}
              </div>
              {/if}

              {* 图片展示 *}
              {if $data['type'] == 2}
              <div class="credit-photo-gallery layui-row layui-col-space15">
                {foreach $data['attachments'] as $item}
                <div class="layui-col-xs4 layui-col-sm12 layui-col-md4">
                  <div class="credit-photo-cover">
                    <img src="{#portal_file_url#}{$item['identify']}" data-image-preview="creditPhoto">
                  </div>
                </div>
                {/foreach}
              </div>
              {/if}
            </div>
          </div>
        </div>
        <!-- 侧栏 -->
        {marmot_widget widget="Base\Package\News\Controller\WidgetController" func=newsWidget}
      </div>
    </div>
  </div>
  {/block}


  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/videojs/video.min.js"></script>
  <script src="{#portal_url#}/js/credit-pat/detail.js"></script>
  <script src="{#portal_url#}/static/libs/share-js/js/jquery.share.min.js"></script>
  <script>
    const shareConfig = {
      disabled: ['google', 'facebook', 'twitter'],
      sites: ['wechat', 'weibo']
    }
    $('.social-share').share(shareConfig)
  </script>
  {/block}
