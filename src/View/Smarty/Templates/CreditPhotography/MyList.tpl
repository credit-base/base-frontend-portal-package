<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/user/index.css">
  <link rel="stylesheet" href="{#portal_url#}/css/user/user-pat.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <!-- 面包屑导航 -->
      <div class="breadcrumb-container">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/members/edit">个人中心</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">我的随手拍</span>
      </div>

      <!-- 侧导航 -->
      {marmot_widget widget="Base\Package\Member\Controller\WidgetController" func=memberWidget parameters=4}
      <div class="home-content">
        <!-- 主体内容 -->
        <div class="panel home-content-panel">
          <div class="panel-header">
            <h3 class="panel-title">
              我的随手拍
            </h3>
          </div>
          <div class="panel-body">
            <div class="pat-tabs js-pat-tabs">
              <div class="tab-head">
                <div class="tab-head-item is-active" data-count="{$list.statisticalNumber['approveTotal']}">
                  已通过（{$list.statisticalNumber['approveTotal']}）</div>
                <div class="tab-head-item" data-count="{$list.statisticalNumber['pendingTotal']}">
                  待审核（{$list.statisticalNumber['pendingTotal']}）</div>
                <div class="tab-head-item" data-count="{$list.statisticalNumber['rejectTotal']}">
                  已驳回（{$list.statisticalNumber['rejectTotal']}）</div>
              </div>

              <div class="tab-body">
                <div class="tab-body-item is-active">
                  <!-- list -->
                  <ul class="pat-list" id="js-pat-list">
                    {if !empty($list.data)}
                    {foreach $list.data as $item}
                    <li class="pat-list-item">
                      <a href="/members/creditPhotography/{$item.id}" class="pat-list-item-info">
                        {* 图片/视频展示 *}
                        <span class="pat-list-item-cover">
                          {if !empty($item['attachments'][0]['identify'])}
                          <img class="pat-list-item-img" src="{#portal_file_url#}{$item['attachments'][0]['identify']}">
                          {else}
                          <img class="pat-list-item-img"
                            src="https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/ossSandbox/202022103257c75c10385343fbf2bbd0cb16b27eca8065388f02.jpg">
                          {/if}
                        </span>
                        {* 图片/视频展示 *}
                        <div class="pat-description">{$item.description}</div>
                      </a>
                      <div class="pat-list-item-detail">
                        <span class="pat-date">{$item.updateTimeFormat}</span>
                        <button type="button" class="layui-btn layui-btn-sm layui-btn-danger js-btn-delete"
                          data-id="{$item.id}">
                          <i class="layui-icon layui-icon-delete"></i> 删除
                        </button>
                      </div>
                    </li>
                    {/foreach}
                    {else}
                    <div class="empty-data-container">
                      <div class="empty-data-icon"></div>
                      <h4 class="empty-data-tip">暂无内容</h4>
                    </div>
                    {/if}
                  </ul>
                  <!-- pagination -->
                  <div class="pagination-container">
                    <div class="pagination" id="pagination"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- 主体内容 -->
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/js/user/user-pat.js"></script>
  {/block}
