<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/credit-pat/form.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="breadcrumb-container credit-repair-breadcrumb">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/creditPhotographys">信用随手拍</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">我要发布</span>
      </div>

      <div class="tabs-container">
        <ul class="tab-head">
          <li class="tab-head-item is-active">信用随手拍</li>
        </ul>
        <div class="tab-body">
          <div class="tab-body-item is-active">
            <div class="form-container credit-pat">
              <div class="form-item">
                <span class="form-item-label">描述:</span>
                <div class="form-item-main">
                  <textarea class="form-textarea" id="textarea_content" maxlength="200"></textarea>
                  <p class="input-limit-tip">最长可输入200个字符，<span class="input-limit-count">0</span>/200</p>
                </div>
              </div>
              <div class="form-item">
                <span class="form-item-label">图片/视频:</span>
                <div class="form-item-main">
                  <div id="progressBar"></div>
                  <div class="image-list-upload-container">
                    <div class="image-upload-list" id="fileList"></div>
                    <div class="pick-files" id="uploadContainer">
                      <div id="pickFiles">
                        <span class="iconfont icon-add"></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-item-btn">
                <button class="form-btn" id="btn_submit" type="button" role="button">确认提交</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/plupload/js/plupload.full.min.js"></script>
  <script src="{#portal_url#}/js/credit-pat/add.js"></script>
  {/block}
