<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/user/index.css">
  <link rel="stylesheet" href="{#portal_url#}/static/libs/videojs/video-js.min.css">
  <link rel="stylesheet" href="{#portal_url#}/css/user/user-pat-detail.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <!-- 面包屑导航 -->
      <div class="breadcrumb-container">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/members/edit">个人中心</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">我的随手拍</span>
      </div>

      <!-- 侧导航 -->
      {marmot_widget widget="Base\Package\Member\Controller\WidgetController" func=memberWidget parameters=4}
      <div class="home-content">
        <!-- 主体内容 -->
        <div class="panel home-content-panel">
          <div class="panel-header">
            <h3 class="panel-title">
              我的随手拍
            </h3>
          </div>
          <div class="panel-body">
            <div class="user-pat-detail">
              <div class="clearfix">
                <div class="detail-item float-left">
                  <span class="detail-item-label">
                    审核状态：
                  </span>
                  <span class="detail-item-content">
                  {if $data.applyStatus.id == 'LC0'}
                    <span class="layui-badge">已驳回</span>
                  {/if}
                  {if $data.applyStatus.id == 'MQ'}
                    <span class="layui-badge layui-bg-green">已通过</span>
                  {/if}
                  {if $data.applyStatus.id == 'Lw'}
                    <span class="layui-badge layui-bg-gray">待审核</span>
                  {/if}
                  </span>
                </div>
                <div class="detail-item float-right">
                  <span class="detail-item-label">
                    更新时间：
                  </span>
                  <span class="detail-item-content">
                    {$data.updateTimeFormat}
                  </span>
                </div>
              </div>
              <!-- 备注：如果审核状态为已驳回才显示驳回原因 -->
              {if $data.applyStatus.id == 'LC0'}
              <div class="detail-item">
                <span class="detail-item-label">
                  驳回原因：
                </span>
                <span class="detail-item-content">
                  {$data.rejectReason}
                </span>
              </div>
              {/if}
              <div class="detail-item">
                <span class="detail-item-label">
                  随手拍描述：
                </span>
                <span class="detail-item-content">
                  {$data.description}
                </span>
              </div>

              {if !empty($data['attachments'])}
              <div class="detail-item">
                <span class="detail-item-label">
                  随手拍附件：
                </span>
                <span class="detail-item-content">
                  {* 图片展示 *}
                  {if $data['type'] == 2}
                  <div class="credit-photo-gallery layui-row layui-col-space15">
                  {foreach $data['attachments'] as $item}
                    <div class="layui-col-xs6 layui-col-sm6 layui-col-md4">
                      <div class="credit-photo-cover">
                        <img src="{#portal_file_url#}{$item['identify']}" data-image-preview="creditPhoto">
                      </div>
                    </div>
                  {/foreach}
                  </div>
                  {/if}

                  {* 图片展示 *}
                  {if $data['type'] == 1}
                  <div class="credit-photo-video">
                  {foreach $data['attachments'] as $item}
                    <video class="video-js vjs-big-play-centered" id="credit_photo_video"
                      poster="{#portal_file_url#}{$item['identify']}">
                      <source src="{#portal_file_url#}{$item['identify']}" type="video/mp4">
                    </video>
                  {/foreach}
                  </div>
                  {/if}
                </span>
              </div>
              {/if}
            </div>
          </div>
        </div>
        <!-- 主体内容 -->
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/videojs/video.min.js"></script>
  <script src="{#portal_url#}/js/user/user-pat.js"></script>
  {/block}
