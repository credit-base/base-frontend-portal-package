<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/publicity/detail.css">
  {/block}

  {block name=body}
  <div class="main" id="main">
    <div class="container">
      <div class="breadcrumb-container mb-15">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/creditPublicities/index">信息公示</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <a class="breadcrumb-link" {if $data['infoClassify']['id'] == 'MA'}href="/doublePublicity?scene=MA"{else}href="/doublePublicity?scene=MQ"{/if}>
          信用双公示{if $data['infoClassify']['id'] == 'MA'}（行政许可）{else}（行政处罚）{/if}
        </a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <span class="breadcrumb-current">详情</span>
      </div>
      <div class="publicity-detail">
        <div class="publicity-detail-title">
          <span class="iconfont icon-gongshi">
            {if $data['infoClassify']['id'] == 'MA'}行政许可详情{else}行政处罚详情{/if}
          </span>
        </div>
        <table class="table-detail-container">
          <tbody>
          {foreach $data['itemsData'] as $item}
            {if $item.dimension.id eq MA}
            <tr>
              <th>{$item['name']}：</th>
              <td>{$item['value']}</td>
            </tr>
            {/if}
          {/foreach}
          </tbody>
        </table>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/masonry/masonry.pkgd.min.js"></script>
  <script src="{#portal_url#}/js/credit-pat/list.js"></script>
  {/block}
