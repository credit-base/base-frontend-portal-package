<{extends file='../Layout/Main.tpl' }>

    {block name=pageStyle}
    <link rel="stylesheet" href="{#portal_url#}/static/libs/swiper/css/swiper.min.css">
    <link rel="stylesheet" href="{#portal_url#}/static/libs/videojs/video-js.min.css">
    <link rel="stylesheet" href="{#portal_url#}/css/home/index.css">
    {if $data.websiteCustomizes.content.memorialStatus eq 1}
    <style rel="stylesheet">
        html {
            filter: grayscale(100%); 
            -webkit-filter: grayscale(100%); 
            -moz-filter: grayscale(100%); 
            -ms-filter: grayscale(100%); 
            -o-filter: grayscale(100%); 
            -webkit-filter: grayscale(1);
        }
    </style>
    {/if}
    {/block}

    {block name=body}
    <div class="main py-0" id="main">
        <!-- banner  视频 -->
        <!-- <div class="home-row home-row-banner">
      <div class="home-row-container">
        <div class="banner-swiper-wrap">
          <div class="swiper-container" id="banner_swiper">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <img class="banner-img" src="{#portal_url#}/img/home/banner1.png">
                <div class="banner-info">
                  <div class="info-title">完善退出制度 激发市场活力</div>
                  <div class="info-discription">
                    完善市场主体退出制度，对推进供给侧结构性改革、完善优胜劣汰的市场机制、激发市场主体竞争活力、推动经济高质量发展具有重要意义近日，国家发改委、最高人民法院等13个部门联合印发《加快完善市场主体退出制度改革方案》（以下简称《方案》），要求逐步建立起与现代化经济体系相适应，覆盖各类市场主体的便利、高效、有序的退出制度。作为中央在经济工作方面重大战略部署的一部分，市场主体退出制度的加快完善，具有重要
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <img class="banner-img" src="{#portal_url#}/img/home/banner2.png">
                <div class="banner-info">
                  <div class="info-title">宜宾市人民政府办公室关于调整完善我市社会信用体系建设联席会议制度的通知</div>
                  <div class="info-discription">
                    （宜府办函﹝2017﹞116号） 各县（区）人民政府，临港开发区管委会，农业科技园区管委会，市直各部门，有关单位： 按照《四川省人民政府办公厅关于建立四川省社会信用体系建设联席会议的通知》（川办函〔2016〕158号）、《四川省人民政府办公厅关于印发四川省社会信用体系建设联席会议工作机制的通知》（川办函〔2017〕10号）要求，为加强我市社会信用体系建设工作的组织领导，市政府决定，调整完
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <img class="banner-img" src="{#portal_url#}/img/home/banner3.png">
                <div class="banner-info">
                  <div class="info-title">吉利造车，能否吉利？</div>
                  <div class="info-discription">
                    刚与百度“联姻”的吉利，又双叒叕要造车了！1月28日，浙江吉利控股集团有限公司与美国共享智能出行生态系统公司Faraday Future签署框架合作协议，双方计划在技术支持和工程服务领域展开合作，并探讨由吉利与富士康的合资公司提供代工服务的可能性。
                    　　 同时作为财务投资人，吉利控股集团还参与了Faraday Future SPAC上市的少量投资。「智能相对论」回
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <img class="banner-img" src="{#portal_url#}/img/home/banner4.png">
                <div class="banner-info">
                  <div class="info-title">从根本上解决“数据出官”“官出数据”将有新动作</div>
                  <div class="info-discription">
                    3月10日，在十三届全国人大二次会议记者会上，全国人大相关负责人就“人大监督工作”答中外记者问。会上有记者就统计数据造假及去年全国人大常委会开展的统计法执法检查情况进行了提问，全国人大财政经济委员会副主任委员尹中卿表示，执法检查推动查处统计违法案件，督促有关部门和地方落实对数据造假、弄虚作假责任人一票否决制，加大案件的通报、曝光、查处的力度，全面排查、专项整治以数谋
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <img class="banner-img" src="{#portal_url#}/img/home/banner5.png">
                <div class="banner-info">
                  <div class="info-title">宜宾“居然之家”开展3.15诚信经营宣誓大会</div>
                  <div class="info-discription">
                    发展的背后，离不开“居然之家”一直秉承的“服务为王”的经营理念：先行赔付、送货安装零延迟、三包服务期延至3年、一个月无理由退换货、一次消费终身服务等八大服务一直推动着整个家居建材行业的服务升级。作为行业龙头，“居然之家”坚持以诚信服务获得消费者信赖，牢牢树立家居建材行业服务标杆的行业地位。
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <img class="banner-img" src="{#portal_url#}/img/home/banner6.png">
                <div class="banner-info">
                  <div class="info-title">宜宾政策解读</div>
                  <div class="info-discription">
                    疫情期间，消费者更频繁地使用电子购物平台。尽管亚马逊为防疫支出了40亿美元的成本，但利润和营收均创下新高。有调查显示，用户花费在社交媒 体上的时间也普遍增加，脸书表示，每月至少使用一次WhatsApp、Instagram和Facebook的人数第二季度增加14%至31亿。
                    同时，由于全球多个市场经济活动逐步重启，企业在线广告支出也在回升，帮助推动科技企业业绩提升。对于依赖广告收入的字母表公司来说，
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-pagination" id="swiper_pagination"></div>
          </div>
        </div>
        <div class="video-wrap">
          <video id="home_video" class="home-video video-js vjs-big-play-centered">
            <source src="https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/ossSandbox/TTRw5YCxzPyc9BCxZJxNBbwm4xecdYsE.mp4" type="video/mp4" />
            <p></p>
          </video>
        </div>
      </div>
    </div> -->

        <div class="home-row">
            <div class="home-row-container">
                <div class="news-banner-container">
                    <div class="banner-swiper swiper-container" id="news_banner_swiper">
                        <div class="swiper-wrapper">
                            {if !empty($data['banners'])}
                            {foreach $data['banners'] as $item}
                            <div class="swiper-slide">
                                <div class="banner-swiper-item">
                                    <div class="banner-swiper-box">
                                        <a class="banner-swiper-link" href="/news/{$item['id']}">
                                            <img class="banner-swiper-image"
                                                src="https://credit-sc-yibin.oss-cn-beijing.aliyuncs.com/ossSandbox/87sA7NGc6Thr2wQs.jpg"
                                                alt="">
                                        </a>
                                    </div>
                                    <div class="banner-swiper-news">
                                        <h3 class="banner-swiper-title">
                                            <a class="link-hover-primary" href="/news/{$item['id']}">{$item.title}</a>
                                        </h3>
                                        <div class="banner-swiper-meta">
                                            <span class="banner-swiper-category">
                                                <span class="category-label">专栏</span>
                                                <span class="category-name">{$item['newsType']['name']}</span>
                                            </span>
                                            <span class="banner-swiper-source">{$item['source']}</span>
                                        </div>
                                        <p class="banner-swiper-description">
                                            <a class="link-hover-primary"
                                                href="/news/{$item['id']}">{$item['description']}</a>
                                        </p>
                                        <div class="banner-swiper-view-more">
                                            <a class="link-primary" href="/news/{$item['id']}">查看详情&gt;&gt;</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/foreach}
                            {/if}
                        </div>
                        <div class="swiper-pagination" id="swiper_pagination"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- 栏目导航 -->
        <div class="home-row column-nav">
            <div class="home-row-container">
                <ul class="column-nav-list">
                    <li class="list-item">
                        <a class="list-item-link" href="javascript:;">
                            <img class="list-item-img" src="{#portal_url#}/img/home/column-nav1.jpg" alt="">
                            <p class="list-item-title">统一社会信用代码</p>
                        </a>
                    </li>
                    <li class="list-item">
                        <a class="list-item-link" href="/doublePublicity?scene=MA">
                            <img class="list-item-img" src="{#portal_url#}/img/home/column-nav2.jpg" alt="">
                            <p class="list-item-title">行政许可和行政处罚查询</p>
                        </a>
                    </li>
                    <li class="list-item">
                        <a class="list-item-link" href="/redBlacks?scene=MA">
                            <img class="list-item-img" src="{#portal_url#}/img/home/column-nav3.jpg" alt="">
                            <p class="list-item-title">红黑榜名单查询</p>
                        </a>
                    </li>
                    <li class="list-item">
                        <a class="list-item-link" href="javascript:;">
                            <img class="list-item-img" src="{#portal_url#}/img/home/column-nav4.jpg" alt="">
                            <p class="list-item-title">企业信用信息查询</p>
                        </a>
                    </li>
                    <li class="list-item">
                        <a class="list-item-link" href="javascript:;">
                            <img class="list-item-img" src="{#portal_url#}/img/home/column-nav5.png" alt="">
                            <p class="list-item-title">诚信企业名录查询</p>
                        </a>
                    </li>
                    <li class="list-item">
                        <a class="list-item-link" href="javascript:;">
                            <img class="list-item-img" src="{#portal_url#}/img/home/column-nav6.jpg" alt="">
                            <p class="list-item-title">信用修复</p>
                        </a>
                    </li>
                    <li class="list-item">
                        <a class="list-item-link" href="javascript:;">
                            <img class="list-item-img" src="{#portal_url#}/img/home/column-nav7.jpg" alt="">
                            <p class="list-item-title">信用申诉</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- 新闻 -->
        <div class="home-row home-row-news">
            <div class="home-row-container">
                <div class="news-wrap">
                    <div class="news-tab-header">
                        <ul class="home-row-tab news-tab" id="news_tab">
                            <li class="tab-item is-active">
                                <span class="iconfont icon-file"></span>
                                <span>信用动态</span>
                            </li>
                            <li class="tab-item">
                                <span>政策法规</span>
                            </li>
                            <li class="tab-item">
                                <span>失信专项治理</span>
                            </li>
                        </ul>
                        <a class="tab-look-more is-active" href="/creditDynamics/index">查看更多&nbsp;>></a>
                        <a class="tab-look-more"
                            href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['POLICY_STATUTE']){/php}">查看更多&nbsp;>></a>
                        <a class="tab-look-more"
                            href="/news?type={php} echo marmot_encode(NEWS_TYPE['SPECIAL_GOVERN']){/php}">查看更多&nbsp;>></a>
                    </div>
                    <div class="news-content" id="news_content">
                        <ul class="content-list is-active">
                            {if !empty($data['dynamics'])}
                            {foreach $data['dynamics'] as $item}
                            <li class="list-item">
                                <a class="list-item-info" href="/news/{$item['id']}">
                                    <h1 class="list-item-title">{$item['title']}</h1>
                                    <p class="list-item-description">{$item['description']}</p>
                                </a>
                                <div class="list-item-main">
                                    <div class="list-item-time">
                                        <span class="iconfont icon-earth"></span>
                                        <span class="list-item-resource">{$item['source']}</span>
                                        <span class="iconfont icon-time"></span>
                                        <span>{$item['updateTimeFormat']}</span>
                                    </div>
                                    <a class="list-item-category" href="/news?type={$item['newsType']['id']}">
                                        <span class="category-label">专栏</span>
                                        <div class="category-main">{$item['newsType']['name']}</div>
                                    </a>
                                </div>
                            </li>
                            {/foreach}
                            {/if}
                        </ul>
                        <ul class="content-list">
                            {if !empty($data['policies'])}
                            {foreach $data['policies'] as $item}
                            <li class="list-item">
                                <a class="list-item-info" href="/news/{$item['id']}">
                                    <h1 class="list-item-title">{$item['title']}</h1>
                                    <p class="list-item-description">{$item['description']}</p>
                                </a>
                                <div class="list-item-main">
                                    <div class="list-item-time">
                                        <span class="iconfont icon-earth"></span>
                                        <span class="list-item-resource">{$item['source']}</span>
                                        <span class="iconfont icon-time"></span>
                                        <span>{$item['updateTimeFormat']}</span>
                                    </div>
                                    <a class="list-item-category" href="/news?type={$item['newsType']['id']}">
                                        <span class="category-label">专栏</span>
                                        <div class="category-main">{$item['newsType']['name']}</div>
                                    </a>
                                </div>
                            </li>
                            {/foreach}
                            {/if}
                        </ul>
                        <ul class="content-list">
                            {if !empty($data['specialGovernments'])}
                            {foreach $data['specialGovernments'] as $item}
                            <li class="list-item">
                                <a class="list-item-info" href="/news/{$item['id']}">
                                    <h1 class="list-item-title">{$item['title']}</h1>
                                    <p class="list-item-description">{$item['description']}</p>
                                </a>
                                <div class="list-item-main">
                                    <div class="list-item-time">
                                        <span class="iconfont icon-earth"></span>
                                        <span class="list-item-resource">{$item['source']}</span>
                                        <span class="iconfont icon-time"></span>
                                        <span>{$item['updateTimeFormat']}</span>
                                    </div>
                                    <a class="list-item-category" href="/news?type={$item['newsType']['id']}">
                                        <span class="category-label">专栏</span>
                                        <div class="category-main">{$item['newsType']['name']}</div>
                                    </a>
                                </div>
                            </li>
                            {/foreach}
                            {/if}
                        </ul>
                    </div>
                </div>
                <!-- 公示 -->
                {block name=newsAside}{include file='./Publicity.tpl'}{/block}
            </div>
        </div>

        <!-- 专题专栏 -->
        <div class="home-row home-row-special">
            <div class="home-row-container">
                <div class="special-wrap">
                    <ul class="home-row-tab">
                        <li class="tab-item is-active">
                            <span class="iconfont icon-file"></span>
                            <span>专题专栏</span>
                        </li>
                    </ul>
                    {if isset($data.websiteCustomizes.content.specialColumn)}
                    <ul class="special-banner-list">
                        {foreach $data.websiteCustomizes.content.specialColumn as $colum}
                            {if $colum.status == 0}
                                <li class="list-item">
                                    <a class="list-item-link"
                                        href="{if !empty($colum.url)}{$colum.url}{else}javascript:;{/if}">
                                        <img class="list-item-img" src="{if !empty($colum.image)}{$colum.image.identify}{else}{#portal_url#}/img/home/special1.jpg{/if}" 
                                        alt="{if !empty($colum.image)}{$colum.image.name}{/if}">
                                    </a>
                                </li>
                            {/if}
                        {/foreach}
                    </ul>
                    {/if}
                    <div class="special-list">
                        <div class="special-list-col">
                            <ul class="home-row-tab">
                                <li class="tab-item is-active">
                                    <span class="iconfont icon-file"></span>
                                    <span>组织架构</span>
                                </li>
                                <a class="tab-look-more"
                                    href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['ORGANIZATIONAL_STRUCTURE']){/php}">查看更多&nbsp;>></a>
                            </ul>
                            <ul class="special-list-item">
                                {if !empty($data['organizations'])}
                                {foreach $data['organizations'] as $item}
                                <li class="list-item">
                                    <a class="list-item-link" href="/news/{$item['id']}">
                                        <div class="list-item-title">{$item['title']}</div>
                                        <div class="list-item-time">{date('Y/m/d',$item['updateTime'])}</div>
                                    </a>
                                </li>
                                {/foreach}
                                {/if}
                            </ul>
                        </div>
                        <div class="special-list-col">
                            <ul class="home-row-tab">
                                <li class="tab-item is-active">
                                    <span class="iconfont icon-file"></span>
                                    <span>信用研究</span>
                                </li>
                                <a class="tab-look-more"
                                    href="/news/index?category={php} echo marmot_encode(NEWS_CATEGORY['CREDIT_RESEARCH']){/php}">查看更多&nbsp;>></a>
                            </ul>
                            <ul class="special-list-item">
                                {if !empty($data['creditResearches'])}
                                {foreach $data['creditResearches'] as $item}
                                <li class="list-item">
                                    <a class="list-item-link" href="/news/{$item['id']}">
                                        <div class="list-item-title">{$item['title']}</div>
                                        <div class="list-item-time">{date('Y/m/d',$item['updateTime'])}</div>
                                    </a>
                                </li>
                                {/foreach}
                                {/if}
                            </ul>
                        </div>
                        <div class="special-list-col">
                            <ul class="home-row-tab">
                                <li class="tab-item is-active">
                                    <span class="iconfont icon-file"></span>
                                    <span>数据报送统计</span>
                                </li>
                            </ul>
                            <!-- FIXME: inline-style -->
                            <div style="margin-top: 0;" class="ranking-stat-wrap | js-tabs">
                                <ul class="tab-head">
                                    <li class="tab-head-item is-active">市级部门排名</li>
                                    <li class="tab-head-item">各县（市）区排名</li>
                                </ul>
                                <div class="tab-body">
                                    <!-- FIXME: inline-style -->
                                    <div style="height: 225px;" class="tab-body-item light-scroll-container is-active">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th class="col-rank">排名</th>
                                                    <th class="col-name">市级部门名称</th>
                                                    <th class="col-number">上报总数</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="1">1</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="发改委">发改委</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="10000">10000</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="2">2</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="医疗保障局">医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="8000">8000</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="3">3</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="行政审批局">行政审批局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="7000">7000</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="4">4</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="监督管理局">监督管理局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="5000">5000</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="5">5</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="税务局">税务局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="3999">3999</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="6">6</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="中级人民法院">中级人民法院</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="2456">2456</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="应急局">应急局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="2344">2344</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="8">8</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="文明办">文明办</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="1222">1222</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="9">9</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="民政局">民政局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="1200">1200</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="10">10</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="金融办">金融办</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="12">12</span>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- FIXME: inline-style -->
                                    <div style="height: 225px;" class="tab-body-item light-scroll-container">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th class="col-rank">排名</th>
                                                    <th class="col-name">各县（市）区排名</th>
                                                    <th class="col-number">上报总数</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="1">1</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="2">2</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="3">3</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="4">4</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="5">5</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="6">6</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-rank">
                                                        <span class="col-cell" title="7">7</span>
                                                    </td>
                                                    <td class="col-name">
                                                        <span class="col-cell" title="吕梁市医疗保障局">吕梁市医疗保障局</span>
                                                    </td>
                                                    <td class="col-number">
                                                        <span class="col-cell" title="82533336">82533336</span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {if isset($data.websiteCustomizes.content.footerBanner)}
                    <div class="special-swiper">
                        <div class="swiper-container" id="special_swiper">
                            <div class="swiper-wrapper">
                                {foreach $data.websiteCustomizes.content.footerBanner as $footerBanner}
                                    {if $footerBanner.status eq 0}
                                    <div class="swiper-slide">
                                        <a href="{if !empty($footerBanner.url)}{$footerBanner.url}{else}javascript:;{/if}">
                                            <img class="banner-img" src="{if !empty($footerBanner.image)}{$footerBanner.image.identify}{else}{#portal_url#}/img/home/special_banner1.png{/if}" >
                                        </a>
                                    </div>
                                    {/if}
                                {/foreach}
                            </div>
                            <div class="swiper-pagination" id="special_swiper_pagination"></div>
                        </div>
                    </div>
                    {/if}
                </div>
            </div>
        </div>

        <!-- 相关链接 -->
        <div class="home-row home-row-link">
            <div class="home-row-container">
                <div class="link-wrap">
                    {if isset($data.websiteCustomizes.content.relatedLinks)}
                    <div class="about-link">
                        <div class="about-link-label">相关链接：</div>
                        <div class="about-link-main">
                            <!-- col-*表示相关链接列数，例如：col-4为4列、col-3为3列、col-2为2列、col-1为1列 -->
                            <ul class="about-link-list col-{count($data.websiteCustomizes.content.relatedLinks)}" id="about_link_list">
                                {foreach $data.websiteCustomizes.content.relatedLinks as $relatedLinks}
                                    {if $relatedLinks.status eq 0}
                                    <li class="list-item">
                                        <div class="list-item-box">{$relatedLinks.name}</div>
                                        {if !empty($relatedLinks.items)}
                                        <ul class="list-item-content">
                                            {foreach $relatedLinks.items as $item}
                                                {if $item.status eq 0}
                                                <li class="list-item-link">
                                                    <a href="{if !empty($item.url)}{$item.url}{else}javascript:;{/if}">{$item.name}</a>
                                                </li>
                                                {/if}
                                            {/foreach}
                                        </ul>
                                        {/if}
                                    </li>
                                    {/if}
                                {/foreach}
                            </ul>
                        </div>
                    </div>
                    {/if}

                    {if isset($data.websiteCustomizes.content.organizationGroup)}
                    <ul class="link-list">
                        {foreach $data.websiteCustomizes.content.organizationGroup as $organizationGroup}
                            {if $organizationGroup.status eq 0}
                            <li class="list-item">
                                <a class="list-item-link" 
                                    href="{if !empty($organizationGroup.url)}{$organizationGroup.url}{else}javascrit:;{/if}">
                                    <img class="list-item-img" src="{if !empty($organizationGroup.image)}{$organizationGroup.image.identify}{else}{#portal_url#}/img/home/link1.jpg{/if}" alt="{$organizationGroup.image.name}">
                                </a>
                            </li>
                            {/if}
                        {/foreach}
                    </ul>
                    {/if}
                </div>
            </div>
        </div>
    </div>
    <!-- 专题 中间弹窗 -->
    {if isset($data.websiteCustomizes.content.centerDialog)}
        {if $data.websiteCustomizes.content.centerDialog.status eq 0}
        <div class="special-alert-center" id="special_center_content" style="display: none;">
            <a href="{if !empty($data.websiteCustomizes.content.centerDialog.url)}{$data.websiteCustomizes.content.centerDialog.url}{else}javascript:;{/if}" 
            target="_black" rel="noopener noreferrer">
                <img src="{if !empty($data.websiteCustomizes.content.centerDialog.image)}{$data.websiteCustomizes.content.centerDialog.image.identify}{else}http://portal.ny.qixinyun.com/images/party-100-year.png{/if}">
            </a>
        </div>
        {/if}
    {/if}
    <!-- 专题 左边浮动 -->
    {if isset($data.websiteCustomizes.content.leftFloatCard)}
    <div class="special-float-ad visible-sm-up">
        {foreach $data.websiteCustomizes.content.leftFloatCard as $leftFloatCard}
            {if $leftFloatCard.status eq 0}
            <a href="{if !empty($leftFloatCard.url)}{$leftFloatCard.url}{else}javascript:;{/if}" class="float-ad-item" target="_black" rel="noopener noreferrer">
                <img src="{if !empty($leftFloatCard.image)}{$leftFloatCard.image.identify}{else}https://credit-system.oss-cn-beijing.aliyuncs.com/portal/images/mid/portal-yiqing-image.png{/if}">
            </a>
            {/if}
        {/foreach}
    </div>
    {/if}
    <!-- 专题 嵌入窗口 -->
    {if isset($data.websiteCustomizes.content.frameWindow)}
        {if $data.websiteCustomizes.content.frameWindow.status eq 0}
        <div class="special-frame-window close-modules visible-sm-up">
            <span class="close-btn close-menu" id="closeSpecialFrame">
                关闭
            </span>
            <iframe src="{if !empty($data.websiteCustomizes.content.frameWindow.url)}{$data.websiteCustomizes.content.frameWindow.url}{else}https://www.yibin.gov.cn/qt_297/test/test1/index.html{/if}" width="328" height="210"
                frameborder="0"></iframe>
        </div>
        {/if}
    {/if}
    <!-- 专题 飘窗 -->
    <input type="hidden" id="animateWindowData" value={if isset($data.websiteCustomizes.content.animateWindow)}{json_encode($data.websiteCustomizes.content.animateWindow)}{/if}>
    {/block}

    {block name=pageScript}
    <script src="{#portal_url#}/static/libs/videojs/video.min.js"></script>
    <script src="{#portal_url#}/static/libs/swiper/js/swiper.min.js"></script>
    <script src="{#portal_url#}/js/common/floating-animation.jquery.js"></script>
    <script src="{#portal_url#}/js/home/index.js"></script>
    {/block}

