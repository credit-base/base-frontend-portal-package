<div class="publicity-wrap">
  <ul class="home-row-tab">
    <li class="tab-item is-active">
      <span class="iconfont icon-notice"></span>
      <span>信用信息公示</span>
    </li>
    <a class="tab-look-more" href="/creditPublicities/index">查看更多&nbsp;>></a>
  </ul>
  <ul class="publicity-list">
    <li class="list-item">
      <a class="list-item-link" href="/doublePublicity?scene=MA">
        <img class="list-item-img" src="{#portal_url#}/img/home/publicity1.jpg" alt="">
        <div class="list-item-title">行政许可信息</div>
      </a>
    </li>
    <li class="list-item">
      <a class="list-item-link" href="/doublePublicity?scene=MQ">
        <img class="list-item-img" src="{#portal_url#}/img/home/publicity2.jpg" alt="">
        <div class="list-item-title">行政处罚信息</div>
      </a>
    </li>
    <li class="list-item">
      <a class="list-item-link" href="/redBlacks?scene=MA">
        <img class="list-item-img" src="{#portal_url#}/img/home/publicity3.jpg" alt="">
        <div class="list-item-title">诚信红榜</div>
      </a>
    </li>
    <li class="list-item">
      <a class="list-item-link" href="/redBlacks?scene=MQ">
        <img class="list-item-img" src="{#portal_url#}/img/home/publicity4.jpg" alt="">
        <div class="list-item-title">失信黑榜</div>
      </a>
    </li>
    <li class="list-item">
      <a class="list-item-link" href="javascript:;">
        <img class="list-item-img" src="{#portal_url#}/img/home/publicity5.jpg" alt="">
        <div class="list-item-title">守信信息</div>
      </a>
    </li>
    <li class="list-item">
      <a class="list-item-link" href="javascript:;">
        <img class="list-item-img" src="{#portal_url#}/img/home/publicity6.jpg" alt="">
        <div class="list-item-title">信用承诺</div>
      </a>
    </li>
  </ul>
</div>
