<{extends file='../Layout/Main.tpl' }>

  {block name=pageStyle}
  <link rel="stylesheet" href="{#portal_url#}/css/publicity/list.css">
  {/block}

  {block name=body}
    <div class="main" id="main">
    <div class="container">
      <div class="breadcrumb-container mb-15">
        <span class="breadcrumb-label">您所在位置：</span>
        <a class="breadcrumb-link" href="/creditPublicities/index">信息公示</a>
        <span class="breadcrumb-seperator">&gt;&gt;</span>
        <a class="breadcrumb-link breadcrumb-current" href="/redBlacks">信用守信信息与信用失信信息</a>
      </div>
      <div class="red-black-container">
        <div class="red-black-header">
          <h2 class="red-black-title">筛&emsp;选:</h2>
          <div class="red-black-form">
            <div class="red-black-form-row">
              <div class="red-black-form-item">
                <span class="red-black-form-label">公示类型:</span>
                <div class="red-black-form-main">
                  <div class="form-item-radio-group">
                    <span class="form-item-radio {if $data['scene']==MA}is-red{/if}" data-scene="MA">诚信红榜</span>
                    <span class="form-item-radio {if $data['scene']==MQ}is-black{/if}" data-scene="MQ">失信黑榜</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="red-black-form-row">
              <div class="red-black-form-item">
                <span class="red-black-form-label">信用主体:</span>
                <div class="red-black-form-main">
                  <select class="form-item-select form-item-select-type form-item-select-mini" name="" id="">
                    <option value="MA">法人及非法人组织</option>
                    <option value="MQ">自然人</option>
                    <option value="Mg">个体工商户</option>
                  </select>
                </div>
              </div>
              {* 行政机构 *}
              {marmot_widget widget="Base\Package\UserGroup\Controller\UserGroupFetchController" func=userGroupWidget}
              <div class="red-black-form-item">
                <span class="red-black-form-label">企业名称:</span>
                <div class="red-black-form-main">
                  <input class="form-item-input form-item-input-name" type="text" name="name">
                </div>
              </div>
              <div class="red-black-form-item">
                <span class="red-black-form-label">统一社会信用代码:</span>
                <div class="red-black-form-main">
                  <input class="form-item-input form-item-input-code" type="text" name="identify" maxlength="18">
                </div>
              </div>
            </div>
            <div class="red-black-form-row text-center">
              <button class="red-black-form-btn" id="btn_search" type="button" role="button">立即查询</button>
            </div>
          </div>
        </div>
        <div class="red-black-body">
          <input type="hidden" id="list-type" value="{$data['scene']}">
          <ul class="red-black-list">
          {if !empty($data['list'])}
          {foreach $data['list'] as $item}
            <li class="list-item {if $data['scene']==MA}is-red{else}is-black{/if}">
              <div class="list-item-main">
                <h3 class="list-item-title">
                  <a class="link-hover-primary" href="/redBlacks/{$item['id']}">{$item['name']}</a>
                </h3>
                <p class="list-item-category">
                  <span>行政信息事项:</span>
                  <span>{$item['template']}</span>
                </p>
              </div>
              {if $data['scene']==MA}
                <a class="list-item-btn" href="/redBlacks/{$item['id']}" role="button">诚信红榜</a>
              {else}
                <a class="list-item-btn" href="/redBlacks/{$item['id']}" role="button">失信黑榜</a>
              {/if}
            </li>
          {/foreach}
          {else}
            <div class="empty-data-container">
              <div class="empty-data-icon"></div>
              <h4 class="empty-data-tip">暂无内容</h4>
            </div>
          {/if}
          </ul>

          <div class="pagination-container">
            <input type="hidden" id="pagination_init_total" value="{$data['total']}">
            <div class="pagination" id="pagination"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {/block}

  {block name=pageScript}
  <script src="{#portal_url#}/static/libs/masonry/masonry.pkgd.min.js"></script>
  <script src="{#portal_url#}/js/red-black/list.js"></script>
  {/block}
