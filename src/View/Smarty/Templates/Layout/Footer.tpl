<div class="footer" id="footer">
  {if isset($footerData.content.silhouette)}
    {if $footerData.content.silhouette.status eq 0}
    <div class="footer-picture visible-sm-up" title="{$footer.content.silhouette.description}" style="background-image: url({$footerData.content.silhouette.image.identify})"></div>
    {/if}
  {/if}
  <div class="footer-main">
    <div class="container">
      <ul class="footer-links">
        {foreach $footerData.content.footerNav as $footerNav}
          {if $footerNav.status eq 0}
          <li class="link-item">
            <a href="{if !empty($footerNav.url)}{$footerNav.url}{else}javascript:;{/if}">{$footerNav.name}</a>
          </li>
          {/if}
        {/foreach}
      </ul>
      <div class="footer-row">
        {foreach $footerData.content.footerTwo as $footerTwo}
          {if $footerTwo.status eq 0}
          {$footerTwo.name}{$footerTwo.description}
          {/if}
        {/foreach}
      </div>
      <div class="footer-row">
      {foreach $footerData.content.footerThree as $footerThree} 
        {if $footerThree.status eq 0}
        {$footerThree.name} {$footerThree.description}
          {if $footerThree.type eq 8 && isset($footerThree.image)}
            <span class="footer-row-text"><img class="icon-police" src="{$footerThree.image.identify}" alt="{$footerThree.image.name}">
          {/if}
          <a href="{$footerThree.url}"></a></span>
        {/if}
      {/foreach}
      </div>
      {if isset($footerData.content.partyAndGovernmentOrgans)}
        {if $footerData.content.partyAndGovernmentOrgans.status eq 0}
        <div class="footer-widget footer-widget-party">
          <a href="{$footerData.content.partyAndGovernmentOrgans.url}">
            <img src="{if !empty($footerData.content.partyAndGovernmentOrgans.image)}{$footerData.content.partyAndGovernmentOrgans.image.identify}{else}{#portal_url#}/img/common/icon-party.png{/if}" 
              alt="{if !empty($footerData.content.partyAndGovernmentOrgans.image.name)}{$footerData.content.partyAndGovernmentOrgans.image.name}{else}党政机关{/if}">
          </a>
        </div>
        {/if}
      {/if}

      {if isset($footerData.content.governmentErrorCorrection)}
        {if $footerData.content.governmentErrorCorrection.status eq 0}
        <div class="footer-widget footer-widget-debug">
          <a href="{$footerData.content.governmentErrorCorrection.url}"></a>
            <img src="{if !empty($footerData.content.governmentErrorCorrection.image)}{$footerData.content.governmentErrorCorrection.image.identify}{else}{#portal_url#}/img/common/icon-debug.png{/if}" alt="{if !empty($footerData.content.governmentErrorCorrection.image.name)}{$footerData.content.governmentErrorCorrection.image.name}{else}政府网站找错{/if}">
          </a>
        </div>
        {/if}
      {/if}
    </div>
  </div>
</div>
