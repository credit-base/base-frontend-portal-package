<!-- PC 页头 -->
<div class="header-pc visible-md-up" id="header_pc{$marmot_isSignIn}" 
  style="background-image: url({$headerData.content.headerBg.identify});">
  <div class="header-bar">
    <div class="container">
      {if isset($headerData.content.headerBarRight)}
      <div class="header-links">
        {foreach $headerData.content.headerBarRight as $headerBarRight}
          {if $headerBarRight.status eq 0}
            {if $headerBarRight.type eq 4}
              {if empty($marmot_isSignIn)}
                <a class="link-hover-primary" href="/members/signIn">登录</a>
                <span>|</span>
                <a class="link-hover-primary" href="/members/add">注册</a>
              {elseif !empty($marmot_isSignIn)}
                <a class="link-hover-primary" href="/members/edit">个人中心</a>
                <span>|</span>
                <a class="link-hover-primary" href="/members/signOut">退出</a>
              {/if}
              <a class="link-hover-primary" href="{#superior_platform_link#}" rel="noopener noreferrer">{#superior_platform#}</a>
            {elseif $headerBarRight.type != 4}
            <a class="link-hover-primary" href="{if !empty($headerBarRight.url)}{$headerBarRight.url}{else}javascript:;{/if}">{$headerBarRight.name}</a>
            {/if}
          {/if}
        {/foreach}
      </div>
      {/if}

      {if isset($headerData.content.headerBarLeft)}
      <div class="header-views">
        {foreach $headerData.content.headerBarLeft as $headerBarLeft}
          {if $headerBarLeft.status eq 0}
            {if $headerBarLeft.type eq 1}
            <span class="header-views-item" >{$headerBarLeft.name}：<em>561</em></span>
            {else}
            <a class="header-views-item" href="{$headerBarLeft.url}">{$headerBarLeft.name}</a>
            {/if}
          {/if}
        {/foreach}
      </div>
      {/if}

    </div>
  </div>
  <div class="header-main">
    <div class="container">
      <a class="header-logo" href="javascript:;">
        <img src="{if !empty($headerData.content.logo)}{$headerData.content.logo.identify}{else}{#portal_url#}{#website_logo#}{/if}" 
          alt="{if !empty($headerData.content.logo)}{$headerData.content.logo.name}{else}{#website_title#}{/if}">
      </a>
      <div class="header-search">
        <div class="header-search-tabs | js-tabs">
          {if isset($headerData.content.headerSearch)}
          <ul class="tab-head">
            {foreach $headerData.content.headerSearch as $key=>$headerSearch}
              {if $headerSearch.status eq 0}
                {if isset($searchNav)}
                <li class="tab-head-item {if $searchNav eq $headerSearch.identify}is-active{/if}">{$headerSearch.name}</li>
                {else}
                <li class="tab-head-item {if $key eq 0}is-active{/if}">{$headerSearch.name}</li>
                {/if}
              {/if}
            {/foreach}
          </ul>
        
          <div class="tab-body">
            {foreach $headerData.content.headerSearch as $key=>$headerSearch}
              {if $headerSearch.status eq 0}
                {if isset($searchNav)}
                <form class="tab-body-item {if $searchNav eq $headerSearch.identify}is-active{/if}" action="{$headerSearch.link}" method="GET">
                  <div class="header-search-params">
                    <input class="header-search-input" type="text" name="keyword" placeholder="{$headerSearch.inputData}">
                    <input type="hidden" name="type" value="{$headerSearch.identify}">
                    <button class="header-search-btn" type="submit" role="button">
                      <span class="iconfont icon-search"></span>
                    </button>
                  </div>
                </form>
                {else}
                <form class="tab-body-item {if $key eq 0}is-active{/if}" action="{$headerSearch.link}" method="GET">
                  <div class="header-search-params">
                    <input class="header-search-input" type="text" name="keyword" placeholder="{$headerSearch.inputData}">
                    <input type="hidden" name="type" value="{$headerSearch.identify}">
                    <button class="header-search-btn" type="submit" role="button">
                      <span class="iconfont icon-search"></span>
                    </button>
                  </div>
                </form>
                {/if}
              {/if}
            {/foreach}
          </div>
          {/if}
        </div>
      </div>
    </div>
  </div>
</div>
<!-- 移动端页头 -->
<div class="header-mobile hidden-md-up" id="header_mobile"
  style="background-image: url({$headerData.content.headerBg.identify});">
  <a class="header-logo" href="/">
    <img src="{if !empty($headerData.content.logo)}{$headerData.content.logo.identify}{else}{#portal_url#}{#website_logo#}{/if}" alt="信用宜宾">
  </a>
  {if isset($headerData.content.headerBarRight)}
  <div class="header-links">
    {foreach $headerData.content.headerBarRight as $headerBarRight}
      {if $headerBarRight.status eq 0}
        {if $headerBarRight.type eq 4}
          {if empty($marmot_isSignIn)}
            <a class="header-link has-gap-line" href="/members/signIn">登录</a>
            <a class="header-link" href="/members/add">注册</a>
          {else}
            <a class="header-link has-gap-line" href="/members/edit">个人中心</a>
            <a class="header-link" href="/members/signOut">退出</a>
          {/if}
        {else}
        <a class="header-link has-gap-line" href="{if !empty($headerBarRight.url)}{$headerBarRight.url}{else}javascript:;{/if}">{$headerBarRight.name}</a>
        {/if}
      {/if}
    {/foreach}
  </div>
  {/if}
</div>
