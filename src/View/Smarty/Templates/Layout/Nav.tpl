<div class="header-nav">
  <div class="container">
    <ul class="main-nav-list">
      {foreach $navData as $val}
        {if $val.status eq 0}
        <li class="list-item 
            {if $nav|default:0 eq $val.nav}is-active{/if}">
          <a class="list-item-title" href="{$val.url}">{$val.name}</a>
          <i class="list-item-icon"></i>
        </li>
        {/if}
      {/foreach}
    </ul>
  </div>
</div>
