<div class="tool-box visible-md-up" id="tool_box">
  {foreach $toolBoxData as $rightToolBar}
    {if $rightToolBar.status eq 0}
      {if $rightToolBar.category eq 1}
      <a class="tool-item" href="javascript:;">
        <span class="tool-item-icon iconfont icon-phone"></span>
        <span class="tool-item-title">{$rightToolBar.name}</span>
        {if !empty($rightToolBar.images)}
        <div class="tool-item-popover">
            {foreach $rightToolBar.images as $images}
            <img class="popover-pic" src="{$images.image.identify}" alt="{$images.image.name}">
            <h4 class="popover-title">{$images.title}</h4>
            {/foreach}
        </div>
        {/if}
      </a>
      {elseif $rightToolBar.category eq 2}
      <a class="tool-item" href="{$rightToolBar.url}">
        <span class="tool-item-icon iconfont icon-phone"></span>
        <span class="tool-item-title">{$rightToolBar.name}</span>
      </a>
      {elseif $rightToolBar.category eq 3}
      <a class="tool-item" href="javascript:;">
        <span class="tool-item-icon iconfont icon-phone"></span>
        <span class="tool-item-title">{$rightToolBar.name}</span>
        {if !empty($rightToolBar.description)}
        <div class="tool-item-popover">
          <div class="popover-text">{$rightToolBar.description}</div>
        </div>
        {/if}
      </a>
      {/if}
    {/if}
  {/foreach}
  <a class="tool-item tool-item-top" id="back_to_top" href="#">
    <span class="tool-item-icon iconfont icon-top"></span>
  </a>
</div>
