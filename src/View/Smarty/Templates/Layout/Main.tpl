{config_load 'smarty.conf'}
{oss_info}
{member_info}

<!DOCTYPE html>
<html lang="zh-CN">

<head>
  <meta charset="UTF-8">
  <meta name="renderer" content="webkit">
  <meta name="force-rendering" content="webkit" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1, user-scalable=no, viewport-fit=cover">
  <meta content="yes" name="apple-mobile-web-app-capable">
  <meta content="black" name="apple-mobile-web-app-status-bar-style">
  <meta content="telephone=no, email=no" name="format-detection">
  <meta name="screen-orientation" content="portrait">
  <meta name="x5-orientation" content="portrait">
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Cache-Control" content="no-cache,must-revalidate">
  <meta http-equiv="Expires" content="0">
  {block name=siteKeywords}
  <meta name="keywords" content="{#website_keywords#}">
  {/block}
  {block name=siteDescription}
  <meta name="description" content="{#website_description#}">
  {/block}
  {block name=siteTitle}
  <title>{#website_title#}</title>
  {/block}
  <link rel="shortcut icon" href="{#website_icon#}">

  {block name=style}
  <link rel="stylesheet" href="//at.alicdn.com/t/font_2561266_x87cqpbb469.css">
  <link rel="stylesheet" href="{#portal_url#}/static/libs/layui/css/layui.css">
  <link rel="stylesheet" href="{#portal_url#}/css/common/style.css">
  {block name=pageStyle}{/block}
  {/block}
</head>

<body>
  <noscript>
    <strong>您的浏览器不支持JavaScript运行，请升级您的浏览器，或者更换Chrome浏览器使用。</strong>
  </noscript>

  {block name=header}{marmot_widget widget='Base\Package\WebsiteCustomize\Controller\WebsiteCustomizeFetchController' func=header parameters=$marmot_isSignIn}{/block}

  {block name=nav}{marmot_widget widget='Base\Package\WebsiteCustomize\Controller\WebsiteCustomizeFetchController' func=nav parameters=$nav|default:1}{/block}

  {block name=search}{news_widget widget='Base\Package\WebsiteCustomize\Controller\WebsiteCustomizeFetchController' func=search}{/block}

  {block name=body}{/block}

  {block name=footer} {news_widget widget='Base\Package\WebsiteCustomize\Controller\WebsiteCustomizeFetchController' func=footer}{/block}

  {block name=toolBox}{news_widget widget='Base\Package\WebsiteCustomize\Controller\WebsiteCustomizeFetchController' func=toolBox}{/block}


  {block name=script}
  <script src="{#portal_url#}/static/libs/jquery/jquery.min.js"></script>
  <script src="{#portal_url#}/static/libs/layui/layui.js"></script>
  <script src="{#portal_url#}/js/common/portal.core.js"></script>
  <script src="{#portal_url#}/js/common/portal.common.js"></script>
  {block name=pageScript}{/block}
  {/block}

</body>

</html>
