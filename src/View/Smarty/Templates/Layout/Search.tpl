<!-- 搜索 -->
<div class="search-mobile hidden-md-up" id="search_mobile">
  <div class="search-mobile-tabs | js-tabs">
    <ul class="tab-head">
      {foreach $searchData as $headerSearch}
        {if $headerSearch.status eq 0}
        <li class="tab-head-item {if $key eq 0}is-active{/if}">{$headerSearch.name}</li>
        {/if}
      {/foreach}
    </ul>
    <div class="tab-body">
      {foreach $searchData as $key=>$headerSearch}
        {if $headerSearch.status eq 0}
        <form class="tab-body-item search-mobile-form {if $key eq 0}is-active{/if}" action="{$headerSearch.link}" method="GET">
          <span class="search-mobile-type">{$headerSearch.name}</span>
          <input class="search-mobile-input" type="text" name="keyword" placeholder="{$headerSearch.inputData}">
          <input type="hidden" name="type" value="{$headerSearch.identify}">
          <button class="search-mobile-btn" type="submit" role="button">
            <span class="iconfont icon-search"></span>
          </button>
        </form>
        {/if}
      {/foreach}
    </div>
  </div>
</div>