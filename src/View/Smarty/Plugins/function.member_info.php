<?php

use Sdk\Member\Model\NullMember;
use Sdk\Member\Repository\MemberRepository;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
function smarty_function_member_info(
    $params,
    Smarty_Internal_Template $template
) {
    $memberId = \Marmot\Core::$container->get('member')->getId();
    $isSignIn = 0;
    $member = new NullMember();

    if (!empty($memberId)) {
        $memberRepository = new MemberRepository();

        $member = $memberRepository->scenario(MemberRepository::FETCH_ONE_MODEL_UN)->fetchOne($memberId);
  
        if (!empty($member->getUserName())) {
            $isSignIn = 1;
        }
    }

    $template->assign('marmot_memberId', $user->getId());
    $template->assign('marmot_userName', $user->getUserName());
    $template->assign('marmot_isSignIn', $isSignIn);
}
