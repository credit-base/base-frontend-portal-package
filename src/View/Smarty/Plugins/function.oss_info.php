<?php

function smarty_function_oss_info(
    $params,
    Smarty_Internal_Template $template
) {
    unset($params);
    $commonUrlImagesUpload = Marmot\Core::$container->get('oss.domain.name').Marmot\Core::$container->get('oss.dir');

    $template->assign('common_url_images_upload', $commonUrlImagesUpload);
    $template->assign('web_domain_name', Marmot\Core::$container->get('web.domain.name'));
}
