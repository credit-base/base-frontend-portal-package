<?php

function smarty_function_marmot_widget($args)
{
    $widget = new $args['widget']();
    $func = $args['func'];

    if (isset($args['parameters'])) {
        $widget->$func($args['parameters']);
    }

    if (!isset($args['parameters'])) {
        $widget->$func();
    }
}
