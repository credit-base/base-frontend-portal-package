<?php
namespace Base\Package\Home\View\Template;

class MockIndexView extends IndexView
{
    public function getCacheData() : array
    {
        return parent::getCacheData();
    }

    public function __construct()
    {
        parent::__construct(array());
    }
}
