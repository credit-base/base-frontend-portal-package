<?php
namespace Base\Package\Home\Cache\Query;

use Sdk\News\Repository\NewsRepository;
use Marmot\Framework\Adapter\ConcurrentAdapter;
use Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

class MockHomeFragmentCacheQuery extends HomeFragmentCacheQuery
{
    public function getRepository() : NewsRepository
    {
        return parent::getRepository();
    }

    public function getConcurrentAdapter() : ConcurrentAdapter
    {
        return parent::getConcurrentAdapter();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function getTtl() : int
    {
        return parent::getTtl();
    }

    public function getWebsiteCustomizeRepository() : WebsiteCustomizeRepository
    {
        return parent::getWebsiteCustomizeRepository();
    }
}
