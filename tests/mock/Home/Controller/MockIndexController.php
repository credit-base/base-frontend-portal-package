<?php
namespace Base\Package\Home\Controller;

class MockIndexController extends IndexController
{
    public function getHomeFragmentCacheQuery()
    {
        return parent::getHomeFragmentCacheQuery();
    }

    public function fetchNews()
    {
        return parent::fetchNews();
    }
}
