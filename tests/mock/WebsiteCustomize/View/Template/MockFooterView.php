<?php
namespace Base\Package\WebsiteCustomize\View\Template;

use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;

class MockFooterView extends FooterView
{
    use MockViewTrait;

    public function getWebsiteCustomize()
    {
        return parent::getWebsiteCustomize();
    }

    public function getParam()
    {
        return parent::getParam();
    }

    public function getTranslator() : WebsiteCustomizeTranslator
    {
        return parent::getTranslator();
    }

    public function getData():array
    {
        return parent::getData();
    }

    public function getDescriptionByDhtml(array $list):array
    {
        return parent::getDescriptionByDhtml($list);
    }

    public function getHeaderSearchAddInputData(array $list):array
    {
        return parent::getHeaderSearchAddInputData($list);
    }
}
