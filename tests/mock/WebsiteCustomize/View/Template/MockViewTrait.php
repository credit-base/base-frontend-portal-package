<?php
namespace Base\Package\WebsiteCustomize\View\Template;

use Sdk\WebsiteCustomize\Model\WebsiteCustomize;

trait MockViewTrait
{
    public function __construct()
    {
        parent::__construct(new WebsiteCustomize(1), 0);
    }
}
