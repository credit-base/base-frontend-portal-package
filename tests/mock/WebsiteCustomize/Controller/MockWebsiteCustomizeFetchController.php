<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;
use Sdk\WebsiteCustomize\Model\WebsiteCustomize;

class MockWebsiteCustomizeFetchController extends WebsiteCustomizeFetchController
{
    public function getRepository():WebsiteCustomizeRepository
    {
        return parent::getRepository();
    }

    public function getCustomizeData(string $category)
    {
        return parent::getCustomizeData($category);
    }
}
