<?php
namespace Base\Package\Common\Controller\Traits;

class MockOperateController
{
    use OperateControllerTrait;

    protected function addView()
    {
        return true;
    }

    protected function addAction()
    {
        return false;
    }

    protected function editView(int $id)
    {
        unset($id);
        return true;
    }

    protected function editAction(int $id)
    {
        unset($id);
        return true;
    }
}
