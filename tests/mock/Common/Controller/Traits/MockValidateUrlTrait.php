<?php
namespace Base\Package\Common\Controller\Traits;

use Sdk\Common\WidgetRules\WidgetRules;

class MockValidateUrlTrait
{
    use ValidateUrlTrait;

    public function publicGetWidgetRules() : WidgetRules
    {
        return $this->getWidgetRules();
    }

    public function publicValidateIndexScenario($id) : bool
    {
        return $this->validateIndexScenario($id);
    }
}
