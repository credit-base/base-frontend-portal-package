<?php
namespace Base\Package\UserGroup\Controller;

use Sdk\UserGroup\Repository\UserGroupRepository;

class MockUserGroupFetchController extends UserGroupFetchController
{
    public function getRepository() : UserGroupRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
