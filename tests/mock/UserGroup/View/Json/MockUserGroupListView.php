<?php
namespace Base\Package\UserGroup\View\Json;

use Sdk\UserGroup\Translator\UserGroupTranslator;

class MockUserGroupListView extends UserGroupListView
{
    public function getUserGroup() : array
    {
        return parent::getUserGroup();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getDataList() : array
    {
        return parent::getDataList();
    }

    public function getTranslator() : UserGroupTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(array(), 0);
    }
}
