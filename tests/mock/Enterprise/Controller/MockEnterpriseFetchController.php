<?php
namespace Base\Package\Enterprise\Controller;

use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

class MockEnterpriseFetchController extends EnterpriseFetchController
{
    public function getRepository() : EnterpriseRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function fetchOneAction($id) : bool
    {
        return parent::fetchOneAction($id);
    }

    public function filterFormatChange($identify, $type) : array
    {
        return parent::filterFormatChange($identify, $type);
    }

    public function getBjSearchDataRepository() : BjSearchDataRepository
    {
        return parent::getBjSearchDataRepository();
    }
}
