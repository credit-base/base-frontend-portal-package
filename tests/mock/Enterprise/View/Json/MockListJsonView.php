<?php
namespace Base\Package\Enterprise\View\Json;

use Sdk\Enterprise\Translator\EnterpriseTranslator;

class MockListJsonView extends ListJsonView
{
    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getList() : array
    {
        return parent::getList();
    }

    public function getTranslator() : EnterpriseTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(array(), 0, SEARCH_NAV['SEARCH_NAV_CREDIT_INFO']);
    }
}
