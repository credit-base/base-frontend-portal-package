<?php


namespace Base\Package\Enterprise\View\Json;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

class MockPermitResourceCatalogDataView extends PermitResourceCatalogDataView
{
    public function getDataList() : array
    {
        return parent::getDataList();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getTranslator() : BjSearchDataTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct([], 0);
    }
}
