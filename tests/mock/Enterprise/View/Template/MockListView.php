<?php


namespace Base\Package\Enterprise\View\Template;

use Base\Package\Enterprise\Controller\EnterpriseFetchController;
use Sdk\Enterprise\Translator\EnterpriseTranslator;

class MockListView extends ListView
{
    public function getEnterpriseList() : array
    {
        return parent::getEnterpriseList();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }
    public function getType() : string
    {
        return parent::getType();
    }

    public function getTranslator() : EnterpriseTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        $enterpriseList = array();
        $count = 0;
        parent::__construct($enterpriseList, $count, SEARCH_NAV['SEARCH_NAV_CREDIT_INFO']);
    }
}
