<?php


namespace Base\Package\Enterprise\View\Template;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Translator\EnterpriseTranslator;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;
use Sdk\Statistical\Model\Statistical;
use Sdk\Statistical\Translator\StaticsEnterpriseRelationInformationCountTranslator as RelationCountTranslator;

class MockEnterpriseView extends EnterpriseView
{
    public function getEnterpriseTranslator() : EnterpriseTranslator
    {
        return parent::getEnterpriseTranslator();
    }

    public function getRelationCountTranslator() : RelationCountTranslator
    {
        return parent::getRelationCountTranslator();
    }

    public function getEnterprise()
    {
        return parent::getEnterprise();
    }

    public function getStatistical()
    {
        return parent::getStatistical();
    }

    public function getPermitCount() : int
    {
        return parent::getPermitCount();
    }

    public function getPermitList() : array
    {
        return parent::getPermitList();
    }

    public function getBjSearchDataTranslator() : BjSearchDataTranslator
    {
        return parent::getBjSearchDataTranslator();
    }

    public function __construct()
    {
        $enterprise = new Enterprise();
        $statistical = new Statistical();
        parent::__construct($enterprise, $statistical);
    }
}
