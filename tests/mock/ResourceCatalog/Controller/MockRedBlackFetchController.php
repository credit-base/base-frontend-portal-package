<?php
namespace Base\Package\ResourceCatalog\Controller;

use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

class MockRedBlackFetchController extends RedBlackFetchController
{
    public function getRepository() : BjSearchDataRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange($type)
    {
        return parent::filterFormatChange($type);
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
