<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Base\Package\ResourceCatalog\View\MockListViewTrait;

class MockDoublePublicityListJsonView extends DoublePublicityListJsonView
{
    use MockListViewTrait;
}
