<?php
namespace Base\Package\ResourceCatalog\View\Json;

use Base\Package\ResourceCatalog\View\MockListViewTrait;

class MockRedBlackListJsonView extends RedBlackListJsonView
{
    use MockListViewTrait;
}
