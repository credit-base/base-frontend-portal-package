<?php
namespace Base\Package\ResourceCatalog\View\Template;

use Base\Package\ResourceCatalog\View\MockViewTrait;

class MockDoublePublicityDataView extends DoublePublicityDataView
{
    use MockViewTrait;
}
