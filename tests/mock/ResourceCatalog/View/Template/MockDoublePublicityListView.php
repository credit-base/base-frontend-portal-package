<?php
namespace Base\Package\ResourceCatalog\View\Template;

use Base\Package\ResourceCatalog\View\MockListViewTrait;

class MockDoublePublicityListView extends DoublePublicityListView
{
    use MockListViewTrait;
}
