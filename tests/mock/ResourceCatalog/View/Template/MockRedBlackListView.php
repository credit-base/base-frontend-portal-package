<?php
namespace Base\Package\ResourceCatalog\View\Template;

use Base\Package\ResourceCatalog\View\MockListViewTrait;

class MockRedBlackListView extends RedBlackListView
{
    use MockListViewTrait;
}
