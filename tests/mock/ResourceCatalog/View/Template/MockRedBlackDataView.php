<?php
namespace Base\Package\ResourceCatalog\View\Template;

use Base\Package\ResourceCatalog\View\MockViewTrait;

class MockRedBlackDataView extends RedBlackDataView
{
    use MockViewTrait;
}
