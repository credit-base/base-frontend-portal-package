<?php
namespace Base\Package\ResourceCatalog\View;

use sdk\ResourceCatalog\Model\BjSearchData;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

trait MockViewTrait
{
    public function getBjSearchData()
    {
        return parent::getBjSearchData();
    }

    public function getTranslator(): BjSearchDataTranslator
    {
        return parent::getTranslator();
    }

    public function getSearchData() : array
    {
        return parent::getSearchData();
    }

    public function itemsRelationTemplate(array $itemsData, array $template):array
    {
        return parent::itemsRelationTemplate($itemsData, $template);
    }

    public function getTemplateIdentify(array $items):array
    {
        return parent::getTemplateIdentify($items);
    }

    public function __construct()
    {
        parent::__construct(new BjSearchData());
    }
}
