<?php
namespace Base\Package\ResourceCatalog\View;

use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

trait MockListViewTrait
{
    public function getBjSearchDataList() : array
    {
        return parent::getBjSearchDataList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function getScene(): string
    {
        return parent::getScene();
    }

    public function getTranslator(): BjSearchDataTranslator
    {
        return parent::getTranslator();
    }

    public function getDataList() : array
    {
        return parent::getDataList();
    }

    public function __construct()
    {
        parent::__construct(array(), 0, '');
    }
}
