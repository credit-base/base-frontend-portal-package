<?php
namespace Base\Package\Journal\View\Json;

use Sdk\Journal\Translator\JournalTranslator;

class MockListJsonView extends ListJsonView
{
    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getList() : array
    {
        return parent::getJournalList();
    }

    public function getTranslator() : JournalTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(array(), 0);
    }
}
