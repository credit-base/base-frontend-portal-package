<?php


namespace Base\Package\Journal\View\Template;

use Sdk\Journal\Translator\JournalTranslator;

class MockListView extends ListView
{
    public function getJournalList() : array
    {
        return parent::getJournalList();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getTranslator() : JournalTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        $journalList = array();
        $count = 0;
        parent::__construct($journalList, $count);
    }
}
