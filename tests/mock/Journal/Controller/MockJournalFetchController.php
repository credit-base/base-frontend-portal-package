<?php
namespace Base\Package\Journal\Controller;

use Sdk\Journal\Repository\JournalRepository;

class MockJournalFetchController extends JournalFetchController
{
    public function getRepository() : JournalRepository
    {
        return parent::getRepository();
    }

    public function filterAction():  bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }
}
