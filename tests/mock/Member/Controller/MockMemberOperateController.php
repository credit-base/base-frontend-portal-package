<?php
namespace Base\Package\Member\Controller;

use Marmot\Framework\Classes\CommandBus;

use Base\Member\Adapter\Member\IMemberAdapter;

class MockMemberOperateController extends MemberOperateController
{
    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editView(int $id) : bool
    {
        return parent::editView($id);
    }

    public function editAction(int $id) : bool
    {
        return parent::editAction($id);
    }

    public function signInView() : bool
    {
        return parent::signInView();
    }

    public function signInAction() : bool
    {
        return parent::signInAction();
    }

    public function locationIndex()
    {
        return parent::locationIndex();
    }

    public function getBackUrl()
    {
        return parent::getBackUrl();
    }
}
