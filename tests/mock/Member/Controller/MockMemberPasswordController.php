<?php
namespace Base\Package\Member\Controller;

class MockMemberPasswordController extends MemberPasswordController
{
    public function resetPasswordView() : bool
    {
        return parent::resetPasswordView();
    }

    public function locationIndex()
    {
        return parent::locationIndex();
    }
    
    public function resetPasswordAction() : bool
    {
        return parent::resetPasswordAction();
    }

    public function updatePasswordView() : bool
    {
        return parent::updatePasswordView();
    }

    public function updatePasswordAction() : bool
    {
        return parent::updatePasswordAction();
    }
}
