<?php
namespace Base\Package\Member\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Classes\CommandBus;

use Sdk\User\WidgetRules\UserWidgetRules;

use Sdk\Member\Repository\MemberRepository;
use Sdk\Member\WidgetRules\MemberWidgetRules;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class MockMemberControllerTrait extends Controller
{
    use MemberControllerTrait;

    public function __construct()
    {
        parent::__construct();
    }

    public function __destruct()
    {
        parent::__destruct();
    }

    public function getMemberWidgetRulesPublic() : MemberWidgetRules
    {
        return $this->getMemberWidgetRules();
    }

    public function getUserWidgetRulesPublic() : UserWidgetRules
    {
        return $this->getUserWidgetRules();
    }

    public function getRepositoryPublic() : MemberRepository
    {
        return $this->getRepository();
    }

    public function getCommandBusPublic() : CommandBus
    {
        return $this->getCommandBus();
    }

    public function validateAddScenarioPublic(
        $userName,
        $realName,
        $cardId,
        $cellphone,
        $email,
        $contactAddress,
        $securityAnswer,
        $password,
        $confirmPassword,
        $securityQuestion
    ) : bool {
        return $this->validateAddScenario(
            $userName,
            $realName,
            $cardId,
            $cellphone,
            $email,
            $contactAddress,
            $securityAnswer,
            $password,
            $confirmPassword,
            $securityQuestion
        );
    }

    public function validateEditScenarioPublic($gender) : bool
    {
        return $this->validateEditScenario($gender);
    }

    public function validateSignInScenarioPublic(
        $userName,
        $password
    ) : bool {
        return $this->validateSignInScenario(
            $userName,
            $password
        );
    }

    public function validateResetPasswordScenarioPublic(
        $userName,
        $securityAnswer,
        $password,
        $confirmPassword
    ) : bool {
        return $this->validateResetPasswordScenario(
            $userName,
            $securityAnswer,
            $password,
            $confirmPassword
        );
    }

    public function validateUpdatePasswordScenarioPublic(
        $oldPassword,
        $password,
        $confirmPassword
    ) : bool {
        return $this->validateUpdatePasswordScenario(
            $oldPassword,
            $password,
            $confirmPassword
        );
    }

    public function validateValidateSecurityScenarioPublic(
        $securityAnswer
    ) : bool {
        return $this->validateValidateSecurityScenario(
            $securityAnswer
        );
    }

    public function validateVerifyUserNameScenarioPublic(
        $userName
    ) : bool {
        return $this->validateVerifyUserNameScenario(
            $userName
        );
    }
}
