<?php
namespace Base\Package\Member\View\Template;

use Sdk\Member\Model\Member;
use Sdk\Member\Translator\MemberTranslator;

class MockEditView extends EditView
{
    public function getMember()
    {
        return parent::getMember();
    }

    public function getTranslator() : MemberTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(new Member());
    }
}
