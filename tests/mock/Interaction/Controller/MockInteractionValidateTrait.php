<?php
namespace Base\Package\Interaction\Controller;

use Sdk\Interaction\WidgetRules\InteractionWidgetRules;

use Sdk\Common\WidgetRules\WidgetRules;

class MockInteractionValidateTrait
{
    use InteractionValidateTrait;

    public function publicGetInteractionWidgetRules():InteractionWidgetRules
    {
        return $this->getInteractionWidgetRules();
    }

    public function publicGetWidgetRules():WidgetRules
    {
        return $this->getWidgetRules();
    }

    public function publicValidateCommonContentScenario(
        $title,
        $content,
        $acceptUserGroupId
    ) : bool {
        return $this->validateCommonContentScenario(
            $title,
            $content,
            $acceptUserGroupId
        );
    }

    public function publicValidateCommonItemsScenario(
        $name,
        $identify,
        $type,
        $contact,
        $images
    ) : bool {
        return $this->validateCommonItemsScenario(
            $name,
            $identify,
            $type,
            $contact,
            $images
        );
    }

    public function publicValidatePraiseAndComplaintScenario(
        $title,
        $content,
        $name,
        $identify,
        $contact,
        $subject,
        $images,
        $type,
        $acceptUserGroupId
    ) : bool {

        return $this->validatePraiseAndComplaintScenario(
            $title,
            $content,
            $name,
            $identify,
            $contact,
            $subject,
            $images,
            $type,
            $acceptUserGroupId
        );
    }

    public function publicValidateCommonScenario(
        $title,
        $content,
        $name,
        $identify,
        $type,
        $contact,
        $images,
        $acceptUserGroupId
    ): bool {
        return $this->validateCommonScenario(
            $title,
            $content,
            $name,
            $identify,
            $type,
            $contact,
            $images,
            $acceptUserGroupId
        );
    }
}
