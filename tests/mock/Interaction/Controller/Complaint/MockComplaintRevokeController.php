<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Marmot\Framework\Classes\CommandBus;

class MockComplaintRevokeController extends ComplaintRevokeController
{
    public function getCommandBus():CommandBus
    {
        return parent::getCommandBus();
    }

    public function revokeAction(int $id)
    {
        return parent::revokeAction($id);
    }
}
