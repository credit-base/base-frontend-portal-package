<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Sdk\Interaction\Repository\ComplaintRepository;

class MockComplaintFetchController extends ComplaintFetchController
{
    public function getRepository() : ComplaintRepository
    {
        return parent::getRepository();
    }

    public function filterAction():  bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange($scene = CONSTANT)
    {
        return parent::filterFormatChange($scene);
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }
}
