<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Marmot\Framework\Classes\CommandBus;

class MockFeedbackRevokeController extends FeedbackRevokeController
{
    public function getCommandBus():CommandBus
    {
        return parent::getCommandBus();
    }

    public function revokeAction(int $id)
    {
        return parent::revokeAction($id);
    }
}
