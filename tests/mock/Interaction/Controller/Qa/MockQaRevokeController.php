<?php
namespace Base\Package\Interaction\Controller\Qa;

use Marmot\Framework\Classes\CommandBus;

class MockQaRevokeController extends QaRevokeController
{
    public function getCommandBus():CommandBus
    {
        return parent::getCommandBus();
    }

    public function revokeAction(int $id)
    {
        return parent::revokeAction($id);
    }
}
