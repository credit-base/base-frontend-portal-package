<?php
namespace Base\Package\Interaction\Controller\Qa;

use Marmot\Framework\Classes\CommandBus;

class MockQaOperateController extends QaOperateController
{
    public function getCommandBus():CommandBus
    {
        return parent::getCommandBus();
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editView(int $id):bool
    {
        return parent::editView($id);
    }

    public function editAction(int $id):bool
    {
        return parent::editAction($id);
    }
}
