<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Framework\Classes\CommandBus;

class MockPraiseRevokeController extends PraiseRevokeController
{
    public function getCommandBus():CommandBus
    {
        return parent::getCommandBus();
    }

    public function revokeAction(int $id)
    {
        return parent::revokeAction($id);
    }
}
