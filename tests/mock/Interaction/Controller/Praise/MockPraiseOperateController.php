<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Framework\Classes\CommandBus;

class MockPraiseOperateController extends PraiseOperateController
{
    public function getCommandBus():CommandBus
    {
        return parent::getCommandBus();
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction() : bool
    {
        return parent::addAction();
    }

    public function editView(int $id):bool
    {
        return parent::editView($id);
    }

    public function editAction(int $id):bool
    {
        return parent::editAction($id);
    }

    public function getAddRequestCommonData() : array
    {
        return parent::getAddRequestCommonData();
    }
}
