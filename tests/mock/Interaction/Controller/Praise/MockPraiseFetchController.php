<?php
namespace Base\Package\Interaction\Controller\Praise;

use Sdk\Interaction\Repository\PraiseRepository;

class MockPraiseFetchController extends PraiseFetchController
{
    public function getRepository() : PraiseRepository
    {
        return parent::getRepository();
    }

    public function filterAction():  bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange($scene = CONSTANT)
    {
        return parent::filterFormatChange($scene);
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }
}
