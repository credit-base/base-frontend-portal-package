<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Sdk\Interaction\Repository\AppealRepository;

class MockAppealFetchController extends AppealFetchController
{
    public function getRepository() : AppealRepository
    {
        return parent::getRepository();
    }

    public function filterAction():  bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange($scene = CONSTANT)
    {
        return parent::filterFormatChange($scene);
    }

    public function fetchOneAction(int $id): bool
    {
        return parent::fetchOneAction($id);
    }
}
