<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Marmot\Framework\Classes\CommandBus;

class MockAppealRevokeController extends AppealRevokeController
{
    public function getCommandBus():CommandBus
    {
        return parent::getCommandBus();
    }

    public function revokeAction(int $id)
    {
        return parent::revokeAction($id);
    }
}
