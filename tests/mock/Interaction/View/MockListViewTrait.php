<?php
namespace Base\Package\Interaction\View;

trait MockListViewTrait
{
    public function getList() : array
    {
        return parent::getList();
    }

    public function getCount(): int
    {
        return parent::getCount();
    }

    public function __construct()
    {
        parent::__construct(array(), 0);
    }
}
