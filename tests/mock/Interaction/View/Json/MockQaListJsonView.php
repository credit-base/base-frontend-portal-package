<?php
namespace Base\Package\Interaction\View\Json;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Qa\QaTranslator;

class MockQaListJsonView extends QaListJsonView
{
    use MockListViewTrait;

    public function getQaTranslator() : QaTranslator
    {
        return parent::getQaTranslator();
    }
}
