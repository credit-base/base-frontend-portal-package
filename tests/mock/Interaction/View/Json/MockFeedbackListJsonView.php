<?php
namespace Base\Package\Interaction\View\Json;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

class MockFeedbackListJsonView extends FeedbackListJsonView
{
    use MockListViewTrait;

    public function getFeedbackTranslator() : FeedbackTranslator
    {
        return parent::getFeedbackTranslator();
    }
}
