<?php
namespace Base\Package\Interaction\View\Json;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Appeal\AppealTranslator;

class MockAppealListJsonView extends AppealListJsonView
{
    use MockListViewTrait;

    public function getAppealTranslator() : AppealTranslator
    {
        return parent::getAppealTranslator();
    }
}
