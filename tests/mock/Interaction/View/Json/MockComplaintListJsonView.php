<?php
namespace Base\Package\Interaction\View\Json;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

class MockComplaintListJsonView extends ComplaintListJsonView
{
    use MockListViewTrait;

    public function getComplaintTranslator() : ComplaintTranslator
    {
        return parent::getComplaintTranslator();
    }
}
