<?php
namespace Base\Package\Interaction\View\Json;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Praise\PraiseTranslator;

class MockPublicPraiseListJsonView extends PublicPraiseListJsonView
{
    use MockListViewTrait;

    public function getPraiseTranslator() : PraiseTranslator
    {
        return parent::getPraiseTranslator();
    }
}
