<?php
namespace Base\Package\Interaction\View\Template\Praise;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Praise\PraiseTranslator;

class MockListView extends ListView
{
    use MockListViewTrait;

    public function getTranslator() : PraiseTranslator
    {
        return parent::getTranslator();
    }
}
