<?php
namespace Base\Package\Interaction\View\Template\Praise;

use Base\Package\Interaction\View\MockViewTrait;

use Sdk\Interaction\Model\Praise;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;

class MockView extends View
{
    use MockViewTrait;

    public function getPraiseTranslator() : PraiseTranslator
    {
        return parent::getPraiseTranslator();
    }

    public function __construct()
    {
        parent::__construct(new Praise());
    }
}
