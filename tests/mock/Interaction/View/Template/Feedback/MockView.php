<?php
namespace Base\Package\Interaction\View\Template\Feedback;

use Base\Package\Interaction\View\MockViewTrait;

use Sdk\Interaction\Model\Feedback;
use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

class MockView extends View
{
    use MockViewTrait;

    public function getFeedbackTranslator() : FeedbackTranslator
    {
        return parent::getFeedbackTranslator();
    }

    public function __construct()
    {
        parent::__construct(new Feedback());
    }
}
