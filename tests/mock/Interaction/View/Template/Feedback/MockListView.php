<?php
namespace Base\Package\Interaction\View\Template\Feedback;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

class MockListView extends ListView
{
    use MockListViewTrait;

    public function getTranslator() : FeedbackTranslator
    {
        return parent::getTranslator();
    }
}
