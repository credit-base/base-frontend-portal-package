<?php
namespace Base\Package\Interaction\View\Template\Appeal;

use Base\Package\Interaction\View\MockViewTrait;

use Sdk\Interaction\Model\Appeal;
use Sdk\Interaction\Translator\Appeal\AppealTranslator;

class MockView extends View
{
    use MockViewTrait;

    public function getAppealTranslator() : AppealTranslator
    {
        return parent::getAppealTranslator();
    }

    public function __construct()
    {
        parent::__construct(new Appeal());
    }
}
