<?php
namespace Base\Package\Interaction\View\Template\Appeal;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Appeal\AppealTranslator;

class MockListView extends ListView
{
    use MockListViewTrait;

    public function getTranslator() : AppealTranslator
    {
        return parent::getTranslator();
    }
}
