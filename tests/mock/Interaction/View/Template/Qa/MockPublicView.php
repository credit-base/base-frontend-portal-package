<?php
namespace Base\Package\Interaction\View\Template\Qa;

use Base\Package\Interaction\View\MockViewTrait;

use Sdk\Interaction\Model\Qa;
use Sdk\Interaction\Translator\Qa\QaTranslator;

class MockPublicView extends PublicView
{
    use MockViewTrait;

    public function getQaTranslator() : QaTranslator
    {
        return parent::getQaTranslator();
    }

    public function __construct()
    {
        parent::__construct(new Qa());
    }
}
