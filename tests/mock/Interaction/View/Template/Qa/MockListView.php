<?php
namespace Base\Package\Interaction\View\Template\Qa;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Qa\QaTranslator;

class MockListView extends ListView
{
    use MockListViewTrait;

    public function getTranslator() : QaTranslator
    {
        return parent::getTranslator();
    }
}
