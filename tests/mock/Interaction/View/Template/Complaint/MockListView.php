<?php
namespace Base\Package\Interaction\View\Template\Complaint;

use Base\Package\Interaction\View\MockListViewTrait;

use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

class MockListView extends ListView
{
    use MockListViewTrait;

    public function getTranslator() : ComplaintTranslator
    {
        return parent::getTranslator();
    }
}
