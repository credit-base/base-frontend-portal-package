<?php
namespace Base\Package\Interaction\View\Template\Complaint;

use Base\Package\Interaction\View\MockViewTrait;

use Sdk\Interaction\Model\Complaint;
use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

class MockView extends View
{
    use MockViewTrait;

    public function getComplaintTranslator() : ComplaintTranslator
    {
        return parent::getComplaintTranslator();
    }

    public function __construct()
    {
        parent::__construct(new Complaint());
    }
}
