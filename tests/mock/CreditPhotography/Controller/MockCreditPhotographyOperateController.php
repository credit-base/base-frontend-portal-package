<?php
namespace Base\Package\CreditPhotography\Controller;

use Marmot\Framework\Classes\CommandBus;

use Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules;

class MockCreditPhotographyOperateController extends CreditPhotographyOperateController
{
    public function getCommandBus() : CommandBus
    {
        return parent::getCommandBus();
    }

    public function getCreditPhotographyWidgetRule() : CreditPhotographyWidgetRules
    {
        return parent::getCreditPhotographyWidgetRule();
    }

    public function addView() : bool
    {
        return parent::addView();
    }

    public function addAction()
    {
        return parent::addAction();
    }

    public function editView(int $id) : bool
    {
        return parent::editView($id) ;
    }

    public function editAction(int $id) : bool
    {
        return parent::editAction($id);
    }
    
    public function validateAddScenario(
        $description,
        $attachments
    ) : bool {
        return parent::validateAddScenario(
            $description,
            $attachments
        );
    }
}
