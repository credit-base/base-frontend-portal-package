<?php
namespace Base\Package\CreditPhotography\Controller;

use Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

class MockMemberCreditPhotographyFetchController extends MemberCreditPhotographyFetchController
{
    public function getRepository() : CreditPhotographyRepository
    {
        return parent::getRepository();
    }

    public function filterAction() : bool
    {
        return parent::filterAction();
    }

    public function filterFormatChange()
    {
        return parent::filterFormatChange();
    }

    public function fetchOneAction(int $id) : bool
    {
        return parent::fetchOneAction($id);
    }
}
