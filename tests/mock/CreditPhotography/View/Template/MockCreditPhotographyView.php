<?php
namespace Base\Package\CreditPhotography\View\Template;

use Base\Package\CreditPhotography\View\MockViewTrait;

class MockCreditPhotographyView extends CreditPhotographyView
{
    use MockViewTrait;
}
