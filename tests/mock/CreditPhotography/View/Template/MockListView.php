<?php
namespace Base\Package\CreditPhotography\View\Template;

use Base\Package\CreditPhotography\View\MockListViewTrait;

class MockListView extends ListView
{
    use MockListViewTrait;
}
