<?php
namespace Base\Package\CreditPhotography\View\Template;

use Base\Package\CreditPhotography\View\MockListViewTrait;

class MockMemberCreditPhotographyListView extends MemberCreditPhotographyListView
{
    use MockListViewTrait;
}
