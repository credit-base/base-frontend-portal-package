<?php
namespace Base\Package\CreditPhotography\View\Template;

use Base\Package\CreditPhotography\View\MockViewTrait;

class MockMemberCreditPhotographyView extends MemberCreditPhotographyView
{
    use MockViewTrait;
}
