<?php
namespace Base\Package\CreditPhotography\View\Json;

use Base\Package\CreditPhotography\View\MockListViewTrait;

class MockMemberCreditPhotographyListJsonView extends MemberCreditPhotographyListJsonView
{
    use MockListViewTrait;
}
