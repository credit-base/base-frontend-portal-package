<?php
namespace Base\Package\CreditPhotography\View\Json;

use Base\Package\CreditPhotography\View\MockListViewTrait;

class MockListJsonView extends ListJsonView
{
    use MockListViewTrait;
}
