<?php
namespace Base\Package\CreditPhotography\View;

use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;
use Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules;

trait MockListViewTrait
{
    public function getCount():int
    {
        return parent::getCount();
    }

    public function getCreditPhotographyList() : array
    {
        return parent::getCreditPhotographyList();
    }

    public function getTranslator() : CreditPhotographyTranslator
    {
        return parent::getTranslator();
    }

    public function getCreditPhotographyWidgetRule() : CreditPhotographyWidgetRules
    {
        return parent::getCreditPhotographyWidgetRule();
    }

    public function getStatistical()
    {
        return parent::getStatistical();
    }

    public function getList() : array
    {
        return parent::getList();
    }

    public function getStaticsNumByType(string $type = 'staticsCreditPhotography')
    {
        return parent::getStaticsNumByType($type);
    }

    public function getDataFormat(array $list) : array
    {
        return parent::getDataFormat($list);
    }

    public function getDataWithType(array $data) : int
    {
        return parent::getDataWithType($data);
    }

    public function validateIdentify(string $identify):bool
    {
        return parent::validateIdentify($identify);
    }

    public function __construct()
    {
        parent::__construct(array(), 0, array());
    }
}
