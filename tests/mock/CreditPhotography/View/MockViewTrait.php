<?php
namespace Base\Package\CreditPhotography\View;

use Sdk\CreditPhotography\Model\CreditPhotography;
use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;
use Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules;

trait MockViewTrait
{
    public function getCount():int
    {
        return parent::getCount();
    }

    public function getCreditPhotography()
    {
        return parent::getCreditPhotography();
    }

    public function getTranslator() : CreditPhotographyTranslator
    {
        return parent::getTranslator();
    }

    public function getCreditPhotographyWidgetRule() : CreditPhotographyWidgetRules
    {
        return parent::getCreditPhotographyWidgetRule();
    }

    public function getData() : array
    {
        return parent::getData();
    }

    public function getDataFormat(array $list) : array
    {
        return parent::getDataFormat($list);
    }

    public function __construct()
    {
        parent::__construct(new CreditPhotography());
    }
}
