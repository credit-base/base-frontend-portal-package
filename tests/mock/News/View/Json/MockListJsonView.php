<?php
namespace Base\Package\News\View\Json;

use Sdk\News\Translator\NewsTranslator;

class MockListJsonView extends ListJsonView
{
    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getList() : array
    {
        return parent::getNewsList();
    }

    public function getType() : int
    {
        return parent::getType();
    }

    public function getTranslator() : NewsTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(array(), 0, NEWS_TYPE['LEADING_GROUP']);
    }
}
