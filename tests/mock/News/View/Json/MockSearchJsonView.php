<?php
namespace Base\Package\News\View\Json;

use Sdk\News\Translator\NewsTranslator;

class MockSearchJsonView extends SearchJsonView
{
    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getList() : array
    {
        return parent::getNewsList();
    }

    public function getKeyword() : string
    {
        return parent::getKeyword();
    }

    public function getTranslator() : NewsTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        $keyword = '';
        parent::__construct(array(), 0, NEWS_TYPE['LEADING_GROUP'], $keyword);
    }
}
