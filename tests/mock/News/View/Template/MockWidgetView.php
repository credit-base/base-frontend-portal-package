<?php
namespace Base\Package\News\View\Template;

use Sdk\News\Translator\NewsTranslator;

class MockWidgetView extends WidgetView
{
    public function getNewsList() : array
    {
        return parent::getNewsList();
    }

    public function getTranslator() : NewsTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(array());
    }
}
