<?php


namespace Base\Package\News\View\Template;

use Sdk\News\Translator\NewsTranslator;

class MockListView extends ListView
{
    public function getNewsList() : array
    {
        return parent::getNewsList();
    }

    public function getBannerList() : array
    {
        return parent::getBannerList();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getType() : int
    {
        return parent::getType();
    }

    public function getTranslator() : NewsTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        $newsList = array();
        $count = 0;
        $bannerList = array();
        parent::__construct(
            $newsList,
            $count,
            NEWS_TYPE['LEADING_GROUP'],
            $bannerList
        );
    }
}
