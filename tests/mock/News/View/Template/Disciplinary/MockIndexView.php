<?php
namespace Base\Package\News\View\Template\Disciplinary;

use Sdk\News\Translator\NewsTranslator;

class MockIndexView extends IndexView
{
    public function getData() : array
    {
        return parent::getData();
    }

    public function getTranslator() : NewsTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(array());
    }
}
