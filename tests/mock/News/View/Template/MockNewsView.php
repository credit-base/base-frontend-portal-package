<?php
namespace Base\Package\News\View\Template;

use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class MockNewsView extends NewsView
{
    public function getNewsList() : array
    {
        return parent::getNewsList();
    }

    public function getNews()
    {
        return parent::getNews();
    }

    public function getTranslator() : NewsTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(new News(), array());
    }
}
