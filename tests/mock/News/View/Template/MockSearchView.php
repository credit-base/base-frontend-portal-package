<?php
namespace Base\Package\News\View\Template;

use Sdk\News\Translator\NewsTranslator;

class MockSearchView extends SearchView
{
    public function getNewsList() : array
    {
        return parent::getNewsList();
    }

    public function getCount() : int
    {
        return parent::getCount();
    }

    public function getKeyword() : string
    {
        return parent::getKeyword();
    }

    public function getTranslator() : NewsTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        $keyword = '';
        parent::__construct(array(), 0, $keyword);
    }
}
