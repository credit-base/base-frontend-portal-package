<?php
namespace Base\Package\News\View\Template;

use Sdk\News\Translator\NewsTranslator;

class MockIndexView extends IndexView
{
    public function getData() : array
    {
        return parent::getData();
    }

    public function getCategory() : int
    {
        return parent::getCategory();
    }

    public function getTranslator() : NewsTranslator
    {
        return parent::getTranslator();
    }

    public function __construct()
    {
        parent::__construct(array(), 0);
    }
}
