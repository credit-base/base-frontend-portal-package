<?php
namespace Base\Package\News\Controller;

class MockNewsFetchController extends NewsFetchController
{
    public function filterAction()
    {
        return parent::filterAction();
    }

    public function filterFormatChange($type)
    {
        return parent::filterFormatChange($type);
    }

    public function fetchOneAction(int $id)
    {
        return parent::fetchOneAction($id);
    }

    public function getOne($id)
    {
        return parent::getOne($id);
    }

    public function relevantArticles($type)
    {
        return parent::relevantArticles($type);
    }
}
