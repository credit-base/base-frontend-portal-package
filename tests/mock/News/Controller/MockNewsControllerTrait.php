<?php
namespace Base\Package\News\Controller;

use Marmot\Framework\Classes\Controller;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Sdk\News\Model\News;
use Sdk\News\Repository\NewsRepository;
use Sdk\News\WidgetRules\NewsWidgetRules;

class MockNewsControllerTrait extends Controller
{
    use NewsControllerTrait;

    public function getNewsPublic() : News
    {
        return $this->getNews();
    }

    public function getRepositoryPublic() : NewsRepository
    {
        return $this->getRepository();
    }

    public function getNewsWidgetRulesPublic() : NewsWidgetRules
    {
        return $this->getNewsWidgetRules();
    }

    public function getConcurrentAdapterPublic() : ConcurrentAdapter
    {
        return $this->getConcurrentAdapter();
    }

    public function validateFilterScenarioPublic($type) : bool
    {
        return $this->validateFilterScenario($type);
    }

    public function validateIndexScenarioPublic($category) : bool
    {
        return $this->validateIndexScenario($category);
    }
}
