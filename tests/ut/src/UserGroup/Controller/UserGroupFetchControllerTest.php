<?php
namespace Base\Package\UserGroup\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\UserGroup\Repository\UserGroupRepository;

use Base\Package\UserGroup\View\Json\UserGroupListView;
use Base\Package\UserGroup\View\Template\WidgetUserGroupView;

class UserGroupFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupFetchController::class)
            ->setMethods(
                [
                    'displayError',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new UserGroupFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\UserGroup\Repository\UserGroupRepository',
            $this->stub->getRepository()
        );
    }

    public function testFilterActionSuccess()
    {
        $this->initialFetchActionSuccess();
    }
    
    public function initialFetchActionSuccess()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getPageAndSize',
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        
        $sort = ['id'];
        $filter = array();
        $page = 2;
        $size = PHP_INT_MAX;
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort, $page, $size]);

        $count = 10;
        $userGroupList = array(1,2);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->scenario(Argument::exact(UserGroupRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($userGroupRepository->reveal());
        $userGroupRepository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count, $userGroupList]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($userGroupRepository->reveal());

        $request = $this->prophesize(Request::class);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new UserGroupListView($userGroupList, $count));

        $result = $this->stub->filterAction();

        $this->assertTrue($result);
    }

    public function testUserGroupWidgetSuccess()
    {
        $this->initialUserGroupWidgetSuccess();
    }
    
    public function initialUserGroupWidgetSuccess()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();
        
        $sort = ['id'];
        $filter = array();
        $page = 1;
        $size = USER_GROUP_ALL_SIZE;

        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort, $page, $size]);

        $count = 100;
        $widgetList = array(1,2);
        $userGroupRepository = $this->prophesize(UserGroupRepository::class);
        $userGroupRepository->scenario(Argument::exact(UserGroupRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($userGroupRepository->reveal());
        $userGroupRepository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count, $widgetList]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($userGroupRepository->reveal());

        $request = $this->prophesize(Request::class);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new WidgetUserGroupView($widgetList, $count));

        $result = $this->stub->userGroupWidget();

        $this->assertTrue($result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupFetchController::class)
            ->setMethods(['getRequest','getPageAndSize'])
            ->getMock();

        $sort = ['id'];
        $page = 1;
        $size = USER_GROUP_ALL_SIZE;
        $filter = array();

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$page, $size]);

        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, $sort, $page, $size], $result);
    }
}
