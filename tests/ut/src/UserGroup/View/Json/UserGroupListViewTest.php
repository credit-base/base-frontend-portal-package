<?php
namespace Base\Package\UserGroup\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\UserGroup\Model\UserGroup;
use Sdk\UserGroup\Translator\UserGroupTranslator;

class UserGroupListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupListView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetUserGroup()
    {
        $result = $this->stub->getUserGroup();
        $this->assertIsArray($result);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\UserGroup\Translator\UserGroupTranslator', $this->stub->getTranslator());
    }

    public function testGetDataList()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupListView::class)->setMethods([
            'encode',
            'getUserGroup',
            'getTranslator',
            'getCount'
        ])->getMock();

        $userGroupData = new UserGroup(1);
        $list = [$userGroupData];
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getUserGroup')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();

        $translator = $this->prophesize(UserGroupTranslator::class);
        $translator->objectToArray(
            $userGroupData,
            array(
                'id',
                'name',
                'shortName'
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        
        $dataList = array(
            'total' => $count,
            'data' => [$result]
        );
        
        $this->assertEquals($dataList, $this->stub->getDataList());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockUserGroupListView::class)->setMethods([
            'encode',
            'getDataList'
        ])->getMock();

        $result = array();
        $this->stub->expects($this->exactly(1))->method('getDataList')->willReturn($result);

        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
