<?php
namespace Base\Package\UserGroup\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class WidgetUserGroupViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockWidgetUserGroupView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockWidgetUserGroupView::class)->setMethods([
            'getView',
            'getDataList'
        ])->getMock();

        $result = array();
        $this->stub->expects($this->exactly(1))->method('getDataList')->willReturn($result);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'UserGroup/WidgetUserGroup.tpl',
            [
                'list'=> [$result]
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
