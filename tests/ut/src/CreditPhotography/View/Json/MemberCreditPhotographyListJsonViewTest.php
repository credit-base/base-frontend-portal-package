<?php
namespace Base\Package\CreditPhotography\View\Json;

use PHPUnit\Framework\TestCase;

class MemberCreditPhotographyListJsonViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyListJsonView::class)
                           ->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }
    
    public function testGetCreditPhotographyList()
    {
        $result = $this->stub->getCreditPhotographyList();
        $this->assertIsArray($result);
    }

    public function testGetStatistical()
    {
        $result = $this->stub->getStatistical();
        $this->assertIsArray($result);
    }

    public function testGetCount()
    {
        $result = $this->stub->getCount();
        $this->assertIsInt($result);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\Translator\CreditPhotographyTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testGetCreditPhotographyWidgetRule()
    {
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules',
            $this->stub->getCreditPhotographyWidgetRule()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
            'getStaticsNumByType'
        ])->getMock();

        $result = array();
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($result);
        $this->stub->expects($this->exactly(1))->method('getStaticsNumByType')->willReturn($result);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
