<?php
namespace Base\Package\CreditPhotography\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\CreditPhotography\Model\CreditPhotography;
use Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules;
use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;

use Sdk\Statistical\Model\Statistical;

class ListJsonViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetList()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'encode',
            'getCreditPhotographyList',
            'getTranslator',
            'getDataFormat'
        ])->getMock();

        $creditPhotography = new CreditPhotography(1);
        $list = [$creditPhotography];

        $this->stub->expects($this->exactly(1))->method('getCreditPhotographyList')->willReturn($list);

        $result = array();

        $translator = $this->prophesize(CreditPhotographyTranslator::class);
        $translator->objectToArray(
            $creditPhotography,
            array(
                'id',
                'description',
                'attachments',
                'member'=>['id','realName'],
                'createTime',
                'updateTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))->method('getDataFormat')->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        
        $this->assertEquals($result, $this->stub->getList());
    }

    public function testGetStaticsNumByType()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'getStatistical',
            'statisticalArray'
        ])->getMock();

        $statistical = new Statistical(1);

        $this->stub->expects($this->exactly(1))->method('getStatistical')->willReturn($statistical);

        $statisticalNumberArray = array(1,2);

        $this->stub->expects($this->exactly(1))->method('statisticalArray')->willReturn($statisticalNumberArray);
        
        $result = $this->stub->getStaticsNumByType();

        $this->assertEquals($statisticalNumberArray, $result);
    }

    public function testGetDataWithTypeOne()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'validateIdentify'
        ])->getMock();
        
        $type = 1;
        $identify = '视频.mp4';
        $data = [
            'attachments'=>array(
                array(
                    'identify'=>$identify
                )
            )
        ];

        $this->stub->expects($this->exactly(1))->method('validateIdentify')->with($identify)->willReturn(true);

        $this->assertEquals($type, $this->stub->getDataWithType($data));
    }

    public function testGetDataWithTypeTwo()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'validateIdentify'
        ])->getMock();
        
        $type = 2;
        $identify = '视频.doc';
        $data = [
            'attachments'=>
            array(
                array(
                'identify'=>$identify
                )
            )
        ];

        $this->stub->expects($this->exactly(1))->method('validateIdentify')->with($identify)->willReturn(false);

        $this->assertEquals($type, $this->stub->getDataWithType($data));
    }

    public function testGetDataFormat()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'getDataWithType'
        ])->getMock();

        $data = array([1,2],[2,4]);
        $type = 1;
        $this->stub->expects($this->any())->method('getDataWithType')->willReturn($type);

        $this->assertIsArray($this->stub->getDataFormat($data));
    }

    public function testValidateIdentifyNull()
    {
        $identify = '';

        $this->assertFalse($this->stub->validateIdentify($identify));
    }

    protected function initValidateIdentify(string $identify, $result)
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'getCreditPhotographyWidgetRule'
        ])->getMock();

        $creditPhotographyWidgetRule = $this->prophesize(CreditPhotographyWidgetRules::class);
        $creditPhotographyWidgetRule->isVideo(
            $identify
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
        ->method('getCreditPhotographyWidgetRule')
        ->willReturn($creditPhotographyWidgetRule->reveal());

        $this->assertEquals($result, $this->stub->validateIdentify($identify));
    }

    public function testValidateIdentifyTrue()
    {
        $identify = '视频.mp4';

        $this->initValidateIdentify($identify, true);
    }

    public function testValidateIdentifyFalse()
    {
        $identify = '视频.doc';

        $this->initValidateIdentify($identify, false);
    }
    
    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
        ])->getMock();

        $result = array();
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($result);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
