<?php
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class ListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods([
            'getView',
            'getList',
            'getCount',
        ])->getMock();

        $list = array();
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'CreditPhotography/List.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PHOTOGRAPHY'],
                'list' => $list
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
