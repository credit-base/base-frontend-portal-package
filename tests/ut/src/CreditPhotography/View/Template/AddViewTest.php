<?php
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class AddViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AddView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(AddView::class)->setMethods([
            'getView',
        ])->getMock();

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'CreditPhotography/Add.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PHOTOGRAPHY']
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
