<?php
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class MemberCreditPhotographyListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyListView::class)
                           ->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyListView::class)->setMethods([
            'getView',
            'getList',
            'getCount',
            'getStaticsNumByType'
        ])->getMock();

        $list = array();
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getStaticsNumByType')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'CreditPhotography/MyList.tpl',
            [
                'list' => $list
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
