<?php
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\CreditPhotography\Model\CreditPhotography;
use Sdk\CreditPhotography\Translator\CreditPhotographyTranslator;

class CreditPhotographyViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCreditPhotography()
    {
        $result = $this->stub->getCreditPhotography();
        $this->assertEquals($result, new CreditPhotography());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\Translator\CreditPhotographyTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testGetCreditPhotographyWidgetRule()
    {
        $this->assertInstanceOf(
            'Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules',
            $this->stub->getCreditPhotographyWidgetRule()
        );
    }

    public function testGetData()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyView::class)->setMethods([
            'encode',
            'getCreditPhotography',
            'getTranslator',
            'getDataWithType'
        ])->getMock();

        $creditPhotography = new CreditPhotography(1);

        $this->stub->expects($this->exactly(1))->method('getCreditPhotography')->willReturn($creditPhotography);

        $result = array(
            'type'=>1
        );

        $translator = $this->prophesize(CreditPhotographyTranslator::class);
        $translator->objectToArray(
            $creditPhotography
        )->shouldBeCalledTimes(1)->willReturn($result);
        
        $type = 1;
        $this->stub->expects($this->exactly(1))->method('getDataWithType')->willReturn($type);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        
        $this->assertEquals($result, $this->stub->getData());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyView::class)->setMethods([
            'getView',
            'getData',
        ])->getMock();

        $data = array();

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($data);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'CreditPhotography/Show.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PHOTOGRAPHY'],
                'data' => $data
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
