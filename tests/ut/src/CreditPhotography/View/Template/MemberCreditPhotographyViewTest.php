<?php
namespace Base\Package\CreditPhotography\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class MemberCreditPhotographyViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyView::class)->setMethods([
            'getView',
            'getData',
        ])->getMock();

        $data = array();

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($data);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'CreditPhotography/MyShow.tpl',
            [
                'data' => $data
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
