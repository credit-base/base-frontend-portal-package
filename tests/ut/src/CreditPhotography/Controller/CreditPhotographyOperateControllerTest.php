<?php

namespace Base\Package\CreditPhotography\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Sdk\Common\WidgetRules\WidgetRules;

use Sdk\CreditPhotography\Model\CreditPhotography;
use Sdk\CreditPhotography\Model\NullCreditPhotography;
use Sdk\CreditPhotography\Repository\CreditPhotographyRepository;
use Base\Package\CreditPhotography\View\Json\ListJsonView;
use Base\Package\CreditPhotography\View\Template\ListView;
use Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\Response;
use Marmot\Framework\Classes\CommandBus;

use Sdk\CreditPhotography\Command\CreditPhotography\AddCreditPhotographyCommand;
use Sdk\CreditPhotography\Command\CreditPhotography\DeleteCreditPhotographyCommand;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class CreditPhotographyOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(MockCreditPhotographyOperateController::class)
            ->setMethods(
                [
                    'render',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new CreditPhotographyOperateController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $controller = new CreditPhotographyOperateController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IOperateAbleController', $controller);
    }

    public function testGetCreditPhotographyWidgetRule()
    {
        $this->assertInstanceof(
            'Sdk\CreditPhotography\WidgetRules\CreditPhotographyWidgetRules',
            $this->controller->getCreditPhotographyWidgetRule()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->controller->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->controller->expects($this->exactly(1))->method('render');

        $result= $this->controller->addView();
        $this->assertTrue($result);
    }

    public function testEditView()
    {
        $id = 1;

        $result= $this->controller->editView($id);
        $this->assertFalse($result);
    }

    public function testEditAction()
    {
        $id = 1;

        $result= $this->controller->editAction($id);
        $this->assertFalse($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        //初始化
        $this->controller = $this->getMockBuilder(MockCreditPhotographyOperateController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validateAddScenario'
                ]
            )->getMock();

        $description = 'description';
        $attachments = array();

        //预言
        $request = $this->prophesize(Request::class);

        $request->post(
            Argument::exact('attachments'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($attachments);

        $request->post(
            Argument::exact('description'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($description);
        
        $this->controller->expects($this->exactly(2))->method('getRequest')->willReturn($request->reveal());
        
        $this->controller->expects($this->exactly(1))
            ->method('validateAddScenario')
            ->with(
                $description,
                $attachments
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddCreditPhotographyCommand(
            $description,
            $attachments
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        //绑定
        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->controller->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->controller->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->controller->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->controller->addAction();
        $this->assertTrue($result);
    }

    public function testValidateAddScenario()
    {
        //初始化
        $this->controller = $this->getMockBuilder(MockCreditPhotographyOperateController::class)
            ->setMethods(
                [
                    'getCreditPhotographyWidgetRule'
                ]
            )->getMock();

        $description = 'description';
        $attachments = array('attachments');

        //预言
        $creditPhotographyWidgetRules = $this->prophesize(CreditPhotographyWidgetRules::class);
        $creditPhotographyWidgetRules->description($description)->shouldBeCalledTimes(1)->willReturn(true);
        $creditPhotographyWidgetRules->attachments($attachments)->shouldBeCalledTimes(1)->willReturn(true);

        //绑定
        $this->controller->expects($this->any())
            ->method('getCreditPhotographyWidgetRule')
            ->willReturn($creditPhotographyWidgetRules->reveal());

        $result = $this->controller->validateAddScenario($description, $attachments);
        //验证
        $this->assertTrue($result);
    }

    public function testDeleteSuccess()
    {
        $id = 0;

        $this->initialDelete($id, true);

        $this->stub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->stub->delete($id);
        
        $this->assertTrue($result);
    }

    public function testDeleteFailure()
    {
        $id = 0;

        $this->initialDelete($id, false);

        $this->stub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->stub->delete($id);
        $this->assertFalse($result);
    }

    private function initialDelete(int $id, bool $result)
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyOperateController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'displaySuccess',
                    'displayError',
                    'validateReasonScenario'
                ]
            )->getMock();

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new DeleteCreditPhotographyCommand(
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }
}
