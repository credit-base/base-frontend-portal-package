<?php
namespace Base\Package\CreditPhotography\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Sdk\CreditPhotography\Repository\CreditPhotographyRepository;
use Sdk\Member\Model\Member;
use Base\Package\CreditPhotography\View\Template\MemberCreditPhotographyListView;
use Base\Package\CreditPhotography\View\Template\MemberCreditPhotographyView;
use Base\Package\CreditPhotography\View\Json\ListJsonView;

class MemberCreditPhotographyFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new MemberCreditPhotographyFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\CreditPhotography\Repository\CreditPhotographyRepository',
            $this->stub->getRepository()
        );
    }

    // public function initialFetchActionSuccess($ajaxResult)
    // {
    //     $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyFetchController::class)
    //         ->setMethods(
    //             [
    //                 'filterFormatChange',
    //                 'getPageAndSize',
    //                 'getRepository',
    //                 'getRequest',
    //                 'render'
    //             ]
    //         )->getMock();

    //     $sort = ['-updateTime'];
    //     $page = 1;
    //     $size = 12;

    //     $applyStatus = "MA";
    //     $filter['applyStatus'] = marmot_decode($applyStatus);
    //     $filter['status'] = CreditPhotography::STATUS['NOMAL'];

    //     $this->stub->expects($this->exactly(1))
    //         ->method('filterFormatChange')
    //         ->willReturn([$filter, $sort]);

    //     $this->stub->expects($this->exactly(1))
    //         ->method('getPageAndSize')
    //         ->willReturn([$page, $size]);
            
    //     $count = 12;
    //     $creditPhotographyArray = array(1,2);

    //     $creditPhotographyRepository = $this->prophesize(CreditPhotographyRepository::class);
    //     $creditPhotographyRepository->scenario(Argument::exact(CreditPhotographyRepository::LIST_MODEL_UN))
    //         ->shouldBeCalledTimes(1)
    //         ->willReturn($creditPhotographyRepository->reveal());

    //     $creditPhotographyRepository->search(
    //         Argument::exact($filter),
    //         Argument::exact($sort),
    //         Argument::exact($page),
    //         Argument::exact($size)
    //     )->shouldBeCalledTimes(1)->willReturn([$count,$creditPhotographyArray]);

    //     $this->stub->expects($this->exactly(1))
    //         ->method('getRepository')
    //         ->willReturn($creditPhotographyRepository->reveal());

    //     $request = $this->prophesize(Request::class);
    //     $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

    //     $this->stub->expects($this->any())
    //         ->method('getRequest')
    //         ->willReturn($request->reveal());

    //     if ($ajaxResult) {
    //         $this->stub->expects($this->exactly(1))
    //             ->method('render')
    //             ->with(new ListJsonView($creditPhotographyArray, $count));
    //     }

    //     if (!$ajaxResult) {
    //         $this->stub->expects($this->exactly(1))
    //             ->method('render')
    //             ->with(new MemberCreditPhotographyListView($creditPhotographyArray, $count));
    //     }

    //     $result = $this->stub->filterAction();
    //     $this->assertTrue($result);
    // }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new CreditPhotography($id);

        $creditPhotographyRepository = $this->prophesize(CreditPhotographyRepository::class);
        $creditPhotographyRepository->scenario(Argument::exact(CreditPhotographyRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($creditPhotographyRepository->reveal());

        $creditPhotographyRepository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($creditPhotographyRepository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new MemberCreditPhotographyView($data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }

    // public function testFilterActionSuccess()
    // {
    //     $this->initialFetchActionSuccess(false);
    // }

    // public function testFilterActionAjaxSuccess()
    // {
    //     $this->initialFetchActionSuccess(true);
    // }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockMemberCreditPhotographyFetchController::class)
            ->setMethods(['getRequest'])
            ->getMock();

        $sort = ['-updateTime'];

        $applyStatus = "MQ";
        $member = new Member(1);
        Core::$container->set('member', $member);
        $filter['member'] = Core::$container->get('member')->getId();
        $filter['applyStatus'] = marmot_decode($applyStatus);
        $filter['status'] = CreditPhotography::STATUS['NOMAL'];

        $request = $this->prophesize(Request::class);

        $request->get(Argument::exact('applyStatus'), Argument::exact('MQ'))
            ->shouldBeCalledTimes(1)->willReturn($applyStatus);

        $this->stub->expects($this->exactly(1))
            ->method('getRequest')->willReturn($request->reveal());

        $expected = [
            [
                'applyStatus'=>marmot_decode($applyStatus),
                'status'=>CreditPhotography::STATUS['NOMAL'],
                'member'=>1
            ],
            $sort
        ];
        
        $result = $this->stub->filterFormatChange();
        $this->assertEquals($expected, $result);
    }
}
