<?php
namespace Base\Package\CreditPhotography\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Base\Sdk\CreditPhotography\Model\CreditPhotography;
use Sdk\CreditPhotography\Repository\CreditPhotographyRepository;

use Base\Package\CreditPhotography\View\Template\ListView;
use Base\Package\CreditPhotography\View\Template\CreditPhotographyView;
use Base\Package\CreditPhotography\View\Json\ListJsonView;

class CreditPhotographyFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new CreditPhotographyFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\CreditPhotography\Repository\CreditPhotographyRepository',
            $this->stub->getRepository()
        );
    }

    public function initialFetchActionSuccess($ajaxResult)
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render'
                ]
            )->getMock();

        $applyStatus = "MQ";
        $filter['applyStatus'] = marmot_decode($applyStatus);
        $filter['status'] = CreditPhotography::STATUS['NOMAL'];

        $sort = ['-updateTime'];
        $page = 1;
        $size = PHP_INT_MAX;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$page, $size]);
            
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $creditPhotographyArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->scenario(Argument::exact(CreditPhotographyRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$creditPhotographyArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);
        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if (!$ajaxResult) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListView($creditPhotographyArray, $count));
        }

        if ($ajaxResult) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListJsonView($creditPhotographyArray, $count));
        }

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialFetchActionSuccess(true);
    }

    public function testFilterActionSuccess()
    {
        $this->initialFetchActionSuccess(false);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockCreditPhotographyFetchController::class)
            ->setMethods(['getRequest'])
            ->getMock();

        $sort = ['-updateTime'];

        $applyStatus = "MQ";
        $filter['applyStatus'] = marmot_decode($applyStatus);
        $filter['status'] = CreditPhotography::STATUS['NOMAL'];

        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, $sort], $result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockCreditPhotographyFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $data = new CreditPhotography($id);

        $repository = $this->prophesize(CreditPhotographyRepository::class);
        $repository->scenario(Argument::exact(CreditPhotographyRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new CreditPhotographyView($data));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }
}
