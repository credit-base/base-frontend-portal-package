<?php
namespace Base\Package\CreditPublicity\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class IndexViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(IndexView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(IndexView::class)->setMethods([
            'getView',
        ])->getMock();

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'CreditPublicity/Index.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PUBLICITY']
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
