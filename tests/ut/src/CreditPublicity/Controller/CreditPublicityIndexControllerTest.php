<?php
namespace Base\Package\CreditPublicity\Controller;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Base\Package\CreditPublicity\View\Template\IndexView;

class CreditPublicityIndexControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(CreditPublicityIndexController::class)
            ->setMethods(
                [
                    'render'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $this->stub);
    }

    public function testImplementsIIndexAbleController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IIndexController',
            $this->stub
        );
    }

    public function testIndexSuccess()
    {
        $this->stub->expects($this->exactly(1))->method('render')->with(new IndexView());

        $result = $this->stub->index();
        $this->assertTrue($result);
    }
}
