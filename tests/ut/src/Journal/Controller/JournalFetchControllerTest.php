<?php
namespace Base\Package\Journal\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Base\Sdk\Common\Model\IEnableAble;

use Sdk\Journal\Model\Journal;
use Sdk\Journal\Model\NullJournal;
use Sdk\Journal\Repository\JournalRepository;

use Base\Package\Journal\View\Template\ListView;
use Base\Package\Journal\View\Json\ListJsonView;

class JournalFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockJournalFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new JournalFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Journal\Repository\JournalRepository',
            $this->stub->getRepository()
        );
    }

    public function initialFetchOneActionSuccess($ajaxResult)
    {
        $this->stub = $this->getMockBuilder(MockJournalFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render'
                ]
            )->getMock();

        $filter['year'] = '2020';
        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        $sort = ['-updateTime'];
        $page = 1;
        $size = PHP_INT_MAX;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->willReturn([$page, $size]);
            
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $journalArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(JournalRepository::class);
        $repository->scenario(Argument::exact(JournalRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$journalArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);

        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListJsonView($journalArray, $count));
        }

        if (!$ajaxResult) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListView($journalArray, $count));
        }

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->initialFetchOneActionSuccess(false);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialFetchOneActionSuccess(true);
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockJournalFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $year = '2020';
        $sort = ['-updateTime'];

        $filter['year'] = $year;
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('year'),
            Argument::exact(date('Y'))
        )->shouldBeCalledTimes(1)->willReturn($year);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, $sort], $result);
    }

    public function testFetchOneAction()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }
}
