<?php
namespace Base\Package\Journal\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;
use Sdk\Journal\Model\Journal;
use Sdk\Journal\Translator\JournalTranslator;

class ListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetJournalList()
    {
        $this->assertIsArray($this->stub->getJournalList());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\Journal\Translator\JournalTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods([
            'getJournalList',
            'getCount',
            'getTranslator',
            'getView',
        ])->getMock();
        $enterpriseData = new Journal(1);
        $list = [$enterpriseData];
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getJournalList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();

        $translator = $this->prophesize(JournalTranslator::class);
        $translator->objectToArray(
            $enterpriseData,
            array(
                'id',
                'title',
                'source',
                'year',
                'cover',
                'attachment',
                'authImages',
                'updateTime',
                'userGroup' => ['id', 'name'],
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Journal/List.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'list' =>  $list
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
