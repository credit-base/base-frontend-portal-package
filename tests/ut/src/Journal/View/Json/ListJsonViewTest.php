<?php
namespace Base\Package\Journal\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Journal\Model\Journal;
use Sdk\Journal\Translator\JournalTranslator;

class ListJsonViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetList()
    {
        $this->assertIsArray($this->stub->getList());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\Journal\Translator\JournalTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'encode',
            'getJournalList',
            'getCount',
            'getTranslator',
        ])->getMock();
        $journal = new Journal();
        $list = [$journal];
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getJournalList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();

        $translator = $this->prophesize(JournalTranslator::class);
        $translator->objectToArray(
            $journal,
            array(
                'id',
                'title',
                'source',
                'cover',
                'attachment',
                'authImages',
                'year',
                'userGroup' => ['id', 'name'],
                'updateTime',
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
