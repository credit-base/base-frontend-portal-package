<?php
namespace Base\Package\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class IndexControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new IndexControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getIndexController('');
            $this->assertInstanceOf(
                'Base\Package\Common\Controller\NullIndexController',
                $controller
            );
    }

    public function testGetIndexController()
    {
        foreach (IndexControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getIndexController($key)
            );
        }
    }
}
