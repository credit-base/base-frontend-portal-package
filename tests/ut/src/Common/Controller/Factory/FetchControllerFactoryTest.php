<?php
namespace Base\Package\Common\Controller\Factory;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class FetchControllerFactoryTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new FetchControllerFactory();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testNullController()
    {
        $controller = $this->controller->getFetchController('');
            $this->assertInstanceOf(
                'Base\Package\Common\Controller\NullFetchController',
                $controller
            );
    }

    public function testGetFetchController()
    {
        foreach (FetchControllerFactory::MAPS as $key => $controller) {
            $this->assertInstanceOf(
                $controller,
                $this->controller->getFetchController($key)
            );
        }
    }
}
