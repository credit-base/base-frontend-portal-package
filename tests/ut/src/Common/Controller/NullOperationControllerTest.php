<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullOperationControllerTest extends TestCase
{
    private $controller;
    
    public function setUp()
    {
        $this->controller = new NullOperationController();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->controller);
    }
    
    public function testImplementIOperateAbleController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $this->controller
        );
    }

    public function testImplementINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->controller
        );
    }

    public function testAdd()
    {
        $this->controller->add();
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }

    public function testEdit()
    {
        $this->controller->edit(0);
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }
}
