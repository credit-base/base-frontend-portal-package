<?php
namespace Base\Package\Common\Controller;

use Marmot\Core;

use PHPUnit\Framework\TestCase;

class NullIndexControllerTest extends TestCase
{
    private $controller;
    
    public function setUp()
    {
        $this->controller = new NullIndexController();
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function tearDown()
    {
        Core::setLastError(ERROR_NOT_DEFINED);
        unset($this->controller);
    }

    public function testImplementIIndexController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IIndexController',
            $this->controller
        );
    }

    public function testImplementINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->controller
        );
    }

    public function testIndex()
    {
        $this->controller->index();
        $this->assertEquals(Core::getLastError()->getId(), ROUTE_NOT_EXIST);
    }
}
