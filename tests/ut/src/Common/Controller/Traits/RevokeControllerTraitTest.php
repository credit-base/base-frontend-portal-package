<?php
namespace Base\Package\Common\Controller\Traits;

use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\Request;

class RevokeControllerTraitTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockRevokeController::class)
        ->setMethods(
            [
                'getRequest',
                'revokeAction',
                'globalCheck',
                'displayError'
            ]
        )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testRevokeFalse()
    {
        $id = 0;
        $this->stub->expects($this->exactly(1))
            ->method('globalCheck')
            ->willReturn(false);

        $this->stub->expects($this->exactly(1))->method('displayError');
        
        $result =$this->stub->revoke($id);

        $this->assertFalse($result);
    }
    /**
     * @dataProvider dataProvider
     */
    public function testRevokeAction($action, $expected)
    {
        $id = 1;
        $request = $this->prophesize(Request::class);

        $this->stub->expects($this->any())->method('getRequest')->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('globalCheck')
            ->willReturn(true);

        $this->stub->expects($this->exactly(1))
            ->method('revokeAction')
            ->with($id)
            ->will($this->returnValue($action));

        $result =$this->stub->revoke($id);

        $this->assertEquals($expected, $result);
    }

    public function dataProvider()
    {
        return [
            [false, false],
            [true, true]
        ];
    }
}
