<?php
namespace Base\Package\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Base\Package\Common\Controller\Interfaces\IIndexController;

class IndexControllerTest extends TestCase
{
    private $controller;

    private $childController;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(IndexController::class)
                          ->setMethods(['getIndexController','displayError'])
                          ->getMock();

        $this->childController = new class extends IndexController
        {
            public function getIndexController(string $resource) : IIndexController
            {
                return parent::getIndexController($resource);
            }
        };
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
    }

    public function testGetIndexController()
    {
        $resource = 'members';
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IIndexController',
            $this->childController->getIndexController($resource)
        );
    }

    public function testIndex()
    {
        $resource = 'test';

        $fetchController = $this->prophesize(IIndexController::class);
        $fetchController->index()->shouldBeCalledTimes(1);

        $this->controller->expects($this->once())
                         ->method('getIndexController')
                         ->with($resource)
                         ->willReturn($fetchController->reveal());

        $this->controller->index($resource);
    }
}
