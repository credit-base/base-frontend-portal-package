<?php
namespace Base\Package\Common\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Controller\WebTrait;
use Base\Package\Common\Controller\Interfaces\IOperateAbleController;

class OperationControllerTest extends TestCase
{
    use WebTrait;

    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(OperationController::class)
                ->setMethods(['getOperationController','displayError', 'validateIndexScenario'])
                ->getMock();
                                 
        $this->childController = new class extends OperationController
        {
            public function getOperationController(string $resource) : IOperateAbleController
            {
                return parent::getOperationController($resource);
            }
        };

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
    }

    public function testGetOperationController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $this->childController->getOperationController($this->resource)
        );
    }

    public function testAdd()
    {
        $operationController = $this->prophesize(IOperateAbleController::class);
        $operationController->add()->shouldBeCalledTimes(1)->willReturn(true);
        $this->controller->expects($this->once())
            ->method('getOperationController')
            ->with($this->resource)
            ->willReturn($operationController->reveal());

        $result = $this->controller->add($this->resource);
        $this->assertTrue($result);
    }

    public function testEditSuccess()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $this->controller->expects($this->once())
            ->method('validateIndexScenario')
            ->with($id)
            ->willReturn(true);

        $operationController = $this->prophesize(IOperateAbleController::class);
        $operationController->edit($id)->shouldBeCalledTimes(1)->willReturn(true);
        $this->controller->expects($this->once())
            ->method('getOperationController')
            ->with($this->resource)
            ->willReturn($operationController->reveal());

        $result = $this->controller->edit($this->resource, $idEncode);
        $this->assertTrue($result);
    }

    public function testEditFail()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $this->controller->expects($this->once())
            ->method('validateIndexScenario')
            ->with($id)
            ->willReturn(false);

        $this->controller->expects($this->once())->method('displayError');

        $result = $this->controller->edit($this->resource, $idEncode);
        $this->assertFalse($result);
    }
}
