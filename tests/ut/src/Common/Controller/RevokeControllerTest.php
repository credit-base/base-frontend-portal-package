<?php
namespace Base\Package\Common\Controller;

use PHPUnit\Framework\TestCase;

use Marmot\Framework\Controller\WebTrait;
use Base\Package\Common\Controller\Interfaces\IRevokeAbleController;

class RevokeControllerTest extends TestCase
{
    use WebTrait;

    private $controller;

    private $childController;

    private $resource;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(RevokeController::class)
                ->setMethods(['getRevokeController','displayError', 'validateIndexScenario'])
                ->getMock();
                                 
        $this->childController = new class extends RevokeController
        {
            public function getRevokeController(string $resource) : IRevokeAbleController
            {
                return parent::getRevokeController($resource);
            }
        };

        $this->resource = 'tests';
    }

    public function tearDown()
    {
        unset($this->controller);
        unset($this->childController);
        unset($this->resource);
    }

    public function testGetRevokeController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IRevokeAbleController',
            $this->childController->getRevokeController($this->resource)
        );
    }

    public function testRevokeSuccess()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $this->controller->expects($this->once())
            ->method('validateIndexScenario')
            ->with($id)
            ->willReturn(true);

        $operationController = $this->prophesize(IRevokeAbleController::class);
        $operationController->revoke($id)->shouldBeCalledTimes(1)->willReturn(true);
        $this->controller->expects($this->once())
            ->method('getRevokeController')
            ->with($this->resource)
            ->willReturn($operationController->reveal());

        $result = $this->controller->revoke($this->resource, $idEncode);
        $this->assertTrue($result);
    }

    public function testRevokeFail()
    {
        $id = 1;
        $idEncode = marmot_encode($id);

        $this->controller->expects($this->once())
            ->method('validateIndexScenario')
            ->with($id)
            ->willReturn(false);

        $this->controller->expects($this->once())->method('displayError');

        $result = $this->controller->revoke($this->resource, $idEncode);
        $this->assertFalse($result);
    }
}
