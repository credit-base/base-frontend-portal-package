<?php
namespace Base\Package\News\Controller\Disciplinary;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Base\Sdk\Common\Model\IEnableAble;

use Sdk\News\Model\News;
use Sdk\News\Model\NullNews;
use Sdk\News\Repository\NewsRepository;
use Sdk\News\Adapter\News\NewsRestfulAdapter;

use Base\Package\News\View\Template\Disciplinary\IndexView;

class NewsIndexControllerTest extends TestCase
{
    private $newsController;

    public function setUp()
    {
        $this->newsController = new MockNewsIndexController();
    }

    public function tearDown()
    {
        unset($this->newsController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $newsController = new NewsIndexController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $newsController);
    }

    public function testImplementsIIndexAbleController()
    {
        $newsController = new NewsIndexController();

        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IIndexController',
            $newsController
        );
    }

    public function testIndexSuccess()
    {
        $this->newsController = $this->getMockBuilder(MockNewsIndexController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getRepository',
                    'render',
                    'getConcurrentAdapter'
                ]
            )->getMock();
        
        list($bannerFilter, $newsFilter, $sort, $newsCategoryList) = $this->filterFormatChange();
        $bannerFilter['parentCategory'] = NEWS_PARENT_CATEGORY['DISCIPLINARY'];
        $count = count($newsCategoryList);

        $this->newsController->expects($this->any(1))->method('filterFormatChange')->willReturn([
            $bannerFilter,
            $newsFilter,
            $sort
        ]);

        $banners = array('banners');
        $newsArray = array('newsArray');
        $data = array($banners, $newsArray);
        
        $newsRestfulAdapter = new NewsRestfulAdapter();

        $newsRepository = $this->prophesize(NewsRepository::class);
        $newsRepository->scenario(Argument::exact(NewsRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($newsRepository->reveal());
        $newsRepository->searchAsync(
            Argument::exact($bannerFilter),
            Argument::exact($sort),
            Argument::exact(DEFAULT_PAGE),
            Argument::exact(BANNER_SIZE)
        )->shouldBeCalledTimes(1)->willReturn([$count,$banners]);

        foreach ($newsCategoryList as $key => $value) {
            unset($value);
            $newsRepository->searchAsync(
                Argument::exact($newsFilter[$key]),
                Argument::exact($sort),
                Argument::exact(DEFAULT_PAGE),
                Argument::exact(NEWS_INDEX_SIZE)
            )->shouldBeCalledTimes(1)->willReturn([$count,$newsArray]);
        }

        $newsRepository->getAdapter()->shouldBeCalledTimes($count+1)->willReturn($newsRestfulAdapter);
        $this->newsController->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($newsRepository->reveal());

        $concurrentAdapter = $this->prophesize(ConcurrentAdapter::class);
        $concurrentAdapter->addPromise(
            'banners',
            [$count,$banners],
            $newsRestfulAdapter
        )->shouldBeCalledTimes(1);

        foreach ($newsCategoryList as $key => $value) {
            $concurrentAdapter->addPromise(
                $key,
                [$count,$newsArray],
                $newsRestfulAdapter
            )->shouldBeCalledTimes(1);
        }

        $concurrentAdapter->run()->shouldBeCalledTimes(1)->willReturn($data);
        $this->newsController->expects($this->exactly($count+2))
            ->method('getConcurrentAdapter')
            ->willReturn($concurrentAdapter->reveal());

        $this->newsController->expects($this->exactly(1))->method('render')->with(new IndexView($data));

        $result = $this->newsController->index();
        $this->assertTrue($result);
    }
    
    private function filterFormatChange()
    {
        $newsCategoryList = NEWS_DISCIPLINARY_INDEX;
        $sort = ['-stick','-updateTime'];

        $newsFilter = array();

        foreach ($newsCategoryList as $key => $value) {
            $newsFilter[$key] = array(
                'category' => $value,
                'dimension' => News::DIMENSION['SOCIOLOGY'],
                'status' => IEnableAble::STATUS['ENABLED']
            );
        }

        $bannerFilter['parentCategory'] = NEWS_PARENT_CATEGORY['DISCIPLINARY'];
        $bannerFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $bannerFilter['status'] = IEnableAble::STATUS['ENABLED'];
        $bannerFilter['bannerStatus'] = News::BANNER_STATUS['ENABLED'];

        return [$bannerFilter, $newsFilter, $sort, $newsCategoryList];
    }

    public function testFilterFormatChange()
    {
        list($bannerFilter, $newsFilter, $sort, $newsCategoryList) = $this->filterFormatChange();
        
        $result = $this->newsController->filterFormatChange($newsCategoryList);

        $this->assertEquals([$bannerFilter, $newsFilter, $sort], $result);
    }
}
