<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Base\Sdk\Common\Model\IEnableAble;

use Sdk\News\Model\News;
use Sdk\News\Model\NullNews;
use Sdk\News\Repository\NewsRepository;
use Base\Package\News\View\Template\SearchView;
use Base\Package\News\View\Json\SearchJsonView;

class NewsSearchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockNewsSearchController();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new NewsSearchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function initialSearchSuccess($ajaxResult)
    {
        $this->stub = $this->getMockBuilder(MockNewsSearchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getRepository',
                    'getRequest',
                    'render'
                ]
            )->getMock();
        
        $keyword = 'keyword';
        $filter['title'] = $keyword;
        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        $sort = ['-stick','-updateTime'];
        $page = 1;

        $this->stub->expects($this->any(1))->method('filterFormatChange')->willReturn([$filter, $sort]);

        $newsArray = array(1,2);
        $count = 2;

        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(Argument::exact(NewsRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact(SEARCH_LIST_SIZE)
        )->shouldBeCalledTimes(1)->willReturn([$count,$newsArray]);

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('keyword'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($keyword);
        $request->get(
            Argument::exact('page'),
            Argument::exact(1)
        )->shouldBeCalledTimes(1)->willReturn($page);

        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new SearchJsonView($newsArray, $count, $keyword));
        }

        if (!$ajaxResult) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new SearchView($newsArray, $count, $keyword));
        }

        $this->stub->expects($this->any())
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->search();
        $this->assertTrue($result);
    }

    public function testSearchSuccess()
    {
        $this->initialSearchSuccess(false);
    }

    public function testSearchAjaxSuccess()
    {
        $this->initialSearchSuccess(true);
    }

    public function testFilterFormatChange()
    {
        $keyword = 'title';
        $sort = ['-stick','-updateTime'];

        $filter['title'] = $keyword;
        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        $result = $this->stub->filterFormatChange($keyword);

        $this->assertEquals([$filter, $sort], $result);
    }
}
