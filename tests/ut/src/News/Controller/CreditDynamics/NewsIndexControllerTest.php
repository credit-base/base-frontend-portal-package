<?php
namespace Base\Package\News\Controller\CreditDynamics;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Base\Sdk\Common\Model\IEnableAble;

use Sdk\News\Model\News;
use Sdk\News\Model\NullNews;
use Sdk\News\Repository\NewsRepository;
use Sdk\News\Adapter\News\NewsRestfulAdapter;

use Base\Package\News\View\Template\CreditDynamics\IndexView;

class NewsIndexControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = new MockNewsIndexController();
    }

    public function tearDown()
    {
        unset($this->controller);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new NewsIndexController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testImplementsIIndexAbleController()
    {
        $controller = new NewsIndexController();

        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IIndexController',
            $controller
        );
    }

    public function testIndexSuccess()
    {
        $this->controller = $this->getMockBuilder(MockNewsIndexController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getRepository',
                    'render',
                    'getConcurrentAdapter'
                ]
            )->getMock();
        
        list($bannerFilter, $newsFilter, $sort, $newsCategoryList) = $this->filterFormatChange();
        $bannerFilter['parentCategory'] = NEWS_PARENT_CATEGORY['CREDIT_DYNAMICS'];
        $count = count($newsCategoryList);

        $this->controller->expects($this->any(1))->method('filterFormatChange')->willReturn([
            $bannerFilter,
            $newsFilter,
            $sort
        ]);

        $bannerArray = array('bannerArray');
        $newsArray = array('newsArray');
        $data = array($bannerArray, $newsArray);
        
        $newsRestfulAdapter = new NewsRestfulAdapter();

        $newsRepository = $this->prophesize(NewsRepository::class);
        $newsRepository->scenario(Argument::exact(NewsRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($newsRepository->reveal());
        $newsRepository->searchAsync(
            Argument::exact($bannerFilter),
            Argument::exact($sort),
            Argument::exact(DEFAULT_PAGE),
            Argument::exact(BANNER_SIZE)
        )->shouldBeCalledTimes(1)->willReturn([$count,$bannerArray]);

        foreach ($newsCategoryList as $key => $value) {
            unset($value);
            $newsRepository->searchAsync(
                Argument::exact($newsFilter[$key]),
                Argument::exact($sort),
                Argument::exact(DEFAULT_PAGE),
                Argument::exact(NEWS_INDEX_SIZE)
            )->shouldBeCalledTimes(1)->willReturn([$count,$newsArray]);
        }

        $newsRepository->getAdapter()->shouldBeCalledTimes($count+1)->willReturn($newsRestfulAdapter);
        $this->controller->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($newsRepository->reveal());

        $concurrentAdapter = $this->prophesize(ConcurrentAdapter::class);
        $concurrentAdapter->addPromise(
            'banners',
            [$count,$bannerArray],
            $newsRestfulAdapter
        )->shouldBeCalledTimes(1);

        foreach ($newsCategoryList as $key => $value) {
            $concurrentAdapter->addPromise(
                $key,
                [$count,$newsArray],
                $newsRestfulAdapter
            )->shouldBeCalledTimes(1);
        }

        $concurrentAdapter->run()->shouldBeCalledTimes(1)->willReturn($data);
        $this->controller->expects($this->exactly($count+2))
            ->method('getConcurrentAdapter')
            ->willReturn($concurrentAdapter->reveal());

        $this->controller->expects($this->exactly(1))->method('render')->with(new IndexView($data));

        $result = $this->controller->index();
        $this->assertTrue($result);
    }
    
    private function filterFormatChange()
    {
        $newsCategoryList = NEWS_CREDIT_DYNAMICS_INDEX;
        $sort = ['-stick','-updateTime'];

        $newsFilter = array();

        foreach ($newsCategoryList as $key => $value) {
            $newsFilter[$key] = array(
                'category' => $value,
                'dimension' => News::DIMENSION['SOCIOLOGY'],
                'status' => IEnableAble::STATUS['ENABLED']
            );
        }

        $bannerFilter['parentCategory'] = NEWS_PARENT_CATEGORY['CREDIT_DYNAMICS'];
        $bannerFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $bannerFilter['status'] = IEnableAble::STATUS['ENABLED'];
        $bannerFilter['bannerStatus'] = News::BANNER_STATUS['ENABLED'];

        return [$bannerFilter, $newsFilter, $sort, $newsCategoryList];
    }

    public function testFilterFormatChange()
    {
        list($bannerFilter, $newsFilter, $sort, $newsCategoryList) = $this->filterFormatChange();
        
        $result = $this->controller->filterFormatChange($newsCategoryList);

        $this->assertEquals([$bannerFilter, $newsFilter, $sort], $result);
    }
}
