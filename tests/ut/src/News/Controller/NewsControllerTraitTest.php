<?php
namespace Base\Package\News\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Sdk\News\Model\News;
use Sdk\News\Repository\NewsRepository;
use Sdk\News\WidgetRules\NewsWidgetRules;

class NewsControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockNewsControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetNewsWidgetRules()
    {
        $this->assertInstanceOf(
            'Sdk\News\WidgetRules\NewsWidgetRules',
            $this->trait->getNewsWidgetRulesPublic()
        );
    }

    public function testGetNews()
    {
        $this->assertInstanceOf(
            'Sdk\News\Model\News',
            $this->trait->getNewsPublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\News\Repository\NewsRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetConcurrentAdapter()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Adapter\ConcurrentAdapter',
            $this->trait->getConcurrentAdapterPublic()
        );
    }

    public function testValidateFilterScenario()
    {
        $controller = $this->getMockBuilder(MockNewsControllerTrait::class)
                 ->setMethods(['getNewsWidgetRules']) ->getMock();

        $type = 'type';

        $newsWidgetRule = $this->prophesize(NewsWidgetRules::class);
        $newsWidgetRule->newsType(Argument::exact($type))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getNewsWidgetRules')
            ->willReturn($newsWidgetRule->reveal());

        $result = $controller->validateFilterScenarioPublic($type);
        
        $this->assertTrue($result);
    }

    protected function validateIndexScenario(
        $category
    ) : bool {
        if (!array_key_exists($category, NEWS_CATEGORY_INDEX)) {
            Core::setLastError(NEWS_TYPE_NOT_EXIST);
            return false;
        }

        return true;
    }
    
    /**
     * @dataProvider invalidValidateIndexScenario
     */
    public function testValidateIndexScenario($actual, $expected)
    {
        $result = $this->trait->validateIndexScenarioPublic($actual);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }
        
        $this->assertFalse($result);
    }

    public function invalidValidateIndexScenario()
    {
        return array(
            array('', false),
            array(NEWS_CATEGORY['POLICY_STATUTE'], true),
            array(0, false),
        );
    }
}
