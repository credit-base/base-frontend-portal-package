<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Base\Sdk\Common\Model\IEnableAble;

use Base\Sdk\News\Model\News;
use Sdk\News\Repository\NewsRepository;
use Base\Package\News\View\Template\WidgetView;

class WidgetControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockWidgetController();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new WidgetController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testNewsWidget()
    {
        $this->stub = $this->getMockBuilder(MockWidgetController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getRepository',
                    'render'
                ]
            )->getMock();

        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        $sort = ['-updateTime'];

        $page = DEFAULT_PAGE;
        $size = WIDGET_SIZE;
            
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $newsArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(Argument::exact(NewsRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$newsArray]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new WidgetView($newsArray));

        $result = $this->stub->newsWidget();
        $this->assertTrue($result);
    }

    public function testFilterFormatChange()
    {
        $sort = ['-updateTime'];

        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        $result = $this->stub->filterFormatChange();

        $this->assertEquals([$filter, $sort], $result);
    }
}
