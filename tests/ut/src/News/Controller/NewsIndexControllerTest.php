<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Adapter\ConcurrentAdapter;

use Base\Sdk\Common\Model\IEnableAble;

use Sdk\News\Model\News;
use Sdk\News\Model\NullNews;
use Sdk\News\Repository\NewsRepository;
use Sdk\News\Adapter\News\NewsRestfulAdapter;

use Base\Package\News\View\Template\IndexView;

class NewsIndexControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new MockNewsIndexController();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new NewsIndexController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testImplementsIIndexAbleController()
    {
        $controller = new NewsIndexController();

        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IIndexController',
            $controller
        );
    }

    public function testIndexSuccess()
    {
        $this->stub = $this->getMockBuilder(MockNewsIndexController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'validateIndexScenario',
                    'getRepository',
                    'getRequest',
                    'render',
                    'getConcurrentAdapter'
                ]
            )->getMock();
        
        $category = key(NEWS_CATEGORY_INDEX);
        $categoryEncode = marmot_encode($category);

        list($bannerFilter, $newsFilter, $sort, $newsTypeList) = $this->filterFormatChange();
        $bannerFilter['category'] = $category;
        $count = count($newsTypeList);

        $request = $this->prophesize(Request::class);
        $request->get(
            Argument::exact('category'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($categoryEncode);
        $this->stub->expects($this->any())->method('getRequest')->willReturn($request->reveal());

        $this->stub->expects($this->exactly(1))->method('validateIndexScenario')->willReturn(true);

        $this->stub->expects($this->any(1))->method('filterFormatChange')->willReturn([
            $bannerFilter,
            $newsFilter,
            $sort
        ]);

        $bannerList = array('bannerList');
        $newsList = array('newsList');
        $data = array($bannerList, $newsList);
        
        $newsRestfulAdapter = new NewsRestfulAdapter();

        $newsRepository = $this->prophesize(NewsRepository::class);
        $newsRepository->scenario(Argument::exact(NewsRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($newsRepository->reveal());
        $newsRepository->searchAsync(
            Argument::exact($bannerFilter),
            Argument::exact($sort),
            Argument::exact(DEFAULT_PAGE),
            Argument::exact(BANNER_SIZE)
        )->shouldBeCalledTimes(1)->willReturn([$count,$bannerList]);

        foreach ($newsTypeList as $key => $value) {
            unset($value);
            $newsRepository->searchAsync(
                Argument::exact($newsFilter[$key]),
                Argument::exact($sort),
                Argument::exact(DEFAULT_PAGE),
                Argument::exact(INDEX_LIST_SIZE)
            )->shouldBeCalledTimes(1)->willReturn([$count,$newsList]);
        }

        $newsRepository->getAdapter()->shouldBeCalledTimes($count+1)->willReturn($newsRestfulAdapter);
        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($newsRepository->reveal());

        $concurrentAdapter = $this->prophesize(ConcurrentAdapter::class);
        $concurrentAdapter->addPromise(
            'banners',
            [$count,$bannerList],
            $newsRestfulAdapter
        )->shouldBeCalledTimes(1);

        foreach ($newsTypeList as $key => $value) {
            $concurrentAdapter->addPromise(
                $key,
                [$count,$newsList],
                $newsRestfulAdapter
            )->shouldBeCalledTimes(1);
        }

        $concurrentAdapter->run()->shouldBeCalledTimes(1)->willReturn($data);
        $this->stub->expects($this->exactly($count+2))
            ->method('getConcurrentAdapter')
            ->willReturn($concurrentAdapter->reveal());

        $this->stub->expects($this->exactly(1))->method('render')->with(new IndexView($data, $category));

        $result = $this->stub->index();
        $this->assertTrue($result);
    }
    
    public function testIndexFail()
    {
        $controller = $this->getMockBuilder(MockNewsIndexController::class)
            ->setMethods(
                [
                    'validateIndexScenario',
                    'getRequest',
                    'displayError'
                ]
            )->getMock();
        
        $category = 1;
        $categoryEncode = marmot_encode($category);

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('category'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($categoryEncode);

        $controller->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('validateIndexScenario')->willReturn(false);

        $controller->expects($this->exactly(1))->method('displayError');

        $result = $controller->index();
        $this->assertFalse($result);
    }

    private function filterFormatChange()
    {
        $newsTypeList = current(NEWS_CATEGORY_INDEX);
        $sort = ['-stick','-updateTime'];

        $newsFilter = array();

        foreach ($newsTypeList as $key => $value) {
            $newsFilter[$key] = array(
                'newsType' => $value,
                'dimension' => News::DIMENSION['SOCIOLOGY'],
                'status' => IEnableAble::STATUS['ENABLED']
            );
        }

        $bannerFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $bannerFilter['status'] = IEnableAble::STATUS['ENABLED'];
        $bannerFilter['bannerStatus'] = News::BANNER_STATUS['ENABLED'];

        return [$bannerFilter, $newsFilter, $sort, $newsTypeList];
    }

    public function testFilterFormatChange()
    {
        list($bannerFilter, $newsFilter, $sort, $newsTypeList) = $this->filterFormatChange();
        
        $result = $this->stub->filterFormatChange($newsTypeList);

        $this->assertEquals([$bannerFilter, $newsFilter, $sort], $result);
    }
}
