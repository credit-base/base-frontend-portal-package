<?php
namespace Base\Package\News\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Base\Sdk\Common\Model\IEnableAble;

use Sdk\News\Model\News;
use Sdk\News\Model\NullNews;
use Sdk\News\Repository\NewsRepository;

use Base\Package\News\View\Template\ListView;
use Base\Package\News\View\Json\ListJsonView;
use Base\Package\News\View\Template\NewsView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class NewsFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getRepository'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new NewsFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testImplementsIFetchAbleController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IFetchAbleController',
            $this->stub
        );
    }

    public function initialFilterActionSuccess($ajaxResult)
    {
        $this->stub = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'validateFilterScenario',
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render'
                ]
            )->getMock();
        
        $type = 1;
        $typeEncode = marmot_encode($type);
        $filter['newsType'] = $type;
        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];
        $sort = ['-stick','-updateTime'];
        $page = 1;
        $size = 6;

        $this->stub->expects($this->exactly(1))->method('validateFilterScenario')->willReturn(true);

        $this->stub->expects($this->exactly(1)) ->method('getPageAndSize')->willReturn([$page, $size]);
            
        $this->stub->expects($this->any(1))->method('filterFormatChange')->willReturn([$filter, $sort]);

        $newsArray = array(1,2);
        $count = 2;

        if ($ajaxResult) {
            $count = 1;
        }

        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(Argument::exact(NewsRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes($count)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$newsArray]);

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('type'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($typeEncode);

        $request->isAjax()->shouldBeCalledTimes($count)->willReturn($ajaxResult);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListJsonView($newsArray, $count, $type));
        }

        if (!$ajaxResult) {
            $bannerFilter['newsType'] = $type;
            $bannerFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
            $bannerFilter['status'] = IEnableAble::STATUS['ENABLED'];

            $this->stub->expects($this->any(1))
                ->method('filterFormatChange')
                ->willReturn([$bannerFilter, $sort]);

            $bannerFilter['bannerStatus'] = News::BANNER_STATUS['ENABLED'];

            $bannerArray = array(1,2);
            $count = 2;
            
            $repository->search(
                Argument::exact($bannerFilter),
                Argument::exact($sort),
                Argument::exact(DEFAULT_PAGE),
                Argument::exact(BANNER_SIZE)
            )->shouldBeCalledTimes(1)->willReturn([$count,$bannerArray]);

            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListView($newsArray, $count, $type, $bannerArray));
        }

        $this->stub->expects($this->any())
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->initialFilterActionSuccess(false);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialFilterActionSuccess(true);
    }

    public function testFilterActionFail()
    {
        $controller = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'validateFilterScenario',
                    'getRequest'
                ]
            )->getMock();
        
        $type = 1;
        $typeEncode = marmot_encode($type);

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('type'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($typeEncode);

        $controller->expects($this->any())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('validateFilterScenario')->willReturn(false);

        $result = $controller->filterAction();
        $this->assertFalse($result);
    }

    public function testFilterFormatChange()
    {
        $type = 1;
        $sort = ['-stick','-updateTime'];

        $filter['newsType'] = $type;
        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];

        $result = $this->stub->filterFormatChange($type);

        $this->assertEquals([$filter, $sort], $result);
    }

    public function testFetchOneActionFail()
    {
        $controller = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'getOne'
                ]
            )->getMock();

        $id = 0;
        $controller->expects($this->exactly(1))->method('getOne')->willReturn(false);

        $result = $controller->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $controller = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'getOne',
                    'relevantArticles',
                    'render'
                ]
            )->getMock();

        $id = 1;
        $news = new News($id);
        $news->setNewsType($id);
        $newsList = array($news);

        $controller->expects($this->exactly(1))->method('getOne')->willReturn($news);

        $controller->expects($this->exactly(1))->method('relevantArticles')->willReturn($newsList);

        $controller->expects($this->exactly(1))->method('render')->with(new NewsView($news, $newsList));

        $result = $controller->fetchOneAction($id);

        $this->assertTrue($result);
    }

    public function testRelevantArticles()
    {
        $controller = $this->getMockBuilder(MockNewsFetchController::class)
            ->setMethods(
                [
                    'getOne',
                    'filterFormatChange',
                    'getRepository'
                ]
            )->getMock();

        $type = 1;
        $sort = ['-stick','-updateTime'];

        $filter['newsType'] = $type;
        $filter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $filter['status'] = IEnableAble::STATUS['ENABLED'];
            
        $controller->expects($this->any(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $newsList = array(1,2);
        $count = 2;
        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(Argument::exact(NewsRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact(DEFAULT_PAGE),
            Argument::exact(NEWS_LIST_SIZE)
        )->shouldBeCalledTimes(1)->willReturn([$count,$newsList]);

        $controller->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $controller->relevantArticles($type);

        $this->assertEquals($newsList, $result);
    }

    public function testGetOneIdNull()
    {
        $id = 0;

        $result = $this->stub->getOne($id);

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testGetOneNull()
    {
        $id = 1;
        $nullNews = new NullNews();

        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(Argument::exact(NewsRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($nullNews);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->getOne($id);

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testGetOneStatusFail()
    {
        $id = 1;
        $news = new News($id);
        $news->setStatus(IEnableAble::STATUS['DISABLED']);

        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(Argument::exact(NewsRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($news);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->getOne($id);

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testGetOneSuccess()
    {
        $id = 1;
        $news = new News($id);
        $news->setStatus(IEnableAble::STATUS['ENABLED']);

        $repository = $this->prophesize(NewsRepository::class);
        $repository->scenario(Argument::exact(NewsRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($news);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->getOne($id);

        $this->assertEquals($news, $result);
    }
}
