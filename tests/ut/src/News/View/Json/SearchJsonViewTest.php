<?php
namespace Base\Package\News\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class SearchJsonViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockSearchJsonView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetKeyword()
    {
        $this->assertIsString($this->stub->getKeyword());
    }

    public function testGetList()
    {
        $this->assertIsArray($this->stub->getList());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\News\Translator\NewsTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockSearchJsonView::class)->setMethods([
            'encode',
            'getNewsList',
            'getCount',
            'getKeyword',
            'getTranslator',
        ])->getMock();
        $news = new News();
        $list = [$news];
        $count = 1;
        $keyword = '';

        $this->stub->expects($this->exactly(1))->method('getNewsList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->stub->expects($this->exactly(1))->method('getKeyword')->willReturn($keyword);

        $result = array();

        $translator = $this->prophesize(NewsTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'title',
                'description',
                'source',
                'updateTime',
                'newsType'
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
