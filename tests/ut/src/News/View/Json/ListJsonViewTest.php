<?php
namespace Base\Package\News\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class ListJsonViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetType()
    {
        $this->assertIsInt($this->stub->getType());
    }

    public function testGetList()
    {
        $this->assertIsArray($this->stub->getList());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\News\Translator\NewsTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'encode',
            'getNewsList',
            'getCount',
            'getType',
            'getTranslator',
        ])->getMock();
        $news = new News();
        $list = [$news];
        $count = 1;
        $type = 151;

        $this->stub->expects($this->exactly(1))->method('getNewsList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->stub->expects($this->exactly(1))->method('getType')->willReturn($type);

        $result = array();

        $translator = $this->prophesize(NewsTranslator::class);
        $translator->objectToArray(
            $news,
            array(
                'id',
                'title',
                'description',
                'newsType',
                'source',
                'updateTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
