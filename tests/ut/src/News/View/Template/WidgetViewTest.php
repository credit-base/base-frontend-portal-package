<?php
namespace Base\Package\News\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;
use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class WidgetViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockWidgetView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetNewsList()
    {
        $this->assertIsArray($this->stub->getNewsList());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\News\Translator\NewsTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockWidgetView::class)->setMethods([
            'getNewsList',
            'getTranslator',
            'getView',
        ])->getMock();
        $newsData = new News(1);
        $list = [$newsData];

        $this->stub->expects($this->exactly(1))->method('getNewsList')->willReturn($list);

        $newsResult = array();
        $translator = $this->prophesize(NewsTranslator::class);
        $translator->objectToArray(
            $newsData,
            array(
                'id',
                'title',
                'newsType'=>[],
            )
        )->shouldBeCalledTimes(1)->willReturn($newsResult);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'News/WidgetNews.tpl',
            [
                'data' => $list,
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
