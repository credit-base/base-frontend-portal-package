<?php
namespace Base\Package\News\View\Template\Disciplinary;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;
use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class IndexViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockIndexView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetData()
    {
        $this->assertIsArray($this->stub->getData());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\News\Translator\NewsTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockIndexView::class)->setMethods([
            'getData',
            'getTranslator',
            'getView',
        ])->getMock();
        $newsData = new News(1);
        $list = ['banners'=>[1,[$newsData]]];

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($list);

        $newsResult = array();
        $translator = $this->prophesize(NewsTranslator::class);
        $translator->objectToArray(
            $newsData,
            array(
                'id',
                'title',
                'description',
                'bannerImage',
                'newsType',
                'updateTime',
                'source'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsResult);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'News/Disciplinary/Index.tpl',
            [
                'data' => $list,
                'nav'=> NAV['NAV_DISCIPLINARY'],
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
