<?php
namespace Base\Package\News\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;
use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class SearchViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockSearchView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetNewsList()
    {
        $this->assertIsArray($this->stub->getNewsList());
    }

    public function testGetKeyword()
    {
        $this->assertIsString($this->stub->getKeyword());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\News\Translator\NewsTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockSearchView::class)->setMethods([
            'getNewsList',
            'getCount',
            'getTranslator',            'getKeyword',
            'getView',
        ])->getMock();
        $newsData = new News(1);
        $list = [$newsData];
        $count = 1;
        $keyword = '';

        $this->stub->expects($this->exactly(1))->method('getNewsList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->stub->expects($this->exactly(1))->method('getKeyword')->willReturn($keyword);

        $newsResult = array();
        $translator = $this->prophesize(NewsTranslator::class);
        $translator->objectToArray(
            $newsData,
            array(
                'id',
                'title',
                'description',
                'source',
                'updateTime',
                'newsType'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsResult);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'News/List.tpl',
            [
                'searchNav'=> SEARCH_NAV['SEARCH_NAV_NEWS'],
                'list' =>  $list,
                'keyword' => $keyword
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
