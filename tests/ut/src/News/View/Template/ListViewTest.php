<?php
namespace Base\Package\News\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;
use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class ListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetNewsList()
    {
        $this->assertIsArray($this->stub->getNewsList());
    }

    public function testGetBannerList()
    {
        $this->assertIsArray($this->stub->getBannerList());
    }

    public function testGetType()
    {
        $this->assertIsInt($this->stub->getType());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\News\Translator\NewsTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods([
            'getNewsList',
            'getCount',
            'getTranslator',
            'getBannerList',
            'getType',
            'getView',
        ])->getMock();
        $newsData = new News(1);
        $list = [$newsData];
        $count = 1;
        $type = 151;

        $this->stub->expects($this->exactly(1))->method('getNewsList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getBannerList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->stub->expects($this->exactly(1))->method('getType')->willReturn($type);

        $newsResult = array();
        $translator = $this->prophesize(NewsTranslator::class);
        $translator->objectToArray(
            $newsData,
            array(
                'id',
                'title',
                'description',
                'newsType',
                'source',
                'updateTime'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsResult);

        $bannerResult = array();
        $translator->objectToArray(
            $newsData,
            array(
                'id',
                'title',
                'newsType',
                'source',
                'description',
                'bannerImage'
            )
        )->shouldBeCalledTimes(1)->willReturn($bannerResult);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'News/List.tpl',
            [
                'nav'=> NAV_NEWS[$type],
                'list' =>  $list,
                'bannerList' => $list
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
