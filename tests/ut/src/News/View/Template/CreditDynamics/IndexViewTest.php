<?php
namespace Base\Package\News\View\Template\CreditDynamics;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;
use Sdk\News\Model\News;
use Sdk\News\Translator\NewsTranslator;

class IndexViewTest extends TestCase
{
    private $creditDynamicsStub;
    public function setUp()
    {
        $this->creditDynamicsStub = $this->getMockBuilder(MockIndexView::class)
                                    ->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->creditDynamicsStub);
    }

    public function testGetData()
    {
        $this->assertIsArray($this->creditDynamicsStub->getData());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\News\Translator\NewsTranslator',
            $this->creditDynamicsStub->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->creditDynamicsStub = $this->getMockBuilder(MockIndexView::class)->setMethods([
            'getData',
            'getTranslator',
            'getView',
        ])->getMock();
        $newsData = new News(1);
        $list = ['banners'=>[1,[$newsData]]];

        $this->creditDynamicsStub->expects($this->exactly(1))->method('getData')->willReturn($list);

        $newsResult = array();
        $translator = $this->prophesize(NewsTranslator::class);
        $translator->objectToArray(
            $newsData,
            array(
                'id',
                'title',
                'description',
                'bannerImage',
                'newsType',
                'updateTime',
                'source'
            )
        )->shouldBeCalledTimes(1)->willReturn($newsResult);

        $this->creditDynamicsStub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'News/CreditDynamics/Index.tpl',
            [
                'data' => $list,
                'nav'=> NAV['NAV_CREDIT_DYNAMICS'],
            ]
        );
        $this->creditDynamicsStub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->creditDynamicsStub->display());
    }
}
