<?php
namespace Base\Package\Interaction\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Qa;
use Sdk\Interaction\Translator\Qa\QaTranslator;

class PublicQaListJsonViewTest extends TestCase
{
    private $publicQaStub;
    public function setUp()
    {
        $this->publicQaStub = $this->getMockBuilder(MockPublicQaListJsonView::class)
                                   ->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->publicQaStub);
    }

    public function testGetQaTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Qa\QaTranslator',
            $this->publicQaStub->getQaTranslator()
        );
    }

    public function testDisplay()
    {
        $this->publicQaStub = $this->getMockBuilder(MockPublicQaListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
            'getQaTranslator'
        ])->getMock();

        $publicQaData = new Qa(1);
        $publicQaList = [$publicQaData];
        $count = 1;

        $this->publicQaStub->expects($this->exactly(1))->method('getList')->willReturn($publicQaList);
        $this->publicQaStub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(QaTranslator::class);
        $translator->objectToArray(
            $publicQaData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->publicQaStub->expects($this->exactly(1))
            ->method('getQaTranslator')
            ->willReturn($translator->reveal());

        $this->publicQaStub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->publicQaStub->display());
    }
}
