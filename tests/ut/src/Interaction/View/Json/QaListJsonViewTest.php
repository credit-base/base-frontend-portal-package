<?php
namespace Base\Package\Interaction\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Qa;
use Sdk\Interaction\Translator\Qa\QaTranslator;

class QaListJsonViewTest extends TestCase
{
    private $qaStub;
    public function setUp()
    {
        $this->qaStub = $this->getMockBuilder(MockQaListJsonView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->qaStub);
    }

    public function testGetQaTranslator()
    {
        $this->assertInstanceOf('Sdk\Interaction\Translator\Qa\QaTranslator', $this->qaStub->getQaTranslator());
    }

    public function testDisplay()
    {
        $this->qaStub = $this->getMockBuilder(MockQaListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
            'getQaTranslator'
        ])->getMock();

        $qaData = new Qa(1);
        $qaList = [$qaData];
        $count = 1;

        $this->qaStub->expects($this->exactly(1))->method('getList')->willReturn($qaList);
        $this->qaStub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(QaTranslator::class);
        $translator->objectToArray(
            $qaData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->qaStub->expects($this->exactly(1))
            ->method('getQaTranslator')
            ->willReturn($translator->reveal());

        $this->qaStub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->qaStub->display());
    }
}
