<?php
namespace Base\Package\Interaction\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Feedback;
use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

class FeedbackListJsonViewTest extends TestCase
{
    private $feedbackStub;
    public function setUp()
    {
        $this->feedbackStub = $this->getMockBuilder(MockFeedbackListJsonView::class)
                                   ->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->feedbackStub);
    }

    public function testGetFeedbackTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Feedback\FeedbackTranslator',
            $this->feedbackStub->getFeedbackTranslator()
        );
    }

    public function testDisplay()
    {
        $this->feedbackStub = $this->getMockBuilder(MockFeedbackListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
            'getFeedbackTranslator'
        ])->getMock();

        $feedbackData = new Feedback(1);
        $feedbackList = [$feedbackData];
        $count = 1;

        $this->feedbackStub->expects($this->exactly(1))->method('getList')->willReturn($feedbackList);
        $this->feedbackStub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(FeedbackTranslator::class);
        $translator->objectToArray(
            $feedbackData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->feedbackStub->expects($this->exactly(1))
            ->method('getFeedbackTranslator')
            ->willReturn($translator->reveal());

        $this->feedbackStub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->feedbackStub->display());
    }
}
