<?php
namespace Base\Package\Interaction\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Praise;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;

class PublicPraiseListJsonViewTest extends TestCase
{
    private $publicPraiseStub;
    public function setUp()
    {
        $this->publicPraiseStub = $this->getMockBuilder(MockPublicPraiseListJsonView::class)
                                    ->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->publicPraiseStub);
    }

    public function testGetPraiseTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Praise\PraiseTranslator',
            $this->publicPraiseStub->getPraiseTranslator()
        );
    }

    public function testDisplay()
    {
        $this->publicPraiseStub = $this->getMockBuilder(MockPublicPraiseListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
            'getPraiseTranslator'
        ])->getMock();

        $publicPraiseData = new Praise(1);
        $publicPraiseList = [$publicPraiseData];
        $count = 1;

        $this->publicPraiseStub->expects($this->exactly(1))->method('getList')->willReturn($publicPraiseList);
        $this->publicPraiseStub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $resultData = array();
        $translator = $this->prophesize(PraiseTranslator::class);
        $translator->objectToArray(
            $publicPraiseData
        )->shouldBeCalledTimes(1)->willReturn($resultData);

        $this->publicPraiseStub->expects($this->exactly(1))
            ->method('getPraiseTranslator')
            ->willReturn($translator->reveal());

        $this->publicPraiseStub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->publicPraiseStub->display());
    }
}
