<?php
namespace Base\Package\Interaction\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Appeal;
use Sdk\Interaction\Translator\Appeal\AppealTranslator;

class AppealListJsonViewTest extends TestCase
{
    private $appealStub;
    public function setUp()
    {
        $this->appealStub = $this->getMockBuilder(MockAppealListJsonView::class)
                                 ->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->appealStub);
    }

    public function testGetList()
    {
        $result = $this->appealStub->getList();
        $this->assertIsArray($result);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->appealStub->getCount());
    }

    public function testGetAppealTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Appeal\AppealTranslator',
            $this->appealStub->getAppealTranslator()
        );
    }

    public function testDisplay()
    {
        $this->appealStub = $this->getMockBuilder(MockAppealListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
            'getAppealTranslator'
        ])->getMock();

        $appealData = new Appeal(1);
        $appealList = [$appealData];
        $count = 1;

        $this->appealStub->expects($this->exactly(1))->method('getList')->willReturn($appealList);
        $this->appealStub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(AppealTranslator::class);
        $translator->objectToArray(
            $appealData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->appealStub->expects($this->exactly(1))
            ->method('getAppealTranslator')
            ->willReturn($translator->reveal());

        $this->appealStub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->appealStub->display());
    }
}
