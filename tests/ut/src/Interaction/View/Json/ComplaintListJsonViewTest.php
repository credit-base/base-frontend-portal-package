<?php
namespace Base\Package\Interaction\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Complaint;
use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

class ComplaintListJsonViewTest extends TestCase
{
    private $complaintStub;
    public function setUp()
    {
        $this->complaintStub = $this->getMockBuilder(MockComplaintListJsonView::class)
                                    ->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->complaintStub);
    }

    public function testGetComplaintTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Complaint\ComplaintTranslator',
            $this->complaintStub->getComplaintTranslator()
        );
    }

    public function testDisplay()
    {
        $this->complaintStub = $this->getMockBuilder(MockComplaintListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
            'getComplaintTranslator'
        ])->getMock();

        $complaintData = new Complaint(1);
        $complaintList = [$complaintData];
        $count = 1;

        $this->complaintStub->expects($this->exactly(1))->method('getList')->willReturn($complaintList);
        $this->complaintStub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(ComplaintTranslator::class);
        $translator->objectToArray(
            $complaintData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->complaintStub->expects($this->exactly(1))
            ->method('getComplaintTranslator')
            ->willReturn($translator->reveal());

        $this->complaintStub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->complaintStub->display());
    }
}
