<?php
namespace Base\Package\Interaction\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Praise;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;

class PraiseListJsonViewTest extends TestCase
{
    private $praiseStub;
    public function setUp()
    {
        $this->praiseStub = $this->getMockBuilder(MockPraiseListJsonView::class)
                                 ->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->praiseStub);
    }

    public function testGetPraiseTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Praise\PraiseTranslator',
            $this->praiseStub->getPraiseTranslator()
        );
    }

    public function testDisplay()
    {
        $this->praiseStub = $this->getMockBuilder(MockPraiseListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
            'getPraiseTranslator'
        ])->getMock();

        $praiseData = new Praise(1);
        $praiseList = [$praiseData];
        $count = 1;

        $this->praiseStub->expects($this->exactly(1))->method('getList')->willReturn($praiseList);
        $this->praiseStub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(PraiseTranslator::class);
        $translator->objectToArray(
            $praiseData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->praiseStub->expects($this->exactly(1))
            ->method('getPraiseTranslator')
            ->willReturn($translator->reveal());

        $this->praiseStub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->praiseStub->display());
    }
}
