<?php
namespace Base\Package\Interaction\View\Template\Appeal;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Appeal;
use Sdk\Interaction\Translator\Appeal\AppealTranslator;

class ViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetData()
    {
        $result = $this->stub->getData();
        $this->assertEquals($result, new Appeal());
    }

    public function testGetAppealTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Appeal\AppealTranslator',
            $this->stub->getAppealTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockView::class)->setMethods([
            'getView',
            'getData',
            'getCount',
            'getAppealTranslator'
        ])->getMock();

        $appealData = new Appeal(1);

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($appealData);

        $result = array();
        $translator = $this->prophesize(AppealTranslator::class);
        $translator->objectToArray(
            $appealData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getAppealTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Interaction/Appeal/Detail.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
