<?php
namespace Base\Package\Interaction\View\Template\Appeal;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Appeal;
use Sdk\Interaction\Translator\Appeal\AppealTranslator;

class ListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetAppealTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Appeal\AppealTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods([
            'getView',
            'getList',
            'getCount',
            'getTranslator'
        ])->getMock();

        $appealData = new Appeal(1);
        $appealList = [$appealData];
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($appealList);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(AppealTranslator::class);
        $translator->objectToArray(
            $appealData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Interaction/Appeal/List.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
