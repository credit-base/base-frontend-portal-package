<?php
namespace Base\Package\Interaction\View\Template\Praise;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Praise;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;

class PublicViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockPublicView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetData()
    {
        $result = $this->stub->getData();
        $this->assertEquals($result, new Praise());
    }

    public function testGetPraiseTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Praise\PraiseTranslator',
            $this->stub->getPraiseTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockPublicView::class)->setMethods([
            'getView',
            'getData',
            'getCount',
            'getPraiseTranslator'
        ])->getMock();

        $praise = new Praise(1);

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($praise);

        $result = array();
        $translator = $this->prophesize(PraiseTranslator::class);
        $translator->objectToArray(
            $praise
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getPraiseTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Interaction/Praise/PublicDetail.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
