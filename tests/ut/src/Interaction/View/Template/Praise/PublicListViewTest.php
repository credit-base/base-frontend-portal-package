<?php
namespace Base\Package\Interaction\View\Template\Praise;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Praise;
use Sdk\Interaction\Translator\Praise\PraiseTranslator;

class PublicListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockPublicListView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetPraiseTranslator()
    {
        $this->assertInstanceOf('Sdk\Interaction\Translator\Praise\PraiseTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockPublicListView::class)->setMethods([
            'getView',
            'getList',
            'getCount',
            'getTranslator'
        ])->getMock();

        $publicPraiseData = new Praise(1);
        $publicPraiseList = [$publicPraiseData];
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($publicPraiseList);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(PraiseTranslator::class);
        $translator->objectToArray(
            $publicPraiseData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Interaction/Praise/PublicList.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
