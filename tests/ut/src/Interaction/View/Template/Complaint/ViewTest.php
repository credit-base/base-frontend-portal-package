<?php
namespace Base\Package\Interaction\View\Template\Complaint;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Complaint;
use Sdk\Interaction\Translator\Complaint\ComplaintTranslator;

class ViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetData()
    {
        $result = $this->stub->getData();
        $this->assertEquals($result, new Complaint());
    }

    public function testGetComplaintTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Complaint\ComplaintTranslator',
            $this->stub->getComplaintTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockView::class)->setMethods([
            'getView',
            'getData',
            'getCount',
            'getComplaintTranslator'
        ])->getMock();

        $complaintData = new Complaint(1);

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($complaintData);

        $result = array();
        $translator = $this->prophesize(ComplaintTranslator::class);
        $translator->objectToArray(
            $complaintData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getComplaintTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Interaction/Complaint/Detail.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
