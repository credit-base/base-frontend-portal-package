<?php
namespace Base\Package\Interaction\View\Template\Qa;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Qa;
use Sdk\Interaction\Translator\Qa\QaTranslator;

class PublicViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockPublicView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetData()
    {
        $result = $this->stub->getData();
        $this->assertEquals($result, new Qa());
    }

    public function testGetQaTranslator()
    {
        $this->assertInstanceOf('Sdk\Interaction\Translator\Qa\QaTranslator', $this->stub->getQaTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockPublicView::class)->setMethods([
            'getView',
            'getData',
            'getCount',
            'getQaTranslator'
        ])->getMock();

        $qaObject = new Qa(1);

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($qaObject);

        $result = array();
        $translator = $this->prophesize(QaTranslator::class);
        $translator->objectToArray(
            $qaObject
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getQaTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Interaction/Qa/PublicDetail.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
