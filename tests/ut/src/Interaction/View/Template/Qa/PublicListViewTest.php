<?php
namespace Base\Package\Interaction\View\Template\Qa;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Qa;
use Sdk\Interaction\Translator\Qa\QaTranslator;

class PublicListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockPublicListView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetQaTranslator()
    {
        $this->assertInstanceOf('Sdk\Interaction\Translator\Qa\QaTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockPublicListView::class)->setMethods([
            'getView',
            'getList',
            'getCount',
            'getTranslator'
        ])->getMock();

        $publicQaData = new Qa(1);
        $publicQaList = [$publicQaData];
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($publicQaList);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(QaTranslator::class);
        $translator->objectToArray(
            $publicQaData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Interaction/Qa/PublicList.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
