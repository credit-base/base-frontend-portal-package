<?php
namespace Base\Package\Interaction\View\Template\Feedback;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Feedback;
use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

class ViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetData()
    {
        $result = $this->stub->getData();
        $this->assertEquals($result, new Feedback());
    }

    public function testGetFeedbackTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Interaction\Translator\Feedback\FeedbackTranslator',
            $this->stub->getFeedbackTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockView::class)->setMethods([
            'getView',
            'getData',
            'getCount',
            'getFeedbackTranslator'
        ])->getMock();

        $feedbackData = new Feedback(1);

        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($feedbackData);

        $result = array();
        $translator = $this->prophesize(FeedbackTranslator::class);
        $translator->objectToArray(
            $feedbackData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getFeedbackTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Interaction/Feedback/Detail.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
