<?php
namespace Base\Package\Interaction\View\Template\Feedback;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Interaction\Model\Feedback;
use Sdk\Interaction\Translator\Feedback\FeedbackTranslator;

class ListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetFeedbackTranslator()
    {
        $this->assertInstanceOf('Sdk\Interaction\Translator\Feedback\FeedbackTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods([
            'getView',
            'getList',
            'getCount',
            'getTranslator'
        ])->getMock();

        $feedbackData = new Feedback(1);
        $feedbackList = [$feedbackData];
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($feedbackList);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();
        $translator = $this->prophesize(FeedbackTranslator::class);
        $translator->objectToArray(
            $feedbackData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Interaction/Feedback/List.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
