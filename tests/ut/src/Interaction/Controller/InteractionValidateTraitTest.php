<?php
namespace Base\Package\Interaction\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\Common\WidgetRules\WidgetRules;
use Sdk\Interaction\WidgetRules\InteractionWidgetRules;

class InteractionValidateTraitTest extends TestCase
{
    use RequestDataTrait;

    private $trait;

    private $faker;

    public function setUp()
    {
        $this->trait = $this->getMockBuilder(MockInteractionValidateTrait::class)
                            ->getMock();

        $this->faker = \Faker\Factory::create();
    }

    public function tearDown()
    {
        unset($this->trait);
        unset($this->faker);
    }

    public function testGetInteractionWidgetRules()
    {
        $trait = new MockInteractionValidateTrait();
        $this->assertInstanceOf(
            'Sdk\Interaction\WidgetRules\InteractionWidgetRules',
            $trait->publicGetInteractionWidgetRules()
        );
    }

    public function testGetWidgetRules()
    {
        $trait = new MockInteractionValidateTrait();
        $this->assertInstanceOf(
            'Sdk\Common\WidgetRules\WidgetRules',
            $trait->publicGetWidgetRules()
        );
    }

    public function testValidateCommonContentScenario()
    {
        $trait = $this->getMockBuilder(MockInteractionValidateTrait::class)
                           ->setMethods(
                               [
                                    'getWidgetRules',
                                    'getInteractionWidgetRules',
                                ]
                           )
                           ->getMock();

        $data = $this->getAddCommonData();

        $commonWidgetRules = $this->prophesize(WidgetRules::class);
        $interactionWidgetRules = $this->prophesize(InteractionWidgetRules::class);

        $commonWidgetRules->formatNumeric(
            Argument::exact($data['acceptUserGroupId']),
            Argument::exact('acceptUserGroupId')
        )->shouldBeCalledTimes(1)->willReturn(true);
    
        $interactionWidgetRules->content(
            Argument::exact($data['content']),
            Argument::exact('content')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $interactionWidgetRules->title(
            Argument::exact($data['title']),
            Argument::exact('title')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getWidgetRules')
            ->willReturn($commonWidgetRules->reveal());

        $trait->expects($this->exactly(1))
            ->method('getInteractionWidgetRules')
            ->willReturn($interactionWidgetRules->reveal());

        $result = $trait->publicValidateCommonContentScenario(
            $data['title'],
            $data['content'],
            $data['acceptUserGroupId']
        );
       
        $this->assertTrue($result);
    }

    public function testValidateCommonItemsScenario()
    {
        $trait = $this->getMockBuilder(MockInteractionValidateTrait::class)
                           ->setMethods(
                               [
                                    'getInteractionWidgetRules',
                                ]
                           )
                           ->getMock();

        $data = $this->getAddCommonData();

        $interactionWidgetRules = $this->prophesize(InteractionWidgetRules::class);

        $interactionWidgetRules->subject(
            Argument::exact($data['name']),
            Argument::exact('name')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $interactionWidgetRules->identify(
            Argument::exact($data['identify'])
        )->shouldBeCalledTimes(1)->willReturn(true);

        $interactionWidgetRules->contact(
            Argument::exact($data['contact'])
        )->shouldBeCalledTimes(1)->willReturn(true);

        $interactionWidgetRules->type(
            Argument::exact($data['type'])
        )->shouldBeCalledTimes(1)->willReturn(true);

        $interactionWidgetRules->images(
            Argument::exact($data['images'])
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getInteractionWidgetRules')
            ->willReturn($interactionWidgetRules->reveal());

        $result = $trait->publicValidateCommonItemsScenario(
            $data['name'],
            $data['identify'],
            $data['type'],
            $data['contact'],
            $data['images']
        );

        $this->assertTrue($result);
    }

    public function testValidatePraiseAndComplaintScenario()
    {
        $trait = $this->getMockBuilder(MockInteractionValidateTrait::class)
                           ->setMethods(
                               [
                                    'validateCommonScenario',
                                    'getInteractionWidgetRules',
                                ]
                           )
                           ->getMock();

        $data = $this->getAddCommonData();

        $interactionWidgetRules = $this->prophesize(InteractionWidgetRules::class);
    
        $interactionWidgetRules->subject(
            Argument::exact($data['subject']),
            Argument::exact('subject')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['title'],
                $data['content'],
                $data['name'],
                $data['identify'],
                $data['type'],
                $data['contact'],
                $data['images'],
                $data['acceptUserGroupId']
            )
            ->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('getInteractionWidgetRules')
            ->willReturn($interactionWidgetRules->reveal());

        $result = $trait->publicValidatePraiseAndComplaintScenario(
            $data['title'],
            $data['content'],
            $data['name'],
            $data['identify'],
            $data['contact'],
            $data['subject'],
            $data['images'],
            $data['type'],
            $data['acceptUserGroupId']
        );
       
        $this->assertTrue($result);
    }

    public function testValidateCommonScenario()
    {
        $trait = $this->getMockBuilder(MockInteractionValidateTrait::class)
                           ->setMethods(
                               [
                                    'validateCommonContentScenario',
                                    'validateCommonItemsScenario',
                                ]
                           )
                           ->getMock();

        $data = $this->getAddCommonData();

        $trait->expects($this->exactly(1))
            ->method('validateCommonContentScenario')
            ->with(
                $data['title'],
                $data['content'],
                $data['acceptUserGroupId']
            )
            ->willReturn(true);

        $trait->expects($this->exactly(1))
            ->method('validateCommonItemsScenario')
            ->with(
                $data['name'],
                $data['identify'],
                $data['type'],
                $data['contact'],
                $data['images']
            )
            ->willReturn(true);

        $result = $trait->publicValidateCommonScenario(
            $data['title'],
            $data['content'],
            $data['name'],
            $data['identify'],
            $data['type'],
            $data['contact'],
            $data['images'],
            $data['acceptUserGroupId']
        );
       
        $this->assertTrue($result);
    }
}
