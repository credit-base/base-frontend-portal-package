<?php

namespace Base\Package\Interaction\Controller\Appeal;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Appeal\AddAppealCommand;
use Sdk\Interaction\WidgetRules\InteractionWidgetRules;

use Base\Package\Interaction\View\Template\Appeal\AddView;
use Base\Package\Interaction\Controller\RequestDataTrait;

class AppealOperateControllerTest extends TestCase
{
    use RequestDataTrait;

    private $appealController;

    public function setUp()
    {
        $this->appealController = $this->getMockBuilder(MockAppealOperateController::class)
            ->setMethods(
                [
                    'render',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->appealController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $appealController = new AppealOperateController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $appealController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $appealController = new AppealOperateController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $appealController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->appealController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->appealController->expects($this->exactly(1))
        ->method('render')
        ->with(new AddView());
        $result = $this->appealController->addView();
        $this->assertTrue($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        $this->appealController = $this->getMockBuilder(MockAppealOperateController::class)
            ->setMethods([
                    'getRequest', 'displayError', 'displaySuccess', 'getCommandBus', 'validateAddAppealScenario',
                    'getAddRequestCommonData'
                ])->getMock();

        $data = $this->getAddCommonData();

        $this->appealController->expects($this->exactly(1))->method('getAddRequestCommonData')->willReturn($data);

        $this->appealController->expects($this->exactly(1))->method('validateAddAppealScenario')
            ->with(
                $data['title'],
                $data['content'],
                $data['name'],
                $data['identify'],
                $data['contact'],
                $data['certificates'],
                $data['images'],
                $data['type'],
                $data['acceptUserGroupId']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddAppealCommand(
            $data['title'],
            $data['content'],
            $data['name'],
            $data['identify'],
            $data['contact'],
            $data['certificates'],
            $data['images'],
            $data['type'],
            $data['acceptUserGroupId']
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->appealController->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->appealController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->appealController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->appealController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->appealController->addAction();
        $this->assertTrue($result);
    }

    public function testEditView()
    {
        $this->appealController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->appealController->editView(1);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }

    public function testEditAction()
    {
        $id = 2;

        $this->appealController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->appealController->editAction($id);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }

    public function testValidateAddAppealScenario()
    {
        $appealController = $this->getMockBuilder(MockAppealOperateController::class)
                           ->setMethods(
                               [
                                    'validateCommonScenario',
                                    'getInteractionWidgetRules',
                                ]
                           )
                           ->getMock();

        $data = $this->getAddCommonData();

        $interactionWidgetRules = $this->prophesize(InteractionWidgetRules::class);
    
        $interactionWidgetRules->certificates(
            Argument::exact($data['certificates']),
            Argument::exact('certificates')
        )->shouldBeCalledTimes(1)->willReturn(true);

        $appealController->expects($this->exactly(1))
            ->method('validateCommonScenario')
            ->with(
                $data['title'],
                $data['content'],
                $data['name'],
                $data['identify'],
                $data['type'],
                $data['contact'],
                $data['images'],
                $data['acceptUserGroupId']
            )
            ->willReturn(true);

        $appealController->expects($this->exactly(1))
            ->method('getInteractionWidgetRules')
            ->willReturn($interactionWidgetRules->reveal());

        $result = $appealController->validateAddAppealScenario(
            $data['title'],
            $data['content'],
            $data['name'],
            $data['identify'],
            $data['contact'],
            $data['certificates'],
            $data['images'],
            $data['type'],
            $data['acceptUserGroupId']
        );
       
        $this->assertTrue($result);
    }
}
