<?php
namespace Base\Package\Interaction\Controller\Appeal;

use Sdk\Member\Model\Member;
use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Model\Appeal;
use Sdk\Interaction\Model\NullAppeal;
use Sdk\Interaction\Repository\AppealRepository;

use Base\Package\Interaction\View\Template\Appeal\ListView;
use Base\Package\Interaction\View\Template\Appeal\View;
use Base\Package\Interaction\View\Json\AppealListJsonView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class AppealFetchControllerTest extends TestCase
{
    private $appealStub;

    public function setUp()
    {
        $this->appealStub = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->appealStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new MockAppealFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\AppealRepository',
            $this->appealStub->getRepository()
        );
    }

    public function initialFetchOneActionSuccess($ajaxResult)
    {
        $this->appealStub = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render',
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $filter['title'] = 'title';
        $filter['member'] = 1;
        $sort = ['-updateTime'];
        $page = 1;
        $size = SIZE;

        $this->appealStub->expects($this->exactly(1))
            ->method('globalCheck')
            ->willReturn(true);

        $this->appealStub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->with($size)
            ->willReturn([$page, $size]);
            
        $this->appealStub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $praiseArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(AppealRepository::class);
        $repository->scenario(Argument::exact(AppealRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$praiseArray]);

        $this->appealStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);

        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->appealStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->appealStub->expects($this->exactly(1))
                ->method('render')
                ->with(new AppealListJsonView($praiseArray, $count));
        }

        if (!$ajaxResult) {
            $this->appealStub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListView($praiseArray, $count));
        }

        $result = $this->appealStub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionFail()
    {
        $controller = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(
                [
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(false);
        $controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        $result = $controller->filterAction();
        $this->assertFalse($result);
    }

    public function testFilterActionSuccess()
    {
        $this->initialFetchOneActionSuccess(false);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialFetchOneActionSuccess(true);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->appealStub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->appealStub = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $praise = new NullAppeal();

        $repository = $this->prophesize(AppealRepository::class);
        $repository->scenario(Argument::exact(AppealRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($praise);

        $this->appealStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->appealStub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->appealStub = $this->getMockBuilder(MockAppealFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $praise = new Appeal($id);

        $repository = $this->prophesize(AppealRepository::class);
        $repository->scenario(
            Argument::exact(AppealRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($praise);

        $this->appealStub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->appealStub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($praise));

        $result = $this->appealStub->fetchOneAction($id);
        $this->assertTrue($result);
    }
}
