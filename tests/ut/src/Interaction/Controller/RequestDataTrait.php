<?php
namespace Base\Package\Interaction\Controller;

use Sdk\Interaction\Model\Praise;

trait RequestDataTrait
{
    public function getAddCommonData()
    {
        $faker = \Faker\Factory::create('zh_CN');
        $fakerData = array(
            'title' => $faker->word,
            'content' => 'content',
            'name' => 'name',
            'identify' => $faker->word,
            'contact' => 16892875054,
            'subject' => $faker->word,
            'images' => array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg'),
                array('name' => 'name', 'identify' => 'identify.png')
            ),
            'type'=>$faker->randomElement(Praise::TYPE),
            'acceptUserGroupId' => 1,
            'certificates'=>array(
                array('name' => 'name', 'identify' => 'identify.jpg'),
                array('name' => 'name', 'identify' => 'identify.jpeg')
            ),
        );

        return $fakerData;
    }
}
