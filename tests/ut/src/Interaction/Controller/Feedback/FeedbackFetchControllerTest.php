<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Sdk\Member\Model\Member;
use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Model\Feedback;
use Sdk\Interaction\Model\NullFeedback;
use Sdk\Interaction\Repository\FeedbackRepository;

use Base\Package\Interaction\View\Template\Feedback\ListView;
use Base\Package\Interaction\View\Template\Feedback\View;
use Base\Package\Interaction\View\Json\FeedbackListJsonView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class FeedbackFetchControllerTest extends TestCase
{
    private $feedbackStub;

    public function setUp()
    {
        $this->feedbackStub = $this->getMockBuilder(MockFeedbackFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->feedbackStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new MockFeedbackFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\FeedbackRepository',
            $this->feedbackStub->getRepository()
        );
    }

    public function initialFetchOneActionSuccess($ajaxResult)
    {
        $this->feedbackStub = $this->getMockBuilder(MockFeedbackFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render',
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $filter['title'] = 'title';
        $filter['member'] = 1;
        $sort = ['-updateTime'];
        $page = 1;
        $size = SIZE;

        $this->feedbackStub->expects($this->exactly(1))
            ->method('globalCheck')
            ->willReturn(true);

        $this->feedbackStub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->with($size)
            ->willReturn([$page, $size]);
            
        $this->feedbackStub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $praiseArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(FeedbackRepository::class);
        $repository->scenario(Argument::exact(FeedbackRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$praiseArray]);

        $this->feedbackStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);

        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->feedbackStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->feedbackStub->expects($this->exactly(1))
                ->method('render')
                ->with(new FeedbackListJsonView($praiseArray, $count));
        }

        if (!$ajaxResult) {
            $this->feedbackStub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListView($praiseArray, $count));
        }

        $result = $this->feedbackStub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionFail()
    {
        $controller = $this->getMockBuilder(MockFeedbackFetchController::class)
            ->setMethods(
                [
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(false);
        $controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        $result = $controller->filterAction();
        $this->assertFalse($result);
    }

    public function testFilterActionSuccess()
    {
        $this->initialFetchOneActionSuccess(false);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialFetchOneActionSuccess(true);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->feedbackStub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->feedbackStub = $this->getMockBuilder(MockFeedbackFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $praise = new NullFeedback();

        $repository = $this->prophesize(FeedbackRepository::class);
        $repository->scenario(Argument::exact(FeedbackRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($praise);

        $this->feedbackStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->feedbackStub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->feedbackStub = $this->getMockBuilder(MockFeedbackFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $praise = new Feedback($id);

        $repository = $this->prophesize(FeedbackRepository::class);
        $repository->scenario(
            Argument::exact(FeedbackRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($praise);

        $this->feedbackStub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->feedbackStub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($praise));

        $result = $this->feedbackStub->fetchOneAction($id);
        $this->assertTrue($result);
    }
}
