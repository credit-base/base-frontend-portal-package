<?php

namespace Base\Package\Interaction\Controller\Feedback;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Feedback\AddFeedbackCommand;

use Base\Package\Interaction\View\Template\Feedback\AddView;
use Base\Package\Interaction\Controller\RequestDataTrait;

class FeedbackOperateControllerTest extends TestCase
{
    use RequestDataTrait;

    private $feedbackController;

    public function setUp()
    {
        $this->feedbackController = $this->getMockBuilder(MockFeedbackOperateController::class)
            ->setMethods(
                [
                    'render',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->feedbackController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $feedbackController = new FeedbackOperateController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $feedbackController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $feedbackController = new FeedbackOperateController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $feedbackController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->feedbackController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->feedbackController->expects($this->exactly(1))
        ->method('render')
        ->with(new AddView());
        $result = $this->feedbackController->addView();
        $this->assertTrue($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        $this->feedbackController = $this->getMockBuilder(MockFeedbackOperateController::class)
            ->setMethods([
                    'getRequest', 'displayError', 'displaySuccess', 'getCommandBus', 'validateCommonContentScenario',
                    'getAddRequestCommonData'
                ])->getMock();

        $data = $this->getAddCommonData();

        $this->feedbackController->expects($this->exactly(1))->method('getAddRequestCommonData')->willReturn($data);

        $this->feedbackController->expects($this->exactly(1))->method('validateCommonContentScenario')
            ->with(
                $data['title'],
                $data['content'],
                $data['acceptUserGroupId']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddFeedbackCommand(
            $data['title'],
            $data['content'],
            $data['acceptUserGroupId']
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->feedbackController->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->feedbackController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->feedbackController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->feedbackController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->feedbackController->addAction();
        $this->assertTrue($result);
    }

    public function testEditView()
    {
        $this->feedbackController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->feedbackController->editView(1);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }

    public function testEditAction()
    {
        $id = 2;

        $this->feedbackController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->feedbackController->editAction($id);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }
}
