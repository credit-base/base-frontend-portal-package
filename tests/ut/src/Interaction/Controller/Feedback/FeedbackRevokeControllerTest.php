<?php
namespace Base\Package\Interaction\Controller\Feedback;

use Sdk\Member\Model\Member;
use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Feedback\RevokeFeedbackCommand;

class FeedbackRevokeControllerTest extends TestCase
{
    private $feedbackStub;

    public function setUp()
    {
        $this->feedbackStub = $this->getMockBuilder(MockFeedbackRevokeController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->feedbackStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new FeedbackRevokeController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new FeedbackRevokeController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IRevokeAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $feedbackStub = new MockFeedbackRevokeController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $feedbackStub->getCommandBus()
        );
    }

   /**
     * 指定 Revoke方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialRevoke(bool $result)
    {
        //初始化
        $this->feedbackStub = $this->getMockBuilder(MockFeedbackRevokeController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus'
                ]
            )->getMock();

        $id = 1;

        $member = new Member(1);
        Core::$container->set('member', $member);

        //预言
        $commandBus = $this->prophesize(CommandBus::class);
        $command = new RevokeFeedbackCommand($id);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        //绑定
        $this->feedbackStub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testRevokeActionFailure()
    {
        $this->initialRevoke(false);
        $id = 1;
        $this->feedbackStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->feedbackStub->revokeAction($id);
        $this->assertFalse($result);
    }

    public function testRevokeActionSuccess()
    {
        $this->initialRevoke(true);
        $id = 1;
        $this->feedbackStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->feedbackStub->revokeAction($id);
        $this->assertTrue($result);
    }
}
