<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Sdk\Member\Model\Member;
use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Model\Complaint;
use Sdk\Interaction\Model\NullComplaint;
use Sdk\Interaction\Repository\ComplaintRepository;

use Base\Package\Interaction\View\Template\Complaint\ListView;
use Base\Package\Interaction\View\Template\Complaint\View;
use Base\Package\Interaction\View\Json\ComplaintListJsonView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class ComplaintFetchControllerTest extends TestCase
{
    private $complaintStub;

    public function setUp()
    {
        $this->complaintStub = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->complaintStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new MockComplaintFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\ComplaintRepository',
            $this->complaintStub->getRepository()
        );
    }

    public function initialFetchOneActionSuccess($ajaxResult)
    {
        $this->complaintStub = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render',
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $filter['title'] = 'title';
        $filter['member'] = 1;
        $sort = ['-updateTime'];
        $page = 1;
        $size = SIZE;

        $this->complaintStub->expects($this->exactly(1))
            ->method('globalCheck')
            ->willReturn(true);

        $this->complaintStub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->with($size)
            ->willReturn([$page, $size]);
            
        $this->complaintStub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $praiseArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(ComplaintRepository::class);
        $repository->scenario(Argument::exact(ComplaintRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$praiseArray]);

        $this->complaintStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);

        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->complaintStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->complaintStub->expects($this->exactly(1))
                ->method('render')
                ->with(new ComplaintListJsonView($praiseArray, $count));
        }

        if (!$ajaxResult) {
            $this->complaintStub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListView($praiseArray, $count));
        }

        $result = $this->complaintStub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionFail()
    {
        $controller = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(
                [
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(false);
        $controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        $result = $controller->filterAction();
        $this->assertFalse($result);
    }

    public function testFilterActionSuccess()
    {
        $this->initialFetchOneActionSuccess(false);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialFetchOneActionSuccess(true);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->complaintStub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->complaintStub = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $praise = new NullComplaint();

        $repository = $this->prophesize(ComplaintRepository::class);
        $repository->scenario(Argument::exact(ComplaintRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($praise);

        $this->complaintStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->complaintStub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->complaintStub = $this->getMockBuilder(MockComplaintFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $praise = new Complaint($id);

        $repository = $this->prophesize(ComplaintRepository::class);
        $repository->scenario(
            Argument::exact(ComplaintRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($praise);

        $this->complaintStub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->complaintStub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($praise));

        $result = $this->complaintStub->fetchOneAction($id);
        $this->assertTrue($result);
    }
}
