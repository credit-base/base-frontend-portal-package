<?php

namespace Base\Package\Interaction\Controller\Complaint;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Complaint\AddComplaintCommand;

use Base\Package\Interaction\View\Template\Complaint\AddView;
use Base\Package\Interaction\Controller\RequestDataTrait;

class ComplaintOperateControllerTest extends TestCase
{
    use RequestDataTrait;

    private $complaintController;

    public function setUp()
    {
        $this->complaintController = $this->getMockBuilder(MockComplaintOperateController::class)
            ->setMethods(
                [
                    'render',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->complaintController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $complaintController = new ComplaintOperateController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $complaintController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $complaintController = new ComplaintOperateController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $complaintController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->complaintController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->complaintController->expects($this->exactly(1))
        ->method('render')
        ->with(new AddView());
        $result = $this->complaintController->addView();
        $this->assertTrue($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        $this->complaintController = $this->getMockBuilder(MockComplaintOperateController::class)
            ->setMethods([
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validatePraiseAndComplaintScenario',
                    'getAddRequestCommonData'
                ])->getMock();

        $requestData = $this->getAddCommonData();

        $this->complaintController->expects($this->exactly(1))
            ->method('getAddRequestCommonData')
            ->willReturn($requestData);

        $this->complaintController->expects($this->exactly(1))
            ->method('validatePraiseAndComplaintScenario')
            ->with(
                $requestData['title'],
                $requestData['content'],
                $requestData['name'],
                $requestData['identify'],
                $requestData['contact'],
                $requestData['subject'],
                $requestData['images'],
                $requestData['type'],
                $requestData['acceptUserGroupId']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddComplaintCommand(
            $requestData['title'],
            $requestData['content'],
            $requestData['name'],
            $requestData['identify'],
            $requestData['contact'],
            $requestData['subject'],
            $requestData['images'],
            $requestData['type'],
            $requestData['acceptUserGroupId']
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->complaintController->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->complaintController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->complaintController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->complaintController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->complaintController->addAction();
        $this->assertTrue($result);
    }

    public function testEditView()
    {
        $this->complaintController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->complaintController->editView(1);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }

    public function testEditAction()
    {
        $id = 2;

        $this->complaintController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->complaintController->editAction($id);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }
}
