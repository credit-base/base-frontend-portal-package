<?php
namespace Base\Package\Interaction\Controller\Complaint;

use Sdk\Member\Model\Member;
use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Complaint\RevokeComplaintCommand;

class ComplaintRevokeControllerTest extends TestCase
{
    private $complaintStub;

    public function setUp()
    {
        $this->complaintStub = $this->getMockBuilder(MockComplaintRevokeController::class)
            ->setMethods(
                [
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'getRequest'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->complaintStub);
    }

    public function testCorrectExtendsController()
    {
        $controller = new ComplaintRevokeController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testCorrectImplementsIEnableAbleController()
    {
        $controller = new ComplaintRevokeController();
        $this->assertInstanceof('Base\Package\Common\Controller\Interfaces\IRevokeAbleController', $controller);
    }

    public function testGetCommandBus()
    {
        $complaintStub = new MockComplaintRevokeController();

        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $complaintStub->getCommandBus()
        );
    }

    /**
     * 指定 Revoke方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialRevoke(bool $result)
    {
        //初始化
        $this->complaintStub = $this->getMockBuilder(MockComplaintRevokeController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus'
                ]
            )->getMock();

        $id = 1;

        $member = new Member(1);
        Core::$container->set('member', $member);

        //预言
        $commandBus = $this->prophesize(CommandBus::class);
        $command = new RevokeComplaintCommand($id);
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        //绑定
        $this->complaintStub->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testRevokeActionFailure()
    {
        $this->initialRevoke(false);
        $id = 1;
        $this->complaintStub->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->complaintStub->revokeAction($id);
        $this->assertFalse($result);
    }

    public function testRevokeActionSuccess()
    {
        $this->initialRevoke(true);
        $id = 1;
        $this->complaintStub->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->complaintStub->revokeAction($id);
        $this->assertTrue($result);
    }
}
