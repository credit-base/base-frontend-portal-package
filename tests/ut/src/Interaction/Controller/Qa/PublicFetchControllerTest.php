<?php
namespace Base\Package\Interaction\Controller\Qa;

use Sdk\Member\Model\Member;
use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Model\Qa;
use Sdk\Interaction\Model\NullQa;
use Sdk\Interaction\Repository\QaRepository;

use Base\Package\Interaction\View\Template\Qa\PublicListView;
use Base\Package\Interaction\View\Template\Qa\PublicView;
use Base\Package\Interaction\View\Json\PublicQaListJsonView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class PublicFetchControllerTest extends TestCase
{
    private $publicQaStub;

    public function setUp()
    {
        $this->publicQaStub = $this->getMockBuilder(MockPublicFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->publicQaStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new MockPublicFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\QaRepository',
            $this->publicQaStub->getRepository()
        );
    }

    public function initialPublicFetchOneAction($ajaxResult)
    {
        $this->publicQaStub = $this->getMockBuilder(MockPublicFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render',
                ]
            )->getMock();

        $publicOaFilter['title'] = 'title';
        $publicOaFilter['member'] = 1;
        $publicOaSort = ['-updateTime'];
        $publicOaPage = 1;
        $publicOaSize = SIZE;

        $this->publicQaStub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->with($publicOaSize)
            ->willReturn([$publicOaPage, $publicOaSize]);
            
        $this->publicQaStub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$publicOaFilter, $publicOaSort]);

        $qaArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(QaRepository::class);
        $repository->scenario(Argument::exact(QaRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($publicOaFilter),
            Argument::exact($publicOaSort),
            Argument::exact($publicOaPage),
            Argument::exact($publicOaSize)
        )->shouldBeCalledTimes(1)->willReturn([$count,$qaArray]);

        $this->publicQaStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);

        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->publicQaStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->publicQaStub->expects($this->exactly(1))
                ->method('render')
                ->with(new PublicQaListJsonView($qaArray, $count));
        }

        if (!$ajaxResult) {
            $this->publicQaStub->expects($this->exactly(1))
                ->method('render')
                ->with(new PublicListView($qaArray, $count));
        }

        $result = $this->publicQaStub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->initialPublicFetchOneAction(false);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialPublicFetchOneAction(true);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->publicQaStub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->publicQaStub = $this->getMockBuilder(MockPublicFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $qa = new NullQa();

        $repository = $this->prophesize(QaRepository::class);
        $repository->scenario(Argument::exact(QaRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($qa);

        $this->publicQaStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->publicQaStub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->publicQaStub = $this->getMockBuilder(MockPublicFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $qa = new Qa($id);

        $repository = $this->prophesize(QaRepository::class);
        $repository->scenario(
            Argument::exact(QaRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($qa);

        $this->publicQaStub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->publicQaStub->expects($this->exactly(1))
            ->method('render')
            ->with(new PublicView($qa));

        $result = $this->publicQaStub->fetchOneAction($id);
        $this->assertTrue($result);
    }
}
