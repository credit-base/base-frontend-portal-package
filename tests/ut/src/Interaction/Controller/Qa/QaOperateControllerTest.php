<?php

namespace Base\Package\Interaction\Controller\Qa;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Qa\AddQaCommand;

use Base\Package\Interaction\View\Template\Qa\AddView;
use Base\Package\Interaction\Controller\RequestDataTrait;

class QaOperateControllerTest extends TestCase
{
    use RequestDataTrait;

    private $qaController;

    public function setUp()
    {
        $this->qaController = $this->getMockBuilder(MockQaOperateController::class)
            ->setMethods(
                [
                    'render',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->qaController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $qaController = new QaOperateController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $qaController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $qaController = new QaOperateController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $qaController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->qaController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->qaController->expects($this->exactly(1))
        ->method('render')
        ->with(new AddView());
        $result = $this->qaController->addView();
        $this->assertTrue($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        $this->qaController = $this->getMockBuilder(MockQaOperateController::class)
            ->setMethods([
                    'getRequest', 'displayError', 'displaySuccess', 'getCommandBus', 'validateCommonContentScenario',
                    'getAddRequestCommonData'
                ])->getMock();

        $data = $this->getAddCommonData();

        $this->qaController->expects($this->exactly(1))->method('getAddRequestCommonData')->willReturn($data);

        $this->qaController->expects($this->exactly(1))->method('validateCommonContentScenario')
            ->with(
                $data['title'],
                $data['content'],
                $data['acceptUserGroupId']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddQaCommand(
            $data['title'],
            $data['content'],
            $data['acceptUserGroupId']
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->qaController->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->qaController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->qaController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->qaController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->qaController->addAction();
        $this->assertTrue($result);
    }

    public function testEditView()
    {
        $this->qaController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->qaController->editView(1);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }

    public function testEditAction()
    {
        $id = 2;

        $this->qaController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->qaController->editAction($id);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }
}
