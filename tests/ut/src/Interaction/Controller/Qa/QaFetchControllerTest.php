<?php
namespace Base\Package\Interaction\Controller\Qa;

use Sdk\Member\Model\Member;
use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Model\Qa;
use Sdk\Interaction\Model\NullQa;
use Sdk\Interaction\Repository\QaRepository;

use Base\Package\Interaction\View\Template\Qa\ListView;
use Base\Package\Interaction\View\Template\Qa\View;
use Base\Package\Interaction\View\Json\QaListJsonView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class QaFetchControllerTest extends TestCase
{
    private $qaStub;

    public function setUp()
    {
        $this->qaStub = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->qaStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new MockQaFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\QaRepository',
            $this->qaStub->getRepository()
        );
    }

    public function initialFetchOneActionSuccess($ajaxResult)
    {
        $this->qaStub = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render',
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $filter['title'] = 'title';
        $filter['member'] = 1;
        $sort = ['-updateTime'];
        $page = 1;
        $size = SIZE;

        $this->qaStub->expects($this->exactly(1))
            ->method('globalCheck')
            ->willReturn(true);

        $this->qaStub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->with($size)
            ->willReturn([$page, $size]);
            
        $this->qaStub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $qaArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(QaRepository::class);
        $repository->scenario(Argument::exact(QaRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$qaArray]);

        $this->qaStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);

        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->qaStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->qaStub->expects($this->exactly(1))
                ->method('render')
                ->with(new QaListJsonView($qaArray, $count));
        }

        if (!$ajaxResult) {
            $this->qaStub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListView($qaArray, $count));
        }

        $result = $this->qaStub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionFail()
    {
        $controller = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(
                [
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(false);
        $controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        $result = $controller->filterAction();
        $this->assertFalse($result);
    }

    public function testFilterActionSuccess()
    {
        $this->initialFetchOneActionSuccess(false);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialFetchOneActionSuccess(true);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->qaStub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->qaStub = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $qaObject = new NullQa();

        $repository = $this->prophesize(QaRepository::class);
        $repository->scenario(Argument::exact(QaRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($qaObject);

        $this->qaStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->qaStub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->qaStub = $this->getMockBuilder(MockQaFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $qaObject = new Qa($id);

        $repository = $this->prophesize(QaRepository::class);
        $repository->scenario(
            Argument::exact(QaRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($qaObject);

        $this->qaStub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->qaStub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($qaObject));

        $result = $this->qaStub->fetchOneAction($id);
        $this->assertTrue($result);
    }
}
