<?php

namespace Base\Package\Interaction\Controller\Praise;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Interaction\Command\Praise\AddPraiseCommand;

use Base\Package\Interaction\View\Template\Praise\AddView;
use Base\Package\Interaction\Controller\RequestDataTrait;

class PraiseOperateControllerTest extends TestCase
{
    use RequestDataTrait;

    private $praiseController;

    public function setUp()
    {
        $this->praiseController = $this->getMockBuilder(MockPraiseOperateController::class)
            ->setMethods(
                [
                    'render',
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->praiseController);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $praiseController = new PraiseOperateController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $praiseController);
    }

    public function testCorrectImplementsIOperateAbleController()
    {
        $praiseController = new PraiseOperateController();
        $this->assertInstanceof(
            'Base\Package\Common\Controller\Interfaces\IOperateAbleController',
            $praiseController
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Classes\CommandBus',
            $this->praiseController->getCommandBus()
        );
    }

    public function testAddView()
    {
        $this->praiseController->expects($this->exactly(1))
        ->method('render')
        ->with(new AddView());
        $result = $this->praiseController->addView();
        $this->assertTrue($result);
    }

    /**
     * 指定 Add方法进行代码覆盖检测，程序被执行
     * @param bool $result
     */
    private function initialAdd(bool $result)
    {
        $this->praiseController = $this->getMockBuilder(MockPraiseOperateController::class)
            ->setMethods([
                    'getRequest',
                    'displayError',
                    'displaySuccess',
                    'getCommandBus',
                    'validatePraiseAndComplaintScenario',
                    'getAddRequestCommonData'
                ])->getMock();

        $data = $this->getAddCommonData();

        $this->praiseController->expects($this->exactly(1))->method('getAddRequestCommonData')->willReturn($data);

        $this->praiseController->expects($this->exactly(1))->method('validatePraiseAndComplaintScenario')
            ->with(
                $data['title'],
                $data['content'],
                $data['name'],
                $data['identify'],
                $data['contact'],
                $data['subject'],
                $data['images'],
                $data['type'],
                $data['acceptUserGroupId']
            )->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $command = new AddPraiseCommand(
            $data['title'],
            $data['content'],
            $data['name'],
            $data['identify'],
            $data['contact'],
            $data['subject'],
            $data['images'],
            $data['type'],
            $data['acceptUserGroupId']
        );
        $commandBus->send(Argument::exact($command))->shouldBeCalledTimes(1)->willReturn($result);

        $this->praiseController->expects($this->exactly(1))
             ->method('getCommandBus')
             ->willReturn($commandBus->reveal());
    }

    public function testAddActionFailure()
    {
        $this->initialAdd(false);

        $this->praiseController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->praiseController->addAction();
        $this->assertFalse($result);
    }

    public function testAddActionSuccess()
    {
        $this->initialAdd(true);

        $this->praiseController->expects($this->exactly(1))
            ->method('displaySuccess')
            ->willReturn(true);

        $result = $this->praiseController->addAction();
        $this->assertTrue($result);
    }

    public function testEditView()
    {
        $this->praiseController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->praiseController->editView(1);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }

    public function testEditAction()
    {
        $id = 2;

        $this->praiseController->expects($this->exactly(1))
            ->method('displayError')
            ->willReturn(false);

        $result = $this->praiseController->editAction($id);
        $this->assertFalse($result);
        $this->assertEquals(
            ROUTE_NOT_EXIST,
            Core::getLastError()->getId()
        );
    }

    public function testGetAddRequestCommonData()
    {
        $data = $this->getAddCommonData();
       
        $typeEncode = marmot_encode($data['type']);

        $acceptUserGroupIdEncode = marmot_encode($data['acceptUserGroupId']);

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($data['title']);
        $request->post(
            Argument::exact('name'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($data['name']);
        $request->post(Argument::exact('content'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['content']);
        $request->post(Argument::exact('identify'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['identify']);
        $request->post(
            Argument::exact('certificates'),
            Argument::exact(array())
        )->shouldBeCalledTimes(1)->willReturn($data['certificates']);
        $request->post(Argument::exact('type'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($typeEncode);
        $request->post(Argument::exact('contact'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['contact']);
        $request->post(Argument::exact('subject'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['subject']);
        $request->post(Argument::exact('images'), Argument::exact(array()))
                ->shouldBeCalledTimes(1)
                ->willReturn($data['images']);
        $request->post(Argument::exact('acceptUserGroupId'), Argument::exact(''))
                ->shouldBeCalledTimes(1)
                ->willReturn($acceptUserGroupIdEncode);

        $this->praiseController->expects($this->exactly(1))->method('getRequest')->willReturn($request->reveal());

        $result = $this->praiseController->getAddRequestCommonData();

        $this->assertEquals($data, $result);
    }
}
