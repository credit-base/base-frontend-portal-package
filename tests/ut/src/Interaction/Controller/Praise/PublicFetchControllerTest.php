<?php
namespace Base\Package\Interaction\Controller\Praise;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Model\Praise;
use Sdk\Interaction\Model\NullPraise;
use Sdk\Interaction\Repository\PraiseRepository;

use Base\Package\Interaction\View\Template\Praise\PublicListView;
use Base\Package\Interaction\View\Template\Praise\PublicView;
use Base\Package\Interaction\View\Json\PublicPraiseListJsonView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class PublicFetchControllerTest extends TestCase
{
    private $publicPraiseStub;

    public function setUp()
    {
        $this->publicPraiseStub = $this->getMockBuilder(MockPublicFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->publicPraiseStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new MockPublicFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\PraiseRepository',
            $this->publicPraiseStub->getRepository()
        );
    }

    public function initialPublicFetchOneAction($ajaxResult)
    {
        $this->publicPraiseStub = $this->getMockBuilder(MockPublicFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render',
                ]
            )->getMock();

        $publicPraiseFilter['title'] = 'title';
        $publicPraiseFilter['member'] = 1;
        $publicPraiseSort = ['-updateTime'];
        $publicPraisePage = 1;
        $publicPraiseSize = SIZE;

        $this->publicPraiseStub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->with($publicPraiseSize)
            ->willReturn([$publicPraisePage, $publicPraiseSize]);
            
        $this->publicPraiseStub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$publicPraiseFilter, $publicPraiseSort]);

        $praiseArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(PraiseRepository::class);
        $repository->scenario(Argument::exact(PraiseRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($publicPraiseFilter),
            Argument::exact($publicPraiseSort),
            Argument::exact($publicPraisePage),
            Argument::exact($publicPraiseSize)
        )->shouldBeCalledTimes(1)->willReturn([$count,$praiseArray]);

        $this->publicPraiseStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);

        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->publicPraiseStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->publicPraiseStub->expects($this->exactly(1))
                ->method('render')
                ->with(new PublicPraiseListJsonView($praiseArray, $count));
        }

        if (!$ajaxResult) {
            $this->publicPraiseStub->expects($this->exactly(1))
                ->method('render')
                ->with(new PublicListView($praiseArray, $count));
        }

        $result = $this->publicPraiseStub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionSuccess()
    {
        $this->initialPublicFetchOneAction(false);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialPublicFetchOneAction(true);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->publicPraiseStub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->publicPraiseStub = $this->getMockBuilder(MockPublicFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $praise = new NullPraise();

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->scenario(Argument::exact(PraiseRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($praise);

        $this->publicPraiseStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->publicPraiseStub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->publicPraiseStub = $this->getMockBuilder(MockPublicFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $praise = new Praise($id);

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->scenario(
            Argument::exact(PraiseRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($praise);

        $this->publicPraiseStub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->publicPraiseStub->expects($this->exactly(1))
            ->method('render')
            ->with(new PublicView($praise));

        $result = $this->publicPraiseStub->fetchOneAction($id);
        $this->assertTrue($result);
    }
}
