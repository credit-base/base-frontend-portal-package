<?php
namespace Base\Package\Interaction\Controller\Praise;

use Sdk\Member\Model\Member;
use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Interaction\Model\Praise;
use Sdk\Interaction\Model\NullPraise;
use Sdk\Interaction\Repository\PraiseRepository;

use Base\Package\Interaction\View\Template\Praise\ListView;
use Base\Package\Interaction\View\Template\Praise\View;
use Base\Package\Interaction\View\Json\PraiseListJsonView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class PraiseFetchControllerTest extends TestCase
{
    private $praiseStub;

    public function setUp()
    {
        $this->praiseStub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->praiseStub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new MockPraiseFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\Interaction\Repository\PraiseRepository',
            $this->praiseStub->getRepository()
        );
    }

    public function initialFetchOneActionSuccess($ajaxResult)
    {
        $this->praiseStub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render',
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $filter['title'] = 'title';
        $filter['member'] = 1;
        $sort = ['-updateTime'];
        $page = 1;
        $size = SIZE;

        $this->praiseStub->expects($this->exactly(1))
            ->method('globalCheck')
            ->willReturn(true);

        $this->praiseStub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->with($size)
            ->willReturn([$page, $size]);
            
        $this->praiseStub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->willReturn([$filter, $sort]);

        $praiseArray = array(1,2);
        $count = 2;
        $repository = $this->prophesize(PraiseRepository::class);
        $repository->scenario(Argument::exact(PraiseRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$praiseArray]);

        $this->praiseStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);

        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->praiseStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if ($ajaxResult) {
            $this->praiseStub->expects($this->exactly(1))
                ->method('render')
                ->with(new PraiseListJsonView($praiseArray, $count));
        }

        if (!$ajaxResult) {
            $this->praiseStub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListView($praiseArray, $count));
        }

        $result = $this->praiseStub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterActionFail()
    {
        $controller = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'globalCheck',
                    'displayError'
                ]
            )->getMock();

        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(false);
        $controller->expects($this->exactly(1))->method('displayError')->willReturn(false);

        $result = $controller->filterAction();
        $this->assertFalse($result);
    }

    public function testFilterActionSuccess()
    {
        $this->initialFetchOneActionSuccess(false);
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialFetchOneActionSuccess(true);
    }

    public function testFilterFormatChange()
    {
        $this->praiseStub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $title = 'title';
        $sort = ['-updateTime'];

        $filter['title'] = $title;
        $filter['member'] = 1;

        $member = new Member(1);
        Core::$container->set('member', $member);

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($title);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);

        $this->praiseStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $result = $this->praiseStub->filterFormatChange();

        $this->assertEquals([$filter, [$sort]], $result);
    }

    public function testFilterFormatChangeScene()
    {
        $this->praiseStub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'getRequest'
                ]
            )->getMock();

        $title = 'title';
        $sort = ['-updateTime'];

        $member = new Member(1);
        Core::$container->set('member', $member);

        $filter['title'] = $title;
        $filter['status'] = Praise::STATUS['PUBLISH'];

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('title'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($title);

        $request->get(
            Argument::exact('sort'),
            Argument::exact('-updateTime')
        )->shouldBeCalledTimes(1)->willReturn($sort);

        $this->praiseStub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $result = $this->praiseStub->filterFormatChange(Praise::STATUS['PUBLISH']);

        $this->assertEquals([$filter, [$sort]], $result);
    }

    public function testFetchOneActionFailure()
    {
        $id = 0;

        $result = $this->praiseStub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionNullFailure()
    {
        $this->praiseStub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(
                [
                    'getRepository'
                ]
            )->getMock();

        $id = 1;
        $praise = new NullPraise();

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->scenario(Argument::exact(PraiseRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($praise);

        $this->praiseStub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->praiseStub->fetchOneAction($id);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $this->praiseStub = $this->getMockBuilder(MockPraiseFetchController::class)
            ->setMethods(['getRepository','render'])->getMock();

        $id = 1;
        $praise = new Praise($id);

        $repository = $this->prophesize(PraiseRepository::class);
        $repository->scenario(
            Argument::exact(PraiseRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($praise);

        $this->praiseStub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $this->praiseStub->expects($this->exactly(1))
            ->method('render')
            ->with(new View($praise));

        $result = $this->praiseStub->fetchOneAction($id);
        $this->assertTrue($result);
    }
}
