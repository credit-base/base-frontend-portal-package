<?php
namespace Base\Package\WebsiteCustomize\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository;

use Sdk\Crew\Model\Crew;

use Base\Package\WebsiteCustomize\View\Template\SearchView;
use Base\Package\WebsiteCustomize\View\Template\FooterView;
use Base\Package\WebsiteCustomize\View\Template\HeaderView;
use Base\Package\WebsiteCustomize\View\Template\ToolBoxView;
use Base\Package\WebsiteCustomize\View\Template\NavView;

class WebsiteCustomizeFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                ]
            )->getMock();
        Core::$container->set('crew', new Crew(1));
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new WebsiteCustomizeFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository',
            $this->stub->getRepository()
        );
    }

    public function testGetCustomizeData()
    {
        $category = 'homePage';
        $this->stub = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                ]
            )->getMock();

        $data = new WebsiteCustomize(1);

        $repository = $this->prophesize(WebsiteCustomizeRepository::class);

        $repository->customize(Argument::exact($category))
            ->shouldBeCalledTimes(1)
            ->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $result = $this->stub->getCustomizeData($category);

        $this->assertEquals($data, $result);
    }

    public function testSearch()
    {
        $controller = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'getCustomizeData',
                    'render'
                ]
            )->getMock();

        $category = 'homePage';

        $array = [1,2];

        $controller->expects($this->exactly(1))
            ->method('getCustomizeData')
            ->with($category)
            ->willReturn($array);

        $controller->expects($this->exactly(1))
            ->method('render')
            ->with(new SearchView($array));

        $result = $controller->search();
        $this->assertTrue($result);
    }

    public function testFooter()
    {
        $controller = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'getCustomizeData',
                    'render'
                ]
            )->getMock();

        $category = 'homePage';

        $array = [1,2];

        $controller->expects($this->exactly(1))
                   ->method('getCustomizeData')
                   ->with($category)
                   ->willReturn($array);

        $controller->expects($this->exactly(1))
            ->method('render')
            ->with(new FooterView($array));

        $result = $controller->footer();
        $this->assertTrue($result);
    }

    public function testHeader()
    {
        $controller = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'getCustomizeData',
                    'render'
                ]
            )->getMock();

        $category = 'homePage';

        $array = [1,2];
        $signInStatus = 1;

        $controller->expects($this->exactly(1))
                   ->method('getCustomizeData')
                   ->with($category)
                   ->willReturn($array);

        $controller->expects($this->exactly(1))
            ->method('render')
            ->with(new HeaderView($array, $signInStatus));

        $result = $controller->header($signInStatus);
        $this->assertTrue($result);
    }

    public function testToolBox()
    {
        $controller = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'getCustomizeData',
                    'render'
                ]
            )->getMock();

        $category = 'homePage';

        $array = [5,6];

        $controller->expects($this->exactly(1))
                   ->method('getCustomizeData')
                   ->with($category)
                   ->willReturn($array);

        $controller->expects($this->exactly(1))
            ->method('render')
            ->with(new ToolBoxView($array));

        $result = $controller->toolBox();
        $this->assertTrue($result);
    }

    public function testNav()
    {
        $controller = $this->getMockBuilder(MockWebsiteCustomizeFetchController::class)
            ->setMethods(
                [
                    'getCustomizeData',
                    'render'
                ]
            )->getMock();

        $category = 'homePage';

        $array = [3,4];

        $controller->expects($this->exactly(1))
                   ->method('getCustomizeData')
                   ->with($category)
                   ->willReturn($array);
        $param = 1;
        $controller->expects($this->exactly(1))
            ->method('render')
            ->with(new NavView($array, $param));

        $result = $controller->nav($param);
        $this->assertTrue($result);
    }
}
