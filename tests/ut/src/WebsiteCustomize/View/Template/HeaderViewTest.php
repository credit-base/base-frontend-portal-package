<?php
namespace Base\Package\WebsiteCustomize\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class HeaderViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockHeaderView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockHeaderView::class)->setMethods([
            'getView',
            'getData',
            'getParam'
        ])->getMock();

        $result = array();
        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($result);

        $param = 0;
        $this->stub->expects($this->exactly(1))->method('getParam')->willReturn($param);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Layout/Header.tpl',
            [
                'marmot_isSignIn'=>$param,
                'headerData' =>  $result,
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
