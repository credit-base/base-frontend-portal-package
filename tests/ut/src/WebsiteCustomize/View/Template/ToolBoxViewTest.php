<?php
namespace Base\Package\WebsiteCustomize\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class ToolBoxViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockToolBoxView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockToolBoxView::class)->setMethods([
            'getView',
            'getData'
        ])->getMock();

        $result = array('content'=>array('rightToolBar'=>1));
        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($result);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Layout/ToolBox.tpl',
            [
                'toolBoxData' =>  $result['content']['rightToolBar'],
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
