<?php
namespace Base\Package\WebsiteCustomize\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\WebsiteCustomize\Model\WebsiteCustomize;
use Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator;

class FooterViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockFooterView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetWebsiteCustomize()
    {
        $result = $this->stub->getWebsiteCustomize();
        $this->assertEquals($result, new WebsiteCustomize(1));
    }

    public function testGetParam()
    {
        $this->assertIsInt($this->stub->getParam());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\WebsiteCustomize\Translator\WebsiteCustomizeTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testGetData()
    {
        $this->stub = $this->getMockBuilder(MockFooterView::class)->setMethods([
            'encode',
            'getWebsiteCustomize',
            'getTranslator',
            'getDescriptionByDhtml',
            'getHeaderSearchAddInputData'
        ])->getMock();

        $data = new WebsiteCustomize(1);

        $this->stub->expects($this->exactly(1))->method('getWebsiteCustomize')->willReturn($data);

        $rightToolBar = array(
            array(
                'description'=>'029-81563990'
            )
        );
        $headerSearch = array(
            'creditInformation' => array(
                'status' => 0
            ),
        );
        $result = array(
            'content' =>array(
                'rightToolBar'=>$rightToolBar,
                'headerSearch' =>$headerSearch
        ));
        
        $translator = $this->prophesize(WebsiteCustomizeTranslator::class);
        $translator->objectToArray(
            $data,
            array(
                'content'
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $headerSearchFormat = array(
            array(
                'identify' =>'creditInformation',
                'inputData' => '请输入企业/事业单位名称',
                'link' =>'/enterprises?type=MA'
            )
        );
        $resultFormat = array(
            'content' =>array(
                'rightToolBar'=>$rightToolBar,
                'headerSearch' =>$headerSearchFormat
        ));

        $this->stub->expects($this->exactly(1))->method('getDescriptionByDhtml')->willReturn($rightToolBar);
        $this->stub->expects($this->exactly(1))->method('getHeaderSearchAddInputData')->willReturn($headerSearchFormat);
     
        $this->assertEquals($resultFormat, $this->stub->getData());
    }

    public function testGetDescriptionByDhtml()
    {
        $rightToolBar = array(
            array(
                'description'=>'029-81563990'
            )
        );
        $this->assertEquals($rightToolBar, $this->stub->getDescriptionByDhtml($rightToolBar));
    }

    public function testGetHeaderSearchAddInputData()
    {
        $headerSearch = array(
            'creditInformation' => array(),
        );

        $headerSearchFormat = array(
            array(
                'identify' =>'creditInformation',
                'inputData' => '请输入企业/事业单位名称',
                'link' =>'/enterprises?type=MA'
            )
        );
        $result = $this->stub->getHeaderSearchAddInputData($headerSearch);
        $this->assertEquals($headerSearchFormat, $result);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockFooterView::class)->setMethods([
            'getView',
            'getData'
        ])->getMock();

        $result = array();
        $this->stub->expects($this->exactly(1))->method('getData')->willReturn($result);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Layout/Footer.tpl',
            [
                'footerData' =>  $result,
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
