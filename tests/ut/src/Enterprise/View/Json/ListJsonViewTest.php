<?php
namespace Base\Package\Enterprise\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Translator\EnterpriseTranslator;

class ListJsonViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetList()
    {
        $this->assertIsArray($this->stub->getList());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\Enterprise\Translator\EnterpriseTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListJsonView::class)->setMethods([
            'encode',
            'getList',
            'getCount',
            'getTranslator',
        ])->getMock();
        $enterprise = new Enterprise();
        $list = [$enterprise];
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();

        $translator = $this->prophesize(EnterpriseTranslator::class);
        $translator->objectToArray(
            $enterprise,
            array(
                'id',
                'name',
                'unifiedSocialCreditCode',
                'registrationStatus',
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
