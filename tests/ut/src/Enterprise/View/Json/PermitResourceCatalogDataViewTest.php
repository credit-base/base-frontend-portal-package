<?php
namespace Base\Package\Enterprise\View\Json;

use Base\Sdk\ResourceCatalog\Model\ISearchData;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\ResourceCatalog\Model\BjSearchData;
use PHPUnit\Framework\TestCase;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

class PermitResourceCatalogDataViewTest extends TestCase
{
    private $view;

    public function setUp()
    {
        $this->view = new MockPermitResourceCatalogDataView();
    }

    public function tearDown()
    {
        unset($this->view);
    }

    public function testGetDataList()
    {
        $this->assertIsArray($this->view->getDataList());
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->view->getCount());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\BjSearchDataTranslator',
            $this->view->getTranslator()
        );
    }

    public function testDisplay()
    {
        $bjSearchData = new BjSearchData();
        $bjSearchData->setInfoClassify(ISearchData::INFO_CLASSIFY['XZXK']);
        $bjSearchDatas = [$bjSearchData];
        $bjSearchDataCount = 1;

        $bjSearchDataArray = array(
            'id' => $bjSearchData->getId(),
            'sourceUnit' => [
                'id' => marmot_encode($bjSearchData->getSourceUnit()->getId()),
                'name' => $bjSearchData->getSourceUnit()->getName(),
            ],
            'template' => [
                'id' => marmot_encode($bjSearchData->getTemplate()->getId()),
                'name' => $bjSearchData->getTemplate()->getName()
            ],
            'url' => '/doublePublicity/'.marmot_encode($bjSearchData->getId())
        );

        $this->view = $this->getMockBuilder(MockPermitResourceCatalogDataView::class)
            ->setMethods(['getTranslator','getDataList','getCount','encode'])
            ->getMock();

        $bJTranslator = $this->prophesize(BjSearchDataTranslator::class);
        $bJTranslator->objectToArray(
            $bjSearchData,
            ['id','sourceUnit','infoClassify','template'=>['id','name']]
        )->shouldBeCalledTimes(1)->willReturn($bjSearchDataArray);
        $this->view->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($bJTranslator->reveal());

        $this->view->expects($this->exactly(1))
            ->method('getDataList')
            ->willReturn($bjSearchDatas);
        $this->view->expects($this->exactly(1))
            ->method('getCount')
            ->willReturn($bjSearchDataCount);
        $this->view->expects($this->exactly(1))
            ->method('encode')
            ->with(
                [
                    'total' => $bjSearchDataCount,
                    'list' => [$bjSearchDataArray],
                ]
            );
        $this->assertIsNotArray($this->view->display());
    }
}
