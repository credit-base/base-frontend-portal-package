<?php
namespace Base\Package\Enterprise\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Translator\EnterpriseTranslator;

class ListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }
    public function testGetType()
    {
        $this->assertIsString($this->stub->getType());
    }

    public function testGetEnterpriseList()
    {
        $this->assertIsArray($this->stub->getEnterpriseList());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\Enterprise\Translator\EnterpriseTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockListView::class)->setMethods([
            'getEnterpriseList',
            'getCount',
            'getTranslator',
            'getView',
        ])->getMock();
        $enterpriseData = new Enterprise(1);
        $list = [$enterpriseData];
        $count = 1;

        $this->stub->expects($this->exactly(1))->method('getEnterpriseList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);

        $result = array();

        $translator = $this->prophesize(EnterpriseTranslator::class);
        $translator->objectToArray(
            $enterpriseData,
            array(
                'id',
                'name',
                'unifiedSocialCreditCode',
                'registrationStatus',
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Enterprise/List.tpl',
            [
                'nav'=> NAV['NAV_INDEX'],
                'list' =>  $list
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
