<?php
namespace Base\Package\Enterprise\View\Template;

use Base\Sdk\ResourceCatalog\Model\ISearchData;
use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;
use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Translator\EnterpriseTranslator;
use Sdk\ResourceCatalog\Model\BjSearchData;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;
use Sdk\Statistical\Model\Statistical;
use Sdk\Statistical\Translator\StaticsEnterpriseRelationInformationCountTranslator;

class EnterpriseViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetEnterpriseTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Translator\EnterpriseTranslator',
            $this->stub->getEnterpriseTranslator()
        );
    }

    public function testGetStatistical()
    {
        $this->assertInstanceOf(
            'Sdk\Statistical\Model\Statistical',
            $this->stub->getStatistical()
        );
    }

    public function testGetEnterprise()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Model\Enterprise',
            $this->stub->getEnterprise()
        );
    }

    public function testGetRelationCountTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Statistical\Translator\StaticsEnterpriseRelationInformationCountTranslator',
            $this->stub->getRelationCountTranslator()
        );
    }

    public function testGetPermitCount()
    {
        $this->assertIsInt($this->stub->getPermitCount());
    }

    public function testGetPermitList()
    {
        $this->assertIsArray($this->stub->getPermitList());
    }

    public function testGetBjSearchDataTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Translator\BjSearchDataTranslator',
            $this->stub->getBjSearchDataTranslator()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseView::class)->setMethods([
            'getEnterpriseTranslator',
            'getEnterprise',
            'getRelationCountTranslator',
            'getView',
            'getStatistical',
            'getPermitList',
            'getPermitCount',
            'getBjSearchDataTranslator',
        ])->getMock();

        $enterprise = new Enterprise();
        $statistical = new Statistical();
        //$bjSearchData = BjSearchDataMockFactory::generateBjSearchData(1);
        $bjSearchData = new BjSearchData();
        $bjSearchData->setInfoClassify(ISearchData::INFO_CLASSIFY['XZXK']);

        $enterpriseArray = array();
        $statisticalArray = array();
        $bJSearchDataArray = array(
            'id' => marmot_encode($bjSearchData->getId()),
            'infoClassify' => [
                'id'=>marmot_encode($bjSearchData->getInfoClassify()),
                'name' => ISearchData::INFO_CLASSIFY_CN[$bjSearchData->getInfoClassify()],
            ],
            'sourceUnit' => [
                'id' => marmot_encode($bjSearchData->getSourceUnit()->getId()),
                'name' => $bjSearchData->getSourceUnit()->getName(),
            ],
            'template' => [
                'id' => marmot_encode($bjSearchData->getTemplate()->getId()),
                'name' => $bjSearchData->getTemplate()->getName()
            ],
            'url' => '/doublePublicity/'.marmot_encode($bjSearchData->getId())
        );

        $this->stub->expects($this->exactly(1))->method('getEnterprise')->willReturn($enterprise);
        $this->stub->expects($this->exactly(1))->method('getStatistical')->willReturn($statistical);
        $this->stub->expects($this->exactly(1))->method('getPermitList')->willReturn([$bjSearchData]);
        $this->stub->expects($this->exactly(1))->method('getPermitCount')->willReturn(1);

        $translator = $this->prophesize(EnterpriseTranslator::class);
        $translator->objectToArray(
            $enterprise,
            [
                'id','name','unifiedSocialCreditCode','enterpriseType',
                'registrationStatus','principal','registrationAuthority',
                'establishmentDate'
            ]
        )->shouldBeCalledTimes(1)->willReturn($enterpriseArray);
        $this->stub->expects($this->exactly(1))
            ->method('getEnterpriseTranslator')
            ->willReturn($translator->reveal());

        $bJTranslator = $this->prophesize(BjSearchDataTranslator::class);
        $bJTranslator->objectToArray(
            $bjSearchData,
            ['id','sourceUnit','infoClassify','template'=>['id','name']]
        )->shouldBeCalledTimes(1)->willReturn($bJSearchDataArray);
        $this->stub->expects($this->exactly(1))
            ->method('getBjSearchDataTranslator')
            ->willReturn($bJTranslator->reveal());

        $relationTranslator = $this->prophesize(StaticsEnterpriseRelationInformationCountTranslator::class);
        $relationTranslator->objectToArray($statistical)->shouldBeCalledTimes(1)->willReturn($statisticalArray);

        $this->stub->expects($this->exactly(1))
            ->method('getRelationCountTranslator')
            ->willReturn($relationTranslator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Enterprise/Show.tpl',
            [
                'enterprise'=>$enterpriseArray,
                'statistical'=>$statisticalArray,
                'nav'=>NAV['NAV_INDEX'],
                'resourceCatalogData' => [
                    'permit' => [
                        'count' => 1,
                        'data' => $bJSearchDataArray
                    ]
                ]
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
