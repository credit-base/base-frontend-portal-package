<?php

namespace Base\Package\Enterprise\Controller;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Base\Package\Enterprise\View\Json\ListJsonView;
use Base\Package\Enterprise\View\Template\EnterpriseView;
use Base\Package\Enterprise\View\Template\ListView;
use Base\Package\Enterprise\View\Json\PermitResourceCatalogDataView;

use Base\Sdk\Enterprise\Model\NullEnterprise;
use Base\Sdk\Statistical\Model\NullStatistical;
use Base\Sdk\ResourceCatalog\Model\ISearchData;

use Sdk\Enterprise\Model\Enterprise;
use Sdk\Enterprise\Repository\EnterpriseRepository;
use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;
use Sdk\Statistical\Model\Statistical;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class EnterpriseFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'getPageAndSize',
                    'render'
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new EnterpriseFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testImplementsIFetchAbleController()
    {
        $this->assertInstanceOf(
            'Base\Package\Common\Controller\Interfaces\IFetchAbleController',
            $this->stub
        );
    }

    public function testGetBjSearchDataRepository()
    {
        $this->assertInstanceOf(
            'Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $this->stub->getBjSearchDataRepository()
        );
    }

    public function testFilterFormatChange()
    {
        $identify = '';
        $type = '';
        $result = [[],['-updateTime']];

        $this->assertEquals($result, $this->stub->filterFormatChange($identify, $type));
        $this->assertIsArray($this->stub->filterFormatChange($identify, $type));
    }
    public function testFilterFormatChangeWithIdentify()
    {
        $identify = 'Identify';
        $type = SEARCH_NAV['SEARCH_NAV_CREDIT_INFO'];
        $result = [['name'=>$identify],['-updateTime']];

        $this->assertEquals($result, $this->stub->filterFormatChange($identify, $type));
        $this->assertIsArray($this->stub->filterFormatChange($identify, $type));
    }
    public function testFilterFormatChangeWithCode()
    {
        $identify = 'Identify';
        $type = SEARCH_NAV['SEARCH_NAV_UNIFIED_SOCIAL_CREDIT_CODE'];
        $result = [['unifiedSocialCreditCode'=>$identify],['-updateTime']];

        $this->assertEquals($result, $this->stub->filterFormatChange($identify, $type));
        $this->assertIsArray($this->stub->filterFormatChange($identify, $type));
    }
    public function testFilterFormatChangeWithPrincipal()
    {
        $identify = 'Identify';
        $type = SEARCH_NAV['SEARCH_NAV_LEGAL_PERSON_NAME'];
        $result = [['principal'=>$identify],['-updateTime']];

        $this->assertEquals($result, $this->stub->filterFormatChange($identify, $type));
        $this->assertIsArray($this->stub->filterFormatChange($identify, $type));
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Enterprise\Repository\EnterpriseRepository',
            $this->stub->getRepository()
        );
    }

    public function testFetchOneActionWithEmptyId()
    {
        $id = 0;
        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }
    public function testFetchOneActionWithNullEnterprise()
    {
        $id = 1;
        $enterprise = new NullEnterprise();
        $statistical = new Statistical();
        $permitLists = [0,[]];
        $this->initFetchOneAction($enterprise, $statistical, $permitLists, $id);
        $result = $this->stub->fetchOneAction($id);

        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
    public function testFetchOneActionWithNullEnterpriseWithNullStatistical()
    {
        $id = 1;
        $enterprise = new Enterprise($id);
        $statistical = new NullStatistical();

        $permitLists = [0,[]];
        $this->initFetchOneAction($enterprise, $statistical, $permitLists, $id);
        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
    public function testFetchOneActionWithNullEnterpriseWithAllNull()
    {
        $id = 1;
        $enterprise = new NullEnterprise();
        $statistical = new NullStatistical();

        $permitLists = [0,[]];
        $this->initFetchOneAction($enterprise, $statistical, $permitLists, $id);
        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
    public function initFetchOneAction($enterprise, $statistical, $permitLists, $id = 1)
    {
        $page = DEFAULT_PAGE;
        $size = INDEX_LIST_SIZE;

        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getBjSearchDataRepository',
                    'statistical',
                    'render'
                ]
            )->getMock();

        $repository = $this->prophesize(EnterpriseRepository::class);
        $repository->scenario(Argument::exact(EnterpriseRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($enterprise);

        $bjSearchDataRepository = $this->prophesize(BjSearchDataRepository::class);
        $bjSearchDataRepository->scenario(Argument::exact(BjSearchDataRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($bjSearchDataRepository->reveal());
        $bjSearchDataRepository->search(
            Argument::exact(array(
                'infoClassify' => ISearchData::INFO_CLASSIFY['XZXK'],
                'dimension' => ISearchData::DIMENSION['SHGK'],
                'identify' => $enterprise->getUnifiedSocialCreditCode()
            )),
            Argument::exact(['-updateTime']),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn($permitLists);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());
        $this->stub->expects($this->exactly(1))
            ->method('getBjSearchDataRepository')
            ->willReturn($bjSearchDataRepository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('statistical')
            ->willReturn($statistical);
    }
    public function testFetchOneAction()
    {
        $id = 1;
        $enterprise = new Enterprise();
        $statistical = new Statistical();

        $permitLists = [0,[]];

        $this->initFetchOneAction($enterprise, $statistical, $permitLists, $id);

        $this->stub->expects($this->exactly(1))->method('render')->with(new EnterpriseView(
            $enterprise,
            $statistical,
            $permitLists[1],
            $permitLists[0]
        ));
        $result = $this->stub->fetchOneAction($id);
        
        $this->assertTrue($result);
    }

    public function initFilterAction($isAjax)
    {
        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render'
                ]
            )->getMock();
        $page = 1;
        $size = 6;
        $filter = [];
        $sort =['-updateTime'];
        $count = 1;
        $list = [
            new Enterprise()
        ];
        $type = SEARCH_NAV['SEARCH_NAV_CREDIT_INFO'];

        $this->stub->expects($this->exactly(1)) ->method('getPageAndSize')->willReturn([$page, $size]);
        $this->stub->expects($this->exactly(1))->method('filterFormatChange')->willReturn([$filter, $sort]);

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('keyword'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn('');
        $request->get(
            Argument::exact('type'),
            Argument::exact($type)
        )->shouldBeCalledTimes(1)->willReturn($type);

        $repository = $this->prophesize(EnterpriseRepository::class);
        $repository->scenario(Argument::exact(EnterpriseRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes($count)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count,$list]);

        $this->stub->expects($this->any())->method('getRequest')->willReturn($request->reveal());
        $this->stub->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        $request->isAjax()->shouldBeCalledTimes($count)->willReturn($isAjax);

        if ($isAjax) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListJsonView($list, $count, $type));
        }
        if (!$isAjax) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new ListView($list, $count, $type));
        }

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }
    public function testFilterActionWithNoAjax()
    {
        $this->initFilterAction(false);
    }
    public function testFilterActionWithAjax()
    {
        $this->initFilterAction(true);
    }
    public function testFilterActionWithErrorType()
    {
        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('keyword'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn('');
        $request->get(
            Argument::exact('type'),
            Argument::exact(SEARCH_NAV['SEARCH_NAV_CREDIT_INFO'])
        )->shouldBeCalledTimes(1)->willReturn('');

        $this->stub->expects($this->any())->method('getRequest')->willReturn($request->reveal());
        $result = $this->stub->filterAction();
        $this->assertFalse($result);
    }

    public function testGetResourceCatalogData()
    {
        $id = 1;
        $enterprise = new Enterprise();
        $enterprise->setId($id);

        $bjDataList = [];
        $bjDataCount = 0;
        $permitLists = [$bjDataCount,$bjDataList];

        $this->stub = $this->getMockBuilder(MockEnterpriseFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getBjSearchDataRepository',
                    'render',
                    'getRequest'
                ]
            )->getMock();

        //request
        $request = $this->prophesize(Request::class);
        $request->get(
            Argument::exact('infoClassify'),
            Argument::exact(marmot_encode(ISearchData::INFO_CLASSIFY['XZXK']))
        )->shouldBeCalledTimes(1)->willReturn(marmot_encode(ISearchData::INFO_CLASSIFY['XZXK']));
        $this->stub->expects($this->any())->method('getRequest')->willReturn($request->reveal());

        //enterpriseRepository
        $enterpriseRepository = $this->prophesize(EnterpriseRepository::class);
        $enterpriseRepository->scenario(Argument::exact(EnterpriseRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($enterpriseRepository->reveal());
        $enterpriseRepository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($enterprise);
        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($enterpriseRepository->reveal());

        //bjDataRepository
        $bjDataRepository = $this->prophesize(BjSearchDataRepository::class);
        $bjDataRepository->scenario(Argument::exact(BjSearchDataRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($bjDataRepository->reveal());
        $bjDataRepository->search(
            Argument::exact(array(
                'identify' => $enterprise->getUnifiedSocialCreditCode(),
                'infoClassify' => ISearchData::INFO_CLASSIFY['XZXK'],
                'dimension' => ISearchData::DIMENSION['SHGK'],
            )),
            Argument::exact(['-updateTime']),
            Argument::exact(DEFAULT_PAGE),
            Argument::exact(INDEX_LIST_SIZE)
        )->shouldBeCalledTimes(1)->willReturn($permitLists);
        $this->stub->expects($this->exactly(1))
            ->method('getBjSearchDataRepository')
            ->willReturn($bjDataRepository->reveal());


        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new PermitResourceCatalogDataView($bjDataList, $bjDataCount));

        $this->assertTrue($this->stub->getResourceCatalogData(marmot_encode($id)));

        $id = '';
        $this->assertFalse($this->stub->getResourceCatalogData($id));
    }
}
