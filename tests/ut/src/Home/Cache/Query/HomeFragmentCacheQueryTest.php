<?php
namespace Base\Package\Home\Cache\Query;

use PHPUnit\Framework\TestCase;

use Base\Sdk\News\Model\News;
use Base\Sdk\Common\Model\IEnableAble;

use Sdk\WebsiteCustomize\Model\WebsiteCustomize;

class HomeFragmentCacheQueryTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockHomeFragmentCacheQuery::class)
            ->setMethods()->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\News\Repository\NewsRepository',
            $this->stub->getRepository()
        );
    }

    public function testGetWebsiteCustomizeRepository()
    {
        $this->assertInstanceof(
            'Sdk\WebsiteCustomize\Repository\WebsiteCustomizeRepository',
            $this->stub->getWebsiteCustomizeRepository()
        );
    }

    public function testGetConcurrentAdapter()
    {
        $this->assertInstanceof(
            'Marmot\Framework\Adapter\ConcurrentAdapter',
            $this->stub->getConcurrentAdapter()
        );
    }

    public function testFilterFormatChange()
    {
        $this->stub = $this->getMockBuilder(MockHomeFragmentCacheQuery::class)
            ->setMethods()->getMock();

        $sort = ['-stick','-updateTime'];

        $bannersFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $bannersFilter['status'] = IEnableAble::STATUS['ENABLED'];
        $bannersFilter['bannerStatus'] = News::BANNER_STATUS['ENABLED'];

        //信用动态
        $dynamicsFilter['parentCategory'] = NEWS_PARENT_CATEGORY['CREDIT_DYNAMICS'];
        $dynamicsFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $dynamicsFilter['status'] = IEnableAble::STATUS['ENABLED'];
        
        //失信专项治理
        $specialGovernmentFilter['newsType'] = NEWS_TYPE['SPECIAL_GOVERN'];
        $specialGovernmentFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $specialGovernmentFilter['status'] = IEnableAble::STATUS['ENABLED'];

        //政策法规
        $policiesFilter['category'] = NEWS_CATEGORY['POLICY_STATUTE'];
        $policiesFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $policiesFilter['status'] = IEnableAble::STATUS['ENABLED'];

        //组织架构
        $organizationsFilter['category'] = NEWS_CATEGORY['ORGANIZATIONAL_STRUCTURE'];
        $organizationsFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $organizationsFilter['status'] = IEnableAble::STATUS['ENABLED'];

        //信用研究
        $creditResearchesFilter['category'] = NEWS_CATEGORY['CREDIT_RESEARCH'];
        $creditResearchesFilter['dimension'] = News::DIMENSION['SOCIOLOGY'];
        $creditResearchesFilter['status'] = IEnableAble::STATUS['ENABLED'];

        //首页配置项
        $websiteCustomizesFilter['category'] = WebsiteCustomize::CATEGORY['HOME_PAGE'];
        $websiteCustomizesFilter['status'] = WebsiteCustomize::STATUS['PUBLISHED'];

        $expected = [
            $bannersFilter,
            $specialGovernmentFilter,
            $organizationsFilter,
            $dynamicsFilter,
            $policiesFilter,
            $creditResearchesFilter,
            $sort,
            $websiteCustomizesFilter
        ];

        $result = $this->stub->filterFormatChange();
        $this->assertEquals($expected, $result);
    }

    public function testGetTtl()
    {
        $this->stub = $this->getMockBuilder(MockHomeFragmentCacheQuery::class)
            ->setMethods()->getMock();
        $expected = 60;
        $result = $this->stub->getTtl();
        $this->assertEquals($expected, $result);
    }
}
