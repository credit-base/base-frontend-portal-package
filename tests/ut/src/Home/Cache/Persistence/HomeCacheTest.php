<?php
namespace Base\Package\Home\Cache\Persistence;

use PHPUnit\Framework\TestCase;

class HomeCacheTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = new HomeCache();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testExtendsCache()
    {
        $this->assertInstanceof('Marmot\Framework\Classes\Cache', $this->stub);
    }
}
