<?php
namespace Base\Package\Home\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class IndexViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockIndexView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetCacheData()
    {
        $this->assertIsArray($this->stub->getCacheData());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockIndexView::class)->setMethods([
            'getView',
            'getCacheData'
        ])->getMock();

        $cacheData = array();
        $this->stub->expects($this->exactly(1))->method('getCacheData')->willReturn($cacheData);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Home/Index.tpl',
            [
                'nav' => NAV['NAV_INDEX'],
                'data' => $cacheData
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
