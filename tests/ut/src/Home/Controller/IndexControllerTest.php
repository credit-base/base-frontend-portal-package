<?php
namespace Base\Package\Home\Controller;

use PHPUnit\Framework\TestCase;
use Base\Package\Home\View\Template\IndexView;
use Base\Package\Home\Cache\Query\HomeFragmentCacheQuery;

class IndexControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockIndexController::class)
            ->setMethods()->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetHomeFragmentCacheQuery()
    {
        $this->assertInstanceof(
            'Base\Package\Home\Cache\Query\HomeFragmentCacheQuery',
            $this->stub->getHomeFragmentCacheQuery()
        );
    }

    public function testIndexSuccess()
    {
        $this->stub = $this->getMockBuilder(MockIndexController::class)
            ->setMethods(
                ['render','fetchNews']
            )->getMock();

        $data = array(1,2);

        $this->stub->expects($this->once())->method('fetchNews')->willReturn($data);
        $this->stub->expects($this->exactly(1))->method('render')->with(new IndexView($data));
        
        $result = $this->stub->index();
        $this->assertTrue($result);
    }

    public function testFetchNews()
    {
        $this->stub = $this->getMockBuilder(MockIndexController::class)
            ->setMethods(['getHomeFragmentCacheQuery'])->getMock();
        $data = [1,2];

        $homeFragmentCacheQuery = $this->prophesize(HomeFragmentCacheQuery::class);

        $homeFragmentCacheQuery->get()->shouldBeCalledTimes(1)->willReturn($data);

        $this->stub->expects($this->exactly(1))
            ->method('getHomeFragmentCacheQuery')
            ->willReturn($homeFragmentCacheQuery->reveal());

        $result = $this->stub->fetchNews();
        $this->assertEquals($data, $result);
    }
}
