<?php
namespace Base\Package\Member\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class AddViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(AddView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(AddView::class)->setMethods([
            'getView',
        ])->getMock();

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Member/SignUp.tpl',
            [
                'securityQuestionList'=> array()
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
