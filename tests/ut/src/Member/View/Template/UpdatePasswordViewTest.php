<?php
namespace Base\Package\Member\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class UpdatePasswordViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(UpdatePasswordView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(UpdatePasswordView::class)->setMethods([
            'getView',
        ])->getMock();

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Member/UpdatePassword.tpl'
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
