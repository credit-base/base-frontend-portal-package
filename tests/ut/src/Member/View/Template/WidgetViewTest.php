<?php
namespace Base\Package\Member\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Member\Model\Member;
use Sdk\Member\Translator\MemberTranslator;

class WidgetViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockWidgetView::class)
        ->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Translator\MemberTranslator',
            $this->stub->getTranslator()
        );
    }

    public function testGetNav()
    {
        $this->assertIsInt($this->stub->getNav());
    }

    public function testGetMember()
    {
        $this->assertEquals(
            new Member(),
            $this->stub->getMember()
        );
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockWidgetView::class)->setMethods([
            'getView',
            'getMember',
            'getTranslator',
            'getNav'
        ])->getMock();

        $memberData = new Member(1);
        $nav = 1;

        $this->stub->expects($this->exactly(1))->method('getMember')->willReturn($memberData);
        $this->stub->expects($this->exactly(1))->method('getNav')->willReturn($nav);

        $result = array();

        $translator = $this->prophesize(MemberTranslator::class);
        $translator->objectToArray(
            $memberData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Member/WidgetMember.tpl',
            [
                'member'=> array(),
                'nav' =>$nav
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
