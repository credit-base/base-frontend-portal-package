<?php
namespace Base\Package\Member\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class ResetPasswordViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(ResetPasswordView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(ResetPasswordView::class)->setMethods([
            'getView',
        ])->getMock();

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Member/ResetPassword.tpl'
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
