<?php
namespace Base\Package\Member\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class SignInViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockSignInView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetBackUrl()
    {
        $this->assertIsString($this->stub->getBackUrl());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockSignInView::class)->setMethods([
            'getView',
            'getBackUrl',
            'getTranslator',
        ])->getMock();

        $backUrl = '';

        $this->stub->expects($this->exactly(1))->method('getBackUrl')->willReturn($backUrl);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Member/SignIn.tpl',
            [
                'backUrl'=>$backUrl
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
