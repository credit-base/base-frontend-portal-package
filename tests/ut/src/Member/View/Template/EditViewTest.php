<?php
namespace Base\Package\Member\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\Member\Model\Member;
use Sdk\Member\Translator\MemberTranslator;

class EditViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockEditView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetMember()
    {
        $this->assertEquals(
            new Member(),
            $this->stub->getMember()
        );
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\Member\Translator\MemberTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockEditView::class)->setMethods([
            'getView',
            'getMember',
            'getTranslator',
        ])->getMock();

        $memberData = new Member(1);

        $this->stub->expects($this->exactly(1))->method('getMember')->willReturn($memberData);

        $result = array();

        $translator = $this->prophesize(MemberTranslator::class);
        $translator->objectToArray(
            $memberData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'Member/Edit.tpl',
            [
                'data'=> array()
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
