<?php
namespace Base\Package\Member\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\Member\Model\Member;
use Sdk\Member\Translator\MemberTranslator;

class VerifyUserNameJsonViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockVerifyUserNameJsonView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetMember()
    {
        $result = $this->stub->getMember();
        $this->assertEquals($result, new Member(1));
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\Member\Translator\MemberTranslator', $this->stub->getTranslator());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockVerifyUserNameJsonView::class)->setMethods([
            'encode',
            'getMember',
            'getTranslator',
        ])->getMock();
        $member = new Member(1);

        $this->stub->expects($this->exactly(1))->method('getMember')->willReturn($member);

        $result = array();

        $translator = $this->prophesize(MemberTranslator::class);
        $translator->objectToArray(
            $member
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
