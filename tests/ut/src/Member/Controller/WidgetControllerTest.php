<?php
namespace Base\Package\Member\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Member\Model\Member;
use Sdk\Member\Repository\MemberRepository;

class WidgetControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(WidgetController::class)
                            ->setMethods([
                                'globalCheck',
                                'displayError',
                                'getRepository',
                                'render'
                                ])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testMemberWidgetFail()
    {
        $this->controller->expects($this->exactly(1))->method('globalCheck')->willReturn(false);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->memberWidget();
        $this->assertFalse($result);
    }

    public function testMemberWidgetSuccess()
    {
        $id = 1;
        $member = new Member($id);
        Core::$container->set('member', $member);
        $this->controller->expects($this->exactly(1))->method('globalCheck')->willReturn(true);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->scenario(
            Argument::exact(MemberRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($member);
        $this->controller->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))->method('render');

        $result= $this->controller->memberWidget();
        $this->assertTrue($result);
    }
}
