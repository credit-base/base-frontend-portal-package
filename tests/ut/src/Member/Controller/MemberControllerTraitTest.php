<?php
namespace Base\Package\Member\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Sdk\User\WidgetRules\UserWidgetRules;

use Sdk\Member\WidgetRules\MemberWidgetRules;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class MemberControllerTraitTest extends TestCase
{
    private $trait;

    public function setUp()
    {
        $this->trait = new MockMemberControllerTrait();
    }

    public function tearDown()
    {
        unset($this->trait);
    }

    public function testGetMemberWidgetRules()
    {
        $this->assertInstanceOf(
            'Sdk\Member\WidgetRules\MemberWidgetRules',
            $this->trait->getMemberWidgetRulesPublic()
        );
    }

    public function testGetUserWidgetRules()
    {
        $this->assertInstanceOf(
            'Sdk\User\WidgetRules\UserWidgetRules',
            $this->trait->getUserWidgetRulesPublic()
        );
    }

    public function testGetRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Member\Repository\MemberRepository',
            $this->trait->getRepositoryPublic()
        );
    }

    public function testGetCommandBus()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\CommandBus',
            $this->trait->getCommandBusPublic()
        );
    }

    public function testValidateAddScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getUserWidgetRules', 'getMemberWidgetRules']) ->getMock();

        $userName = 'userName';
        $realName = 'realName';
        $cardId = 'cardId';
        $cellphone = 'cellphone';
        $email = 'email';
        $contactAddress = 'contactAddress';
        $securityAnswer = 'securityAnswer';
        $password = 'password';
        $confirmPassword = 'confirmPassword';
        $securityQuestion = 'securityQuestion';

        $userWidgetRule = $this->prophesize(UserWidgetRules::class);
        $userWidgetRule->realName(Argument::exact($realName))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->cardId(Argument::exact($cardId))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->cellphone(Argument::exact($cellphone))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->password(Argument::exact($password))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->confirmPassword(
            Argument::exact($password),
            Argument::exact($confirmPassword)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getUserWidgetRules')
            ->willReturn($userWidgetRule->reveal());

        $memberWidgetRule = $this->prophesize(MemberWidgetRules::class);
        $memberWidgetRule->userName(Argument::exact($userName))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->email(Argument::exact($email))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->contactAddress(Argument::exact($contactAddress))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->securityAnswer(Argument::exact($securityAnswer))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->securityQuestion(
            Argument::exact($securityQuestion)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRules')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateAddScenarioPublic(
            $userName,
            $realName,
            $cardId,
            $cellphone,
            $email,
            $contactAddress,
            $securityAnswer,
            $password,
            $confirmPassword,
            $securityQuestion
        );
        
        $this->assertTrue($result);
    }

    public function testValidateEditScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getMemberWidgetRules']) ->getMock();

        $gender = 'gender';

        $memberWidgetRule = $this->prophesize(MemberWidgetRules::class);
        $memberWidgetRule->gender(Argument::exact($gender))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRules')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateEditScenarioPublic($gender);
        
        $this->assertTrue($result);
    }

    public function testValidateSignInScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getUserWidgetRules', 'getMemberWidgetRules']) ->getMock();

        $userName = 'userName';
        $password = 'password';

        $userWidgetRule = $this->prophesize(UserWidgetRules::class);
        $userWidgetRule->password(Argument::exact($password))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getUserWidgetRules')
            ->willReturn($userWidgetRule->reveal());

        $memberWidgetRule = $this->prophesize(MemberWidgetRules::class);
        $memberWidgetRule->userName(Argument::exact($userName))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRules')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateSignInScenarioPublic(
            $userName,
            $password
        );
        
        $this->assertTrue($result);
    }

    public function testValidateResetPasswordScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getUserWidgetRules', 'getMemberWidgetRules']) ->getMock();

        $userName = 'userName';
        $securityAnswer = 'securityAnswer';
        $password = 'password';
        $confirmPassword = 'confirmPassword';

        $userWidgetRule = $this->prophesize(UserWidgetRules::class);
        $userWidgetRule->password(Argument::exact($password))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->confirmPassword(
            Argument::exact($password),
            Argument::exact($confirmPassword)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getUserWidgetRules')
            ->willReturn($userWidgetRule->reveal());

        $memberWidgetRule = $this->prophesize(MemberWidgetRules::class);
        $memberWidgetRule->userName(Argument::exact($userName))->shouldBeCalledTimes(1)->willReturn(true);
        $memberWidgetRule->securityAnswer(Argument::exact($securityAnswer))->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRules')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateResetPasswordScenarioPublic(
            $userName,
            $securityAnswer,
            $password,
            $confirmPassword
        );
        
        $this->assertTrue($result);
    }

    public function testValidateUpdatePasswordScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getUserWidgetRules']) ->getMock();

        $oldPassword = 'oldPassword';
        $password = 'password';
        $confirmPassword = 'confirmPassword';

        $userWidgetRule = $this->prophesize(UserWidgetRules::class);
        $userWidgetRule->password(
            Argument::exact($oldPassword),
            Argument::exact('oldPassword')
        )->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->password(Argument::exact($password))->shouldBeCalledTimes(1)->willReturn(true);
        $userWidgetRule->confirmPassword(
            Argument::exact($password),
            Argument::exact($confirmPassword)
        )->shouldBeCalledTimes(1)->willReturn(true);

        $controller->expects($this->exactly(1))
            ->method('getUserWidgetRules')
            ->willReturn($userWidgetRule->reveal());

        $result = $controller->validateUpdatePasswordScenarioPublic(
            $oldPassword,
            $password,
            $confirmPassword
        );
        
        $this->assertTrue($result);
    }

    public function testValidateValidateSecurityScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getMemberWidgetRules']) ->getMock();

        $securityAnswer = 'securityAnswer';

        $memberWidgetRule = $this->prophesize(MemberWidgetRules::class);
        $memberWidgetRule->securityAnswer(Argument::exact($securityAnswer))->shouldBeCalledTimes(1)->willReturn(true);
        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRules')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateValidateSecurityScenarioPublic($securityAnswer);
        
        $this->assertTrue($result);
    }

    public function testValidateVerifyUserNameScenario()
    {
        $controller = $this->getMockBuilder(MockMemberControllerTrait::class)
                 ->setMethods(['getMemberWidgetRules']) ->getMock();

        $userName = 'userName';

        $memberWidgetRule = $this->prophesize(MemberWidgetRules::class);
        $memberWidgetRule->userName(Argument::exact($userName))->shouldBeCalledTimes(1)->willReturn(true);
        $controller->expects($this->exactly(1))
            ->method('getMemberWidgetRules')
            ->willReturn($memberWidgetRule->reveal());

        $result = $controller->validateVerifyUserNameScenarioPublic($userName);
        
        $this->assertTrue($result);
    }
}
