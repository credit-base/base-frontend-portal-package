<?php
namespace Base\Package\Member\Controller;

use Prophecy\Argument;
use PHPUnit\Framework\TestCase;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Base\User\WidgetRule\UserWidgetRule;

use Sdk\Member\Model\Member;
use Sdk\Member\Model\NullMember;
use Sdk\Member\Repository\MemberRepository;
use Sdk\Member\Command\Member\AddMemberCommand;
use Sdk\Member\Command\Member\EditMemberCommand;
use Sdk\Member\Command\Member\SignInMemberCommand;
use Sdk\Member\Command\Member\SignOutMemberCommand;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class MemberOperateControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'displaySuccess',
                                'displayError',
                                'getRequest',
                                'render',
                                'getRepository',
                                'getCommandBus',
                                'globalCheck',
                                'getBackUrl'
                                ])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testAddGet()
    {
        $controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'addView',
                                'getRequest'
                                ])
                            ->getMock();

        Core::$container->set('member', new NullMember());

        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(true);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('addView')->willReturn(true);

        $controller->add();
    }

    public function testAddPost()
    {
        $controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'addAction',
                                'getRequest'
                                ])
                            ->getMock();

        Core::$container->set('member', new NullMember());
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(false);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('addAction')->willReturn(true);

        $controller->add();
    }

    public function testAdd()
    {
        $controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'locationIndex',
                                'addAction',
                                'getRequest'
                                ])
                            ->getMock();

        Core::$container->set('member', new Member(1));

        $controller->expects($this->once())->method('locationIndex')->willReturn(true);
        
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(false);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('addAction')->willReturn(true);

        $controller->add();
    }

    public function testAddView()
    {
        $this->controller->expects($this->exactly(1))->method('render');

        $result= $this->controller->addView();
        $this->assertTrue($result);
    }

    protected function initAddAction(bool $result)
    {
        $this->controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'validateAddScenario',
                                'getRequest',
                                'getCommandBus',
                                'displaySuccess',
                                'displayError',
                                'decrypt'
                                ])
                            ->getMock();

        $userName = 'userName';
        $realName = 'realName';
        $cardId = 'cardId';
        $cellphone = 'cellphone';
        $email = 'email';
        $contactAddress = 'contactAddress';
        $securityAnswer = 'securityAnswer';
        $securityQuestion = 1;
        $securityQuestionEncode = marmot_encode($securityQuestion);
        $password = 'password';
        $passwordDecrypt = 'passwordDecrypted';
        $confirmPassword = 'confirmPassword';
        
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('userName'), Argument::exact(''))->shouldBeCalledTimes(1)->willReturn($userName);
        $request->post(Argument::exact('realName'), Argument::exact(''))->shouldBeCalledTimes(1)->willReturn($realName);
        $request->post(Argument::exact('cardId'), Argument::exact(''))->shouldBeCalledTimes(1)->willReturn($cardId);
        $request->post(
            Argument::exact('cellphone'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($cellphone);
        $request->post(Argument::exact('email'), Argument::exact(''))->shouldBeCalledTimes(1)->willReturn($email);
        $request->post(
            Argument::exact('contactAddress'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($contactAddress);
        $request->post(
            Argument::exact('securityAnswer'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($securityAnswer);
        $request->post(
            Argument::exact('securityQuestion'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($securityQuestionEncode);
        $request->post(
            Argument::exact('confirmPassword'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($confirmPassword);
        $request->post(
            Argument::exact('password'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($password);

        $this->controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $this->controller->expects($this->exactly(2))
                    ->method('decrypt')
                    ->will($this->returnValueMap(
                        [
                        ['password','passwordDecrypted'],
                        ['confirmPassword','confirmPasswordDecrypted']
                        ]
                    ));

        $this->controller->expects($this->exactly(1))->method('validateAddScenario')->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new AddMemberCommand(
                    $userName,
                    $realName,
                    $cardId,
                    $cellphone,
                    $email,
                    $contactAddress,
                    $securityAnswer,
                    $passwordDecrypt,
                    $securityQuestion
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    public function testAddActionSuccess()
    {
        $this->initAddAction(true);

        $this->controller->expects($this->exactly(1))->method('displaySuccess');

        $result = $this->controller->addAction();
        $this->assertTrue($result);
    }

    public function testAddActionFail()
    {
        $this->initAddAction(false);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->addAction();
        $this->assertFalse($result);
    }

    public function testEditFail()
    {
        $controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'globalCheck',
                                'displayError'
                                ])
                            ->getMock();
                    
        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(false);

        $controller->expects($this->exactly(1))->method('displayError');

        $result = $controller->edit();
        $this->assertFalse($result);
    }

    public function testEditGet()
    {
        $controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'getRequest',
                                'globalCheck',
                                'editView'
                                ])
                            ->getMock();

        Core::$container->set('member', new Member(1));
        
        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(true);
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(true);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('editView')->willReturn(true);

        $controller->edit();
    }

    public function testEditPost()
    {
        $controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'globalCheck',
                                'getRequest',
                                'editAction'
                                ])
                            ->getMock();
                 
        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(true);
        Core::$container->set('member', new Member(1));
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(false);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('editAction')->willReturn(true);

        $controller->edit();
    }

    public function testEditViewFail()
    {
        $id = 0;
        $member = new NullMember($id);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->scenario(
            Argument::exact(MemberRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($member);
        $this->controller->expects($this->once())->method('getRepository')->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result= $this->controller->editView($id);
        $this->assertFalse($result);
    }

    public function testEditViewSuccess()
    {
        $id = 1;
        $member = new Member($id);

        $repository = $this->prophesize(MemberRepository::class);
        $repository->scenario(
            Argument::exact(MemberRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->fetchOne(Argument::exact($id))->shouldBeCalledTimes(1)->willReturn($member);
        $this->controller->expects($this->any())->method('getRepository')->willReturn($repository->reveal());

        $this->controller->expects($this->exactly(1))->method('render');

        $result= $this->controller->editView($id);
        $this->assertTrue($result);
    }

    private function initEditAction(bool $result, $id)
    {
        $this->controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'validateEditScenario',
                                'getRequest',
                                'getCommandBus',
                                'displaySuccess',
                                'displayError',
                                'decrypt'
                                ])
                            ->getMock();

        $gender = 1;
        $genderEncode = marmot_encode($gender);
        
        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('gender'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($genderEncode);

        $this->controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $this->controller->expects($this->exactly(1))->method('validateEditScenario')->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new EditMemberCommand(
                    $gender,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    public function testEditActionSuccess()
    {
        $id = 1;
        $this->initEditAction(true, $id);

        $this->controller->expects($this->exactly(1))->method('displaySuccess');

        $result = $this->controller->editAction($id);
        $this->assertTrue($result);
    }

    public function testEditActionFail()
    {
        $id = 1;

        $this->initEditAction(false, $id);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->editAction($id);
        $this->assertFalse($result);
    }

    public function testSignInGet()
    {
        $controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'signInView',
                                'getRequest'
                                ])
                            ->getMock();

        Core::$container->set('member', new NullMember());
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(true);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('signInView')->willReturn(true);

        $controller->signIn();
    }

    public function testSignInPost()
    {
        $controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'signInAction',
                                'getRequest'
                                ])
                            ->getMock();

        Core::$container->set('member', new NullMember());

        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(false);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('signInAction')->willReturn(true);

        $controller->signIn();
    }

    public function testSignIn()
    {
        $controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'locationIndex',
                                'signInAction',
                                'getRequest'
                                ])
                            ->getMock();

        Core::$container->set('member', new Member(1));

        $controller->expects($this->once())->method('locationIndex')->willReturn(true);
        
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(false);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('signInAction')->willReturn(true);

        $controller->signIn();
    }

    public function testSignInView()
    {
        $backUrl = 'backUrl';

        $this->controller->expects($this->exactly(1))->method('getBackUrl')->willReturn($backUrl);

        $this->controller->expects($this->exactly(1))->method('render');

        $result= $this->controller->signInView();
        $this->assertTrue($result);
    }

    protected function initSignInAction(bool $result)
    {
        $this->controller = $this->getMockBuilder(MockMemberOperateController::class)
                            ->setMethods([
                                'validateSignInScenario',
                                'getRequest',
                                'getCommandBus',
                                'displaySuccess',
                                'displayError',
                                'decrypt'
                                ])
                            ->getMock();

        $userName = 'userName';
        $password = 'password';
        $passwordDecrypt = 'passwordDecrypted';
        
        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('userName'), Argument::exact(''))->shouldBeCalledTimes(1)->willReturn($userName);
        $request->post(
            Argument::exact('password'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($password);

        $this->controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $this->controller->expects($this->exactly(1))
                    ->method('decrypt')
                    ->with('password')
                    ->willReturn($passwordDecrypt);

        $this->controller->expects($this->exactly(1))->method('validateSignInScenario')->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new SignInMemberCommand(
                    $userName,
                    $passwordDecrypt
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    public function testSignInActionSuccess()
    {
        $this->initSignInAction(true);

        $this->controller->expects($this->exactly(1))->method('displaySuccess');

        $result = $this->controller->signInAction();
        $this->assertTrue($result);
    }

    public function testSignInActionFail()
    {
        $this->initSignInAction(false);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->signInAction();
        $this->assertFalse($result);
    }

    private function initialSignOut(bool $result)
    {
        $this->controller = $this->getMockBuilder(MockMemberOperateController::class)
            ->setMethods(
                [
                    'getCommandBus',
                    'getRequest',
                    'displaySuccess',
                    'displayError',
                    'locationIndex'
                ]
            )->getMock();

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(Argument::exact(new SignOutMemberCommand()))->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))
            ->method('getCommandBus')
            ->willReturn($commandBus->reveal());
    }

    public function testSignOutSuccessAjax()
    {
        $this->initialSignOut(true);

        $request = $this->prophesize(Request::class);
        $request->isAjax()->shouldBeCalledTimes(1)->willReturn(true);

        $this->controller->expects($this->exactly(1))
                   ->method('getRequest')
                   ->willReturn($request->reveal());

        $this->controller->expects($this->any())
                   ->method('displaySuccess')
                   ->willReturn(true);

        $result = $this->controller->signOut();
        $this->assertTrue($result);
    }

    public function testSignOutSuccess()
    {
        $this->initialSignOut(true);

        $request = $this->prophesize(Request::class);
        $request->isAjax()->shouldBeCalledTimes(1)->willReturn(false);

        $this->controller->expects($this->exactly(1))
                   ->method('getRequest')
                   ->willReturn($request->reveal());

        $this->controller->expects($this->once())->method('locationIndex');

        $this->controller->signOut();
    }

    public function testSignOutFailure()
    {
        $this->initialSignOut(false);

        $this->controller->expects($this->exactly(1))
                   ->method('displayError')
                   ->willReturn(false);

        $result = $this->controller->signOut();
        $this->assertFalse($result);
    }
}
