<?php
namespace Base\Package\Member\Controller;

use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

use Marmot\Core;
use Marmot\Framework\Classes\Request;
use Marmot\Framework\Classes\CommandBus;

use Sdk\Member\Model\Member;
use Sdk\Member\Model\NullMember;
use Sdk\Member\Repository\MemberRepository;
use Sdk\Member\Command\Member\ResetPasswordMemberCommand;
use Sdk\Member\Command\Member\UpdatePasswordMemberCommand;
use Sdk\Member\Command\Member\ValidateSecurityMemberCommand;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class MemberPasswordControllerTest extends TestCase
{
    private $controller;

    public function setUp()
    {
        $this->controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'displaySuccess',
                                'displayError',
                                'getRequest',
                                'render',
                                'getRepository',
                                'getCommandBus',
                                'globalCheck'
                                ])
                            ->getMock();
    }

    public function tearDown()
    {
        unset($this->controller);
    }

    public function testExtendsController()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Classes\Controller',
            $this->controller
        );
    }

    public function testResetPasswordGet()
    {
        $controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'resetPasswordView',
                                'getRequest'
                                ])
                            ->getMock();

        Core::$container->set('member', new NullMember());
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(true);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('resetPasswordView')->willReturn(true);

        $controller->resetPassword();
    }

    public function testResetPasswordPost()
    {
        $controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'resetPasswordAction',
                                'getRequest'
                                ])
                            ->getMock();

        Core::$container->set('member', new NullMember());
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(false);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('resetPasswordAction')->willReturn(true);

        $controller->resetPassword();
    }

    public function testResetPassword()
    {
        $controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'locationIndex',
                                'resetPasswordAction',
                                'getRequest'
                                ])
                            ->getMock();

        Core::$container->set('member', new Member(1));

        $controller->expects($this->once())->method('locationIndex')->willReturn(true);
        
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(false);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('resetPasswordAction')->willReturn(true);

        $controller->resetPassword();
    }

    public function testResetPasswordView()
    {
        $this->controller->expects($this->exactly(1))->method('render');

        $result= $this->controller->resetPasswordView();
        $this->assertTrue($result);
    }

    protected function initResetPasswordAction(bool $result)
    {
        $this->controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'validateResetPasswordScenario',
                                'getRequest',
                                'getCommandBus',
                                'displaySuccess',
                                'displayError',
                                'decrypt'
                                ])
                            ->getMock();

        $userName = 'userName';
        $securityAnswer = 'securityAnswer';
        $password = 'password';
        $passwordDecrypt = 'passwordDecrypted';
        $confirmPassword = 'confirmPassword';

        $request = $this->prophesize(Request::class);
        $request->post(Argument::exact('userName'), Argument::exact(''))->shouldBeCalledTimes(1)->willReturn($userName);
        $request->post(
            Argument::exact('securityAnswer'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($securityAnswer);
        $request->post(
            Argument::exact('password'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($password);
        $request->post(
            Argument::exact('confirmPassword'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($confirmPassword);

        $this->controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $this->controller->expects($this->exactly(2))
                    ->method('decrypt')
                    ->will($this->returnValueMap(
                        [
                        ['password','passwordDecrypted'],
                        ['confirmPassword','confirmPasswordDecrypted']
                        ]
                    ));

        $this->controller->expects($this->exactly(1))->method('validateResetPasswordScenario')->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ResetPasswordMemberCommand(
                    $userName,
                    $securityAnswer,
                    $passwordDecrypt
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    public function testResetPasswordActionSuccess()
    {
        $this->initResetPasswordAction(true);

        $this->controller->expects($this->exactly(1))->method('displaySuccess');

        $result = $this->controller->resetPasswordAction();
        $this->assertTrue($result);
    }

    public function testResetPasswordActionFail()
    {
        $this->initResetPasswordAction(false);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->resetPasswordAction();
        $this->assertFalse($result);
    }

    public function testUpdatePasswordFail()
    {
        $controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'globalCheck',
                                'displayError'
                                ])
                            ->getMock();
                    
        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(false);

        $controller->expects($this->exactly(1))->method('displayError');

        $result = $controller->updatePassword();
        $this->assertFalse($result);
    }

    public function testUpdatePasswordGet()
    {
        $controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'getRequest',
                                'globalCheck',
                                'updatePasswordView'
                                ])
                            ->getMock();
        
        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(true);

        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(true);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('updatePasswordView')->willReturn(true);

        $controller->updatePassword();
    }

    public function testUpdatePasswordPost()
    {
        $controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'globalCheck',
                                'getRequest',
                                'updatePasswordAction'
                                ])
                            ->getMock();
                 
        $controller->expects($this->exactly(1))->method('globalCheck')->willReturn(true);
    
        $request = $this->prophesize(Request::class);
        $request->isGetMethod()->shouldBeCalledTimes(1)->willReturn(false);
        $controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $controller->expects($this->exactly(1))->method('updatePasswordAction')->willReturn(true);

        $controller->updatePassword();
    }

    public function testUpdatePasswordView()
    {
        $this->controller->expects($this->exactly(1))->method('render');

        $result= $this->controller->updatePasswordView();
        $this->assertTrue($result);
    }

    private function initUpdatePasswordAction(bool $result)
    {
        $this->controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'validateUpdatePasswordScenario',
                                'getRequest',
                                'getCommandBus',
                                'displaySuccess',
                                'displayError',
                                'decrypt'
                                ])
                            ->getMock();

        $password = 'password';
        $passwordDecrypt = 'passwordDecrypted';
        $confirmPassword = 'confirmPassword';
        $oldPassword = 'oldPassword';
        $oldPasswordDecrypt = 'oldPasswordDecrypted';
        
        $id = 1;

        Core::$container->set('member', new Member($id));

        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('password'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($password);
        $request->post(
            Argument::exact('oldPassword'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($oldPassword);
        $request->post(
            Argument::exact('confirmPassword'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($confirmPassword);

        $this->controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $this->controller->expects($this->exactly(1))->method('validateUpdatePasswordScenario')->willReturn(true);

        $this->controller->expects($this->exactly(3))
                    ->method('decrypt')
                    ->will($this->returnValueMap(
                        [
                        ['password','passwordDecrypted'],
                        ['confirmPassword','confirmPasswordDecrypted'],
                        ['oldPassword','oldPasswordDecrypted'],
                        ]
                    ));

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new UpdatePasswordMemberCommand(
                    $passwordDecrypt,
                    $oldPasswordDecrypt,
                    $id
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    public function testUpdatePasswordActionSuccess()
    {
        $this->initUpdatePasswordAction(true);

        $this->controller->expects($this->exactly(1))->method('displaySuccess');

        $result = $this->controller->updatePasswordAction();
        $this->assertTrue($result);
    }

    public function testUpdatePasswordActionFail()
    {
        $this->initUpdatePasswordAction(false);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->updatePasswordAction();
        $this->assertFalse($result);
    }

    private function initValidateSecurity(bool $result, $id)
    {
        $this->controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'validateValidateSecurityScenario',
                                'getRequest',
                                'getCommandBus',
                                'displaySuccess',
                                'displayError'
                                ])
                            ->getMock();

        $securityAnswer = 'securityAnswer';
        
        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('securityAnswer'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($securityAnswer);

        $this->controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $this->controller->expects($this->exactly(1))->method('validateValidateSecurityScenario')->willReturn(true);

        $commandBus = $this->prophesize(CommandBus::class);
        $commandBus->send(
            Argument::exact(
                new ValidateSecurityMemberCommand(
                    $securityAnswer,
                    marmot_decode($id)
                )
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->controller->expects($this->exactly(1))->method('getCommandBus')->willReturn($commandBus->reveal());
    }

    public function testValidateSecuritySuccess()
    {
        $id = marmot_encode(1);
        $this->initValidateSecurity(true, $id);

        $this->controller->expects($this->exactly(1))->method('displaySuccess');

        $result = $this->controller->validateSecurity($id);
        $this->assertTrue($result);
    }

    public function testValidateSecurityFail()
    {
        $id = marmot_encode(1);

        $this->initValidateSecurity(false, $id);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->validateSecurity($id);
        $this->assertFalse($result);
    }

    private function initVerifyUserName(bool $result)
    {
        $this->controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'validateVerifyUserNameScenario',
                                'getRequest',
                                'getRepository',
                                'render',
                                'displayError'
                                ])
                            ->getMock();

        $this->controller->expects($this->exactly(1))->method('validateVerifyUserNameScenario')->willReturn(true);

        $userName = 'userName';
        
        $request = $this->prophesize(Request::class);
        $request->post(
            Argument::exact('userName'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userName);

        $this->controller->expects($this->once())->method('getRequest')->willReturn($request->reveal());

        $member = new Member(1);

        $count = 0;
        $list = array();

        if ($result) {
            $count = 1;
            $list = array($member);
        }

        $filter['userName'] = $userName;
        $sort = ['-updateTime'];

        $repository = $this->prophesize(MemberRepository::class);
        $repository->scenario(
            Argument::exact(MemberRepository::FETCH_ONE_MODEL_UN)
        )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort)
        )->shouldBeCalledTimes(1)->willReturn([$count, $list]);

        $this->controller->expects($this->any())
                   ->method('getRepository')
                   ->willReturn($repository->reveal());
    }

    public function testVerifyUserNameSuccess()
    {
        $this->initVerifyUserName(true);

        $this->controller->expects($this->exactly(1))->method('render');

        $result = $this->controller->verifyUserName();
        $this->assertTrue($result);
    }

    public function testVerifyUserNameFail()
    {
        $this->initVerifyUserName(false);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->verifyUserName();

        $this->assertFalse($result);
    }

    public function testVerifyUserNameUserNameFail()
    {
        $this->controller = $this->getMockBuilder(MockMemberPasswordController::class)
                            ->setMethods([
                                'validateVerifyUserNameScenario',
                                'displayError'
                                ])
                            ->getMock();

        $this->controller->expects($this->exactly(1))->method('validateVerifyUserNameScenario')->willReturn(false);

        $this->controller->expects($this->exactly(1))->method('displayError');

        $result = $this->controller->verifyUserName();

        $this->assertFalse($result);
    }
}
