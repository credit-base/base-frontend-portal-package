<?php
namespace Base\Package\ResourceCatalog\Controller;

use Marmot\Core;
use Prophecy\Argument;
use PHPUnit\Framework\TestCase;
use Marmot\Framework\Classes\Request;

use Sdk\Template\Model\BjTemplate;
use Base\Sdk\ResourceCatalog\Model\SearchData;
use Sdk\ResourceCatalog\Repository\BjSearchDataRepository;

use Base\Package\ResourceCatalog\View\Json\RedBlackListJsonView;
use Base\Package\ResourceCatalog\View\Template\RedBlackDataView;
use Base\Package\ResourceCatalog\View\Template\RedBlackListView;

/**
* 屏蔽类中所有PMD警告
* @SuppressWarnings(PHPMD)
*/
class RedBlackFetchControllerTest extends TestCase
{
    private $stub;

    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockRedBlackFetchController::class)
            ->setMethods(
                [
                    'getRequest',
                    'displayError',
                ]
            )->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
        Core::setLastError(ERROR_NOT_DEFINED);
    }

    public function testCorrectExtendsController()
    {
        $controller = new RedBlackFetchController();
        $this->assertInstanceof('Marmot\Framework\Classes\Controller', $controller);
    }

    public function testGetRepository()
    {
        $this->assertInstanceof(
            'Sdk\ResourceCatalog\Repository\BjSearchDataRepository',
            $this->stub->getRepository()
        );
    }

    public function testFilterActionAjaxSuccess()
    {
        $this->initialFetchActionSuccess(true);
    }

    public function testFilterActionSuccess()
    {
        $this->initialFetchActionSuccess(false);
    }

    public function initialFetchActionSuccess($ajaxResult)
    {
        $this->stub = $this->getMockBuilder(MockRedBlackFetchController::class)
            ->setMethods(
                [
                    'filterFormatChange',
                    'getPageAndSize',
                    'getRepository',
                    'getRequest',
                    'render'
                ]
            )->getMock();

        $scene =  "MA";
        $filter['dimension'] = BjTemplate::DIMENSION['SHGK'];
        $filter['status'] = SearchData::DATA_STATUS['CONFIRM'];

        $sort = ['-updateTime'];
        $page = 2;
        $size = SEARCH_LIST_SIZE;

        $this->stub->expects($this->exactly(1))
            ->method('getPageAndSize')
            ->with(SEARCH_LIST_SIZE)
            ->willReturn([$page, $size]);
            
        $this->stub->expects($this->exactly(1))
            ->method('filterFormatChange')
            ->with('RedBlack')
            ->willReturn([$filter, $sort, $scene]);

        $redBlackList = array(1,2);
        $count = 10;
        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->scenario(Argument::exact(BjSearchDataRepository::LIST_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());
        $repository->search(
            Argument::exact($filter),
            Argument::exact($sort),
            Argument::exact($page),
            Argument::exact($size)
        )->shouldBeCalledTimes(1)->willReturn([$count, $redBlackList]);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $request = $this->prophesize(Request::class);
        $request->isAjax()->shouldBeCalledTimes(1)->willReturn($ajaxResult);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        if (!$ajaxResult) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new RedBlackListView($redBlackList, $count, $scene));
        }

        if ($ajaxResult) {
            $this->stub->expects($this->exactly(1))
                ->method('render')
                ->with(new RedBlackListJsonView($redBlackList, $count, $scene));
        }

        $result = $this->stub->filterAction();
        $this->assertTrue($result);
    }

    public function testFilterFormatChangeByMAScene()
    {
        $data['scene'] = 'MA';
        $data['infoClassify'] = BjTemplate::INFO_CLASSIFY['RMD'];
        $this->initialFilterFormatChange($data);
    }

    public function testFilterFormatChangeByMQScene()
    {
        $data['scene'] = 'MQ';
        $data['infoClassify'] = BjTemplate::INFO_CLASSIFY['HMD'];
        $this->initialFilterFormatChange($data);
    }
    
    public function initialFilterFormatChange($data)
    {
        $this->stub = $this->getMockBuilder(MockDoublePublicityFetchController::class)
            ->setMethods(['getRequest'])
            ->getMock();

        $scene = $data['scene'];
        $infoClassify = $data['infoClassify'];
        
        $identify = 'identify';
        $name = 'name';
        $subjectCategory = '3';
        $subjectCategoryEncode = marmot_encode($subjectCategory);
        $userGroup = '1';
        $userGroupEncode = marmot_encode($userGroup);

        $filter['name'] = $name;
        $filter['identify'] = $identify;
        $filter['dimension'] = BjTemplate::DIMENSION['SHGK'];
        $filter['subjectCategory'] = $subjectCategory;
        $filter['infoClassify'] = $infoClassify;
        $filter['sourceUnit'] = $userGroup;
        $filter['status'] = SearchData::DATA_STATUS['CONFIRM'];

        $request = $this->prophesize(Request::class);

        $request->get(
            Argument::exact('scene'),
            Argument::exact('MA')
        )->shouldBeCalledTimes(1)->willReturn($scene);

        $request->get(
            Argument::exact('userGroup'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($userGroupEncode);

        $request->get(
            Argument::exact('name'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($name);

        $request->get(
            Argument::exact('identify'),
            Argument::exact('')
        )->shouldBeCalledTimes(1)->willReturn($identify);
        
        $request->get(
            Argument::exact('subjectCategory'),
            Argument::exact('MA')
        )->shouldBeCalledTimes(1)->willReturn($subjectCategoryEncode);

        $this->stub->expects($this->any())
            ->method('getRequest')
            ->willReturn($request->reveal());

        $result = $this->stub->filterFormatChange('RedBlack');

        $this->assertEquals([$filter, ['-updateTime'], $scene], $result);
    }

    public function testFetchOneActionIdFailure()
    {
        $id = 0;

        $result = $this->stub->fetchOneAction($id);
        $this->assertFalse($result);
    }

    public function testFetchOneActionSuccess()
    {
        $id = 1;
        $this->stub = $this->getMockBuilder(MockRedBlackFetchController::class)
            ->setMethods(
                [
                    'getRepository',
                    'getResponse',
                    'render'
                ]
            )->getMock();

        $redBlackData = new BjTemplate($id);

        $repository = $this->prophesize(BjSearchDataRepository::class);
        $repository->scenario(Argument::exact(BjSearchDataRepository::FETCH_ONE_MODEL_UN))
            ->shouldBeCalledTimes(1)
            ->willReturn($repository->reveal());

        $repository->fetchOne(Argument::exact($id))
            ->shouldBeCalledTimes(1)
            ->willReturn($redBlackData);

        $this->stub->expects($this->exactly(1))
            ->method('getRepository')
            ->willReturn($repository->reveal());

        $this->stub->expects($this->exactly(1))
            ->method('render')
            ->with(new RedBlackDataView($redBlackData));

        $result = $this->stub->fetchOneAction($id);
        $this->assertTrue($result);
    }
}
