<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;

class RedBlackListJsonViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockRedBlackListJsonView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockRedBlackListJsonView::class)->setMethods([
            'encode',
            'getDataList'
        ])->getMock();

        $result = array();
        $this->stub->expects($this->exactly(1))->method('getDataList')->willReturn($result);

        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
