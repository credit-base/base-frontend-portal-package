<?php
namespace Base\Package\ResourceCatalog\View\Json;

use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Model\BjSearchData;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

class DoublePublicityListJsonViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDoublePublicityListJsonView::class)->setMethods(['encode'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetBjSearchDataList()
    {
        $result = $this->stub->getBjSearchDataList();
        $this->assertIsArray($result);
    }

    public function testGetCount()
    {
        $this->assertIsInt($this->stub->getCount());
    }

    public function testGetScene()
    {
        $this->assertIsString($this->stub->getScene());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\ResourceCatalog\Translator\BjSearchDataTranslator', $this->stub->getTranslator());
    }

    public function testGetDataList()
    {
        $this->stub = $this->getMockBuilder(MockDoublePublicityListJsonView::class)->setMethods([
            'encode',
            'getBjSearchDataList',
            'getTranslator',
            'getCount',
            'getScene'
        ])->getMock();

        $bjSearchData = new BjSearchData(1);
        $list = [$bjSearchData];
        $count = 1;
        $scene = 'scene';

        $this->stub->expects($this->exactly(1))->method('getBjSearchDataList')->willReturn($list);
        $this->stub->expects($this->exactly(1))->method('getCount')->willReturn($count);
        $this->stub->expects($this->exactly(1))->method('getScene')->willReturn($scene);

        $result = array(
            'template'=>array('name'=>'name')
        );

        $translator = $this->prophesize(BjSearchDataTranslator::class);
        $translator->objectToArray(
            $bjSearchData,
            array(
                'id',
                'name',
                'template' => []
            )
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        
        $this->assertIsArray($this->stub->getDataList());
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockDoublePublicityListJsonView::class)->setMethods([
            'encode',
            'getDataList'
        ])->getMock();

        $result = array();
        $this->stub->expects($this->exactly(1))->method('getDataList')->willReturn($result);

        $this->stub->expects($this->exactly(1))->method('encode');
        $this->assertIsNotArray($this->stub->display());
    }
}
