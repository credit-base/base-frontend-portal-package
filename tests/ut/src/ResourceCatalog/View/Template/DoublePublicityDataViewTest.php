<?php
namespace Base\Package\ResourceCatalog\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

use Sdk\ResourceCatalog\Model\BjSearchData;
use Sdk\ResourceCatalog\Translator\BjSearchDataTranslator;

class DoublePublicityDataViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockDoublePublicityDataView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testGetBjSearchData()
    {
        $result = $this->stub->getBjSearchData();
        $this->assertEquals($result, new BjSearchData());
    }

    public function testGetTranslator()
    {
        $this->assertInstanceOf('Sdk\ResourceCatalog\Translator\BjSearchDataTranslator', $this->stub->getTranslator());
    }

    public function testGetSearchData()
    {
        $this->stub = $this->getMockBuilder(MockDoublePublicityDataView::class)->setMethods([
            'getBjSearchData',
            'getTranslator',
            'itemsRelationTemplate',
        ])->getMock();

        $bjSearchData = new BjSearchData(1);

        $this->stub->expects($this->exactly(1))->method('getBjSearchData')->willReturn($bjSearchData);

        $itemsData = array(
            'data'=>array(1,2)
        );

        $result = array(
            'itemsData'=> $itemsData,
            'template'=>array(
                'items'=>array(
                    array(
                        'identify'=>1
                    )
                )
            )
        );

        $this->stub->expects($this->exactly(1))->method('itemsRelationTemplate')->willReturn($itemsData);

        $translator = $this->prophesize(BjSearchDataTranslator::class);
        $translator->objectToArray(
            $bjSearchData
        )->shouldBeCalledTimes(1)->willReturn($result);

        $this->stub->expects($this->exactly(1))
            ->method('getTranslator')
            ->willReturn($translator->reveal());
        
        $this->assertIsArray($this->stub->getSearchData());
    }

    public function testGetTemplateIdentify()
    {
        $data =array(
            array(
                'identify'=>1
            )
        );
        $this->assertIsArray($this->stub->getTemplateIdentify($data));
    }

    public function testItemsRelationTemplate()
    {
        $this->stub = $this->getMockBuilder(MockDoublePublicityDataView::class)->setMethods([
            'getTemplateIdentify',
        ])->getMock();

        $items=array(
                1=>array(
                    'identify'=>1,
                    'name' =>'name',
                    'dimension' => 'dimension',
                    'maskRule'=> array(2,4),
                    'isMasked' =>array(
                        'id' => 'MA',
                    )
                 )
        );

        $this->stub->expects($this->exactly(1))->method('getTemplateIdentify')->willReturn($items);


        $itemsData  = array(
            'data' =>array(
                1=>'11253'
            )
        );
        $template = array(
            'items' =>$items
        );

        $actualData = $this->stub->itemsRelationTemplate($itemsData, $template);
        $this->assertIsArray($actualData);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockDoublePublicityDataView::class)->setMethods([
            'getView',
            'getSearchData'
        ])->getMock();

        $result = array();
        $this->stub->expects($this->exactly(1))->method('getSearchData')->willReturn($result);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'DoublePublicity/List.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PUBLICITY'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
