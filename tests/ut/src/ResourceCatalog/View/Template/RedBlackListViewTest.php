<?php
namespace Base\Package\ResourceCatalog\View\Template;

use Marmot\Framework\View\Smarty;
use PHPUnit\Framework\TestCase;

class RedBlackListViewTest extends TestCase
{
    private $stub;
    public function setUp()
    {
        $this->stub = $this->getMockBuilder(MockRedBlackListView::class)->setMethods(['getView'])->getMock();
    }

    public function tearDown()
    {
        unset($this->stub);
    }

    public function testDisplay()
    {
        $this->stub = $this->getMockBuilder(MockRedBlackListView::class)->setMethods([
            'getView',
            'getDataList'
        ])->getMock();

        $result = array();
        $this->stub->expects($this->exactly(1))->method('getDataList')->willReturn($result);

        $view = $this->prophesize(Smarty::class);
        $view->display(
            'RedBlack/List.tpl',
            [
                'nav'=> NAV['NAV_CREDIT_PUBLICITY'],
                'data' => $result
            ]
        );
        $this->stub->expects($this->exactly(1))
            ->method('getView')
            ->willReturn($view->reveal());

        $this->assertIsNotArray($this->stub->display());
    }
}
